OpenZMS Heartbeats
==================

OpenZMS may require "heartbeat" messages to be sent by users operating
Grants.  Heartbeat messages (or "acks") communicate the current `OpStatus`
from the grantee's perspective---is the spectrum granted being used
(`OpStatusActive`) or is use paused (`OpStatusPaused`).  (If the grant's
`StartsAt` time has not yet arrived, it may also be in the
`OpStatusSubmitted` or `OpStatusAccepted` operational states.)

Heartbeats communicate to OpenZMS that the grantee is using spectrum.

Notifications
-------------

You are not required to listen for notifications from OpenZMS, but doing so
will help OpenZMS to more effectively scheduling sharing and to handle
potential interference---whether that affects your grant or another party's
grant.  Furthermore, use of notifications allows you the maximum amount of
flexibility in terms of required heartbeat response time.  In other words,
if you poll "too late" in the heartbeat interval period, you may run short
of time to take required action (e.g. to pause spectrum use) and to send a
heartbeat notifying that you have acted.

Required Heartbeats
-------------------

You are only required to periodically send heartbeats (which signal your
OpStatus) *if* the Grant's `AllowSkipAcks` field is set to `true`.  *This will
not change during the lifetime of your grant.*

If you cannot successfully send a required heartbeat on time (prior to the
Grant's current `StatusAckBy` field time), you must *cease spectrum use*.

Heartbeat Interval
------------------

If you are required to send heartbeats, you must do so before the time in
Grant's `StatusAckBy` field, if set.  If unset, a heartbeat is currently not
required.  Each time you send a heartbeat, the reply field will be updated
with time of the next required heartbeat, if required.

If you are polling instead of listening for notifications, you must
determine your polling time, based on your Grant's requested StartsAt time,
and OpenZMS's configured heartbeat interval time.

Heartbeats and Grant State
--------------------------

Heartbeats must conform to the following state machine requirements.

When you create a grant, it may

  * be immediately, synchronously Approved/Denied/Active/Paused
  * be placed into Created/Scheduling states, where scheduling and approvals
    continues asynchronously

When a grant is Approved, it must be Accepted via heartbeat notification.
This heartbeat is only required once; but may be sent multiple times,
according to OpenZMS's configured heartbeat interval time.

When a grant is moved into the Active or Paused states when the `StartsAt`
time has arrived, heartbeats become required until a terminating state
(`Deleted`, `Revoked`, `Replacing`) or expiration has been acknowledged via
a final `OpStatusPaused` heartbeat.

The grantee may only use the spectrum authority included in the Grant when
its `Status` is `Active`.  If `Status` enters any other state or if the
`ExpiresAt` time has been reached, the grantee must immediately cease use of
spectrum authority in the grant and acknowledge by sending an
`OpStatusPaused` heartbeat in accordance with the required heartbeat
interval.

In summary:

  * When `status` == `approved`: grantee must send at least one `accepted`
    heartbeat.
  * When `status` == `active`: grantee must send `active` heartbeats as long
    as the grant is being actively used and should be kept alive.
  * When `status` == `paused`: grantee must send `paused` heartbeats as long
    as the grant should be kept alive.
  * When `status` == `replacing` or `revoked`: grantee should send one
    `paused` heartbeat once it has ceased use of the existing grant.  These
    are terminal status states and no further heartbeats are required.

When the grant has been acknowledged in a terminal `status`
(`replacing,revoked,deleted`), the `ack_status_by` field will be set to
`null` and no further heartbeats are required.

Heartbeat Implementation
------------------------

You must send heartbeats to OpenZMS with a `PUT` to the
`/v1/grants/<grant_id>/opstatus` endpoint.  This message contains a required
`op_status` field, and an optional `message` field (which can be used to
communicate additional detail to the OpenZMS grant audit log).
