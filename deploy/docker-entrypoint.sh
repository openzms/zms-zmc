#!/bin/sh

( cd /usr/local/share/zms-zmc/db \
      && /usr/local/share/zms-zmc/db/zms-apply-migrations.sh ) \
   || exit $?

exec /usr/local/bin/zms-zmc
