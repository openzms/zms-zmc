-- reverse: add column "allow_skip_acks" to table: "grants"
ALTER TABLE `grants` DROP COLUMN `allow_skip_acks`;
-- reverse: create index "idx_policy_spectrum_id_element_id" to table: "policies"
DROP INDEX `idx_policy_spectrum_id_element_id`;
-- reverse: create "new_policies" table
DROP TABLE `new_policies`;
