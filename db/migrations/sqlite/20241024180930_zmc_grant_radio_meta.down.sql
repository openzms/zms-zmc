-- reverse: add column "html_url" to table: "grants"
ALTER TABLE `grants` DROP COLUMN `html_url`;
-- reverse: add column "ext_id" to table: "grants"
ALTER TABLE `grants` DROP COLUMN `ext_id`;
-- reverse: add column "html_url" to table: "radios"
ALTER TABLE `radios` DROP COLUMN `html_url`;
-- reverse: add column "html_url" to table: "monitors"
ALTER TABLE `monitors` DROP COLUMN `html_url`;
-- reverse: add column "device_id" to table: "monitors"
ALTER TABLE `monitors` DROP COLUMN `device_id`;
-- reverse: add column "device_id" to table: "radio_ports"
ALTER TABLE `radio_ports` DROP COLUMN `device_id`;
