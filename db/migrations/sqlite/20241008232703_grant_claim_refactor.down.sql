-- reverse: create "monitor_groups" table
DROP TABLE `monitor_groups`;
-- reverse: create index "idx_grants_element_id" to table: "grants"
DROP INDEX `idx_grants_element_id`;
-- reverse: create index "idx_grants_spectrum_id" to table: "grants"
DROP INDEX `idx_grants_spectrum_id`;
-- reverse: create index "idx_grants_status" to table: "grants"
DROP INDEX `idx_grants_status`;
-- reverse: create index "idx_grants_op_status" to table: "grants"
DROP INDEX `idx_grants_op_status`;
-- reverse: create index "idx_grants_creator_id" to table: "grants"
DROP INDEX `idx_grants_creator_id`;
-- reverse: create index "idx_grants_approved_at" to table: "grants"
DROP INDEX `idx_grants_approved_at`;
-- reverse: create index "idx_grants_deleted_at" to table: "grants"
DROP INDEX `idx_grants_deleted_at`;
-- reverse: create "new_grants" table
DROP TABLE `new_grants`;
-- reverse: drop "claim_radio_ports" table
CREATE TABLE `claim_radio_ports` (
  `id` uuid NULL,
  `claim_id` uuid NOT NULL,
  `radio_port_id` uuid NOT NULL,
  `cbrs_cbsd_id` text NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_claim_radio_ports_radio_port` FOREIGN KEY (`radio_port_id`) REFERENCES `radio_ports` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT `fk_claims_radio_ports` FOREIGN KEY (`claim_id`) REFERENCES `claims` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- reverse: create index "idx_claims_type" to table: "claims"
DROP INDEX `idx_claims_type`;
-- reverse: create index "idx_claims_source" to table: "claims"
DROP INDEX `idx_claims_source`;
-- reverse: create index "idx_claims_html_url" to table: "claims"
DROP INDEX `idx_claims_html_url`;
-- reverse: create index "idx_claims_name" to table: "claims"
DROP INDEX `idx_claims_name`;
-- reverse: create index "idx_claims_creator_id" to table: "claims"
DROP INDEX `idx_claims_creator_id`;
-- reverse: create index "idx_claims_deleted_at" to table: "claims"
DROP INDEX `idx_claims_deleted_at`;
-- reverse: create index "idx_claims_element_id" to table: "claims"
DROP INDEX `idx_claims_element_id`;
-- reverse: create index "idx_claim_ext_id" to table: "claims"
DROP INDEX `idx_claim_ext_id`;
-- reverse: create "new_claims" table
DROP TABLE `new_claims`;
-- reverse: drop "claim_constraints" table
CREATE TABLE `claim_constraints` (
  `id` uuid NULL,
  `claim_id` uuid NOT NULL,
  `constraint_id` uuid NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_claim_constraints_constraint` FOREIGN KEY (`constraint_id`) REFERENCES `constraints` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT `fk_claims_constraints` FOREIGN KEY (`claim_id`) REFERENCES `claims` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
);
