-- reverse: create "grant_conflicts" table
DROP TABLE `grant_conflicts`;
-- reverse: create index "idx_grant_logs_status" to table: "grant_logs"
DROP INDEX `idx_grant_logs_status`;
-- reverse: create index "idx_grant_logs_op_status" to table: "grant_logs"
DROP INDEX `idx_grant_logs_op_status`;
-- reverse: create "grant_logs" table
DROP TABLE `grant_logs`;
-- reverse: create index "idx_grants_approved_at" to table: "grants"
DROP INDEX `idx_grants_approved_at`;
-- reverse: create index "idx_grants_deleted_at" to table: "grants"
DROP INDEX `idx_grants_deleted_at`;
-- reverse: create index "idx_grants_element_id" to table: "grants"
DROP INDEX `idx_grants_element_id`;
-- reverse: create index "idx_grants_spectrum_id" to table: "grants"
DROP INDEX `idx_grants_spectrum_id`;
-- reverse: create index "idx_grants_status" to table: "grants"
DROP INDEX `idx_grants_status`;
-- reverse: create index "idx_grants_op_status" to table: "grants"
DROP INDEX `idx_grants_op_status`;
-- reverse: create index "idx_grants_creator_id" to table: "grants"
DROP INDEX `idx_grants_creator_id`;
-- reverse: create "new_grants" table
DROP TABLE `new_grants`;
-- reverse: create index "idx_policy_spectrum_id_element_id" to table: "policies"
DROP INDEX `idx_policy_spectrum_id_element_id`;
-- reverse: create "new_policies" table
DROP TABLE `new_policies`;
