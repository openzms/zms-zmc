-- disable the enforcement of foreign-keys constraints
PRAGMA foreign_keys = off;
-- create "new_policies" table
CREATE TABLE `new_policies` (
  `id` uuid NULL,
  `spectrum_id` uuid NOT NULL,
  `element_id` uuid NULL,
  `allowed` numeric NOT NULL,
  `auto_approve` numeric NOT NULL,
  `priority` integer NOT NULL,
  `max_duration` integer NOT NULL,
  `when_unoccupied` numeric NOT NULL,
  `disable_emit_check` numeric NOT NULL DEFAULT false,
  `allow_skip_acks` numeric NOT NULL DEFAULT false,
  `allow_inactive` numeric NOT NULL DEFAULT false,
  `allow_conflicts` numeric NOT NULL DEFAULT false,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_spectrums_policies` FOREIGN KEY (`spectrum_id`) REFERENCES `spectrums` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- copy rows from old table "policies" to new temporary table "new_policies"
INSERT INTO `new_policies` (`id`, `spectrum_id`, `element_id`, `allowed`, `auto_approve`, `priority`, `max_duration`, `when_unoccupied`, `disable_emit_check`, `allow_inactive`, `allow_conflicts`) SELECT `id`, `spectrum_id`, `element_id`, `allowed`, `auto_approve`, `priority`, `max_duration`, `when_unoccupied`, `disable_emit_check`, `allow_inactive`, `allow_conflicts` FROM `policies`;
-- drop "policies" table after copying rows
DROP TABLE `policies`;
-- rename temporary table "new_policies" to "policies"
ALTER TABLE `new_policies` RENAME TO `policies`;
-- create index "idx_policy_spectrum_id_element_id" to table: "policies"
CREATE UNIQUE INDEX `idx_policy_spectrum_id_element_id` ON `policies` (`spectrum_id`, `element_id`);
-- add column "allow_skip_acks" to table: "grants"
ALTER TABLE `grants` ADD COLUMN `allow_skip_acks` numeric NULL;
-- enable back the enforcement of foreign-keys constraints
PRAGMA foreign_keys = on;
