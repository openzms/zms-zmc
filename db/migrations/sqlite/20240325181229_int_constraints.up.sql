-- create "rt_int_constraints" table
CREATE TABLE `rt_int_constraints` (
  `id` uuid NULL,
  `name` text NOT NULL,
  `description` text NULL,
  `metric` text NULL,
  `comparator` text NOT NULL DEFAULT 'gt',
  `value` real NULL,
  `aggregator` text NULL,
  `period` integer NULL,
  PRIMARY KEY (`id`)
);
-- create "int_constraints" table
CREATE TABLE `int_constraints` (
  `id` uuid NULL,
  `name` text NOT NULL,
  `description` text NULL,
  `max_power` real NULL,
  `area_id` uuid NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_int_constraints_area` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- create index "idx_int_constraints_max_power" to table: "int_constraints"
CREATE INDEX `idx_int_constraints_max_power` ON `int_constraints` (`max_power`);
-- create "grant_rt_int_constraints" table
CREATE TABLE `grant_rt_int_constraints` (
  `id` uuid NULL,
  `grant_id` uuid NOT NULL,
  `rt_int_constraint_id` uuid NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_grants_rt_int_constraints` FOREIGN KEY (`grant_id`) REFERENCES `grants` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT `fk_grant_rt_int_constraints_rt_int_constraint` FOREIGN KEY (`rt_int_constraint_id`) REFERENCES `rt_int_constraints` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- create "grant_int_constraints" table
CREATE TABLE `grant_int_constraints` (
  `id` uuid NULL,
  `grant_id` uuid NOT NULL,
  `int_constraint_id` uuid NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_grants_int_constraints` FOREIGN KEY (`grant_id`) REFERENCES `grants` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT `fk_grant_int_constraints_int_constraint` FOREIGN KEY (`int_constraint_id`) REFERENCES `int_constraints` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
);
