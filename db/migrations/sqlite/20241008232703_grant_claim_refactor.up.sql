-- disable the enforcement of foreign-keys constraints
PRAGMA foreign_keys = off;
-- drop "claim_constraints" table
DROP TABLE `claim_constraints`;
-- create "new_claims" table
CREATE TABLE `new_claims` (
  `id` uuid NULL,
  `element_id` uuid NOT NULL,
  `ext_id` text NOT NULL,
  `type` text NOT NULL,
  `source` text NOT NULL,
  `html_url` text NULL,
  `name` text NOT NULL DEFAULT '',
  `description` text NULL,
  `creator_id` uuid NOT NULL,
  `updater_id` uuid NULL,
  `created_at` datetime NULL,
  `updated_at` datetime NULL,
  `verified_at` datetime NULL,
  `denied_at` datetime NULL,
  `deleted_at` datetime NULL,
  `grant_id` uuid NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_claims_grant` FOREIGN KEY (`grant_id`) REFERENCES `grants` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- copy rows from old table "claims" to new temporary table "new_claims"
INSERT INTO `new_claims` (`id`, `element_id`, `description`, `creator_id`, `updater_id`, `created_at`, `updated_at`, `verified_at`, `denied_at`, `deleted_at`) SELECT `id`, `element_id`, `description`, `creator_id`, `updater_id`, `created_at`, `updated_at`, `verified_at`, `denied_at`, `deleted_at` FROM `claims`;
-- drop "claims" table after copying rows
DROP TABLE `claims`;
-- rename temporary table "new_claims" to "claims"
ALTER TABLE `new_claims` RENAME TO `claims`;
-- create index "idx_claim_ext_id" to table: "claims"
CREATE UNIQUE INDEX `idx_claim_ext_id` ON `claims` (`ext_id`);
-- create index "idx_claims_element_id" to table: "claims"
CREATE INDEX `idx_claims_element_id` ON `claims` (`element_id`);
-- create index "idx_claims_deleted_at" to table: "claims"
CREATE INDEX `idx_claims_deleted_at` ON `claims` (`deleted_at`);
-- create index "idx_claims_creator_id" to table: "claims"
CREATE INDEX `idx_claims_creator_id` ON `claims` (`creator_id`);
-- create index "idx_claims_name" to table: "claims"
CREATE INDEX `idx_claims_name` ON `claims` (`name`);
-- create index "idx_claims_html_url" to table: "claims"
CREATE INDEX `idx_claims_html_url` ON `claims` (`html_url`);
-- create index "idx_claims_source" to table: "claims"
CREATE INDEX `idx_claims_source` ON `claims` (`source`);
-- create index "idx_claims_type" to table: "claims"
CREATE INDEX `idx_claims_type` ON `claims` (`type`);
-- drop "claim_radio_ports" table
DROP TABLE `claim_radio_ports`;
-- create "new_grants" table
CREATE TABLE `new_grants` (
  `id` uuid NULL,
  `element_id` uuid NOT NULL,
  `name` text NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `priority` integer NOT NULL DEFAULT 0,
  `is_claim` numeric NOT NULL DEFAULT false,
  `spectrum_id` uuid NULL,
  `allow_skip_acks` numeric NULL,
  `allow_inactive` numeric NULL,
  `allow_conflicts` numeric NULL,
  `starts_at` datetime NOT NULL,
  `expires_at` datetime NULL,
  `status` text NOT NULL DEFAULT 'unknown',
  `status_ack_by` datetime NULL,
  `op_status` text NOT NULL DEFAULT 'unknown',
  `op_status_updated_at` datetime NULL,
  `creator_id` uuid NOT NULL,
  `updater_id` uuid NULL,
  `created_at` datetime NULL,
  `updated_at` datetime NULL,
  `approved_at` datetime NULL,
  `owner_approved_at` datetime NULL,
  `denied_at` datetime NULL,
  `revoked_at` datetime NULL,
  `deleted_at` datetime NULL,
  PRIMARY KEY (`id`)
);
-- copy rows from old table "grants" to new temporary table "new_grants"
INSERT INTO `new_grants` (`id`, `element_id`, `name`, `description`, `priority`, `spectrum_id`, `allow_skip_acks`, `allow_inactive`, `allow_conflicts`, `starts_at`, `expires_at`, `status`, `status_ack_by`, `op_status`, `op_status_updated_at`, `creator_id`, `updater_id`, `created_at`, `updated_at`, `approved_at`, `owner_approved_at`, `denied_at`, `revoked_at`, `deleted_at`) SELECT `id`, `element_id`, `name`, `description`, `priority`, `spectrum_id`, `allow_skip_acks`, `allow_inactive`, `allow_conflicts`, `starts_at`, `expires_at`, `status`, `status_ack_by`, `op_status`, `op_status_updated_at`, `creator_id`, `updater_id`, `created_at`, `updated_at`, `approved_at`, `owner_approved_at`, `denied_at`, `revoked_at`, `deleted_at` FROM `grants`;
-- drop "grants" table after copying rows
DROP TABLE `grants`;
-- rename temporary table "new_grants" to "grants"
ALTER TABLE `new_grants` RENAME TO `grants`;
-- create index "idx_grants_deleted_at" to table: "grants"
CREATE INDEX `idx_grants_deleted_at` ON `grants` (`deleted_at`);
-- create index "idx_grants_approved_at" to table: "grants"
CREATE INDEX `idx_grants_approved_at` ON `grants` (`approved_at`);
-- create index "idx_grants_creator_id" to table: "grants"
CREATE INDEX `idx_grants_creator_id` ON `grants` (`creator_id`);
-- create index "idx_grants_op_status" to table: "grants"
CREATE INDEX `idx_grants_op_status` ON `grants` (`op_status`);
-- create index "idx_grants_status" to table: "grants"
CREATE INDEX `idx_grants_status` ON `grants` (`status`);
-- create index "idx_grants_spectrum_id" to table: "grants"
CREATE INDEX `idx_grants_spectrum_id` ON `grants` (`spectrum_id`);
-- create index "idx_grants_element_id" to table: "grants"
CREATE INDEX `idx_grants_element_id` ON `grants` (`element_id`);
-- create "monitor_groups" table
CREATE TABLE `monitor_groups` (
  `id` uuid NULL,
  `element_id` uuid NULL,
  `name` text NOT NULL,
  `description` text NULL,
  PRIMARY KEY (`id`)
);
-- enable back the enforcement of foreign-keys constraints
PRAGMA foreign_keys = on;
