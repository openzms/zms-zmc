-- disable the enforcement of foreign-keys constraints
PRAGMA foreign_keys = off;
-- create "new_policies" table
CREATE TABLE `new_policies` (
  `id` uuid NULL,
  `spectrum_id` uuid NOT NULL,
  `element_id` uuid NULL,
  `allowed` numeric NOT NULL,
  `auto_approve` numeric NOT NULL,
  `priority` integer NOT NULL,
  `max_duration` integer NOT NULL,
  `when_unoccupied` numeric NOT NULL,
  `disable_emit_check` numeric NOT NULL DEFAULT false,
  `allow_inactive` numeric NOT NULL DEFAULT false,
  `allow_conflicts` numeric NOT NULL DEFAULT false,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_spectrums_policies` FOREIGN KEY (`spectrum_id`) REFERENCES `spectrums` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- copy rows from old table "policies" to new temporary table "new_policies"
INSERT INTO `new_policies` (`id`, `spectrum_id`, `element_id`, `allowed`, `auto_approve`, `priority`, `max_duration`, `when_unoccupied`) SELECT `id`, `spectrum_id`, `element_id`, `allowed`, `auto_approve`, `priority`, `max_duration`, `when_unoccupied` FROM `policies`;
-- drop "policies" table after copying rows
DROP TABLE `policies`;
-- rename temporary table "new_policies" to "policies"
ALTER TABLE `new_policies` RENAME TO `policies`;
-- create index "idx_policy_spectrum_id_element_id" to table: "policies"
CREATE UNIQUE INDEX `idx_policy_spectrum_id_element_id` ON `policies` (`spectrum_id`, `element_id`);
-- create "new_grants" table
CREATE TABLE `new_grants` (
  `id` uuid NULL,
  `element_id` uuid NOT NULL,
  `name` text NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `priority` integer NOT NULL DEFAULT 0,
  `spectrum_id` uuid NULL,
  `allow_inactive` numeric NULL,
  `allow_conflicts` numeric NULL,
  `starts_at` datetime NOT NULL,
  `expires_at` datetime NULL,
  `status` text NOT NULL DEFAULT 'unknown',
  `status_ack_by` datetime NULL,
  `op_status` text NOT NULL DEFAULT 'unknown',
  `op_status_updated_at` datetime NULL,
  `creator_id` uuid NOT NULL,
  `updater_id` uuid NULL,
  `created_at` datetime NULL,
  `updated_at` datetime NULL,
  `approved_at` datetime NULL,
  `owner_approved_at` datetime NULL,
  `denied_at` datetime NULL,
  `revoked_at` datetime NULL,
  `deleted_at` datetime NULL,
  PRIMARY KEY (`id`)
);
-- copy rows from old table "grants" to new temporary table "new_grants"
INSERT INTO `new_grants` (`id`, `element_id`, `description`, `spectrum_id`, `starts_at`, `expires_at`, `creator_id`, `updater_id`, `created_at`, `updated_at`, `approved_at`, `owner_approved_at`, `denied_at`, `revoked_at`, `deleted_at`) SELECT `id`, `element_id`, `description`, `spectrum_id`, `starts_at`, `expires_at`, `creator_id`, `updater_id`, `created_at`, `updated_at`, `approved_at`, `owner_approved_at`, `denied_at`, `revoked_at`, `deleted_at` FROM `grants`;
-- drop "grants" table after copying rows
DROP TABLE `grants`;
-- rename temporary table "new_grants" to "grants"
ALTER TABLE `new_grants` RENAME TO `grants`;
-- create index "idx_grants_creator_id" to table: "grants"
CREATE INDEX `idx_grants_creator_id` ON `grants` (`creator_id`);
-- create index "idx_grants_op_status" to table: "grants"
CREATE INDEX `idx_grants_op_status` ON `grants` (`op_status`);
-- create index "idx_grants_status" to table: "grants"
CREATE INDEX `idx_grants_status` ON `grants` (`status`);
-- create index "idx_grants_spectrum_id" to table: "grants"
CREATE INDEX `idx_grants_spectrum_id` ON `grants` (`spectrum_id`);
-- create index "idx_grants_element_id" to table: "grants"
CREATE INDEX `idx_grants_element_id` ON `grants` (`element_id`);
-- create index "idx_grants_deleted_at" to table: "grants"
CREATE INDEX `idx_grants_deleted_at` ON `grants` (`deleted_at`);
-- create index "idx_grants_approved_at" to table: "grants"
CREATE INDEX `idx_grants_approved_at` ON `grants` (`approved_at`);
-- create "grant_logs" table
CREATE TABLE `grant_logs` (
  `id` uuid NULL,
  `grant_id` uuid NOT NULL,
  `status` text NOT NULL,
  `op_status` text NOT NULL,
  `message` text NOT NULL,
  `created_at` datetime NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_grants_logs` FOREIGN KEY (`grant_id`) REFERENCES `grants` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- create index "idx_grant_logs_op_status" to table: "grant_logs"
CREATE INDEX `idx_grant_logs_op_status` ON `grant_logs` (`op_status`);
-- create index "idx_grant_logs_status" to table: "grant_logs"
CREATE INDEX `idx_grant_logs_status` ON `grant_logs` (`status`);
-- create "grant_conflicts" table
CREATE TABLE `grant_conflicts` (
  `id` uuid NULL,
  `a_grant_id` uuid NOT NULL,
  `b_grant_id` uuid NOT NULL,
  PRIMARY KEY (`id`)
);
-- enable back the enforcement of foreign-keys constraints
PRAGMA foreign_keys = on;
