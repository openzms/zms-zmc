-- add column "antenna_elevation_delta" to table: "radio_ports"
ALTER TABLE `radio_ports` ADD COLUMN `antenna_elevation_delta` real NULL;
