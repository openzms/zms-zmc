-- disable the enforcement of foreign-keys constraints
PRAGMA foreign_keys = off;
-- add column "device_id" to table: "radio_ports"
ALTER TABLE `radio_ports` ADD COLUMN `device_id` text NULL;
-- add column "device_id" to table: "monitors"
ALTER TABLE `monitors` ADD COLUMN `device_id` text NULL;
-- add column "html_url" to table: "monitors"
ALTER TABLE `monitors` ADD COLUMN `html_url` text NULL;
-- add column "html_url" to table: "radios"
ALTER TABLE `radios` ADD COLUMN `html_url` text NULL;
-- add column "ext_id" to table: "grants"
ALTER TABLE `grants` ADD COLUMN `ext_id` text NULL;
-- add column "html_url" to table: "grants"
ALTER TABLE `grants` ADD COLUMN `html_url` text NULL;
-- enable back the enforcement of foreign-keys constraints
PRAGMA foreign_keys = on;
