-- reverse: create index "idx_grant_logs_status" to table: "grant_logs"
DROP INDEX "idx_grant_logs_status";
-- reverse: create index "idx_grant_logs_op_status" to table: "grant_logs"
DROP INDEX "idx_grant_logs_op_status";
-- reverse: create "grant_logs" table
DROP TABLE "grant_logs";
-- reverse: create index "idx_grants_status" to table: "grants"
DROP INDEX "idx_grants_status";
-- reverse: create index "idx_grants_op_status" to table: "grants"
DROP INDEX "idx_grants_op_status";
-- reverse: modify "grants" table
ALTER TABLE "grants" DROP COLUMN "op_status_updated_at", DROP COLUMN "op_status", DROP COLUMN "status_ack_by", DROP COLUMN "status", DROP COLUMN "allow_conflicts", DROP COLUMN "allow_inactive", DROP COLUMN "priority", DROP COLUMN "name";
-- reverse: create "grant_conflicts" table
DROP TABLE "grant_conflicts";
-- reverse: modify "policies" table
ALTER TABLE "policies" DROP COLUMN "allow_conflicts", DROP COLUMN "allow_inactive", DROP COLUMN "disable_emit_check";
-- reverse: create enum type "grant_op_status_enum"
DROP TYPE "grant_op_status_enum";
-- reverse: create enum type "grant_status_enum"
DROP TYPE "grant_status_enum";
