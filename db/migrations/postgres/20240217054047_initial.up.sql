-- create "areas" table
CREATE TABLE "areas" (
  "id" uuid NOT NULL,
  "element_id" uuid NOT NULL,
  "name" character varying(256) NULL,
  "description" character varying(1024) NULL,
  "srid" bigint NOT NULL,
  PRIMARY KEY ("id")
);
-- create index "idx_areas_element_id" to table: "areas"
CREATE INDEX "idx_areas_element_id" ON "areas" ("element_id");
-- create index "idx_areas_name" to table: "areas"
CREATE INDEX "idx_areas_name" ON "areas" ("name");
-- create "area_points" table
CREATE TABLE "area_points" (
  "id" uuid NOT NULL,
  "area_id" uuid NOT NULL,
  "x" numeric NOT NULL,
  "y" numeric NOT NULL,
  "z" numeric NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_areas_points" FOREIGN KEY ("area_id") REFERENCES "areas" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- create "constraints" table
CREATE TABLE "constraints" (
  "id" uuid NOT NULL,
  "min_freq" bigint NULL,
  "max_freq" bigint NULL,
  "bandwidth" bigint NULL,
  "max_eirp" numeric NULL,
  "min_eirp" numeric NULL,
  "exclusive" boolean NOT NULL,
  "area_id" uuid NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_constraints_area" FOREIGN KEY ("area_id") REFERENCES "areas" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- create index "idx_constraints_exclusive" to table: "constraints"
CREATE INDEX "idx_constraints_exclusive" ON "constraints" ("exclusive");
-- create index "idx_constraints_max_eirp" to table: "constraints"
CREATE INDEX "idx_constraints_max_eirp" ON "constraints" ("max_eirp");
-- create index "idx_constraints_max_freq" to table: "constraints"
CREATE INDEX "idx_constraints_max_freq" ON "constraints" ("max_freq");
-- create index "idx_constraints_min_freq" to table: "constraints"
CREATE INDEX "idx_constraints_min_freq" ON "constraints" ("min_freq");
-- create "claims" table
CREATE TABLE "claims" (
  "id" uuid NOT NULL,
  "element_id" uuid NOT NULL,
  "description" character varying(4096) NULL,
  "ext_type" character varying(512) NULL,
  "ext_name" character varying(512) NULL,
  "ext_endpoint" character varying(512) NULL,
  "ext_grant_id" character varying(256) NULL,
  "creator_id" uuid NOT NULL,
  "updater_id" uuid NULL,
  "created_at" timestamptz NULL,
  "updated_at" timestamptz NULL,
  "verified_at" timestamptz NULL,
  "approved_at" timestamptz NULL,
  "denied_at" timestamptz NULL,
  "spectrum_id" uuid NULL,
  "starts_at" timestamptz NULL,
  "expires_at" timestamptz NULL,
  "revoked_at" timestamptz NULL,
  "deleted_at" timestamptz NULL,
  PRIMARY KEY ("id")
);
-- create index "idx_claims_approved_at" to table: "claims"
CREATE INDEX "idx_claims_approved_at" ON "claims" ("approved_at");
-- create index "idx_claims_creator_id" to table: "claims"
CREATE INDEX "idx_claims_creator_id" ON "claims" ("creator_id");
-- create index "idx_claims_deleted_at" to table: "claims"
CREATE INDEX "idx_claims_deleted_at" ON "claims" ("deleted_at");
-- create index "idx_claims_element_id" to table: "claims"
CREATE INDEX "idx_claims_element_id" ON "claims" ("element_id");
-- create index "idx_claims_spectrum_id" to table: "claims"
CREATE INDEX "idx_claims_spectrum_id" ON "claims" ("spectrum_id");
-- create "claim_constraints" table
CREATE TABLE "claim_constraints" (
  "id" uuid NOT NULL,
  "claim_id" uuid NOT NULL,
  "constraint_id" uuid NOT NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_claim_constraints_constraint" FOREIGN KEY ("constraint_id") REFERENCES "constraints" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "fk_claims_constraints" FOREIGN KEY ("claim_id") REFERENCES "claims" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- create "antennas" table
CREATE TABLE "antennas" (
  "id" uuid NOT NULL,
  "element_id" uuid NOT NULL,
  "name" character varying(256) NOT NULL,
  "description" character varying(4096) NULL,
  "type" character varying(256) NULL,
  "vendor" character varying(256) NULL,
  "model" character varying(256) NULL,
  "url" character varying(512) NULL,
  "gain" numeric NULL,
  "beam_width" bigint NULL,
  "gain_profile_data" bytea NULL,
  "gain_profile_format" character varying(32) NULL,
  "gain_profile_url" text NULL,
  "creator_id" uuid NOT NULL,
  "updater_id" uuid NULL,
  "created_at" timestamptz NULL,
  "updated_at" timestamptz NULL,
  "deleted_at" timestamptz NULL,
  PRIMARY KEY ("id")
);
-- create index "idx_antennas_creator_id" to table: "antennas"
CREATE INDEX "idx_antennas_creator_id" ON "antennas" ("creator_id");
-- create index "idx_antennas_deleted_at" to table: "antennas"
CREATE INDEX "idx_antennas_deleted_at" ON "antennas" ("deleted_at");
-- create index "idx_antennas_element_id" to table: "antennas"
CREATE INDEX "idx_antennas_element_id" ON "antennas" ("element_id");
-- create "locations" table
CREATE TABLE "locations" (
  "id" uuid NOT NULL,
  "element_id" uuid NOT NULL,
  "name" character varying(256) NULL,
  "srid" bigint NOT NULL,
  "x" numeric NOT NULL,
  "y" numeric NOT NULL,
  "z" numeric NULL,
  "creator_id" uuid NOT NULL,
  "updater_id" uuid NULL,
  "created_at" timestamptz NULL,
  "updated_at" timestamptz NULL,
  "deleted_at" timestamptz NULL,
  PRIMARY KEY ("id")
);
-- create index "idx_locations_creator_id" to table: "locations"
CREATE INDEX "idx_locations_creator_id" ON "locations" ("creator_id");
-- create index "idx_locations_deleted_at" to table: "locations"
CREATE INDEX "idx_locations_deleted_at" ON "locations" ("deleted_at");
-- create index "idx_locations_element_id" to table: "locations"
CREATE INDEX "idx_locations_element_id" ON "locations" ("element_id");
-- create index "idx_locations_name" to table: "locations"
CREATE INDEX "idx_locations_name" ON "locations" ("name");
-- create "radios" table
CREATE TABLE "radios" (
  "id" uuid NOT NULL,
  "element_id" uuid NOT NULL,
  "name" character varying(256) NOT NULL,
  "description" character varying(4096) NULL,
  "fcc_id" character varying(64) NULL,
  "device_id" character varying(256) NULL,
  "serial_number" character varying(256) NULL,
  "location_id" uuid NULL,
  "enabled" boolean NOT NULL,
  "creator_id" uuid NOT NULL,
  "updater_id" uuid NULL,
  "created_at" timestamptz NULL,
  "updated_at" timestamptz NULL,
  "deleted_at" timestamptz NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_radios_location" FOREIGN KEY ("location_id") REFERENCES "locations" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- create index "idx_radios_creator_id" to table: "radios"
CREATE INDEX "idx_radios_creator_id" ON "radios" ("creator_id");
-- create index "idx_radios_deleted_at" to table: "radios"
CREATE INDEX "idx_radios_deleted_at" ON "radios" ("deleted_at");
-- create index "idx_radios_element_id" to table: "radios"
CREATE INDEX "idx_radios_element_id" ON "radios" ("element_id");
-- create "radio_ports" table
CREATE TABLE "radio_ports" (
  "id" uuid NOT NULL,
  "radio_id" uuid NOT NULL,
  "name" character varying(256) NOT NULL,
  "tx" boolean NOT NULL,
  "rx" boolean NOT NULL,
  "min_freq" bigint NOT NULL,
  "max_freq" bigint NOT NULL,
  "max_power" numeric NOT NULL,
  "antenna_id" uuid NULL,
  "antenna_location_id" uuid NULL,
  "antenna_azimuth_angle" numeric NULL,
  "antenna_elevation_angle" numeric NULL,
  "enabled" boolean NOT NULL,
  "creator_id" uuid NOT NULL,
  "updater_id" uuid NULL,
  "created_at" timestamptz NULL,
  "updated_at" timestamptz NULL,
  "deleted_at" timestamptz NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_radio_ports_antenna" FOREIGN KEY ("antenna_id") REFERENCES "antennas" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "fk_radio_ports_antenna_location" FOREIGN KEY ("antenna_location_id") REFERENCES "locations" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "fk_radios_ports" FOREIGN KEY ("radio_id") REFERENCES "radios" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- create index "idx_radio_ports_creator_id" to table: "radio_ports"
CREATE INDEX "idx_radio_ports_creator_id" ON "radio_ports" ("creator_id");
-- create index "idx_radio_ports_deleted_at" to table: "radio_ports"
CREATE INDEX "idx_radio_ports_deleted_at" ON "radio_ports" ("deleted_at");
-- create "claim_radio_ports" table
CREATE TABLE "claim_radio_ports" (
  "id" uuid NOT NULL,
  "claim_id" uuid NOT NULL,
  "radio_port_id" uuid NOT NULL,
  "cbrs_cbsd_id" character varying(512) NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_claim_radio_ports_radio_port" FOREIGN KEY ("radio_port_id") REFERENCES "radio_ports" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "fk_claims_radio_ports" FOREIGN KEY ("claim_id") REFERENCES "claims" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- create "grants" table
CREATE TABLE "grants" (
  "id" uuid NOT NULL,
  "element_id" uuid NOT NULL,
  "description" character varying(4096) NOT NULL,
  "creator_id" uuid NOT NULL,
  "updater_id" uuid NULL,
  "created_at" timestamptz NULL,
  "updated_at" timestamptz NULL,
  "approved_at" timestamptz NULL,
  "owner_approved_at" timestamptz NULL,
  "denied_at" timestamptz NULL,
  "starts_at" timestamptz NOT NULL,
  "expires_at" timestamptz NULL,
  "revoked_at" timestamptz NULL,
  "deleted_at" timestamptz NULL,
  "spectrum_id" uuid NULL,
  PRIMARY KEY ("id")
);
-- create index "idx_grants_approved_at" to table: "grants"
CREATE INDEX "idx_grants_approved_at" ON "grants" ("approved_at");
-- create index "idx_grants_creator_id" to table: "grants"
CREATE INDEX "idx_grants_creator_id" ON "grants" ("creator_id");
-- create index "idx_grants_deleted_at" to table: "grants"
CREATE INDEX "idx_grants_deleted_at" ON "grants" ("deleted_at");
-- create index "idx_grants_element_id" to table: "grants"
CREATE INDEX "idx_grants_element_id" ON "grants" ("element_id");
-- create index "idx_grants_spectrum_id" to table: "grants"
CREATE INDEX "idx_grants_spectrum_id" ON "grants" ("spectrum_id");
-- create "grant_constraints" table
CREATE TABLE "grant_constraints" (
  "id" uuid NOT NULL,
  "grant_id" uuid NOT NULL,
  "constraint_id" uuid NOT NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_grant_constraints_constraint" FOREIGN KEY ("constraint_id") REFERENCES "constraints" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "fk_grants_constraints" FOREIGN KEY ("grant_id") REFERENCES "grants" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- create "grant_radio_ports" table
CREATE TABLE "grant_radio_ports" (
  "id" uuid NOT NULL,
  "grant_id" uuid NOT NULL,
  "radio_port_id" uuid NOT NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_grant_radio_ports_radio_port" FOREIGN KEY ("radio_port_id") REFERENCES "radio_ports" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "fk_grants_radio_ports" FOREIGN KEY ("grant_id") REFERENCES "grants" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- create "monitors" table
CREATE TABLE "monitors" (
  "id" uuid NOT NULL,
  "radio_port_id" uuid NOT NULL,
  "monitored_radio_port_id" uuid NULL,
  "element_id" uuid NULL,
  "name" character varying(256) NOT NULL,
  "description" character varying(1024) NULL,
  "types" character varying(512) NOT NULL,
  "formats" character varying(512) NOT NULL,
  "config" jsonb NULL,
  "exclusive" boolean NOT NULL,
  "enabled" boolean NOT NULL,
  "creator_id" uuid NOT NULL,
  "updater_id" uuid NULL,
  "created_at" timestamptz NULL,
  "updated_at" timestamptz NULL,
  "deleted_at" timestamptz NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_monitors_monitored_radio_port" FOREIGN KEY ("monitored_radio_port_id") REFERENCES "radio_ports" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "fk_monitors_radio_port" FOREIGN KEY ("radio_port_id") REFERENCES "radio_ports" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- create index "idx_monitors_creator_id" to table: "monitors"
CREATE INDEX "idx_monitors_creator_id" ON "monitors" ("creator_id");
-- create index "idx_monitors_deleted_at" to table: "monitors"
CREATE INDEX "idx_monitors_deleted_at" ON "monitors" ("deleted_at");
-- create "spectrums" table
CREATE TABLE "spectrums" (
  "id" uuid NOT NULL,
  "element_id" uuid NOT NULL,
  "name" character varying(256) NOT NULL,
  "description" character varying(4096) NULL,
  "url" character varying(512) NOT NULL,
  "enabled" boolean NOT NULL,
  "creator_id" uuid NOT NULL,
  "updater_id" uuid NULL,
  "created_at" timestamptz NULL,
  "updated_at" timestamptz NULL,
  "approved_at" timestamptz NULL,
  "denied_at" timestamptz NULL,
  "starts_at" timestamptz NULL,
  "expires_at" timestamptz NULL,
  "deleted_at" timestamptz NULL,
  PRIMARY KEY ("id")
);
-- create index "idx_spectrums_creator_id" to table: "spectrums"
CREATE INDEX "idx_spectrums_creator_id" ON "spectrums" ("creator_id");
-- create index "idx_spectrums_deleted_at" to table: "spectrums"
CREATE INDEX "idx_spectrums_deleted_at" ON "spectrums" ("deleted_at");
-- create index "idx_spectrums_element_id" to table: "spectrums"
CREATE INDEX "idx_spectrums_element_id" ON "spectrums" ("element_id");
-- create "policies" table
CREATE TABLE "policies" (
  "id" uuid NOT NULL,
  "spectrum_id" uuid NOT NULL,
  "element_id" uuid NULL,
  "allowed" boolean NOT NULL,
  "auto_approve" boolean NOT NULL,
  "priority" bigint NOT NULL,
  "max_duration" bigint NOT NULL,
  "when_unoccupied" boolean NOT NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_spectrums_policies" FOREIGN KEY ("spectrum_id") REFERENCES "spectrums" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- create index "idx_policy_spectrum_id_element_id" to table: "policies"
CREATE UNIQUE INDEX "idx_policy_spectrum_id_element_id" ON "policies" ("spectrum_id", "element_id");
-- create "spectrum_constraints" table
CREATE TABLE "spectrum_constraints" (
  "id" uuid NOT NULL,
  "spectrum_id" uuid NOT NULL,
  "constraint_id" uuid NOT NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_spectrum_constraints_constraint" FOREIGN KEY ("constraint_id") REFERENCES "constraints" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "fk_spectrums_constraints" FOREIGN KEY ("spectrum_id") REFERENCES "spectrums" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- create "zones" table
CREATE TABLE "zones" (
  "id" uuid NOT NULL,
  "element_id" uuid NOT NULL,
  "name" character varying(256) NOT NULL,
  "description" character varying(4096) NULL,
  "area_id" uuid NULL,
  "creator_id" uuid NOT NULL,
  "updater_id" uuid NULL,
  "created_at" timestamptz NULL,
  "updated_at" timestamptz NULL,
  "deleted_at" timestamptz NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_zones_area" FOREIGN KEY ("area_id") REFERENCES "areas" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- create index "idx_zones_creator_id" to table: "zones"
CREATE INDEX "idx_zones_creator_id" ON "zones" ("creator_id");
-- create index "idx_zones_deleted_at" to table: "zones"
CREATE INDEX "idx_zones_deleted_at" ON "zones" ("deleted_at");
-- create index "idx_zones_name" to table: "zones"
CREATE INDEX "idx_zones_name" ON "zones" ("name");
