-- reverse: modify "radios" table
ALTER TABLE "radios" DROP COLUMN "html_url";
-- reverse: modify "radio_ports" table
ALTER TABLE "radio_ports" DROP COLUMN "device_id";
-- reverse: modify "monitors" table
ALTER TABLE "monitors" DROP COLUMN "html_url", DROP COLUMN "device_id";
-- reverse: modify "grants" table
ALTER TABLE "grants" DROP COLUMN "html_url", DROP COLUMN "ext_id";
