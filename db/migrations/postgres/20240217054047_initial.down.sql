-- reverse: create index "idx_zones_name" to table: "zones"
DROP INDEX "idx_zones_name";
-- reverse: create index "idx_zones_deleted_at" to table: "zones"
DROP INDEX "idx_zones_deleted_at";
-- reverse: create index "idx_zones_creator_id" to table: "zones"
DROP INDEX "idx_zones_creator_id";
-- reverse: create "zones" table
DROP TABLE "zones";
-- reverse: create "spectrum_constraints" table
DROP TABLE "spectrum_constraints";
-- reverse: create index "idx_policy_spectrum_id_element_id" to table: "policies"
DROP INDEX "idx_policy_spectrum_id_element_id";
-- reverse: create "policies" table
DROP TABLE "policies";
-- reverse: create index "idx_spectrums_element_id" to table: "spectrums"
DROP INDEX "idx_spectrums_element_id";
-- reverse: create index "idx_spectrums_deleted_at" to table: "spectrums"
DROP INDEX "idx_spectrums_deleted_at";
-- reverse: create index "idx_spectrums_creator_id" to table: "spectrums"
DROP INDEX "idx_spectrums_creator_id";
-- reverse: create "spectrums" table
DROP TABLE "spectrums";
-- reverse: create index "idx_monitors_deleted_at" to table: "monitors"
DROP INDEX "idx_monitors_deleted_at";
-- reverse: create index "idx_monitors_creator_id" to table: "monitors"
DROP INDEX "idx_monitors_creator_id";
-- reverse: create "monitors" table
DROP TABLE "monitors";
-- reverse: create "grant_radio_ports" table
DROP TABLE "grant_radio_ports";
-- reverse: create "grant_constraints" table
DROP TABLE "grant_constraints";
-- reverse: create index "idx_grants_spectrum_id" to table: "grants"
DROP INDEX "idx_grants_spectrum_id";
-- reverse: create index "idx_grants_element_id" to table: "grants"
DROP INDEX "idx_grants_element_id";
-- reverse: create index "idx_grants_deleted_at" to table: "grants"
DROP INDEX "idx_grants_deleted_at";
-- reverse: create index "idx_grants_creator_id" to table: "grants"
DROP INDEX "idx_grants_creator_id";
-- reverse: create index "idx_grants_approved_at" to table: "grants"
DROP INDEX "idx_grants_approved_at";
-- reverse: create "grants" table
DROP TABLE "grants";
-- reverse: create "claim_radio_ports" table
DROP TABLE "claim_radio_ports";
-- reverse: create index "idx_radio_ports_deleted_at" to table: "radio_ports"
DROP INDEX "idx_radio_ports_deleted_at";
-- reverse: create index "idx_radio_ports_creator_id" to table: "radio_ports"
DROP INDEX "idx_radio_ports_creator_id";
-- reverse: create "radio_ports" table
DROP TABLE "radio_ports";
-- reverse: create index "idx_radios_element_id" to table: "radios"
DROP INDEX "idx_radios_element_id";
-- reverse: create index "idx_radios_deleted_at" to table: "radios"
DROP INDEX "idx_radios_deleted_at";
-- reverse: create index "idx_radios_creator_id" to table: "radios"
DROP INDEX "idx_radios_creator_id";
-- reverse: create "radios" table
DROP TABLE "radios";
-- reverse: create index "idx_locations_name" to table: "locations"
DROP INDEX "idx_locations_name";
-- reverse: create index "idx_locations_element_id" to table: "locations"
DROP INDEX "idx_locations_element_id";
-- reverse: create index "idx_locations_deleted_at" to table: "locations"
DROP INDEX "idx_locations_deleted_at";
-- reverse: create index "idx_locations_creator_id" to table: "locations"
DROP INDEX "idx_locations_creator_id";
-- reverse: create "locations" table
DROP TABLE "locations";
-- reverse: create index "idx_antennas_element_id" to table: "antennas"
DROP INDEX "idx_antennas_element_id";
-- reverse: create index "idx_antennas_deleted_at" to table: "antennas"
DROP INDEX "idx_antennas_deleted_at";
-- reverse: create index "idx_antennas_creator_id" to table: "antennas"
DROP INDEX "idx_antennas_creator_id";
-- reverse: create "antennas" table
DROP TABLE "antennas";
-- reverse: create "claim_constraints" table
DROP TABLE "claim_constraints";
-- reverse: create index "idx_claims_spectrum_id" to table: "claims"
DROP INDEX "idx_claims_spectrum_id";
-- reverse: create index "idx_claims_element_id" to table: "claims"
DROP INDEX "idx_claims_element_id";
-- reverse: create index "idx_claims_deleted_at" to table: "claims"
DROP INDEX "idx_claims_deleted_at";
-- reverse: create index "idx_claims_creator_id" to table: "claims"
DROP INDEX "idx_claims_creator_id";
-- reverse: create index "idx_claims_approved_at" to table: "claims"
DROP INDEX "idx_claims_approved_at";
-- reverse: create "claims" table
DROP TABLE "claims";
-- reverse: create index "idx_constraints_min_freq" to table: "constraints"
DROP INDEX "idx_constraints_min_freq";
-- reverse: create index "idx_constraints_max_freq" to table: "constraints"
DROP INDEX "idx_constraints_max_freq";
-- reverse: create index "idx_constraints_max_eirp" to table: "constraints"
DROP INDEX "idx_constraints_max_eirp";
-- reverse: create index "idx_constraints_exclusive" to table: "constraints"
DROP INDEX "idx_constraints_exclusive";
-- reverse: create "constraints" table
DROP TABLE "constraints";
-- reverse: create "area_points" table
DROP TABLE "area_points";
-- reverse: create index "idx_areas_name" to table: "areas"
DROP INDEX "idx_areas_name";
-- reverse: create index "idx_areas_element_id" to table: "areas"
DROP INDEX "idx_areas_element_id";
-- reverse: create "areas" table
DROP TABLE "areas";
