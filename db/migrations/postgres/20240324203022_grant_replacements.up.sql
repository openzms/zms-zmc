-- create "grant_replacements" table
CREATE TABLE "grant_replacements" (
  "id" uuid NOT NULL,
  "grant_id" uuid NOT NULL,
  "new_grant_id" uuid NOT NULL,
  "description" character varying(4096) NOT NULL,
  "created_at" timestamptz NULL,
  "constraint_change_id" uuid NULL,
  "observation_id" uuid NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_grants_replacement" FOREIGN KEY ("grant_id") REFERENCES "grants" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- create index "idx_grant_replacement_grant_id_new_grant_id" to table: "grant_replacements"
CREATE UNIQUE INDEX "idx_grant_replacement_grant_id_new_grant_id" ON "grant_replacements" ("grant_id", "new_grant_id");
