-- add value to enum type: "grant_status_enum"
ALTER TYPE "grant_status_enum" ADD VALUE 'pending' AFTER 'active';
-- modify "grants" table
ALTER TABLE "grants" ADD COLUMN "allow_skip_acks" boolean NULL;
-- modify "policies" table
ALTER TABLE "policies" ADD COLUMN "allow_skip_acks" boolean NOT NULL DEFAULT false;
