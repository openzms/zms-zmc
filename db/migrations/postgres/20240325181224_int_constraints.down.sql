-- reverse: create "grant_rt_int_constraints" table
DROP TABLE "grant_rt_int_constraints";
-- reverse: create "rt_int_constraints" table
DROP TABLE "rt_int_constraints";
-- reverse: create "grant_int_constraints" table
DROP TABLE "grant_int_constraints";
-- reverse: create index "idx_int_constraints_max_power" to table: "int_constraints"
DROP INDEX "idx_int_constraints_max_power";
-- reverse: create "int_constraints" table
DROP TABLE "int_constraints";
-- reverse: create enum type "comparator_enum"
DROP TYPE "comparator_enum";
