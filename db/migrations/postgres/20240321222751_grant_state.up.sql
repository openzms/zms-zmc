-- create enum type "grant_status_enum"
CREATE TYPE "grant_status_enum" AS ENUM ('unknown', 'created', 'scheduling', 'approving', 'approved', 'active', 'paused', 'replacing', 'denied', 'revoked', 'deleted');
-- create enum type "grant_op_status_enum"
CREATE TYPE "grant_op_status_enum" AS ENUM ('unknown', 'submitted', 'accepted', 'active', 'paused');
-- modify "policies" table
ALTER TABLE "policies" ADD COLUMN "disable_emit_check" boolean NOT NULL DEFAULT false, ADD COLUMN "allow_inactive" boolean NOT NULL DEFAULT false, ADD COLUMN "allow_conflicts" boolean NOT NULL DEFAULT false;
-- create "grant_conflicts" table
CREATE TABLE "grant_conflicts" (
  "id" uuid NOT NULL,
  "a_grant_id" uuid NOT NULL,
  "b_grant_id" uuid NOT NULL,
  PRIMARY KEY ("id")
);
-- modify "grants" table
ALTER TABLE "grants" ADD COLUMN "name" character varying(256) NOT NULL DEFAULT '', ADD COLUMN "priority" bigint NOT NULL DEFAULT 0, ADD COLUMN "allow_inactive" boolean NULL, ADD COLUMN "allow_conflicts" boolean NULL, ADD COLUMN "status" "grant_status_enum" NOT NULL DEFAULT 'unknown', ADD COLUMN "status_ack_by" timestamptz NULL, ADD COLUMN "op_status" "grant_op_status_enum" NOT NULL DEFAULT 'unknown', ADD COLUMN "op_status_updated_at" timestamptz NULL;
-- create index "idx_grants_op_status" to table: "grants"
CREATE INDEX "idx_grants_op_status" ON "grants" ("op_status");
-- create index "idx_grants_status" to table: "grants"
CREATE INDEX "idx_grants_status" ON "grants" ("status");
-- create "grant_logs" table
CREATE TABLE "grant_logs" (
  "id" uuid NOT NULL,
  "grant_id" uuid NOT NULL,
  "status" "grant_status_enum" NOT NULL,
  "op_status" "grant_op_status_enum" NOT NULL,
  "message" character varying(4096) NOT NULL,
  "created_at" timestamptz NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_grants_logs" FOREIGN KEY ("grant_id") REFERENCES "grants" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- create index "idx_grant_logs_op_status" to table: "grant_logs"
CREATE INDEX "idx_grant_logs_op_status" ON "grant_logs" ("op_status");
-- create index "idx_grant_logs_status" to table: "grant_logs"
CREATE INDEX "idx_grant_logs_status" ON "grant_logs" ("status");
