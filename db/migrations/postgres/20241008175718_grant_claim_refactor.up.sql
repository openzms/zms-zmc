-- modify "grants" table
ALTER TABLE "grants" ADD COLUMN "is_claim" boolean NOT NULL DEFAULT false;
-- modify "claims" table
ALTER TABLE "claims" DROP COLUMN "ext_type", DROP COLUMN "ext_name", DROP COLUMN "ext_endpoint", DROP COLUMN "ext_grant_id", DROP COLUMN "approved_at", DROP COLUMN "spectrum_id", DROP COLUMN "starts_at", DROP COLUMN "expires_at", DROP COLUMN "revoked_at", ADD COLUMN "ext_id" character varying(256) NOT NULL, ADD COLUMN "type" character varying(512) NOT NULL, ADD COLUMN "source" character varying(512) NOT NULL, ADD COLUMN "html_url" character varying(512) NULL, ADD COLUMN "name" character varying(512) NOT NULL DEFAULT '', ADD COLUMN "grant_id" uuid NULL, ADD
 CONSTRAINT "fk_claims_grant" FOREIGN KEY ("grant_id") REFERENCES "grants" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
-- create index "idx_claim_ext_id" to table: "claims"
CREATE INDEX "idx_claim_ext_id" ON "claims" ("ext_id");
-- create index "idx_claims_html_url" to table: "claims"
CREATE INDEX "idx_claims_html_url" ON "claims" ("html_url");
-- create index "idx_claims_name" to table: "claims"
CREATE INDEX "idx_claims_name" ON "claims" ("name");
-- create index "idx_claims_source" to table: "claims"
CREATE INDEX "idx_claims_source" ON "claims" ("source");
-- create index "idx_claims_type" to table: "claims"
CREATE INDEX "idx_claims_type" ON "claims" ("type");
-- drop "claim_constraints" table
DROP TABLE "claim_constraints";
-- drop "claim_radio_ports" table
DROP TABLE "claim_radio_ports";
