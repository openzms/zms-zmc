-- modify "grants" table
ALTER TABLE "grants" ADD COLUMN "ext_id" character varying(256) NULL, ADD COLUMN "html_url" character varying(512) NULL;
-- modify "monitors" table
ALTER TABLE "monitors" ADD COLUMN "device_id" character varying(256) NULL, ADD COLUMN "html_url" character varying(512) NULL;
-- modify "radio_ports" table
ALTER TABLE "radio_ports" ADD COLUMN "device_id" character varying(256) NULL;
-- modify "radios" table
ALTER TABLE "radios" ADD COLUMN "html_url" character varying(512) NULL;
