-- reverse: drop "claim_radio_ports" table
CREATE TABLE "claim_radio_ports" (
  "id" uuid NOT NULL,
  "claim_id" uuid NOT NULL,
  "radio_port_id" uuid NOT NULL,
  "cbrs_cbsd_id" character varying(512) NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_claim_radio_ports_radio_port" FOREIGN KEY ("radio_port_id") REFERENCES "radio_ports" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "fk_claims_radio_ports" FOREIGN KEY ("claim_id") REFERENCES "claims" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- reverse: drop "claim_constraints" table
CREATE TABLE "claim_constraints" (
  "id" uuid NOT NULL,
  "claim_id" uuid NOT NULL,
  "constraint_id" uuid NOT NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_claim_constraints_constraint" FOREIGN KEY ("constraint_id") REFERENCES "constraints" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "fk_claims_constraints" FOREIGN KEY ("claim_id") REFERENCES "claims" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- reverse: create index "idx_claims_type" to table: "claims"
DROP INDEX "idx_claims_type";
-- reverse: create index "idx_claims_source" to table: "claims"
DROP INDEX "idx_claims_source";
-- reverse: create index "idx_claims_name" to table: "claims"
DROP INDEX "idx_claims_name";
-- reverse: create index "idx_claims_html_url" to table: "claims"
DROP INDEX "idx_claims_html_url";
-- reverse: create index "idx_claim_ext_id" to table: "claims"
DROP INDEX "idx_claim_ext_id";
-- reverse: modify "claims" table
ALTER TABLE "claims" DROP CONSTRAINT "fk_claims_grant", DROP COLUMN "grant_id", DROP COLUMN "name", DROP COLUMN "html_url", DROP COLUMN "source", DROP COLUMN "type", DROP COLUMN "ext_id", ADD COLUMN "revoked_at" timestamptz NULL, ADD COLUMN "expires_at" timestamptz NULL, ADD COLUMN "starts_at" timestamptz NULL, ADD COLUMN "spectrum_id" uuid NULL, ADD COLUMN "approved_at" timestamptz NULL, ADD COLUMN "ext_grant_id" character varying(256) NULL, ADD COLUMN "ext_endpoint" character varying(512) NULL, ADD COLUMN "ext_name" character varying(512) NULL, ADD COLUMN "ext_type" character varying(512) NULL;
-- reverse: modify "grants" table
ALTER TABLE "grants" DROP COLUMN "is_claim";
