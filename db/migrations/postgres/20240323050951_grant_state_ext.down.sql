-- reverse: modify "policies" table
ALTER TABLE "policies" DROP COLUMN "allow_skip_acks";
-- reverse: modify "grants" table
ALTER TABLE "grants" DROP COLUMN "allow_skip_acks";
