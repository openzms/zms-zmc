package main

import (
	"fmt"
	"io"
	"os"
	"strings"

	"ariga.io/atlas-provider-gorm/gormschema"

	"gitlab.flux.utah.edu/openzms/zms-zmc/pkg/store"
)

func addCustomSqlPostgres(b *strings.Builder) {
	for _, stmt := range store.CustomSqlPostgres {
		b.WriteString(stmt)
		b.WriteString(";\n\n")
	}
}

func addAllTables(b *strings.Builder) {
	if stmtList, err := gormschema.New("postgres").Load(store.AllTables...); err != nil {
		fmt.Fprintf(os.Stderr, "Error: failed to load gorm postgres schema from zms-zmc store pkg: %+v\n", err)
		os.Exit(1)
	} else {
		b.WriteString(stmtList)
		b.WriteString("\n")
	}
}

func main() {
	b := &strings.Builder{}
	addCustomSqlPostgres(b)
	addAllTables(b)
	io.WriteString(os.Stdout, b.String())
}
