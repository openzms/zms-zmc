<!--
SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
SPDX-License-Identifier: Apache-2.0
-->

# zms-zmc (OpenZMS zone management controller service)

The [OpenZMS software](https://gitlab.flux.utah.edu/openzms) is a prototype
automatic spectrum-sharing management system for radio dynamic zones.
OpenZMS provides mechanisms to share electromagnetic (radio-frequency)
spectrum between experimental or test systems and existing spectrum users,
and between multiple experimental systems.  We are building and deploying
OpenZMS within the context of the POWDER wireless testbed in Salt Lake City,
Utah, part of the NSF-sponsored Platforms for Advanced Wireless Research
program, to create [POWDER-RDZ](https://rdz.powderwireless.net).

This `zms-zmc` repository contains a controller service (spectrum, grants,
radios, etc) that is the primary interface between the Zone
and its Elements.


## Quick dev mode start

Build the project:

```
make
```

The [zms-identity](https://gitlab.flux.utah.edu/openzms/zms-identity) service
verifies tokens and establishes RBAC for the zmc service.  Therefore, you
must start that service prior to running this service, and must point this
service to the identity service's southbound (gRPC) endpoint at start.

Run the `identity` service as instructed in the
[zms-identity instructions](https://gitlab.flux.utah.edu/openzms/zms-identity).

If you ran the `identity` service with default endpoints, you shouldn't need
to specify any endpoint arguments for the `zmc` service.  If you changed the
`identity` service's endpoints, you will need to customize the arguments
below to correspond (see `zms-zmc --help`).

Run the `zms-zmc` service:

```
mkdir -p tmp
./build/output/zms-zmc \
    --db-dsn tmp/zms-zmc.sqlite \
    -debug
```
