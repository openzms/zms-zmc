package grant

import (
	"container/heap"
	"slices"
	"testing"
	"time"

	"github.com/google/uuid"
)

var debug bool = false

var expected []GrantWatch = []GrantWatch{
	{
		GrantId:  uuid.MustParse("00000000-0000-0000-0000-000000000000"),
		NextTime: time.Unix(0, 0),
	},
	{
		GrantId:  uuid.MustParse("00000000-0000-0000-0000-000000000001"),
		NextTime: time.Unix(0, 0).Add(time.Second * 15),
	},
	{
		GrantId:  uuid.MustParse("00000000-0000-0000-0000-000000000002"),
		NextTime: time.Unix(0, 0).Add(time.Second * 15 * 2),
	},
	{
		GrantId:  uuid.MustParse("00000000-0000-0000-0000-000000000003"),
		NextTime: time.Unix(0, 0).Add(time.Second * 15 * 3),
	},
	{
		GrantId:  uuid.MustParse("00000000-0000-0000-0000-000000000004"),
		NextTime: time.Unix(0, 0).Add(time.Second * 15 * 4),
	},
}

func MakeQueuePush(contents []GrantWatch, t *testing.T) *GrantWatchQueue {
	q := NewGrantWatchQueue(0, 0)
	for i := 0; i < len(contents); i++ {
		heap.Push(q, &contents[i])
	}
	return q
}

func MakeQueueDirect(contents []GrantWatch, t *testing.T) *GrantWatchQueue {
	q := make(GrantWatchQueue, len(contents))
	if gwp := q.Peek(); gwp != nil {
		t.Fatalf(`Peek was not nil: %v`, gwp)
	}
	for i := 0; i < len(expected); i++ {
		q[i] = &contents[i]
	}
	heap.Init(&q)
	return &q
}

func DrainPeekPop(q *GrantWatchQueue, expected []GrantWatch, t *testing.T) {
	qlen := len(*q)
	for i := 0; i < qlen; i++ {
		gwp := q.Peek()
		if gwp.GrantId != expected[i].GrantId {
			t.Fatalf(
				`index %d: Peek not least element (got %s:%s, expected %s:%s)`,
				i, gwp.GrantId, gwp.NextTime, expected[i].GrantId, expected[i].NextTime)
		}
		gw := heap.Pop(q).(*GrantWatch)
		if gw.GrantId != expected[i].GrantId {
			t.Fatalf(
				`index %d: not least element (got %s:%s, expected %s:%s)`,
				i, gw.GrantId, gw.NextTime, expected[i].GrantId, expected[i].NextTime)
		} else if debug {
			t.Logf(
				`index %d: correct least element (got %s:%s, expected %s:%s)`,
				i, gw.GrantId, gw.NextTime, expected[i].GrantId, expected[i].NextTime)
		}
	}
}

func TestGrantWatchQueuePeekEmpty(t *testing.T) {
	q := MakeQueuePush(nil, t)
	if qwp := q.Peek(); qwp != nil {
		t.Fatalf(`empty: Peek not nil (%v)`, qwp)
	}
}

func TestGrantWatchQueue(t *testing.T) {
	q := MakeQueueDirect(expected, t)
	DrainPeekPop(q, expected, t)
}

func TestGrantWatchQueueReverseInit(t *testing.T) {
	newInit := slices.Clone(expected)
	slices.Reverse(newInit)
	q := MakeQueueDirect(newInit, t)
	DrainPeekPop(q, expected, t)
}

func TestGrantWatchQueuePush(t *testing.T) {
	q := MakeQueuePush(expected, t)
	DrainPeekPop(q, expected, t)
}

func TestGrantWatchQueuePushUpdate(t *testing.T) {
	newExpected := slices.Clone(expected)
	q := MakeQueuePush(newExpected, t)

	nt := expected[len(expected)-1].NextTime.Add(time.Second * 15)
	ugw := &newExpected[0]
	newExpected = newExpected[1:]
	newExpected = append(newExpected, *ugw)
	q.Update(ugw, nt)
	DrainPeekPop(q, newExpected, t)
}
