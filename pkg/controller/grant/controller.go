package grant

import (
	"container/heap"
	"context"
	"errors"
	"fmt"
	"slices"
	"sort"
	"sync"
	"time"

	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"gorm.io/gorm"

	event_v1 "gitlab.flux.utah.edu/openzms/zms-api/go/zms/event/v1"
	zmc_v1 "gitlab.flux.utah.edu/openzms/zms-api/go/zms/zmc/v1"
	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/client"
	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/subscription"

	"gitlab.flux.utah.edu/openzms/zms-zmc/pkg/config"
	"gitlab.flux.utah.edu/openzms/zms-zmc/pkg/sched"
	"gitlab.flux.utah.edu/openzms/zms-zmc/pkg/store"
)

type GrantController struct {
	config     *config.Config
	db         *gorm.DB
	sm         *subscription.SubscriptionManager[*subscription.Event]
	rclient    *client.ZmsClient
	running    bool
	watchMap   map[uuid.UUID]*GrantWatch
	watchQueue *GrantWatchQueue

	mutex  sync.RWMutex
	done   chan interface{}
	update chan interface{}
}

func NewGrantController(serverConfig *config.Config, db *gorm.DB, sm *subscription.SubscriptionManager[*subscription.Event], rclient *client.ZmsClient) *GrantController {
	return &GrantController{
		config:     serverConfig,
		db:         db,
		sm:         sm,
		rclient:    rclient,
		running:    false,
		watchMap:   make(map[uuid.UUID]*GrantWatch),
		watchQueue: NewGrantWatchQueue(0, 0),
	}
}

var liveGrantStatusList []store.GrantStatus = []store.GrantStatus{
	store.StatusApproved, store.StatusActive, store.StatusPaused,
}

// These states are terminal but require an ack.
// They have been acked if grant.StatusAckBy is NULL and
// grant.OpStatusUpdatedAt is not NULL.
var terminalAckGrantStatusList []store.GrantStatus = []store.GrantStatus{
	store.StatusReplacing, store.StatusRevoked,
}
var statusTransitionMap map[store.GrantStatus][]store.GrantOpStatus = map[store.GrantStatus][]store.GrantOpStatus{
	store.StatusApproved:  []store.GrantOpStatus{store.OpStatusAccepted},
	store.StatusActive:    []store.GrantOpStatus{store.OpStatusActive, store.OpStatusPaused},
	store.StatusPaused:    []store.GrantOpStatus{store.OpStatusActive, store.OpStatusPaused},
	store.StatusReplacing: []store.GrantOpStatus{store.OpStatusPaused},
	store.StatusRevoked:   []store.GrantOpStatus{store.OpStatusPaused},
}

func (gc *GrantController) Load() error {
	if gc.running {
		return errors.New("controller already running; cannot load")
	}

	log.Info().Msg("gc: loading live grants")

	now := time.Now().UTC()
	watchInterval := gc.config.GetGrantAcksIntervalDuration()

	var grantList []store.Grant
	q := gc.db.
		// If it's been deleted, we ignore.
		Where("grants.deleted_at is NULL").
		Where("grants.StatusAckBy is not NULL").
		// If the grant is in a terminal state and StatusAckBy is required
		// (not null), this grant must still be tracked, unless expired.
		Where("((grants.status in ?) or (grants.status in ? and grants.status_ack_by is not NULL))", liveGrantStatusList, terminalAckGrantStatusList).
		Where("(grants.expires_at is NOT NULL or grants.expires_at > ?)")
	if qres := q.Find(&grantList); qres.Error != nil {
		return fmt.Errorf("gc: failed to load live grants: %s", qres.Error.Error())
	} else if qres.RowsAffected < 1 {
		return nil
	}

	for _, g := range grantList {
		nextAckRequired := g.StatusAckBy != nil
		var nextTime *time.Time
		if nextAckRequired {
			nt := now.Add(watchInterval)
			if g.ExpiresAt != nil && nt.After(*g.ExpiresAt) {
				nt = *g.ExpiresAt
			} else {
				nextTime = &nt
			}
		} else if g.StartsAt.Before(now) {
			nextTime = &g.StartsAt
		} else if g.ExpiresAt != nil {
			nextTime = g.ExpiresAt
		}
		gc.WatchGrant(&g, g.Status, statusTransitionMap[g.Status], nextAckRequired, true, nextTime)
	}

	return nil
}

func (gc *GrantController) Notify() {
	gc.update <- nil
}

func (gc *GrantController) UnwatchGrant(grantId uuid.UUID) error {
	gc.mutex.Lock()
	defer gc.mutex.Unlock()

	if gw, exists := gc.watchMap[grantId]; !exists {
		return errors.New("grant not watched")
	} else {
		m := fmt.Sprintf("unwatching grant (%s, %s)", gw.GrantId, gw.AckStatus)
		log.Debug().Msg("gc: " + m)
		delete(gc.watchMap, grantId)
		if gw.index > -1 {
			heap.Remove(gc.watchQueue, gw.index)
		}
	}

	return nil
}

// Watch a new grant or update a watched grant.
func (gc *GrantController) WatchGrant(grant *store.Grant, newStatus store.GrantStatus, validOpStatusList []store.GrantOpStatus, nextAckRequired bool, clearMissed bool, nextTime *time.Time) {
	now := time.Now().UTC()
	if nextTime == nil {
		nt := now.Add(gc.config.GetGrantAcksIntervalDuration())
		if grant.ExpiresAt != nil && nt.After(*grant.ExpiresAt) {
			nextTime = grant.ExpiresAt
		} else {
			nextTime = &nt
		}
	}
	watchInterval := (*nextTime).Sub(now)
	grantId := grant.Id

	gc.mutex.Lock()
	defer gc.mutex.Unlock()

	if gw, exists := gc.watchMap[grantId]; !exists {
		// Determine if acks are required.  The controller will revoke
		// un-acked grants if enforcement is enabled, and if this grant is
		// not allowed to skip acks.
		acksRequired := gc.config.GrantAcksEnforced
		if grant.AllowSkipAcks != nil && *grant.AllowSkipAcks {
			acksRequired = false
		}

		gw = &GrantWatch{
			GrantId:           grantId,
			AckStatus:         newStatus,
			ValidOpStatusList: validOpStatusList,
			AcksRequired:      acksRequired,
			NextAckRequired:   nextAckRequired,
			NextTime:          *nextTime,
			missedAckCounter:  0,
		}
		if grant.OpStatusUpdatedAt != nil {
			gw.LastAckTime = *grant.OpStatusUpdatedAt
		}
		gc.watchMap[grantId] = gw
		heap.Push(gc.watchQueue, gw)
		msg := fmt.Sprintf(
			"WatchGrant: add (%s; %s/%v->%v; %v/%v; +%v)",
			grantId, newStatus, grant.OpStatus, validOpStatusList, acksRequired, nextAckRequired, watchInterval)
		log.Debug().Msg(msg)
	} else {
		msg := fmt.Sprintf(
			"WatchGrant: update (%s; %s -> %s/%v->%v; %v/%v; +%v)",
			grantId, gw.AckStatus, newStatus, grant.OpStatus, validOpStatusList, gw.AcksRequired, nextAckRequired, watchInterval)
		log.Debug().Msg(msg)

		gw.AckStatus = newStatus
		gw.ValidOpStatusList = validOpStatusList
		gw.NextAckRequired = nextAckRequired
		if clearMissed {
			gw.missedAckCounter = 0
		}
		if gw.index < 0 {
			// This GrantWatch is temporarily dequeued, being handled in
			// Run(); re-insert it.
			gw.NextTime = *nextTime
			heap.Push(gc.watchQueue, gw)
		} else {
			// This GrantWatch is already enqueued; just update it.
			gc.watchQueue.Update(gw, *nextTime)
		}
	}
}

// Handle a watched grant whose nextTime has arrived.
//
// We may change Grant.Status as follows:
// * Revoke a grant due to lack of ack (or warn that we should have done so);
// * Expire (pause) a grant because its expiresAt time has arrived;
// * Activate a grant because its startsAt time has arrived.
//
// We may change GrantWatch.NextTime
func (gc *GrantController) handleQueue(gw *GrantWatch) error {
	now := time.Now().UTC()
	watchInterval := gc.config.GetGrantAcksIntervalDuration()
	nextIntervalTime := now.Add(watchInterval)

	// Load grant.
	g := store.Grant{}
	if res := gc.db.First(&g, gw.GrantId); res.Error != nil {
		return fmt.Errorf("gc: handleQueue: error loading grant %s: %s", gw.GrantId, res.Error)
	} else if res.RowsAffected != 1 {
		return fmt.Errorf("gc: handleQueue: no such grant %s", gw.GrantId)
	}

	// Here is our queue-watching state machine:
	//
	// * If Status == Deleted, stop watching.
	//
	// * If expired:
	//   - If Status == Active, stop grant
	//   - If Status == Paused, wait for ack

	// First, handle deleted/expiration cases:
	// * status == Deleted -> Unwatch
	// * status == Active -> ChangeGrantStatus(Paused), need ack
	// * status == Paused -> no ack and acks required -> freeze user
	if g.Status == store.StatusDeleted {
		// Stop watching deleted grants.
		m := fmt.Sprintf("grant %s deleted; unwatching", g.Id)
		log.Debug().Msg("gc: handleQueue: " + m)
		gc.UnwatchGrant(g.Id)
		return nil
	} else if g.ExpiresAt != nil && g.ExpiresAt.Before(now) {
		// If the grant is not yet Expired (Paused), or has not yet ack'd
		newStatus := g.Status
		if newStatus == store.StatusActive {
			// XXX: change to store.StatusExpired?
			newStatus = store.StatusPaused
		}
		if g.Status != newStatus {
			m := fmt.Sprintf("stopping expired grant %s (%s/%s)", g.Id, g.Status, g.OpStatus)
			log.Debug().Msg("gc: handleQueue: " + m)
			if cerr := gc.ChangeGrantStatus(&g, newStatus, m, nil, nil); cerr != nil {
				cerr = fmt.Errorf("failed to stop expired grant %s: %s", g.Id, cerr.Error())
				log.Error().Msg("gc: handleQueue: " + cerr.Error())
				return cerr
			} else {
				log.Debug().Msg("gc: handleQueue: " + m)
				return nil
			}
		} else if g.OpStatus != store.OpStatusPaused {
			// XXX: if no ack and enforcing, freeze user
			m := fmt.Sprintf("not waiting for paused, expired grant %s (%s/%s) (%d missed acks)", g.Id, g.Status, g.OpStatus, gw.missedAckCounter)
			log.Debug().Msg("gc: handleQueue: " + m)
			gc.UnwatchGrant(g.Id)
			return nil
		} else {
			// Let the expired grant go.
			m := fmt.Sprintf("grant %s expired properly; unwatching", g.Id)
			log.Debug().Msg("gc: handleQueue: " + m)
			gc.UnwatchGrant(g.Id)
			return nil
		}
	} else if g.Status == store.StatusApproved {
		// If status == approved:
		// * StartsAt is still in future:
		//   - Accepted: watch but nextTime is StartsAt, next ack not required
		//   - Not Accepted: watch til min(interval, StartsAt)
		// * Starts at is now or earlier:
		//   - Accepted -> changeGrantStatus(Active)
		//   - Not accepted && not enforcing -> changeGrantStatus(Active) (warn)
		//   - Not accepted && enforcing -> changeGrantStatus(Revoke) (log, warn)
		if g.StartsAt.After(now) {
			if g.OpStatus == store.OpStatusAccepted {
				m := fmt.Sprintf("accepted grant %s ready for future start (%v)", g.Id, g.StartsAt.Sub(now))
				log.Debug().Msg("gc: handleQueue: " + m)
				gc.WatchGrant(&g, g.Status, []store.GrantOpStatus{store.OpStatusAccepted}, false, true, &g.StartsAt)
				return nil
			} else {
				gw.missedAckCounter += 1
				m := fmt.Sprintf("unaccepted grant %s not ready for future start (%v) (%d missed acks)", g.Id, g.StartsAt.Sub(now), gw.missedAckCounter)
				log.Debug().Msg("gc: handleQueue: " + m)
				nextTime := nextIntervalTime
				if nextIntervalTime.After(g.StartsAt) {
					nextTime = g.StartsAt
				}
				gc.WatchGrant(&g, g.Status, []store.GrantOpStatus{store.OpStatusAccepted}, true, false, &nextTime)
				return nil
			}
		} else {
			// If g.StartsAt is now or earlier, activate or revoke the grant,
			// depending on compliance with ack protocol.
			if g.OpStatus == store.OpStatusAccepted || !gw.AcksRequired || int64(gw.missedAckCounter) < gc.config.GrantAcksGrace {
				cm := "accepted"
				if !gw.AcksRequired {
					cm = "unaccepted-no-acks"
				}
				m := fmt.Sprintf("activating %s grant %s (%s/%s)", cm, g.Id, g.Status, g.OpStatus)
				// Activate properly-Accepted grant.
				if cerr := gc.ChangeGrantStatus(&g, store.StatusActive, m, nil, nil); cerr != nil {
					m := fmt.Sprintf("failed to activate %s grant %s: %s", cm, g.Id, cerr.Error())
					log.Error().Msg("gc: handleQueue: " + m)
					return cerr
				} else {
					m := fmt.Sprintf("activated %s grant %s (%s/%s)", cm, g.Id, g.Status, g.OpStatus)
					log.Debug().Msg("gc: handleQueue: " + m)
					return nil
				}
			} else {
				// Revoke unaccepted, AcksRequired grant.
				m := fmt.Sprintf("revoking unaccepted grant %s (%s/%s)", g.Id, g.Status, g.OpStatus)
				if cerr := gc.ChangeGrantStatus(&g, store.StatusRevoked, m, nil, nil); cerr != nil {
					m := fmt.Sprintf("failed to revoke unaccepted grant %s: %s", g.Id, cerr.Error())
					log.Error().Msg("gc: handleQueue: " + m)
					return cerr
				} else {
					m := fmt.Sprintf("revoked unaccepted grant %s (%s/%s)", g.Id, g.Status, g.OpStatus)
					log.Debug().Msg("gc: handleQueue: " + m)
					return nil
				}
			}
		}
	} else if g.Status == store.StatusActive || g.Status == store.StatusPaused {
		// If we reach this point, the grant was not ack'd within the
		// interval.

		// If !NextAckRequired, just update the nextTime field, keeping the
		// value of the NextAckRequired field; that will only be changed at
		// the next ChangeGrantStatus call.
		//
		// If !AcksRequired, just log a bit differently, and extend
		// NextTime.
		//
		// If AcksRequired, revoke the grant.
		nextTime := nextIntervalTime
		if g.ExpiresAt != nil && nextIntervalTime.After(*g.ExpiresAt) {
			nextTime = *g.ExpiresAt
		}
		if !gw.NextAckRequired || !gw.AcksRequired || (g.Status == store.StatusActive && int64(gw.missedAckCounter) < gc.config.GrantAcksGrace) {
			gc.WatchGrant(&g, g.Status, []store.GrantOpStatus{store.OpStatusActive, store.OpStatusPaused}, true, false, &nextTime)
		} else {
			// Revoke unacknowledged
			gw.missedAckCounter += 1
			m := fmt.Sprintf("revoking unacked grant %s (%s/%s) (%d missed acks)", g.Id, g.Status, g.OpStatus, gw.missedAckCounter)
			if cerr := gc.ChangeGrantStatus(&g, store.StatusRevoked, m, nil, nil); cerr != nil {
				m := fmt.Sprintf("failed to revoke unacked grant %s: %s", g.Id, cerr.Error())
				log.Error().Msg("gc: handleQueue: " + m)
				return cerr
			} else {
				m := fmt.Sprintf("revoked unacked grant %s (%s/%s)", g.Id, g.Status, g.OpStatus)
				log.Debug().Msg("gc: handleQueue: " + m)
				return nil
			}
		}
	} else if g.Status == store.StatusReplacing || g.Status == store.StatusRevoked {
		// If acks are not required, simply let this grant exit the watchQueue.
		//
		// XXX: If acks are required, record a grant_ack_violation.
		nextTime := nextIntervalTime
		if g.ExpiresAt != nil && nextIntervalTime.After(*g.ExpiresAt) {
			nextTime = *g.ExpiresAt
		}
		if !gw.NextAckRequired || !gw.AcksRequired {
			gc.WatchGrant(&g, g.Status, []store.GrantOpStatus{store.OpStatusPaused}, true, false, &nextTime)
		} else {
			// XXX: warn/freeze user
			gw.missedAckCounter += 1
			m := fmt.Sprintf("terminal grant unacked, unwatching %s (%s/%s) (%d missed acks)", g.Id, g.Status, g.OpStatus, gw.missedAckCounter)
			log.Error().Msg("gc: handleQueue: " + m)
			return nil
		}
	}

	return nil
}

func (gc *GrantController) Run() error {
	if gc.running {
		return errors.New("controller already running")
	}
	go func() {
		log.Debug().Msg("starting grant controller")
		var nextTimer *time.Timer = time.NewTimer(time.Second * 60)
		for {
			t := time.Now().UTC()
			var ngw *GrantWatch
			for {
				gc.mutex.Lock()
				ngw = gc.watchQueue.Peek()
				if ngw == nil {
					gc.mutex.Unlock()
					break
				} else if ngw.NextTime.After(t) {
					gc.mutex.Unlock()
					break
				} else {
					// XXX: eventually, we want to batch changes to related
					// grants that happen at the "same time", and process
					// higher-prio grants first.  This is important to avoid
					// churn in the multi-priority case where a lower-prio
					// grant is activated by the expiry of a higher-prio
					// grant, *but* there was another higher-prio grant
					// following immediately in the schedule.
					ngw = heap.Pop(gc.watchQueue).(*GrantWatch)

					// NB: drop our lock during handling.  Handling a single
					// grant inside the controller is effectively no
					// different than an external input (e.g. API) causing a
					// status change to a grant.
					gc.mutex.Unlock()

					// Handle watched grant.  If the grant should be
					// reinjected, handleQueue must do so.
					if herr := gc.handleQueue(ngw); herr != nil {
						// XXX:
						msg := fmt.Sprintf("gc: catastrophic failure handing grant %s: %s", ngw.GrantId, herr.Error())
						log.Error().Msg(msg)
					}
					continue
				}
			}
			if ngw != nil {
				t = time.Now().UTC()
				d := ngw.NextTime.Sub(t)
				nextTimer.Reset(d)
				log.Debug().Msg(fmt.Sprintf("gc: next watch time: %v", d))
			} else {
				log.Debug().Msg("gc: no grants")
				nextTimer.Reset(time.Second * 60)
			}

			select {
			case <-gc.done:
				gc.running = false
				gc.done <- nil
				return
			case <-gc.update:
				// The watchQueue was updated; may need to adjust watch timer.
				continue
			case <-nextTimer.C:
				// Time to process our next watched grant, perhaps.
				continue
			}
		}
	}()
	gc.running = true
	return nil
}

func (gc *GrantController) Shutdown(ctx context.Context, wait int) (err error) {
	if !gc.running {
		return errors.New("grant controller not running")
	}

	gc.done <- nil

	shutdownCtx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()
	select {
	case <-shutdownCtx.Done():
		if err = ctx.Err(); err != nil {
			log.Error().Err(err).Msg("failed to stop grant controller")
			return err
		} else {
			log.Info().Msg("gracefully stopped grant controller")
		}
	case <-gc.done:
		log.Info().Msg("gracefully stopped grant controller")
	}
	return nil
}

func (gc *GrantController) AddLog(grant *store.Grant, message string, t *time.Time) error {
	if t == nil {
		n := time.Now().UTC()
		t = &n
	}
	grantLog := store.GrantLog{
		Id:        uuid.New(),
		GrantId:   grant.Id,
		Status:    grant.Status,
		OpStatus:  grant.OpStatus,
		Message:   message,
		CreatedAt: *t,
	}
	if res := gc.db.Save(&grantLog); res.Error != nil {
		return res.Error
	}
	grant.Logs = append(grant.Logs, grantLog)
	return nil
}

func GrantStatusToEventType(status store.GrantStatus) int32 {
	switch status {
	case store.StatusCreated:
		return int32(event_v1.EventType_ET_CREATED)
	case store.StatusScheduling:
		return int32(event_v1.EventType_ET_OTHER)
	case store.StatusApproving:
		return int32(event_v1.EventType_ET_SCHEDULED)
	case store.StatusApproved:
		return int32(event_v1.EventType_ET_APPROVED)
	case store.StatusPending:
		return int32(event_v1.EventType_ET_PENDING)
	case store.StatusActive:
		return int32(event_v1.EventType_ET_STARTED)
	case store.StatusPaused:
		return int32(event_v1.EventType_ET_STOPPED)
	case store.StatusReplacing:
		return int32(event_v1.EventType_ET_OTHER)
	case store.StatusDenied:
		return int32(event_v1.EventType_ET_DENIED)
	case store.StatusRevoked:
		return int32(event_v1.EventType_ET_REVOKED)
	case store.StatusDeleted:
		return int32(event_v1.EventType_ET_DELETED)
	default:
		return int32(event_v1.EventType_ET_UNSPECIFIED)
	}
}

func (gc *GrantController) LoadGrantConflicts(grantId uuid.UUID) (gcl []store.GrantConflict, err error) {
	var conflictList []store.GrantConflict
	if res := gc.db.Where(gc.db.Where(&store.GrantConflict{AGrantId: grantId}).Or(&store.GrantConflict{BGrantId: grantId})).Find(&conflictList); res.Error != nil {
		return nil, res.Error
	} else {
		return conflictList, nil
	}
}

func (gc *GrantController) LoadConflicts(grantId uuid.UUID) (gl []*store.Grant, gcl []store.GrantConflict, err error) {
	var conflictList []store.GrantConflict
	if gcl, err := gc.LoadGrantConflicts(grantId); err != nil {
		return nil, nil, err
	} else {
		conflictList = gcl
	}

	gl = make([]*store.Grant, 0)
	conflictMap := make(map[uuid.UUID]interface{})

	for _, c := range conflictList {
		var cu uuid.UUID
		if c.AGrantId == grantId {
			cu = c.BGrantId
		} else {
			cu = c.AGrantId
		}
		if _, exists := conflictMap[cu]; !exists {
			cg := store.Grant{}
			if res2 := gc.db.First(&cg, cu); res2.Error != nil {
				return nil, conflictList, res2.Error
			} else if res2.RowsAffected != 1 {
				log.Error().Msg(fmt.Sprintf("inconsistent GrantConflict entry: %+v (ignoring)", c))
				continue
			} else {
				conflictMap[cu] = nil
				gl = append(gl, &cg)
			}
		}
	}

	if len(gl) > 0 {
		sort.Slice(gl, func(i, j int) bool {
			if gl[i].Priority == gl[j].Priority {
				if gl[i].ApprovedAt != nil && gl[j].ApprovedAt != nil {
					return (*gl[i].ApprovedAt).Before(*(gl[j].ApprovedAt))
				} else {
					return gl[i].CreatedAt.Before(gl[j].CreatedAt)
				}
			} else {
				return gl[i].Priority < gl[j].Priority
			}
		})
	}

	return gl, conflictList, nil
}

func (gc *GrantController) Schedule(grant *store.Grant, userId uuid.UUID, ignoreGrantIdList []uuid.UUID, approvalMsg string) (err error) {
	// Update OpStatus before scheduling.
	grant.OpStatus = store.OpStatusSubmitted
	if res := gc.db.Save(grant); res.Error != nil {
		return res.Error
	}

	// Attempt an inline schedule.
	if solution, err := sched.SimpleScheduler(gc.db, grant, gc.rclient, ignoreGrantIdList); err != nil {
		if err := gc.ChangeGrantStatus(grant, store.StatusDenied, err.Error(), &userId, nil); err != nil {
			m := fmt.Sprintf("error changing grant status to denied: %s", err.Error())
			log.Error().Err(err).Msg(m)
			return errors.New(m)
		}
	} else if solution == nil {
		m := "no available spectrum matched grant constraints"
		log.Info().Msg(m + "(" + grant.Id.String() + ")")
		if err := gc.ChangeGrantStatus(grant, store.StatusDenied, m, &userId, nil); err != nil {
			m := fmt.Sprintf("error changing grant status to denied: %s", err.Error())
			log.Error().Err(err).Msg(m)
			return errors.New(m)
		}
	} else {
		t := time.Now().UTC()
		grant.SpectrumId = &solution.SpectrumId
		grant.ApprovedAt = &t
		grant.OwnerApprovedAt = &t

		// Handle conflicts.
		conflictList := make([]store.GrantConflict, 0, len(solution.LowerPrioConflicts) + len(solution.HigherOrEqualPrioConflicts))
		for _, gcc := range solution.LowerPrioConflicts {
			log.Debug().Msg(fmt.Sprintf("lowerPrioConflict(%s, %s)", grant.Id, gcc.Id))
			gcon := store.GrantConflict{
				Id: uuid.New(),
				AGrantId: grant.Id,
				BGrantId: gcc.Id,
			}
			conflictList = append(conflictList, gcon)
		}
		for _, gcc := range solution.HigherOrEqualPrioConflicts {
			log.Debug().Msg(fmt.Sprintf("higherOrEqualPrioConflict(%s, %s)", grant.Id, gcc.Id))
			gcon := store.GrantConflict{
				Id: uuid.New(),
				AGrantId: grant.Id,
				BGrantId: gcc.Id,
			}
			conflictList = append(conflictList, gcon)
		}
		if len(conflictList) > 0 {
			if res := gc.db.Create(&conflictList); res.Error != nil {
				m := "error saving GrantConflict list: " + res.Error.Error()
				log.Error().Err(err).Msg(m)
				return errors.New(m)
			}
		}

		// NB: do save associations recursively; scheduler may have updated
		// the constraints in some scheduling cases.
		gc.db.Session(&gorm.Session{FullSaveAssociations: true}).Updates(grant)

		if err := gc.ChangeGrantStatus(grant, store.StatusApproved, approvalMsg, &userId, nil); err != nil {
			m := fmt.Sprintf("error changing grant status to approved: %s", err.Error())
			log.Error().Err(err).Msg(m)
			return errors.New(m)
		}

		// NB: activate the grant if we're already past the proposed
		// StartsAt time.
		//
		// XXX: this should be handled by the grant controller, as a side
		// effect of the ChangeGrantStatus(approved) just above, but that
		// case is not currently covered, and easy enough to do it here.
		//
		// NB: we are skipping the heartbeat protocol in the Approve state
		// deliberately, and allowing the grant to transition immediately to
		// Active, and getting ack'd there.
		at := time.Now().UTC()
		if grant.StartsAt.Before(at) {
			// Figure out what state this grant starts in: if not conflicting
			// with higher-prio grant, it can go active; else, pending.
			nextStatus := store.StatusActive
			if len(solution.HigherOrEqualPrioConflicts) > 0 {
				nextStatus = store.StatusPending
			}

			if err := gc.ChangeGrantStatus(grant, nextStatus, "", &userId, nil); err != nil {
				m := fmt.Sprintf("error changing grant status to %s: %s", string(nextStatus), err.Error())
				log.Error().Err(err).Msg(m)
				return errors.New(m)
			}
		} else {
			log.Debug().Msg(fmt.Sprintf("not auto-activating future grant %s", grant.Id))
		}
	}

	return nil
}

// Replaces one grant (revoking and deleting it) with another.
//
// NB: for now, we only support the narrowest of changes, a reduction in
// Constraint.MaxEirp.
func (gc *GrantController) ReplaceGrant(grant *store.Grant, newGrant *store.Grant, replaceMsg string, constraintChangeId *uuid.UUID, observationId *uuid.UUID) (err error) {
	// XXX: verify that there have been no changes except to Constraints,
	// and then only to reduce MaxEirp.

	// Save the replacement log
	grep := store.GrantReplacement{
		Id:                 uuid.New(),
		GrantId:            grant.Id,
		NewGrantId:         newGrant.Id,
		Description:        replaceMsg,
		CreatedAt:          time.Now().UTC(),
		ConstraintChangeId: constraintChangeId,
		ObservationId:      observationId,
	}
	grant.Replacement = &grep
	if res := gc.db.Create(&grep); res.Error != nil {
		m := fmt.Sprintf("error saving new GrantReplacement: %s", res.Error.Error())
		log.Error().Err(err).Msg(m)
		return res.Error
	}

	// Replace the old grant
	oldGrantStatus := grant.Status
	replaceMsg = fmt.Sprintf("replacing with grant %s (%s)", newGrant.Id, replaceMsg)
	if err := gc.ChangeGrantStatus(grant, store.StatusReplacing, replaceMsg, nil, nil); err != nil {
		m := fmt.Sprintf("error changing grant status to replacing: %s", err.Error())
		log.Error().Err(err).Msg(m)
	}

	// Schedule the new grant
	ignoreGrantIdList := []uuid.UUID{grant.Id}
	replaceMsg = fmt.Sprintf("replaces grant %s", grant.Id)
	if err := gc.Schedule(newGrant, newGrant.CreatorId, ignoreGrantIdList, replaceMsg); err != nil || newGrant.ApprovedAt == nil {
		var m string
		if newGrant.ApprovedAt == nil {
			m = fmt.Sprintf("cannot schedule replacement grant: no solution")
			log.Info().Err(err).Msg(m)
		} else {
			m = fmt.Sprintf("error scheduling replacement grant: %s", err.Error())
			log.Error().Err(err).Msg(m)
		}

		// Change the old grant status back
		if errc := gc.ChangeGrantStatus(grant, oldGrantStatus, m, nil, nil); errc != nil {
			m := fmt.Sprintf("error reverting old grant status: %s", errc.Error())
			log.Error().Err(errc).Msg(m)
		}

		// Delete the replacement.
		grant.Replacement = nil
		if res := gc.db.Delete(&grep); res.Error != nil {
			m := fmt.Sprintf("error deleting GrantReplacement after failed replace: %s", res.Error.Error())
			log.Error().Err(err).Msg(m)
			return res.Error
		}

		return err
	}

	// Revoke the old grant
	if err := gc.ChangeGrantStatus(grant, store.StatusRevoked, "revoked during replace", nil, nil); err != nil {
		m := fmt.Sprintf("error changing grant status to revoked: %s", err.Error())
		log.Error().Err(err).Msg(m)
	}

	// Reload to ensure we have proper state across relationships.
	rq := gc.db.
		Preload("Constraints.Constraint").
		Preload("RadioPorts.RadioPort").
		Preload("IntConstraints.IntConstraint").
		Preload("RtIntConstraints.RtIntConstraint")
	if res := rq.First(&newGrant, newGrant.Id); res.Error != nil {
		return res.Error
	}

	log.Debug().Msg(fmt.Sprintf("replacement grant: %+v", newGrant))

	return nil
}

// Only the server can change grant.Status.  This operates that state
// machine.  Basically, we're handling a bunch of policy (e.g. is grant
// element ack required), and potential displacement of lower-prio grants.
func (gc *GrantController) ChangeGrantStatus(grant *store.Grant, newStatus store.GrantStatus, msg string, updaterId *uuid.UUID, updatedAt *time.Time) error {
	// Grab the grant's conflict list first.
	var conflictList []*store.Grant
	var grantConflictList []store.GrantConflict
	if cl, gcl, err := gc.LoadConflicts(grant.Id); err != nil {
		return err
	} else {
		conflictList = cl
		grantConflictList = gcl
	}

	t := time.Now().UTC()
	if updatedAt == nil {
		updatedAt = &t
	}
	ackInterval := gc.config.GetGrantAcksIntervalDuration()
	at := t.Add(ackInterval)

	// Update the grant.
	grant.Status = newStatus
	// NB: we *always* want a status change given time to be acked, even if
	// the grant has expired.
	if false && grant.ExpiresAt != nil && at.After(*grant.ExpiresAt) {
		grant.StatusAckBy = grant.ExpiresAt
	} else if newStatus != store.StatusCreated {
		grant.StatusAckBy = &at
	}
	if updaterId != nil {
		grant.UpdaterId = updaterId
	}

	// Update specific timestamp status fields for the grant.
	if newStatus == store.StatusCreated {
		grant.CreatedAt = *updatedAt
	} else if newStatus == store.StatusApproved {
		grant.ApprovedAt = updatedAt
	} else if newStatus == store.StatusDenied {
		grant.DeniedAt = updatedAt
	} else if newStatus == store.StatusRevoked {
		grant.RevokedAt = updatedAt
	} else if newStatus == store.StatusDeleted {
		grant.DeletedAt = updatedAt
	}

	autoDeleted := false
	var autoDeletedClaim *store.Claim
	// NB: we do *not* change grant.Status to StatusDeleted; we leave it in
	// the other terminal state deliberately.
	if !gc.config.GrantManualDelete && slices.Contains(store.TerminalGrantStatusList, newStatus) && newStatus != store.StatusDeleted {
		m := fmt.Sprintf("auto-deleting terminal grant %s", grant.Id)
		log.Debug().Msg(m)
		grant.DeletedAt = updatedAt
		autoDeleted = true

		// If this was a claim, also delete the claim.
		if grant.IsClaim {
			var claim store.Claim
			gcq := gc.db.Where("grant_id = ?", grant.Id).
				Preload("Grant.Constraints.Constraint").
				Preload("Grant.IntConstraints.IntConstraint").
				Preload("Grant.RtIntConstraints.RtIntConstraint").
				Preload("Grant.RadioPorts.RadioPort").
				Preload("Grant.Logs").
				Preload("Grant.Replacement").
				Preload("Grant.Replaces")
			if gcres := gcq.First(&claim); gcres.Error != nil {
				emsg := fmt.Sprintf("error auto-deleting terminal grant %s claim: no matching claim (%s)", grant.Id, gcres.Error.Error())
				log.Error().Err(gcres.Error).Msg(emsg)
			} else if claim.DeletedAt != nil {
				// Watch for already-deleted claims, e.g. from direct
				// DeleteClaim.
				claim.DeletedAt = updatedAt
				if gcures := gc.db.Save(claim); gcures.Error != nil {
					emsg := fmt.Sprintf("error auto-deleting terminal grant %s claim: %s", grant.Id, gcures.Error.Error())
					log.Error().Err(gcures.Error).Msg(emsg)
				} else {
					autoDeletedClaim = &claim
				}
			}
		}
	}

	// Add a log entry; nonfatal.
	if err := gc.AddLog(grant, msg, updatedAt); err != nil {
		log.Error().Err(err).Msg("error logging grant status change: " + err.Error())
	}

	// Save changes to the grant.
	if res := gc.db.Save(grant); res.Error != nil {
		return res.Error
	}

	// Set up a watch condition to ensure OpStatus changes.  Add a handler
	// for when it does.  Default handler notifies lower-prio grants to
	// either pause or go active, and waits for them to do so.  For now we
	// are not yet requiring confirmation.
	if newStatus == store.StatusApproved {
		gc.WatchGrant(grant, newStatus, []store.GrantOpStatus{store.OpStatusAccepted}, true, true, grant.StatusAckBy)
	} else if newStatus == store.StatusActive {
		gc.WatchGrant(grant, newStatus, []store.GrantOpStatus{store.OpStatusActive, store.OpStatusPaused}, true, true, grant.StatusAckBy)
	} else if newStatus == store.StatusRevoked || newStatus == store.StatusPaused {
		gc.WatchGrant(grant, newStatus, []store.GrantOpStatus{store.OpStatusPaused}, true, true, grant.StatusAckBy)
	}

	// Notify watcher
	go gc.Notify()

	eventType := GrantStatusToEventType(newStatus)

	// NB: for now, send main grant event and any auto-deletion events in
	// batch mode, to ensure in-order delivery.
	eventList := make([]*subscription.Event, 0, 1)

	// Send a grant event.
	oid := grant.Id.String()
	userIdStr := grant.CreatorId.String()
	elementIdStr := grant.ElementId.String()
	eh := subscription.EventHeader{
		Type:       eventType,
		Code:       int32(zmc_v1.EventCode_EC_GRANT),
		SourceType: int32(event_v1.EventSourceType_EST_ZMC),
		SourceId:   gc.config.ServiceId,
		Id:         uuid.New().String(),
		ObjectId:   &oid,
		Time:       updatedAt,
		UserId:     &userIdStr,
		ElementId:  &elementIdStr,
	}
	e := subscription.Event{
		Header: eh,
		Object: grant,
	}
	eventList = append(eventList, &e)

	if autoDeleted {
		// Send a grant deleted event.
		oid := grant.Id.String()
		userIdStr := grant.CreatorId.String()
		elementIdStr := grant.ElementId.String()
		eh := subscription.EventHeader{
			Type:       int32(event_v1.EventType_ET_DELETED),
			Code:       int32(zmc_v1.EventCode_EC_GRANT),
			SourceType: int32(event_v1.EventSourceType_EST_ZMC),
			SourceId:   gc.config.ServiceId,
			Id:         uuid.New().String(),
			ObjectId:   &oid,
			Time:       updatedAt,
			UserId:     &userIdStr,
			ElementId:  &elementIdStr,
		}
		e := subscription.Event{
			Header: eh,
			Object: grant,
		}
		eventList = append(eventList, &e)

		if autoDeletedClaim != nil {
			// Send a claim deleted event.
			oid := autoDeletedClaim.Id.String()
			userIdStr := autoDeletedClaim.CreatorId.String()
			elementIdStr := autoDeletedClaim.ElementId.String()
			eh := subscription.EventHeader{
				Type:       int32(event_v1.EventType_ET_DELETED),
				Code:       int32(zmc_v1.EventCode_EC_CLAIM),
				SourceType: int32(event_v1.EventSourceType_EST_ZMC),
				SourceId:   gc.config.ServiceId,
				Id:         uuid.New().String(),
				ObjectId:   &oid,
				Time:       updatedAt,
				UserId:     &userIdStr,
				ElementId:  &elementIdStr,
			}
			e := subscription.Event{
				Header: eh,
				Object: grant,
			}
			eventList = append(eventList, &e)
		}
	}

	// Notify of grant events.
	go gc.sm.NotifyMany(eventList)

	// Handle activation of conflicting, pending grants if the current grant
	// is going away.
	if newStatus == store.StatusRevoked || newStatus == store.StatusDeleted || newStatus == store.StatusPaused {
		if len(grantConflictList) > 0 {
			gc.db.Delete(grantConflictList)
		}
		if newStatus == store.StatusDeleted {
			// Stop watching deleted grant.
			m := fmt.Sprintf("grant %s deleted; unwatching", grant.Id)
			log.Debug().Msg("gc: " + m)
			gc.UnwatchGrant(grant.Id)
		}
		for _, cg := range conflictList {
			if cg.Status == store.StatusPending {
				m := "activating pending grant"
				log.Info().Msg(fmt.Sprintf("%s (exiting=%s, pending=%s)", m, grant.Id, cg.Id))
				// XXX: again skipping heartbeats
				// XXX: have to consider the target grant's conflict set
				// too, ensure that all higher-prio grants are not active.
				go gc.ChangeGrantStatus(cg, store.StatusActive, m, nil, nil)
			}
		}
	} else if newStatus == store.StatusReplacing {
		// NB: do not activate lower-prio grants.  If possible, this will
		// happen when the grant being replaced is auto-deleted after the
		// new grant is approved.
	} else if newStatus == store.StatusActive {
		for _, cg := range conflictList {
			if cg.Status == store.StatusActive {
				m := "pending active grant"
				log.Info().Msg(fmt.Sprintf("%s (activating=%s, pending=%s)", m, grant.Id, cg.Id))
				// XXX: again skipping heartbeats
				// XXX: have to consider the target grant's conflict set
				// too, ensure that all higher-prio grants are not active.
				go gc.ChangeGrantStatus(cg, store.StatusPending, m, nil, nil)
			}
		}
	}

	return nil
}

// Elements and admins can change this.  Basically, it implements the
// heartbeat protocol for grant recipients.  Note that callers are only
// allowed to update according to a finite state machine:
//
// * Status == (unknown) OpStatus == (submitted) (push to scheduler)
// * Status == (approved) OpStatus == (accepted) (Status->active at StartsAt)
// * Status == (active|paused) OpStatus == (active|paused) (Change Status if flips)
// * Status == (revoked|replacing|deleted) OpStatus == (paused) (unwatch)
//
// Other transitions result in errors.
//
// If heartbeats are required, and the StatusAckBy field is violated
// (heartbeat doesn't arrive in time), then grant is revoked.
func (gc *GrantController) ChangeGrantOpStatus(grant *store.Grant, newOpStatus store.GrantOpStatus, msg string, updaterId *uuid.UUID, updatedAt *time.Time) error {
	t := time.Now().UTC()
	oldStatus := grant.Status
	oldOpStatus := grant.OpStatus

	if grant.StatusAckBy != nil && grant.StatusAckBy.Before(t) {
		m := fmt.Sprintf("ChangeGrantOpStatus: late by %s (%s|%s->%s) (%s); ignoring", t.Sub(*grant.StatusAckBy), oldStatus, oldOpStatus, newOpStatus, grant.Id)
		log.Warn().Msg("gc: " + m)
		return errors.New(m)
	}

	ackInterval := gc.config.GetGrantAcksIntervalDuration()
	at := t.Add(ackInterval)
	if updatedAt == nil {
		updatedAt = &t
	}

	if grant.Status == store.StatusCreated {
		// If the grant is Created and the OpStatus is changing to
		// submitted, push to the scheduler.
		if newOpStatus == store.OpStatusSubmitted {
			m := fmt.Sprintf("ChangeGrantOpStatus: update %s|%s->%s (%s)", oldStatus, oldOpStatus, newOpStatus, grant.Id)
			log.Debug().Msg("gc: " + m)
			grant.OpStatus = newOpStatus
			grant.StatusAckBy = nil
			if updaterId != nil {
				grant.UpdaterId = updaterId
			}
			grant.OpStatusUpdatedAt = &t

			if err := gc.Schedule(grant, *updaterId, nil, ""); err != nil {
				m := fmt.Sprintf("error scheduling submitted grant: %s", err.Error())
				log.Error().Err(err).Msg(m)
				return fmt.Errorf("error scheduling submitted grant", err.Error())
			}
		} else {
			m := fmt.Sprintf("ChangeGrantOpStatus: invalid update, refusing %s|%s->%s (%s)", oldStatus, oldOpStatus, newOpStatus, grant.Id)
			log.Error().Msg("gc: " + m)
			return errors.New(m)
		}
	} else if grant.Status == store.StatusApproved {
		// If grant is Approved, the only valid response is to set Accepted;
		// else recipient can ignore or delete the grant.
		if newOpStatus == store.OpStatusAccepted {
			m := fmt.Sprintf("ChangeGrantOpStatus: update %s|%s->%s (%s)", oldStatus, oldOpStatus, newOpStatus, grant.Id)
			log.Debug().Msg("gc: " + m)
			grant.OpStatus = store.OpStatusAccepted
			grant.StatusAckBy = nil
			if updaterId != nil {
				grant.UpdaterId = updaterId
			}
			grant.OpStatusUpdatedAt = &t

			// NB: in this case, once accepted, further accepts are not
			// required (although permitted without error), and we push out
			// the nextTime to the start of the grant.
			gc.WatchGrant(grant, grant.Status, []store.GrantOpStatus{store.OpStatusAccepted}, false, true, &grant.StartsAt)
		} else {
			m := fmt.Sprintf("ChangeGrantOpStatus: invalid update, refusing %s|%s->%s (%s)", oldStatus, oldOpStatus, newOpStatus, grant.Id)
			log.Error().Msg("gc: " + m)
			return errors.New(m)
		}
	} else if grant.Status == store.StatusActive {
		// For active->active (heartbeat) transitions, we do not log nor notify.
		// We simple update the current watch, scheduling expectation of another
		// heartbeat.
		if newOpStatus == store.OpStatusActive {
			m := fmt.Sprintf("ChangeGrantOpStatus: update %s|%s->%s (%s)", oldStatus, oldOpStatus, newOpStatus, grant.Id)
			log.Debug().Msg("gc: " + m)
			grant.StatusAckBy = &at
			if updaterId != nil {
				grant.UpdaterId = updaterId
			}
			grant.OpStatus = store.OpStatusActive
			grant.OpStatusUpdatedAt = &t
		} else if newOpStatus == store.OpStatusPaused {
			grant.StatusAckBy = &at
			if updaterId != nil {
				grant.UpdaterId = updaterId
			}
			grant.OpStatus = newOpStatus
			grant.OpStatusUpdatedAt = &t

			m := fmt.Sprintf("ChangeGrantOpStatus: changed opstatus and status: %s->%s|%s->%s (%s)", grant.Status, store.StatusPaused, oldOpStatus, newOpStatus, grant.Id)
			log.Debug().Msg("gc: " + m)
			if err := gc.ChangeGrantStatus(grant, store.StatusPaused, m, nil, nil); err != nil {
				m := fmt.Sprintf("ChangeGrantOpStatus: failure changing opstatus and status: %s->%s|%s->%s (%s): %s", grant.Status, store.StatusPaused, oldOpStatus, newOpStatus, grant.Id, err.Error())
				log.Error().Err(err).Msg("gc: " + m)
				// Revert OpStatus change!
				grant.OpStatus = store.OpStatusActive
				return errors.New(m)
			}
		} else {
			m := fmt.Sprintf("ChangeGrantOpStatus: invalid update refusing %s|%s->%s (%s)", oldStatus, oldOpStatus, newOpStatus, grant.Id)
			log.Error().Msg("gc: " + m)
			return errors.New(m)
		}
		// Continue to watch the grant
		gc.WatchGrant(grant, grant.Status, []store.GrantOpStatus{store.OpStatusActive, store.OpStatusPaused}, true, true, nil)
	} else if grant.Status == store.StatusPaused {
		if newOpStatus == store.OpStatusPaused {
			m := fmt.Sprintf("ChangeGrantOpStatus: update %s|%s->%s (%s)", oldStatus, oldOpStatus, newOpStatus, grant.Id)
			log.Debug().Msg("gc: " + m)
			grant.StatusAckBy = &at
			if updaterId != nil {
				grant.UpdaterId = updaterId
			}
			grant.OpStatus = store.OpStatusPaused
			grant.OpStatusUpdatedAt = &t
		} else if newOpStatus == store.OpStatusActive {
			grant.StatusAckBy = &at
			if updaterId != nil {
				grant.UpdaterId = updaterId
			}
			grant.OpStatus = store.OpStatusActive
			grant.OpStatusUpdatedAt = &t

			m := fmt.Sprintf("ChangeGrantOpStatus: changing opstatus and status: %s->%s|%s->%s (%s)", grant.Status, store.StatusActive, oldOpStatus, newOpStatus, grant.Id)
			log.Debug().Msg("gc: " + m)
			if err := gc.ChangeGrantStatus(grant, store.StatusActive, m, nil, nil); err != nil {
				m := fmt.Sprintf("ChangeGrantOpStatus: failure changing opstatus and status: %s->%s|%s->%s (%s): %s", grant.Status, store.StatusActive, oldOpStatus, newOpStatus, grant.Id, err.Error())
				log.Error().Err(err).Msg("gc: " + m)
				// Revert OpStatus change!
				grant.OpStatus = store.OpStatusPaused
				return errors.New(m)
			}
		} else {
			m := fmt.Sprintf("ChangeGrantOpStatus: invalid update, refusing %s|%s->%s (%s)", oldStatus, oldOpStatus, newOpStatus, grant.Id)
			log.Error().Msg("gc: " + m)
			return errors.New(m)
		}
		// Continue to watch the grant
		gc.WatchGrant(grant, grant.Status, []store.GrantOpStatus{store.OpStatusActive, store.OpStatusPaused}, true, true, nil)
	} else if grant.Status == store.StatusRevoked || grant.Status == store.StatusReplacing || grant.Status == store.StatusDeleted {
		if newOpStatus == store.OpStatusPaused {
			m := fmt.Sprintf("ChangeGrantOpStatus: update %s|%s->%s (%s)", oldStatus, oldOpStatus, newOpStatus, grant.Id)
			log.Debug().Msg("gc: " + m)
			grant.StatusAckBy = nil
			if updaterId != nil {
				grant.UpdaterId = updaterId
			}
			grant.OpStatus = store.OpStatusPaused
			grant.OpStatusUpdatedAt = &t
		} else {
			m := fmt.Sprintf("ChangeGrantOpStatus: invalid, refusing %s|%s->%s (%s)", oldStatus, oldOpStatus, newOpStatus, grant.Id)
			log.Error().Msg(m)
			return errors.New(m)
		}
		// Stop watching the grant.
		gc.UnwatchGrant(grant.Id)
	} else {
		m := fmt.Sprintf("ChangeGrantOpStatus: invalid current status, refusing %s|%s->%s (%s)", oldStatus, oldOpStatus, newOpStatus, grant.Id)
		log.Error().Msg(m)
		return errors.New(m)
	}

	// Save changes.
	if res := gc.db.Omit("UpdatedAt").Save(grant); res.Error != nil {
		return res.Error
	}
	return nil
}
