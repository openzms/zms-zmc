package grant

import (
	"container/heap"
	"errors"
	"time"

	"github.com/google/uuid"

	"gitlab.flux.utah.edu/openzms/zms-zmc/pkg/store"
)

//type GrantWatchHandler (gw *GrantWatch) func (gc *GrantController, grant *store.Grant, oldStatus GrantStatus, oldOpStatus GrantOpStatus)

type GrantWatch struct {
	GrantId           uuid.UUID
	AckStatus         store.GrantStatus
	ValidOpStatusList []store.GrantOpStatus
	// Generally, are heartbeat acks required and enforced for this grant.
	AcksRequired bool
	// Is (another) heartbeat ack required prior to NextTime?  Setting this
	// false means that if another ack does not arrive prior to NextTime, we
	// do not revoke the grant.  This is useful if, for instance, a future
	// grant recipient has Accepted the grant; we do not require continuing
	// Accepted updates.
	NextAckRequired bool
	NextTime        time.Time
	LastAckTime     time.Time
	index           int
	missedAckCounter int
	//Grant             *store.Grant
	//AckOpStatus       *store.GrantOpStatus
	//Handler           GrantWatchHandler
}

// A simple PriorityQueue, right out of container/heap.
type GrantWatchQueue []*GrantWatch

func (q GrantWatchQueue) Len() int {
	return len(q)
}

func (q GrantWatchQueue) Less(i, j int) bool {
	// Pop must give us the least time.
	return q[i].NextTime.Before(q[j].NextTime)
}

func (q GrantWatchQueue) Swap(i, j int) {
	q[i], q[j] = q[j], q[i]
	q[i].index = i
	q[j].index = j
}

func (q *GrantWatchQueue) Push(x any) {
	n := len(*q)
	gw := x.(*GrantWatch)
	gw.index = n
	*q = append(*q, gw)
}

func (q *GrantWatchQueue) Pop() any {
	old := *q
	n := len(old)
	gw := old[n-1]
	// don't stop the GC from reclaiming the item eventually
	old[n-1] = nil
	gw.index = -1
	*q = old[0 : n-1]
	return gw
}

// update modifies the priority and value of an GrantWatch in the queue.
func (q *GrantWatchQueue) Update(gw *GrantWatch, nextTime time.Time) error {
	// Ensure that GrantWatch is not being handled elsewhere (e.g. by
	// GrantWatchQueue.Run).
	if gw.index < 0 {
		return errors.New("grant watch update in progress; ignoring")
	}
	gw.NextTime = nextTime
	heap.Fix(q, gw.index)
	return nil
}

func (q GrantWatchQueue) Peek() *GrantWatch {
	if len(q) < 1 {
		return nil
	} else {
		return q[0]
	}
}

func NewGrantWatchQueue(qlen, qcap int) *GrantWatchQueue {
	q := make(GrantWatchQueue, qlen, qcap)
	heap.Init(&q)
	return &q
}
