// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package config

import (
	"flag"
	"os"
	"strconv"
	"time"
)

type Config struct {
	Verbose             bool
	Debug               bool
	HttpEndpointListen  string
	HttpEndpoint        string
	HealthHttpEndpoint  string
	RpcEndpointListen   string
	RpcEndpoint         string
	ServiceId           string
	ServiceName         string
	DbDriver            string
	DbDsn               string
	DbGormMigrate       bool
	Bootstrap           bool
	ZoneName            string
	ZoneArea            string
	ZoneDescription     string
	IdentityRpcEndpoint string
	GrantAcksEnforced   bool
	GrantAcksGrace      int64
	GrantAcksAllowSkipDefault bool
	GrantAcksInterval   string
	GrantManualDelete   bool

	grantAcksIntervalDuration *time.Duration
}

func (c *Config) parseGrantAcksIntervalDuration() (*time.Duration, error) {
	if c.grantAcksIntervalDuration == nil {
		if d, err := time.ParseDuration(c.GrantAcksInterval); err != nil {
			return nil, err
		} else {
			c.grantAcksIntervalDuration = &d
		}
	}
	return c.grantAcksIntervalDuration, nil
}

func (c *Config) GetGrantAcksIntervalDuration() time.Duration {
	return *c.grantAcksIntervalDuration
}

func getEnvBool(name string, defValue bool) bool {
	if val, ok := os.LookupEnv(name); ok {
		if pval, err := strconv.ParseBool(val); err != nil {
			pval = false
		} else {
			return pval
		}
	}
	return defValue
}

func getEnvString(name string, defValue string) string {
	if val, ok := os.LookupEnv(name); ok {
		return val
	}
	return defValue
}

func getEnvInt64(name string, defValue int64) int64 {
	if val, ok := os.LookupEnv(name); ok {
		if pval, err := strconv.ParseInt(val, 10, 64); err != nil {
			pval = -1
		} else {
			return pval
		}
	}
	return defValue
}

func LoadConfig() (*Config, error) {
	var config Config
	flag.BoolVar(&config.Debug, "debug",
		getEnvBool("LOG_DEBUG", false), "Enable debug logging.")
	flag.BoolVar(&config.Verbose, "verbose",
		getEnvBool("LOG_VERBOSE", false), "Enable verbose logging.")

	flag.StringVar(&config.HttpEndpointListen, "http-endpoint-listen",
		getEnvString("HTTP_ENDPOINT_LISTEN", "0.0.0.0:8010"), "HTTP endpoint bind address")
	flag.StringVar(&config.HttpEndpoint, "http-endpoint",
		getEnvString("HTTP_ENDPOINT", "0.0.0.0:8010"), "HTTP endpoint address")
	flag.StringVar(&config.HealthHttpEndpoint, "health-http-endpoint",
		getEnvString("HEALTH_HTTP_ENDPOINT", "0.0.0.0:8011"), "HTTP health endpoint address")
	flag.StringVar(&config.RpcEndpointListen, "rpc-endpoint-listen",
		getEnvString("RPC_ENDPOINT_LISTEN", "0.0.0.0:8012"), "RPC endpoint bind address")
	flag.StringVar(&config.RpcEndpoint, "rpc-endpoint",
		getEnvString("RPC_ENDPOINT", "0.0.0.0:8012"), "RPC endpoint address")
	flag.StringVar(&config.ServiceId, "service-id",
		getEnvString("SERVICE_ID", "33453677-4573-4b4a-b006-8fd073220002"), "Service ID")
	flag.StringVar(&config.ServiceName, "service-name",
		getEnvString("SERVICE_NAME", "zmc"), "Service Name")
	flag.StringVar(&config.DbDriver, "db-driver",
		getEnvString("DB_DRIVER", "sqlite"), "Database driver (sqlite, postgres)")
	flag.StringVar(&config.DbDsn, "db-dsn",
		getEnvString("DB_DSN", "file::memory:?cache=shared"), "Database DSN")
	flag.BoolVar(&config.DbGormMigrate, "db-gorm-migrate",
		getEnvBool("DB_GORM_MIGRATE", false), "Enable/disable GORM automatic database migration.  WARNING: you should never do this.  Use Atlas migrations!")
	flag.BoolVar(&config.Bootstrap, "bootstrap",
		getEnvBool("BOOTSTRAP", false), "Enable/disable database bootstrap")
	flag.StringVar(&config.IdentityRpcEndpoint, "identity-rpc-endpoint",
		getEnvString("IDENTITY_RPC_ENDPOINT", "0.0.0.0:8002"), "Identity service RPC endpoint address")
	flag.StringVar(&config.ZoneName, "zone-name",
		getEnvString("ZONE_NAME", ""), "Radio Dynamic Zone Name")
	flag.StringVar(&config.ZoneArea, "zone-area",
		getEnvString("ZONE_AREA", ""), "Identity service RPC endpoint address")
	flag.StringVar(&config.ZoneDescription, "zone-description",
		getEnvString("ZONE_DESCRIPTION", ""), "Identity service RPC endpoint address")
	flag.BoolVar(&config.GrantAcksEnforced, "grant-acks-enforced",
		getEnvBool("GRANT_ACKS_ENFORCED", false), "Set if grant ack protocol should be enabled and enforcing by default.")
	flag.Int64Var(&config.GrantAcksGrace, "grant-acks-grace",
		getEnvInt64("GRANT_ACKS_GRACE", 0), "Set to the number of intervals of grace we will extend to grants with missed `active` acks.  This does not apply to critical state changes, like revoke, pause, replace.")
	flag.BoolVar(&config.GrantAcksAllowSkipDefault, "grant-acks-allow-skip-default",
		getEnvBool("GRANT_ACKS_ALLOW_SKIP_DEFAULT", false), "Set true if you want the Grants.AllowSkipAcks field to default to true, otherwise will default to null.  This allows an opt-in policy for grants, where if GrantAcksEnforced and if a grant sets Grant.AllowSkipAcks to false, then, acks will be enforced for that grant.")
	flag.StringVar(&config.GrantAcksInterval, "grant-acks-interval",
		getEnvString("GRANT_ACKS_INTERVAL", "15m"), "Default grant ack interval: default 15m (https://pkg.go.dev/time#ParseDuration).")
	flag.BoolVar(&config.GrantManualDelete, "grant-manual-delete",
		getEnvBool("GRANT_MANUAL_DELETE", false),
		"Do not automatically delete terminal grants (denied, revoked) if this option is set true; if unset or false, grants will be deleted immediately after being revoked or denied.")

	flag.Parse()
	if _, err := config.parseGrantAcksIntervalDuration(); err != nil {
		return nil, err
	}

	return &config, nil
}
