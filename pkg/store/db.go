// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package store

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/google/uuid"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"

	"gitlab.flux.utah.edu/openzms/zms-zmc/pkg/config"
)

func GetDatabase(serverConfig *config.Config, dbConfig *gorm.Config) (db *gorm.DB, err error) {
	var localConfig gorm.Config
	if dbConfig != nil {
		localConfig = *dbConfig
	}
	localConfig.NowFunc = func() time.Time { return time.Now().UTC() }
	switch serverConfig.DbDriver {
	case "sqlite":
		db, err = gorm.Open(sqlite.Open(serverConfig.DbDsn), &localConfig)
	case "postgres":
		db, err = gorm.Open(postgres.Open(serverConfig.DbDsn), &localConfig)
	default:
		db, err = nil, errors.New("unsupported database driver")
	}
	return db, err
}

func InitDatabase(serverConfig *config.Config, db *gorm.DB) (err error) {
	if serverConfig.DbGormMigrate {
		err = MigrateDatabase(serverConfig, db)
	}
	return err
}

func MigrateDatabase(serverConfig *config.Config, db *gorm.DB) error {
	if db.Dialector.Name() == "postgres" {
		if err := CreateGrantStatusEnumPostgres(db); err != nil {
			return err
		}
		if err := CreateGrantOpStatusEnumPostgres(db); err != nil {
			return err
		}
		if err := CreateComparatorEnumPostgres(db); err != nil {
			return err
		}
	}
	return db.AutoMigrate(
		AllTables...
	)
}

func PostRegisterBootstrap(serverConfig *config.Config, db *gorm.DB, bootstrapElementId *uuid.UUID, bootstrapUserId *uuid.UUID) (*Zone, error) {
	if serverConfig.Bootstrap && bootstrapElementId != nil && bootstrapUserId != nil &&
		serverConfig.ZoneName != "" && serverConfig.ZoneArea != "" {
		area := &Area{
			Id: uuid.New(), ElementId: *bootstrapElementId, Name: serverConfig.ZoneName,
			Description: serverConfig.ZoneDescription,
		}
		spla := strings.Split(serverConfig.ZoneArea, ";")
		for _, sp := range spla {
			spa := strings.Split(sp, ",")
			if len(spa) != 2 {
				return nil, fmt.Errorf("malformed zone area substring (%s) in (%s)", sp, serverConfig.ZoneArea)
			}
			var err error
			var lat float64
			var long float64
			if lat, err = strconv.ParseFloat(spa[0], 64); err != nil {
				return nil, fmt.Errorf("malformed zone area substring (%s) in (%s)", sp, serverConfig.ZoneArea)
			}
			if long, err = strconv.ParseFloat(spa[1], 64); err != nil {
				return nil, fmt.Errorf("malformed zone area substring (%s) in (%s)", sp, serverConfig.ZoneArea)
			}
			area.Points = append(area.Points, AreaPoint{Id: uuid.New(), AreaId: area.Id, Y: lat, X: long})
		}
		if len(area.Points) < 4 {
			return nil, fmt.Errorf("zone area must include at least four points, defining a polygon area.  The start and end coordinates should be identical to close the shape.")
		}
		zone := &Zone{
			ElementId: *bootstrapElementId, Name: serverConfig.ZoneName,
			Description: serverConfig.ZoneDescription, Area: area,
			CreatorId: *bootstrapUserId,
		}
		res := db.FirstOrCreate(zone)
		if res.Error != nil {
			return nil, res.Error
		}
		return zone, nil
	}
	return nil, nil
}

func Paginate(page int, itemsPerPage int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		offset := (page - 1) * itemsPerPage
		return db.Offset(offset).Limit(itemsPerPage)
	}
}

func MakeILikeClause(config *config.Config, field string) string {
	if config.DbDriver == "postgres" {
		return field + " ilike ?"
	} else {
		return field + " like ?"
	}
}
