// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package store

import (
	"google.golang.org/protobuf/types/known/timestamppb"

	geo_v1 "gitlab.flux.utah.edu/openzms/zms-api/go/zms/geo/v1"
	zmc_v1 "gitlab.flux.utah.edu/openzms/zms-api/go/zms/zmc/v1"
)

// NB: caller must Preload any relations; elaborate parameter only controls
// output of them, to give caller more flexibility.

func (in *AreaPoint) ToProto(out *geo_v1.AreaPoint, elaborate bool) error {
	out.Id = in.Id.String()
	out.AreaId = in.AreaId.String()
	out.X = in.X
	out.Y = in.Y
	out.Z = &in.Z
	return nil
}

func (in *Area) ToProto(out *geo_v1.Area, elaborate bool) error {
	out.Srid = int32(in.Srid)
	for _, ap := range in.Points {
		var op geo_v1.AreaPoint
		ap.ToProto(&op, elaborate)
		out.Points = append(out.Points, &op)
	}
	return nil
}

func (in *Zone) ToProto(out *zmc_v1.Zone, elaborate bool) error {
	out.Id = in.Id.String()
	out.ElementId = in.ElementId.String()
	out.Name = in.Name
	out.Description = in.Description
	if in.AreaId != nil {
		s := in.AreaId.String()
		out.AreaId = &s
	}
	if elaborate && in.Area != nil {
		out.Area = new(geo_v1.Area)
		in.Area.ToProto(out.Area, elaborate)
	}
	out.CreatorId = in.CreatorId.String()
	if in.UpdaterId != nil {
		s := in.UpdaterId.String()
		out.UpdaterId = &s
	}
	out.CreatedAt = timestamppb.New(in.CreatedAt)
	if in.UpdatedAt != nil {
		out.UpdatedAt = timestamppb.New(*in.UpdatedAt)
	}
	if in.DeletedAt != nil {
		out.DeletedAt = timestamppb.New(*in.DeletedAt)
	}

	return nil
}

func (in *Location) ToProto(out *zmc_v1.Location, elaborate bool) error {
	out.Id = in.Id.String()
	out.ElementId = in.ElementId.String()
	out.Name = in.Name
	out.Srid = int32(in.Srid)
	out.X = in.X
	out.Y = in.Y
	out.Z = &in.Z
	out.CreatorId = in.CreatorId.String()
	if in.UpdaterId != nil {
		s := in.UpdaterId.String()
		out.UpdaterId = &s
	}
	out.CreatedAt = timestamppb.New(in.CreatedAt)
	if in.UpdatedAt != nil {
		out.UpdatedAt = timestamppb.New(*in.UpdatedAt)
	}
	if in.DeletedAt != nil {
		out.DeletedAt = timestamppb.New(*in.DeletedAt)
	}

	return nil
}

func (in *Antenna) ToProto(out *zmc_v1.Antenna, elaborate bool) error {
	out.Id = in.Id.String()
	out.ElementId = in.ElementId.String()
	out.Name = in.Name
	out.Description = in.Description
	out.Type = in.Type
	out.Vendor = in.Vendor
	out.Model = in.Model
	out.Url = in.Url
	out.Gain = in.Gain
	out.BeamWidth = int32(in.BeamWidth)
	out.GainProfileData = in.GainProfileData
	out.GainProfileFormat = &in.GainProfileFormat
	out.GainProfileUrl = &in.GainProfileUrl
	out.CreatorId = in.CreatorId.String()
	if in.UpdaterId != nil {
		s := in.UpdaterId.String()
		out.UpdaterId = &s
	}
	out.CreatedAt = timestamppb.New(in.CreatedAt)
	if in.UpdatedAt != nil {
		out.UpdatedAt = timestamppb.New(*in.UpdatedAt)
	}
	if in.DeletedAt != nil {
		out.DeletedAt = timestamppb.New(*in.DeletedAt)
	}

	return nil
}

func (in *RadioPort) ToProto(out *zmc_v1.RadioPort, elaborate bool) error {
	out.Id = in.Id.String()
	out.RadioId = in.RadioId.String()
	out.Name = in.Name
	out.Tx = in.Tx
	out.Rx = in.Rx
	out.MinFreq = in.MinFreq
	out.MaxPower = in.MaxPower
	if in.AntennaId != nil {
		s := in.AntennaId.String()
		out.AntennaId = &s
	}
	if in.AntennaLocationId != nil {
		s := in.AntennaLocationId.String()
		out.AntennaLocationId = &s
	}
	out.AntennaAzimuthAngle = in.AntennaAzimuthAngle
	out.AntennaElevationAngle = in.AntennaElevationAngle
	out.AntennaElevationDelta = in.AntennaElevationDelta
	out.Enabled = in.Enabled
	out.CreatorId = in.CreatorId.String()
	if in.UpdaterId != nil {
		s := in.UpdaterId.String()
		out.UpdaterId = &s
	}
	out.CreatedAt = timestamppb.New(in.CreatedAt)
	if in.UpdatedAt != nil {
		out.UpdatedAt = timestamppb.New(*in.UpdatedAt)
	}
	if in.DeletedAt != nil {
		out.DeletedAt = timestamppb.New(*in.DeletedAt)
	}
	if elaborate {
		if in.Antenna != nil {
			out.Antenna = new(zmc_v1.Antenna)
			in.Antenna.ToProto(out.Antenna, elaborate)
		}
		if in.AntennaLocation != nil {
			out.AntennaLocation = new(zmc_v1.Location)
			in.AntennaLocation.ToProto(out.AntennaLocation, elaborate)
		}
	}

	return nil
}

func (in *Radio) ToProto(out *zmc_v1.Radio, elaborate bool) error {
	out.Id = in.Id.String()
	out.ElementId = in.ElementId.String()
	out.Name = in.Name
	out.Description = in.Description
	out.FccId = in.FccId
	out.DeviceId = in.DeviceId
	out.SerialNumber = in.SerialNumber
	if in.LocationId != nil {
		s := in.LocationId.String()
		out.LocationId = &s
	}
	if in.Location != nil && elaborate {
		out.Location = new(zmc_v1.Location)
		in.Location.ToProto(out.Location, elaborate)
	}
	out.Enabled = in.Enabled
	out.CreatorId = in.CreatorId.String()
	if in.UpdaterId != nil {
		s := in.UpdaterId.String()
		out.UpdaterId = &s
	}
	out.CreatedAt = timestamppb.New(in.CreatedAt)
	if in.UpdatedAt != nil {
		out.UpdatedAt = timestamppb.New(*in.UpdatedAt)
	}
	if in.DeletedAt != nil {
		out.DeletedAt = timestamppb.New(*in.DeletedAt)
	}
	if elaborate && len(in.Ports) > 0 {
		for _, radioPort := range in.Ports {
			port := new(zmc_v1.RadioPort)
			radioPort.ToProto(port, elaborate)
			out.Ports = append(out.Ports, port)
		}
	}

	return nil
}

func (in *Monitor) ToProto(out *zmc_v1.Monitor, elaborate bool) error {
	out.Id = in.Id.String()
	out.RadioPortId = in.RadioPortId.String()
	if in.MonitoredRadioPort != nil {
		s := in.MonitoredRadioPortId.String()
		out.MonitoredRadioPortId = &s
	}
	out.Name = in.Name
	out.Description = in.Description
	out.Types = in.Types
	out.Formats = in.Formats
	out.Config = in.Config.String()
	out.Exclusive = in.Exclusive
	out.Enabled = in.Enabled
	out.CreatorId = in.CreatorId.String()
	if in.UpdaterId != nil {
		s := in.UpdaterId.String()
		out.UpdaterId = &s
	}
	out.CreatedAt = timestamppb.New(in.CreatedAt)
	if in.UpdatedAt != nil {
		out.UpdatedAt = timestamppb.New(*in.UpdatedAt)
	}
	if in.DeletedAt != nil {
		out.DeletedAt = timestamppb.New(*in.DeletedAt)
	}
	if elaborate {
		out.RadioPort = new(zmc_v1.RadioPort)
		in.RadioPort.ToProto(out.RadioPort, elaborate)
		if in.MonitoredRadioPort != nil {
			out.MonitoredRadioPort = new(zmc_v1.RadioPort)
			in.MonitoredRadioPort.ToProto(out.MonitoredRadioPort, elaborate)
		}
	}

	return nil
}

func (in *Constraint) ToProto(out *zmc_v1.Constraint, elaborate bool) error {
	out.Id = in.Id.String()
	out.MinFreq = in.MinFreq
	out.MaxFreq = in.MaxFreq
	out.Bandwidth = in.Bandwidth
	out.MaxEirp = in.MaxEirp
	out.MinEirp = in.MinEirp
	out.Exclusive = in.Exclusive
	if in.AreaId != nil {
		s := in.AreaId.String()
		out.AreaId = &s
	}
	if elaborate && in.Area != nil {
		out.Area = new(geo_v1.Area)
		in.Area.ToProto(out.Area, elaborate)
	}

	return nil
}

func (in *IntConstraint) ToProto(out *zmc_v1.IntConstraint, elaborate bool) error {
	out.Id = in.Id.String()
	out.Name = in.Name
	out.Description = in.Description
	out.MaxPower = in.MaxPower
	if in.AreaId != nil {
		s := in.AreaId.String()
		out.AreaId = &s
	}
	if elaborate && in.Area != nil {
		out.Area = new(geo_v1.Area)
		in.Area.ToProto(out.Area, elaborate)
	}

	return nil
}

func (in *RtIntConstraint) ToProto(out *zmc_v1.RtIntConstraint, elaborate bool) error {
	out.Id = in.Id.String()
	out.Name = in.Name
	out.Description = in.Description
	out.Metric = in.Metric
	out.Comparator = string(in.Comparator)
	out.Value = in.Value
	out.Aggregator = in.Aggregator
	if in.Period != nil {
		p := int32(*in.Period)
		out.Period = &p
	}
	if in.Criticality != nil {
		c := int32(*in.Criticality)
		out.Criticality = &c
	}

	return nil
}

func (in *GrantLog) ToProto(out *zmc_v1.GrantLog, elaborate bool) error {
	out.Id = in.Id.String()
	out.GrantId = in.GrantId.String()
	out.Status = string(in.Status)
	out.OpStatus = string(in.OpStatus)
	out.Message = in.Message
	out.CreatedAt = timestamppb.New(in.CreatedAt)

	return nil
}

func (in *GrantReplacement) ToProto(out *zmc_v1.GrantReplacement, elaborate bool) error {
	out.Id = in.Id.String()
	out.GrantId = in.GrantId.String()
	out.NewGrantId = in.NewGrantId.String()
	out.Description = in.Description
	out.CreatedAt = timestamppb.New(in.CreatedAt)
	if in.ConstraintChangeId != nil {
		s := in.ConstraintChangeId.String()
		out.ConstraintChangeId = &s
	}
	if in.ObservationId != nil {
		s := in.ObservationId.String()
		out.ObservationId = &s
	}

	return nil
}

func (in *Grant) ToProto(out *zmc_v1.Grant, elaborate bool) error {
	out.Id = in.Id.String()
	out.ElementId = in.ElementId.String()
	out.Name = in.Name
	out.Description = in.Description
	out.Priority = int32(in.Priority)
	if in.SpectrumId != nil {
		s := in.SpectrumId.String()
		out.SpectrumId = &s
	}
	out.StartsAt = timestamppb.New(in.StartsAt)
	if in.ExpiresAt != nil {
		out.ExpiresAt = timestamppb.New(*in.ExpiresAt)
	}
	out.Status = string(in.Status)
	if in.StatusAckBy != nil {
		out.StatusAckBy = timestamppb.New(*in.StatusAckBy)
	}
	out.OpStatus = string(in.OpStatus)
	if in.OpStatusUpdatedAt != nil {
		out.OpStatusUpdatedAt = timestamppb.New(*in.OpStatusUpdatedAt)
	}
	out.CreatorId = in.CreatorId.String()
	if in.UpdaterId != nil {
		s := in.UpdaterId.String()
		out.UpdaterId = &s
	}
	out.CreatedAt = timestamppb.New(in.CreatedAt)
	if in.UpdatedAt != nil {
		out.UpdatedAt = timestamppb.New(*in.UpdatedAt)
	}
	if in.ApprovedAt != nil {
		out.ApprovedAt = timestamppb.New(*in.ApprovedAt)
	}
	if in.OwnerApprovedAt != nil {
		out.OwnerApprovedAt = timestamppb.New(*in.OwnerApprovedAt)
	}
	if in.DeniedAt != nil {
		out.DeniedAt = timestamppb.New(*in.DeniedAt)
	}
	if in.RevokedAt != nil {
		out.RevokedAt = timestamppb.New(*in.RevokedAt)
	}
	if in.DeletedAt != nil {
		out.DeletedAt = timestamppb.New(*in.DeletedAt)
	}
	if elaborate {
		if in.Constraints != nil {
			for _, inC := range in.Constraints {
				outC := &zmc_v1.Constraint{}
				inC.Constraint.ToProto(outC, elaborate)
				out.Constraints = append(out.Constraints, outC)
			}
		}
		if in.IntConstraints != nil {
			for _, inC := range in.IntConstraints {
				outC := &zmc_v1.IntConstraint{}
				inC.IntConstraint.ToProto(outC, elaborate)
				out.IntConstraints = append(out.IntConstraints, outC)
			}
		}
		if in.RtIntConstraints != nil {
			for _, inC := range in.RtIntConstraints {
				outC := &zmc_v1.RtIntConstraint{}
				inC.RtIntConstraint.ToProto(outC, elaborate)
				out.RtIntConstraints = append(out.RtIntConstraints, outC)
			}
		}
		if in.RadioPorts != nil {
			out.RadioPorts = make([]*zmc_v1.RadioPort, 0, len(in.RadioPorts))
			for _, inRP := range in.RadioPorts {
				outRP := &zmc_v1.RadioPort{}
				inRP.RadioPort.ToProto(outRP, elaborate)
				out.RadioPorts = append(out.RadioPorts, outRP)
			}
		}
		if in.Logs != nil {
			for _, inLog := range in.Logs {
				outLog := &zmc_v1.GrantLog{}
				inLog.ToProto(outLog, false)
				out.Logs = append(out.Logs, outLog)
			}
		}
		if in.Replacement != nil {
			out.Replacement = &zmc_v1.GrantReplacement{}
			in.Replacement.ToProto(out.Replacement, false)
		}
	}

	return nil
}

func (in *Claim) ToProto(out *zmc_v1.Claim, elaborate bool) error {
	out.Id = in.Id.String()
	out.ElementId = in.ElementId.String()
	out.ExtId = in.ExtId
	out.Type = in.Type
	out.Source = in.Source
	out.HtmlUrl = in.HtmlUrl
	out.Name = in.Name
	out.Description = in.Description
	out.CreatorId = in.CreatorId.String()
	if in.UpdaterId != nil {
		s := in.UpdaterId.String()
		out.UpdaterId = &s
	}
	out.CreatedAt = timestamppb.New(in.CreatedAt)
	if in.UpdatedAt != nil {
		out.UpdatedAt = timestamppb.New(*in.UpdatedAt)
	}
	if in.VerifiedAt != nil {
		out.VerifiedAt = timestamppb.New(*in.VerifiedAt)
	}
	if in.DeniedAt != nil {
		out.DeniedAt = timestamppb.New(*in.DeniedAt)
	}
	if in.DeletedAt != nil {
		out.DeletedAt = timestamppb.New(*in.DeletedAt)
	}
	if in.GrantId != nil {
		s := in.GrantId.String()
		out.GrantId = &s
	}
	if elaborate {
		if in.Grant != nil {
			out.Grant = &zmc_v1.Grant{}
			in.Grant.ToProto(out.Grant, false)
		}
	}

	return nil
}

func (in *Policy) ToProto(out *zmc_v1.Policy, elaborate bool) error {
	out.Id = in.Id.String()
	out.SpectrumId = in.SpectrumId.String()
	if in.ElementId != nil {
		s := in.ElementId.String()
		out.ElementId = &s
	}
	out.Allowed = in.Allowed
	out.AutoApprove = in.AutoApprove
	out.Priority = int32(in.Priority)
	out.MaxDuration = int32(in.MaxDuration)
	out.WhenUnoccupied = in.WhenUnoccupied
	out.DisableEmitCheck = in.DisableEmitCheck
	out.AllowInactive = in.AllowInactive
	out.AllowConflicts = in.AllowConflicts

	return nil
}

func (in *Spectrum) ToProto(out *zmc_v1.Spectrum, elaborate bool) error {
	out.Id = in.Id.String()
	out.ElementId = in.ElementId.String()
	out.Name = in.Name
	out.Description = in.Description
	out.Url = in.Url
	out.Enabled = in.Enabled
	out.CreatorId = in.CreatorId.String()
	if in.UpdaterId != nil {
		s := in.UpdaterId.String()
		out.UpdaterId = &s
	}
	out.CreatedAt = timestamppb.New(in.CreatedAt)
	if in.UpdatedAt != nil {
		out.UpdatedAt = timestamppb.New(*in.UpdatedAt)
	}
	if in.ApprovedAt != nil {
		out.ApprovedAt = timestamppb.New(*in.ApprovedAt)
	}
	if in.DeniedAt != nil {
		out.DeniedAt = timestamppb.New(*in.DeniedAt)
	}
	out.StartsAt = timestamppb.New(in.StartsAt)
	if in.ExpiresAt != nil {
		out.ExpiresAt = timestamppb.New(*in.ExpiresAt)
	}
	if in.DeletedAt != nil {
		out.DeletedAt = timestamppb.New(*in.DeletedAt)
	}
	if elaborate {
		if in.Constraints != nil {
			for _, inC := range in.Constraints {
				outC := &zmc_v1.Constraint{}
				inC.Constraint.ToProto(outC, elaborate)
				out.Constraints = append(out.Constraints, outC)
			}
		}
		if in.Policies != nil {
			for _, inP := range in.Policies {
				outP := &zmc_v1.Policy{}
				inP.ToProto(outP, elaborate)
				out.Policies = append(out.Policies, outP)
			}
		}
	}

	return nil
}
