// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package store

import (
	"errors"
	"fmt"
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
	"gorm.io/datatypes"
	"gorm.io/gorm/schema"
)

type AreaPoint struct {
	Id     uuid.UUID `gorm:"primaryKey;type:uuid" json:"id"`
	AreaId uuid.UUID `gorm:"foreignKey:AreaId;type:uuid;not null" json:"area_id"`
	X      float64   `gorm:"not null" json:"x" binding:"required"`
	Y      float64   `gorm:"not null" json:"y" binding:"required"`
	Z      float64   `json:"z"`
}

func (x *AreaPoint) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type Area struct {
	Id          uuid.UUID   `gorm:"primaryKey;type:uuid" json:"id"`
	ElementId   uuid.UUID   `gorm:"not null;type:uuid;index" json:"element_id" binding:"required"`
	Name        string      `gorm:"size:256;index" json:"name" binding:"required"`
	Description string      `gorm:"size:1024" json:"description"`
	Srid        int         `gorm:"not null" json:"srid" binding:"required"`
	Points      []AreaPoint `json:"points"`
}

func (x *Area) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type Location struct {
	Id        uuid.UUID  `gorm:"primaryKey;type:uuid" json:"id"`
	ElementId uuid.UUID  `gorm:"not null;type:uuid;index" json:"element_id" binding:"required"`
	Name      string     `gorm:"size:256;index" json:"name" binding:"required"`
	Srid      int        `gorm:"not null" json:"srid" binding:"required"`
	X         float64    `gorm:"not null" json:"x" binding:"required"`
	Y         float64    `gorm:"not null" json:"y" binding:"required"`
	Z         float64    `json:"z"`
	CreatorId uuid.UUID  `gorm:"not null;type:uuid;index" json:"creator_id"`
	UpdaterId *uuid.UUID `gorm:"type:uuid" json:"updater_id"`
	CreatedAt time.Time  `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt *time.Time `gorm:"autoUpdateTime" json:"updated_at"`
	DeletedAt *time.Time `gorm:"index" json:"deleted_at"`
}

func (x *Location) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type Zone struct {
	Id          uuid.UUID  `gorm:"primaryKey;type:uuid" json:"id"`
	ElementId   uuid.UUID  `gorm:"not null;type:uuid" json:"element_id" binding:"required"`
	Name        string     `gorm:"not null;size:256;index" json:"name" binding:"required"`
	Description string     `gorm:"size:4096" json:"description" binding:"required"`
	AreaId      *uuid.UUID `gorm:"foreignKey:AreaId;type:uuid" json:"area_id"`
	Area        *Area      `json:"area"`
	CreatorId   uuid.UUID  `gorm:"not null;type:uuid;index" json:"creator_id"`
	UpdaterId   *uuid.UUID `gorm:"type:uuid" json:"updater_id"`
	CreatedAt   time.Time  `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt   *time.Time `gorm:"autoUpdateTime" json:"updated_at"`
	DeletedAt   *time.Time `gorm:"index" json:"deleted_at"`
}

func (x *Zone) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type Constraint struct {
	Id        uuid.UUID  `gorm:"primaryKey;type:uuid" json:"id"`
	MinFreq   int64      `gorm:"index" json:"min_freq" binding:"required"`
	MaxFreq   int64      `gorm:"index" json:"max_freq" binding:"required"`
	Bandwidth int64      `json:"bandwidth"`
	MaxEirp   float64    `gorm:"index" json:"max_eirp"`
	MinEirp   *float64   `json:"min_eirp"`
	Exclusive bool       `gorm:"not null;index" json:"exclusive"`
	AreaId    *uuid.UUID `gorm:"foreignKey:AreaId;type:uuid" json:"area_id"`
	Area      *Area      `json:"area"`
}

func (x *Constraint) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type IntConstraint struct {
	Id          uuid.UUID  `gorm:"primaryKey;type:uuid" json:"id"`
	Name        string     `gorm:"not null;size:256" json:"name" binding:"required"`
	Description *string    `gorm:"size:1024" json:"description"`
	MaxPower    *float64   `gorm:"index" json:"max_power"`
	AreaId      *uuid.UUID `gorm:"foreignKey:AreaId;type:uuid" json:"area_id"`
	Area        *Area      `json:"area"`
}

func (x *IntConstraint) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type Comparator string

const (
	CompNotEqual           Comparator = "ne"
	CompEqual              Comparator = "eq"
	CompGreaterThan        Comparator = "gt"
	CompLessThan           Comparator = "lt"
	CompGreaterThanOrEqual Comparator = "gte"
	CompLessThanOrEqual    Comparator = "lte"
)

// NB: ensure this matches the above enumerated string type.
const comparatorCreatePostgres string = "CREATE TYPE comparator_enum AS ENUM ('ne', 'eq', 'gt', 'lt', 'gte', 'lte')"

func (c *Comparator) Scan(value interface{}) error {
	s, ok := value.(string)
	if !ok {
		return errors.New("invalid Comparator value")
	}
	*c = Comparator(s)
	return nil
}

func (c Comparator) Value() (interface{}, error) {
	return string(c), nil
}

func (Comparator) GormDBDataType(db *gorm.DB, field *schema.Field) string {
	switch db.Dialector.Name() {
	case "sqlite":
		return "text"
	case "postgres":
		return "comparator_enum"
	}
	return ""
}

func CreateComparatorEnumPostgres(db *gorm.DB) (err error) {
	res := db.Debug().Exec(fmt.Sprintf(`
	DO $$ BEGIN
		%s
	EXCEPTION
		WHEN duplicate_object THEN null;
	END $$;`, comparatorCreatePostgres))
	if res.Error != nil {
		return res.Error
	}
	return nil
}

type RtIntConstraint struct {
	Id          uuid.UUID  `gorm:"primaryKey;type:uuid" json:"id"`
	Name        string     `gorm:"not null;size:256" json:"name" binding:"required"`
	Description *string    `gorm:"size:1024" json:"description"`
	Metric      string     `json:"metric" json:"name" binding:"required"`
	Comparator  Comparator `gorm:"not null;default:'gt'" json:"comparator"`
	Value       float64    `json:"value" binding:"required"`
	Aggregator  *string    `json:"aggregator"`
	Period      *int       `json:"period"`
	Criticality *int       `json:"criticality"`
}

func (x *RtIntConstraint) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type Spectrum struct {
	Id          uuid.UUID            `gorm:"primaryKey;type:uuid" json:"id"`
	ElementId   uuid.UUID            `gorm:"not null;type:uuid;index" json:"element_id" binding:"required"`
	Name        string               `gorm:"not null;size:256" json:"name" binding:"required"`
	Description string               `gorm:"size:4096" json:"description" binding:"required"`
	ExtId       *string              `gorm:"size:256" json:"ext_id"`
	Url         string               `gorm:"not null;size:512" json:"url" binding:"required"`
	Enabled     bool                 `gorm:"not null" json:"enabled"`
	CreatorId   uuid.UUID            `gorm:"not null;type:uuid;index" json:"creator_id"`
	UpdaterId   *uuid.UUID           `gorm:"type:uuid" json:"updater_id"`
	CreatedAt   time.Time            `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt   *time.Time           `gorm:"autoUpdateTime" json:"updated_at"`
	ApprovedAt  *time.Time           `json:"approved_at"`
	DeniedAt    *time.Time           `json:"denied_at"`
	StartsAt    time.Time            `json:"starts_at" binding:"required"`
	ExpiresAt   *time.Time           `json:"expires_at" binding:"omitempty,gtfield=StartsAt"`
	DeletedAt   *time.Time           `gorm:"index" json:"deleted_at"`
	Constraints []SpectrumConstraint `json:"constraints"`
	Policies    []Policy             `json:"policies"`
}

func (x *Spectrum) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type SpectrumConstraint struct {
	Id           uuid.UUID  `gorm:"primaryKey;type:uuid" json:"id"`
	SpectrumId   uuid.UUID  `gorm:"foreignKey:SpectrumId;type:uuid;not null" json:"spectrum_id"`
	ConstraintId uuid.UUID  `gorm:"foreignKey:ConstraintId;type:uuid;not null" json:"constraint_id"`
	Constraint   Constraint `json:"constraint"`
}

func (x *SpectrumConstraint) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type Policy struct {
	Id          uuid.UUID  `gorm:"primaryKey;type:uuid" json:"id"`
	SpectrumId  uuid.UUID  `gorm:"foreignKey:SpectrumId;type:uuid;not null;uniqueIndex:idx_policy_spectrum_id_element_id" json:"spectrum_id"`
	ElementId   *uuid.UUID `gorm:"type:uuid;uniqueIndex:idx_policy_spectrum_id_element_id" json:"element_id"`
	Allowed     bool       `gorm:"not null" json:"allowed" binding:"required"`
	AutoApprove bool       `gorm:"not null" json:"auto_approve"`
	Priority    int        `gorm:"not null" json:"priority" binding:"omitempty,min=-1023,max=1023"`
	MaxDuration int        `gorm:"not null" json:"max_duration"`
	WhenUnoccupied bool    `gorm:"not null" json:"when_unoccupied"`
	DisableEmitCheck bool  `gorm:"not null;default:false" json:"disable_emit_check"`
	AllowSkipAcks bool     `gorm:"not null;default:false" json:"allow_skip_acks"`
	AllowInactive bool     `gorm:"not null;default:false" json:"allow_inactive"`
	AllowConflicts bool    `gorm:"not null;default:false" json:"allow_conflicts"`
}

func (x *Policy) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type Antenna struct {
	Id                uuid.UUID  `gorm:"primaryKey;type:uuid" json:"id"`
	ElementId         uuid.UUID  `gorm:"type:uuid;not null;index" json:"element_id"`
	Name              string     `gorm:"not null;size:256" json:"name" binding:"required"`
	Description       string     `gorm:"size:4096" json:"description"`
	Type              string     `gorm:"size:256" json:"type"`
	Vendor            string     `gorm:"size:256" json:"vendor"`
	Model             string     `gorm:"size:256" json:"model"`
	Url               string     `gorm:"size:512" json:"url"`
	Gain              float32    `json:"gain"`
	BeamWidth         int        `json:"beam_width"`
	GainProfileData   []byte     `json:"gain_profile_data"`
	GainProfileFormat string     `gorm:"size:32" json:"gain_profile_format"`
	GainProfileUrl    string     `json:"gain_profile_url"`
	CreatorId         uuid.UUID  `gorm:"not null;type:uuid;index" json:"creator_id"`
	UpdaterId         *uuid.UUID `gorm:"type:uuid" json:"updater_id"`
	CreatedAt         time.Time  `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt         *time.Time `gorm:"autoUpdateTime" json:"updated_at"`
	DeletedAt         *time.Time `gorm:"index" json:"deleted_at"`
}

func (x *Antenna) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type Radio struct {
	Id           uuid.UUID   `gorm:"primaryKey;type:uuid" json:"id"`
	ElementId    uuid.UUID   `gorm:"not null;type:uuid;index" json:"element_id"`
	Name         string      `gorm:"not null;size:256" json:"name" binding:"required"`
	Description  string      `gorm:"size:4096" json:"description"`
	FccId        string      `gorm:"size:64" json:"fcc_id"`
	DeviceId     string      `gorm:"size:256" json:"device_id"`
	SerialNumber string      `gorm:"size:256" json:"serial_number"`
	HtmlUrl      string      `gorm:"size:512" json:"html_url"`
	LocationId   *uuid.UUID  `gorm:"foreignKey:LocationId;type:uuid" json:"location_id"`
	Location     *Location   `json:"location"`
	Enabled      bool        `gorm:"not null" json:"enabled"`
	CreatorId    uuid.UUID   `gorm:"not null;type:uuid;index" json:"creator_id"`
	UpdaterId    *uuid.UUID  `gorm:"type:uuid" json:"updater_id"`
	CreatedAt    time.Time   `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt    *time.Time  `gorm:"autoUpdateTime" json:"updated_at"`
	DeletedAt    *time.Time  `gorm:"index" json:"deleted_at"`
	Ports        []RadioPort `json:"ports"`
}

func (x *Radio) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type RadioPort struct {
	Id                    uuid.UUID  `gorm:"primaryKey;type:uuid" json:"id"`
	RadioId               uuid.UUID  `gorm:"foreignKey:RadioId;type:uuid;not null" json:"radio_id"`
	Name                  string     `gorm:"not null;size:256" json:"name" binding:"required"`
	DeviceId              *string    `gorm:"size:256" json:"device_id"`
	Tx                    bool       `gorm:"not null" json:"tx"`
	Rx                    bool       `gorm:"not null" json:"rx"`
	MinFreq               int64      `gorm:"not null" json:"min_freq"`
	MaxFreq               int64      `gorm:"not null" json:"max_freq" binding:"required"`
	MaxPower              float64    `gorm:"not null" json:"max_power"`
	AntennaId             *uuid.UUID `gorm:"foreignKey:AntennaId;type:uuid" json:"antenna_id"`
	AntennaLocationId     *uuid.UUID `gorm:"foreignKey:LocationId;type:uuid" json:"antenna_location_id"`
	AntennaAzimuthAngle   *float32   `json:"antenna_azimuth_angle"`
	AntennaElevationAngle *float32   `json:"antenna_elevation_angle"`
	AntennaElevationDelta *float32   `json:"antenna_elevation_delta"`
	Enabled               bool       `gorm:"not null" json:"enabled"`
	CreatorId             uuid.UUID  `gorm:"not null;type:uuid;index" json:"creator_id"`
	UpdaterId             *uuid.UUID `gorm:"type:uuid" json:"updater_id"`
	CreatedAt             time.Time  `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt             *time.Time `gorm:"autoUpdateTime" json:"updated_at"`
	DeletedAt             *time.Time `gorm:"index" json:"deleted_at"`
	Antenna               *Antenna   `json:"antenna"`
	AntennaLocation       *Location  `json:"antenna_location"`
}

func (x *RadioPort) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type Monitor struct {
	Id          uuid.UUID  `gorm:"primaryKey;type:uuid" json:"id"`
	RadioPortId uuid.UUID  `gorm:"joinForeignKey:RadioPortId;type:uuid;not null" json:"radio_port_id" binding:"required"`
	MonitoredRadioPortId *uuid.UUID `gorm:"joinForeignKey:RadioPortId;type:uuid" json:"monitored_radio_port_id"`
	ElementId   *uuid.UUID `gorm:"type:uuid" json:"element_id"`
	Name        string     `gorm:"not null;size:256" json:"name" binding:"required"`
	Description string     `gorm:"size:1024" json:"description"`
	DeviceId    *string    `gorm:"size:256" json:"device_id"`
	HtmlUrl     *string    `gorm:"size:512" json:"html_url"`
	Types       string     `gorm:"not null;size:512" json:"types" binding:"required"`
	Formats     string     `gorm:"not null;size:512" json:"formats" binding:"required"`
	Config      datatypes.JSON `json:"config"`
	Exclusive   bool       `gorm:"not null" json:"exclusive"`
	Enabled     bool       `gorm:"not null" json:"enabled"`
	CreatorId   uuid.UUID  `gorm:"not null;type:uuid;index" json:"creator_id"`
	UpdaterId   *uuid.UUID `gorm:"type:uuid" json:"updater_id"`
	CreatedAt   time.Time  `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt   *time.Time `gorm:"autoUpdateTime" json:"updated_at"`
	DeletedAt   *time.Time `gorm:"index" json:"deleted_at"`
	RadioPort   RadioPort  `gorm:"foreignKey:RadioPortId" json:"radio_port" binding:"-"`
	MonitoredRadioPort *RadioPort `gorm:"foreignKey:MonitoredRadioPortId" json:"monitored_radio_port" binding:"-"`
}

func (x *Monitor) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type GrantStatus string

const (
	StatusUnknown    GrantStatus = "unknown"
	StatusCreated    GrantStatus = "created"
	StatusScheduling GrantStatus = "scheduling"
	StatusApproving  GrantStatus = "approving"
	StatusApproved   GrantStatus = "approved"
	StatusPending    GrantStatus = "pending"
	StatusActive     GrantStatus = "active"
	StatusPaused     GrantStatus = "paused"
	StatusReplacing  GrantStatus = "replacing"
	StatusDenied     GrantStatus = "denied"
	StatusRevoked    GrantStatus = "revoked"
	StatusDeleted    GrantStatus = "deleted"
)

var AnyOperationalGrantStatusList = []GrantStatus{
	StatusScheduling,
	StatusApproving,
	StatusApproved,
	StatusPending,
	StatusActive,
	StatusPaused,
	StatusReplacing,
}

var RunningGrantStatusList = []GrantStatus{
	StatusPending,
	StatusActive,
	StatusPaused,
	StatusReplacing,
}

var TerminalGrantStatusList = []GrantStatus{
	StatusDenied,
	StatusRevoked,
	StatusDeleted,
}

// NB: ensure this matches the above enumerated string type.
const grantStatusCreatePostgres string = "CREATE TYPE grant_status_enum AS ENUM ('unknown', 'created', 'scheduling', 'approving', 'approved', 'active', 'pending', 'paused', 'replacing', 'denied', 'revoked', 'deleted')"

func (gs *GrantStatus) Scan(value interface{}) error {
	s, ok := value.(string)
	if !ok {
		return errors.New("invalid GrantStatus value")
	}
	*gs = GrantStatus(s)
	return nil
}

func (gs GrantStatus) Value() (interface{}, error) {
	return string(gs), nil
}

func (GrantStatus) GormDBDataType(db *gorm.DB, field *schema.Field) string {
	switch db.Dialector.Name() {
	case "sqlite":
		return "text"
	case "postgres":
		return "grant_status_enum"
	}
	return ""
}


func CreateGrantStatusEnumPostgres(db *gorm.DB) (err error) {
	res := db.Debug().Exec(fmt.Sprintf(`
	DO $$ BEGIN
		%s
	EXCEPTION
		WHEN duplicate_object THEN null;
	END $$;`, grantStatusCreatePostgres))
	if res.Error != nil {
		return res.Error
	}
	return nil
}

type GrantOpStatus string

const (
	OpStatusUnknown   GrantOpStatus = "unknown"
	OpStatusSubmitted GrantOpStatus = "submitted"
	OpStatusAccepted  GrantOpStatus = "accepted"
	OpStatusActive    GrantOpStatus = "active"
	OpStatusPaused    GrantOpStatus = "paused"
)
// NB: ensure this matches the above enumerated string type.
const grantOpStatusCreatePostgres string = "CREATE TYPE grant_op_status_enum AS ENUM ('unknown', 'submitted', 'accepted', 'active', 'paused');"

func (gos *GrantOpStatus) Scan(value interface{}) error {
	s, ok := value.(string)
	if !ok {
		return errors.New("invalid GrantOpStatus value")
	}
	*gos = GrantOpStatus(s)
	return nil
}

func (gos GrantOpStatus) Value() (interface{}, error) {
	return string(gos), nil
}

func (GrantOpStatus) GormDBDataType(db *gorm.DB, field *schema.Field) string {
	switch db.Dialector.Name() {
	case "sqlite":
		return "text"
	case "postgres":
		return "grant_op_status_enum"
	}
	return ""
}

func CreateGrantOpStatusEnumPostgres(db *gorm.DB) (err error) {
	res := db.Debug().Exec(fmt.Sprintf(`
	DO $$ BEGIN
		%s
	EXCEPTION
		WHEN duplicate_object THEN null;
	END $$;`, grantOpStatusCreatePostgres))
	if res.Error != nil {
		return res.Error
	}
	return nil
}

type Grant struct {
	Id              uuid.UUID         `gorm:"primaryKey;type:uuid" json:"id"`
	ElementId       uuid.UUID         `gorm:"not null;type:uuid;index" json:"element_id"`
	Name            string            `gorm:"not null;default:'';size:256" json:"name"`
	Description     string            `gorm:"not null;size:4096" json:"description"`
	ExtId           *string           `gorm:"size:256" json:"ext_id"`
	HtmlUrl         *string           `gorm:"size:512" json:"html_url"`
	Priority        int               `gorm:"not null;default:0" json:"priority" binding:"omitempty,min=-1023,max=1023"`
	IsClaim         bool              `gorm:"not null;default:false" json:"is_claim"`
	ClaimIdRef      *uuid.UUID        `gorm:"type:uuid" json:"claim_id"`
	SpectrumId      *uuid.UUID        `gorm:"foreignKey:SpectrumId;type:uuid;index" json:"spectrum_id"`
	AllowSkipAcks   *bool             `json:"allow_skip_acks"`
	AllowInactive   *bool             `json:"allow_inactive"`
	AllowConflicts  *bool             `json:"allow_conflicts"`
	StartsAt        time.Time         `gorm:"not null" json:"starts_at"`
	ExpiresAt       *time.Time        `json:"expires_at"`
	Status          GrantStatus       `gorm:"not null;default:'unknown';index" json:"status"`
	StatusAckBy     *time.Time        `json:"status_ack_by"`
	OpStatus        GrantOpStatus     `sql:"" gorm:"type:grant_op_status_enum;not null;default:'unknown';index" json:"op_status"`
	OpStatusUpdatedAt *time.Time      `json:"op_status_updated_at"`
	CreatorId       uuid.UUID         `gorm:"not null;type:uuid;index" json:"creator_id"`
	UpdaterId       *uuid.UUID        `gorm:"type:uuid" json:"updater_id"`
	CreatedAt       time.Time         `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt       *time.Time        `gorm:"autoUpdateTime" json:"updated_at"`
	ApprovedAt      *time.Time        `gorm:"index" json:"approved_at"`
	OwnerApprovedAt *time.Time        `json:"owner_approved_at"`
	DeniedAt        *time.Time        `json:"denied_at"`
	RevokedAt       *time.Time        `json:"revoked_at"`
	DeletedAt       *time.Time        `gorm:"index" json:"deleted_at"`
	Constraints     []GrantConstraint `json:"constraints"`
	IntConstraints  []GrantIntConstraint `json:"int_constraints"`
	RtIntConstraints []GrantRtIntConstraint `json:"rt_int_constraints"`
	RadioPorts      []GrantRadioPort  `json:"radio_ports"`
	Logs            []GrantLog        `json:"logs"`
	Replacement     *GrantReplacement `gorm:"foreignKey:GrantId" json:"replacement"`
	Replaces        *GrantReplacement `gorm:"-:migration;foreignKey:NewGrantId" json:"replaces"`
}

func (x *Grant) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

func (x *Grant) Clone(elaborate bool) *Grant {
	newGrant := *x
	newGrant.Id = uuid.New()
	newGrant.CreatedAt = time.Now().UTC()
	newGrant.UpdatedAt = nil
	newGrant.Constraints = make([]GrantConstraint, 0, len(x.Constraints))
	newGrant.IntConstraints = make([]GrantIntConstraint, 0, len(x.IntConstraints))
	newGrant.RtIntConstraints = make([]GrantRtIntConstraint, 0, len(x.RtIntConstraints))
	newGrant.RadioPorts = make([]GrantRadioPort, 0, len(x.RadioPorts))
	newGrant.Logs = make([]GrantLog, 0)
	newGrant.Replacement = nil

	for _, gc := range x.Constraints {
		ngc := GrantConstraint{
			Id:           uuid.New(),
			GrantId:      newGrant.Id,
			ConstraintId: uuid.New(),
			Constraint:   gc.Constraint,
		}
		ngc.Constraint.Id = ngc.ConstraintId
		newGrant.Constraints = append(newGrant.Constraints, ngc)
	}
	for _, grp := range x.RadioPorts {
		ngp := GrantRadioPort{
			Id:          uuid.New(),
			GrantId:     newGrant.Id,
			RadioPortId: grp.RadioPortId,
			RadioPort:   nil,
		}
		newGrant.RadioPorts = append(newGrant.RadioPorts, ngp)
	}
	for _, gc := range x.IntConstraints {
		ngc := GrantIntConstraint{
			Id:              uuid.New(),
			GrantId:         newGrant.Id,
			IntConstraintId: gc.IntConstraintId,
			IntConstraint:   gc.IntConstraint,
		}
		ngc.IntConstraint.Id = ngc.IntConstraintId
		newGrant.IntConstraints = append(newGrant.IntConstraints, ngc)
	}
	for _, gc := range x.RtIntConstraints {
		ngc := GrantRtIntConstraint{
			Id:                uuid.New(),
			GrantId:           newGrant.Id,
			RtIntConstraintId: gc.RtIntConstraintId,
			RtIntConstraint:   gc.RtIntConstraint,
		}
		ngc.RtIntConstraint.Id = ngc.RtIntConstraintId
		newGrant.RtIntConstraints = append(newGrant.RtIntConstraints, ngc)
	}

	return &newGrant
}

// Conflicting grants may not be active simultaneously.
type GrantConflict struct {
	Id        uuid.UUID `gorm:"primaryKey;type:uuid" json:"id"`
	AGrantId  uuid.UUID `gorm:"foreignKey:GrantId;type:uuid;not null" json:"a_grant_id"`
	BGrantId  uuid.UUID `gorm:"foreignKey:GrantId;type:uuid;not null" json:"b_grant_id"`

}

func (x *GrantConflict) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type GrantLog struct {
	Id        uuid.UUID     `gorm:"primaryKey;type:uuid" json:"id"`
	GrantId   uuid.UUID     `gorm:"foreignKey:GrantId;type:uuid;not null" json:"grant_id"`
	Status    GrantStatus   `gorm:"index;not null" json:"status"`
	OpStatus  GrantOpStatus `gorm:"index;not null" json:"op_status"`
	Message   string        `gorm:"not null;size:4096" json:"message"`
	CreatedAt time.Time     `gorm:"autoCreateTime" json:"created_at"`
}

func (x *GrantLog) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type GrantReplacement struct {
	Id                 uuid.UUID  `gorm:"primaryKey;type:uuid" json:"id"`
	GrantId            uuid.UUID  `gorm:"foreignKey:GrantId;type:uuid;not null;uniqueIndex:idx_grant_replacement_grant_id_new_grant_id" json:"grant_id"`
	NewGrantId         uuid.UUID  `gorm:"foreignKey:NewGrantId;type:uuid;not null;uniqueIndex:idx_grant_replacement_grant_id_new_grant_id" json:"new_grant_id"`
	Description        string     `gorm:"not null;size:4096" json:"description"`
	CreatedAt          time.Time  `gorm:"autoCreateTime" json:"created_at"`
	ConstraintChangeId *uuid.UUID `gorm:"type:uuid" json:"constraint_change_id"`
	ObservationId      *uuid.UUID `gorm:"type:uuid" json:"observation_id"`
}

func (x *GrantReplacement) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type GrantRadioPort struct {
	Id          uuid.UUID  `gorm:"primaryKey;type:uuid" json:"id"`
	GrantId     uuid.UUID  `gorm:"foreignKey:GrantId;type:uuid;not null" json:"grant_id"`
	RadioPortId uuid.UUID  `gorm:"foreignKey:RadioPortId;type:uuid;not null" json:"radio_port_id"`
	RadioPort   *RadioPort `json:"radio_port"`
}

func (x *GrantRadioPort) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type GrantConstraint struct {
	Id           uuid.UUID  `gorm:"primaryKey;type:uuid" json:"id"`
	GrantId      uuid.UUID  `gorm:"foreignKey:GrantId;type:uuid;not null" json:"grant_id"`
	ConstraintId uuid.UUID  `gorm:"foreignKey:ConstraintId;type:uuid;not null" json:"constraint_id"`
	Constraint   Constraint `json:"constraint"`
}

func (x *GrantConstraint) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type GrantIntConstraint struct {
	Id              uuid.UUID     `gorm:"primaryKey;type:uuid" json:"id"`
	GrantId         uuid.UUID     `gorm:"foreignKey:GrantId;type:uuid;not null" json:"grant_id"`
	IntConstraintId uuid.UUID     `gorm:"foreignKey:IntConstraintId;type:uuid;not null" json:"int_constraint_id"`
	IntConstraint   IntConstraint `json:"int_constraint"`
}

func (x *GrantIntConstraint) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type GrantRtIntConstraint struct {
	Id                uuid.UUID       `gorm:"primaryKey;type:uuid" json:"id"`
	GrantId           uuid.UUID       `gorm:"foreignKey:GrantId;type:uuid;not null" json:"grant_id"`
	RtIntConstraintId uuid.UUID       `gorm:"foreignKey:RtIntConstraintId;type:uuid;not null" json:"rt_int_constraint_id"`
	RtIntConstraint   RtIntConstraint `json:"rt_int_constraint"`
}

func (x *GrantRtIntConstraint) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type Claim struct {
	Id              uuid.UUID         `gorm:"primaryKey;type:uuid" json:"id"`
	ElementId       uuid.UUID         `gorm:"not null;type:uuid;index" json:"element_id" binding:"required"`
	ExtId           string            `gorm:"not null;size:256;index:idx_claim_ext_id" json:"ext_id" binding:"required"`
	Type            string            `gorm:"not null;size:512;index" json:"type" binding:"required"`
	Source          string            `gorm:"not null;size:512;index" json:"source" binding:"required"`
	HtmlUrl         *string           `gorm:"size:512;index" json:"html_url"`
	Name            string            `gorm:"not null;default:'';size:512;index" json:"name" binding:"required"`
	Description     *string           `gorm:"size:4096" json:"description"`
	CreatorId       uuid.UUID         `gorm:"not null;type:uuid;index" json:"creator_id"`
	UpdaterId       *uuid.UUID        `gorm:"type:uuid" json:"updater_id"`
	CreatedAt       time.Time         `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt       *time.Time        `gorm:"autoUpdateTime" json:"updated_at"`
	VerifiedAt      *time.Time        `json:"verified_at"`
	DeniedAt        *time.Time        `json:"denied_at"`
	DeletedAt       *time.Time        `gorm:"index" json:"deleted_at"`
	GrantId         *uuid.UUID        `gorm:"foreignKey:GrantId;type:uuid" json:"grant_id"`
	Grant           *Grant            `json:"grant"`
}

func (x *Claim) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

var CustomSqlPostgres []string = []string{
	grantStatusCreatePostgres,
	grantOpStatusCreatePostgres,
	comparatorCreatePostgres,
}

var AllTables []interface{} = []interface{}{
	&Zone{}, &Area{}, &AreaPoint{}, &Location{},
	&Constraint{}, &Spectrum{}, &SpectrumConstraint{}, &Policy{},
	&Antenna{}, &Radio{}, &RadioPort{},
	&Monitor{},
	&Grant{}, &GrantConstraint{}, &GrantRadioPort{},
	&GrantLog{}, &GrantConflict{}, &GrantReplacement{},
	&IntConstraint{}, &GrantIntConstraint{}, 
	&RtIntConstraint{}, &GrantRtIntConstraint{}, 
	&Claim{},
}
