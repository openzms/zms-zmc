// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package sched

import (
	"context"
	"errors"
	"fmt"
	"sort"
	"time"

	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
	"gorm.io/gorm"

	dst "gitlab.flux.utah.edu/openzms/zms-api/go/zms/dst/v1"
	geo "gitlab.flux.utah.edu/openzms/zms-api/go/zms/geo/v1"
	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/client"

	"gitlab.flux.utah.edu/openzms/zms-zmc/pkg/store"
)

func MaxInt64(x, y int64) int64 {
	if x > y {
		return x
	} else {
		return y
	}
}

func MinInt64(x, y int64) int64 {
	if x < y {
		return x
	} else {
		return y
	}
}

// Holds a single spectrum range-grant tuple.
type SpectrumRange struct {
	Start      int64
	End        int64
	Grant      *store.Grant
	IsConflict bool
}

// Intended to capture all "live" (approved/active/paused) grants within a
// given time period that occupy [Start,End) that *might* conflict with one
// another.
type SpectrumRangeAvail struct {
	Start    int64
	End      int64
	Grants   []*SpectrumRange
}

func (sra *SpectrumRangeAvail) HasHigherOrEqualPrioGrants(grantPriority int) bool {
	for _, g := range sra.Grants {
		if g.Grant != nil && g.IsConflict && g.Grant.Priority >= grantPriority {
			return true
		}
	}
	return false
}

func (sra *SpectrumRangeAvail) HasLowerPrioGrants(grantPriority int) bool {
	for _, g := range sra.Grants {
		if g.Grant != nil && g.IsConflict && g.Grant.Priority < grantPriority {
			return true
		}
	}
	return false
}

func (sra *SpectrumRangeAvail) GetHigherOrEqualPrioGrants(grantPriority int) (list []*store.Grant) {
	list = make([]*store.Grant, 0)
	for _, g := range sra.Grants {
		if g.Grant != nil && g.IsConflict && g.Grant.Priority >= grantPriority {
			list = append(list, g.Grant)
		}
	}
	return list
}

func (sra *SpectrumRangeAvail) GetLowerPrioGrants(grantPriority int) (list []*store.Grant) {
	list = make([]*store.Grant, 0)
	for _, g := range sra.Grants {
		if g.Grant != nil && g.IsConflict && g.Grant.Priority < grantPriority {
			list = append(list, g.Grant)
		}
	}
	return list
}

// Adds a new SpectrumRange to the availability map.  What we are doing is
// chopping up the entire [Start,End) range and maintaining a list of
// approved/active/paused Grants for each unique sub-range (unique meaning
// that the set of associated Grants changes at the sub-range Start).  We
// also fill "gaps" with SpectrumRangeAvail objects with .Grants = nil.
// This allows the scheduler to simply walk the list and look for possible
// range solutions.
//
// Basic approach:
// always make a new split at new.Start (unless sra.Start == new.Start)
// sort sra.Grants by least End -> sraGrantsByEnd
// for sraGrantsByEnd:
//   if g.End < new.Start (make a split at g.End)
//   else copy over g (whose .End is >= new.Start)
// set sra.End max to max contained .End or to new.Start
// set new split min End to 0?
// if sra.End < new.Start, add a gap avail between sra.End
func (sra *SpectrumRangeAvail) Add(new *SpectrumRange, grantPriority int) (closed []*SpectrumRangeAvail, newAvail *SpectrumRangeAvail) {
	// If we're simply adding a grant to the current start point, just add
	// the grant and possibly adjust the end of the current sra; we will
	// close it later.
	if new.Start == sra.Start {
		sra.Grants = append(sra.Grants, new)
		return nil, sra
	}

	// We are going to generate a newAvail from new.Start to sra.End and
	// return that.  However, we also need to fill in the range between
	// sra.Start and new.Start with SpectrumRangeAvail objects, one for each
	// r.End, as well as unused ones for non-overlapping start/end.
	closed = make([]*SpectrumRangeAvail, 0)

	// Close off the old ranges based off sra grants.i.End, for all
	// grants.i.End < new.Start.  Add a range with no grants between greatest
	// grants.i.End and new.Start .
	sraGrantsByEnd := make([]*SpectrumRange, len(sra.Grants))
	copy(sraGrantsByEnd, sra.Grants)
	sort.Slice(sraGrantsByEnd, func (i, j int) bool {
		return sraGrantsByEnd[i].End < sraGrantsByEnd[j].End
	})
	for _, r := range sraGrantsByEnd {
		if r.End <= new.Start {
			sra.End = r.End
			closed = append(closed, sra)
			next := &SpectrumRangeAvail{
				Start: r.End,
				End: 0,
				Grants: make([]*SpectrumRange, 0),
			}
			for _, r2 := range sra.Grants {
				if r2.End > next.Start {
					next.Grants = append(next.Grants, r2)
				}
			}
			sra = next
		}
	}
	if sra.Start < new.Start {
		// Add another gap with matching grants, if any:
		lastEnd := new.Start
		if len(sra.Grants) > 0 {
			for _, r := range sra.Grants {
				if r.End > lastEnd {
					lastEnd = r.End
				}
			}
		}
		lastEnd = MinInt64(lastEnd, new.Start)
		sra.End = lastEnd
		closed = append(closed, sra)
	}

	newRange := &SpectrumRangeAvail{
		Start: new.Start,
		End: 0,
		Grants: make([]*SpectrumRange, 0),
	}
	for _, r := range sra.Grants {
		if r.End >= new.Start {
			newRange.Grants = append(newRange.Grants, r)
		}
	}
	newRange.Grants = append(newRange.Grants, new)

	return closed, newRange
}

func (sra *SpectrumRangeAvail) Finish(maxFreq int64) (closed []*SpectrumRangeAvail, newAvail *SpectrumRangeAvail) {
	if len(sra.Grants) == 0 {
		sra.End = maxFreq
		return nil, sra
	}
	
	closed = make([]*SpectrumRangeAvail, 0)
	
	sraGrantsByEnd := make([]*SpectrumRange, len(sra.Grants))
	copy(sraGrantsByEnd, sra.Grants)
	sort.Slice(sraGrantsByEnd, func (i, j int) bool {
		return sraGrantsByEnd[i].End < sraGrantsByEnd[j].End
	})
	for _, r := range sraGrantsByEnd {
		if r.End < maxFreq {
			sra.End = r.End
			closed = append(closed, sra)
			next := &SpectrumRangeAvail{
				Start: r.End,
				End: 0,
				Grants: make([]*SpectrumRange, 0),
			}
			for _, r2 := range sra.Grants {
				if r2.End > next.Start {
					next.Grants = append(next.Grants, r2)
				}
			}
			sra = next
		}
	}

	sra.End = maxFreq
	return closed, sra
}

func CheckConflict(new *store.Grant, existing *store.Grant, existingPolicy *store.Policy, dstClient *dst.DstClient) bool {
	// XXX: ignoring Constraint.Area/Eirp and assuming Exclusive == true
	// (except for rx-only grants *and* tx grants in same element with
	// exclusive=false).
	//
	// We assume (due to the scheduler architecture) that we are only
	// considering time-overlapping grants as input.
	//
	// NB: current strategy is
	//   * check for frequency overlap in any of the constraints; if no
	//     overlap, no conflict;
	//   * check for tx port presence in both grants.  If one grant is
	//     rx-only and not Exclusive, it does not conflict with the others.
	//   * all other grants conflict.
	existingRxOnly := true
	if len(existing.RadioPorts) > 0 {
		for _, grp := range existing.RadioPorts {
			if grp.RadioPort == nil {
				m := fmt.Sprintf("existing grant (%s) RadioPort nil; cannot assume RxOnly", existing.Id)
				log.Debug().Msg(m)
				existingRxOnly = false
				//break
			} else if grp.RadioPort.Tx {
				m := fmt.Sprintf("existing grant (%s) RadioPort (%s) is Tx; cannot assume RxOnly", existing.Id, grp.RadioPort.Id)
				log.Debug().Msg(m)

				existingRxOnly = false
				//break
			}
		}
	} else {
		m := fmt.Sprintf("existing grant (%s) has no RadioPorts; cannot assume RxOnly", existing.Id)
		log.Debug().Msg(m)
		existingRxOnly = false
	}
	existingNonExclusive := true
	for _, grc := range existing.Constraints {
		if grc.Constraint.Exclusive {
			existingNonExclusive = false
			break
		}
	}
	newRxOnly := true
	if len(new.RadioPorts) > 0 {
		for _, grp := range new.RadioPorts {
			if grp.RadioPort == nil {
				m := fmt.Sprintf("new grant (%s) RadioPort nil; cannot assume RxOnly", new.Id)
				log.Debug().Msg(m)
				newRxOnly = false
				//break
			} else if grp.RadioPort.Tx {
				m := fmt.Sprintf("new grant (%s) RadioPort (%s) is Tx; cannot assume RxOnly", new.Id, grp.RadioPort.Id)
				log.Debug().Msg(m)

				newRxOnly = false
				//break
			}
		}
	} else {
		m := fmt.Sprintf("new grant (%s) has no RadioPorts; cannot assume RxOnly", new.Id)
		log.Debug().Msg(m)
		newRxOnly = false
	}
	newNonExclusive := true
	for _, grc := range new.Constraints {
		if grc.Constraint.Exclusive {
			newNonExclusive = false
			break
		}
	}

	if newNonExclusive && existingNonExclusive && new.ElementId == existing.ElementId && existingPolicy != nil && existingPolicy.AllowConflicts {
		m := fmt.Sprintf("new grant (%s) non-exclusive and existing grant (%s) is non-exclusive, same element %s, and policy.AllowConflicts=%t, no conflict with existing grant", new.Id, existing.Id, new.ElementId, existingPolicy.AllowConflicts)
		log.Debug().Msg(m)
		return false
	}

	// If one of the grants is rx-only and not exclusive, there is no
	// schedule-time conflict.  There may be a runtime interference
	// conflict.
	if newRxOnly && newNonExclusive {
		m := fmt.Sprintf("new grant (%s) is rx-only and non-exclusive, no conflict with existing grant (%s)", new.Id, existing.Id)
		log.Debug().Msg(m)
		return false
	} else if existingRxOnly && existingNonExclusive {
		m := fmt.Sprintf("existing grant (%s) is rx-only and non-exclusive, no conflict with new grant (%s)", existing.Id, new.Id)
		log.Debug().Msg(m)
		return false
	}

	// If there is no frequency overlap, there is trivially no conflict.
	freqOverlap := false
	for _, grcNew := range new.Constraints {
		newMinFreq := grcNew.Constraint.MinFreq
		newMaxFreq := grcNew.Constraint.MaxFreq
		for _, grcEx := range existing.Constraints {
			exMinFreq := grcEx.Constraint.MinFreq
			exMaxFreq := grcEx.Constraint.MaxFreq

			if (newMinFreq >= exMinFreq && newMinFreq < exMaxFreq) || (newMaxFreq >= exMinFreq && newMaxFreq < exMaxFreq) || (newMinFreq <= exMinFreq && newMaxFreq >= exMaxFreq) {
				freqOverlap = true
				break
			}
		}
		if freqOverlap {
			break
		}
	}
	if !freqOverlap {
		m := fmt.Sprintf("existing grant (%s) has no frequency overlap with new grant (%s), no conflict", existing.Id, new.Id)
		log.Debug().Msg(m)
		return false
	} else if !newNonExclusive {
		m := fmt.Sprintf("new exclusive grant (%s) has frequency overlap with existing grant (%s); trivial conflict", new.Id, existing.Id)
		log.Debug().Msg(m)
		return true
	} else if !existingNonExclusive {
		m := fmt.Sprintf("existing exclusive grant (%s) has frequency overlap with new grant (%s); trivial conflict", existing.Id, new.Id)
		log.Debug().Msg(m)
		return true
	} else if dstClient == nil {
		m := fmt.Sprintf("existing grant (%s) has frequency overlap with new grant (%s), but no dst available for geospatial check; assuming conflict", existing.Id, new.Id)
		log.Debug().Msg(m)
		m = fmt.Sprintf("conflict: existingRxOnly=%t, existingNonExclusive=%t, newRxOnly=%t, newNonExclusive=%t (new=%s, existing=%s)", existingRxOnly, existingNonExclusive, newRxOnly, newNonExclusive, new.Id, existing.Id)
		log.Debug().Msg(m)
		return true
	} else {
		m := fmt.Sprintf("existing grant (%s) has frequency overlap with new grant (%s), doing geospatial check", existing.Id, new.Id)
		log.Debug().Msg(m)

		// If we can't contact the DST and compare power/interference
		// constraints, or if there is an error, or if there is a conflict
		// -- all these cases require a conflict to be flagged.

		reqId := uuid.New().String()
		hdr := dst.RequestHeader{
			ReqId: &reqId,
		}
		req := dst.CheckGrantConflictRequest{
			Header:   &hdr,
			Grant1Id: existing.Id.String(),
			Grant2Id: new.Id.String(),
		}
		ctx, cancel := context.WithTimeout(context.Background(), 16*time.Second)
		defer cancel()
		if resp, err := (*dstClient).CheckGrantConflict(ctx, &req); err != nil {
			m := fmt.Sprintf("existing grant (%s) dst frequency overlap check with new (%s) failed, assuming conflict (%+v)", existing.Id, new.Id, err.Error())
			log.Info().Msg(m)
			m = fmt.Sprintf("conflict: existingRxOnly=%t, existingNonExclusive=%t, newRxOnly=%t, newNonExclusive=%t (new=%s, existing=%s)", existingRxOnly, existingNonExclusive, newRxOnly, newNonExclusive, new.Id, existing.Id)
			log.Debug().Msg(m)
			return true
		} else if resp.IsConflict {
			m := fmt.Sprintf("existing grant (%s) dst frequency overlap check with new (%s): is conflict", existing.Id, new.Id)
			log.Debug().Msg(m)
			m = fmt.Sprintf("conflict: existingRxOnly=%t, existingNonExclusive=%t, newRxOnly=%t, newNonExclusive=%t (new=%s, existing=%s)", existingRxOnly, existingNonExclusive, newRxOnly, newNonExclusive, new.Id, existing.Id)
			log.Debug().Msg(m)
			return true
		} else {
			// No geospatial conflict!
			m := fmt.Sprintf("existing grant (%s) frequency overlap with new grant (%s): geospatial check does not conflict", existing.Id, new.Id)
			log.Debug().Msg(m)
			return false
		}
	}

	// Finally, assume a conflict, although this indicates an internal error
	// or bug.
	m := fmt.Sprintf("conflict: existingRxOnly=%t, existingNonExclusive=%t, newRxOnly=%t, newNonExclusive=%t (new=%s, existing=%s) (default assumed conflict)", existingRxOnly, existingNonExclusive, newRxOnly, newNonExclusive, new.Id, existing.Id)
	log.Error().Msg(m)
	return true
}

type SchedulerSolution struct {
	SpectrumId                 uuid.UUID
	AllowInactive              bool
	AllowConflicts             bool
	// Conflicting grants whose IntConstraints overlap with candidate
	// Grant's Constraints.  This can be known at scheduling, or can emerge
	// at runtime between two co-scheduled Grants.
	LowerPrioConflicts         []*store.Grant
	HigherOrEqualPrioConflicts []*store.Grant
}

func SimpleScheduler(db *gorm.DB, grant *store.Grant, rclient *client.ZmsClient, ignoreGrantIdList []uuid.UUID) (*SchedulerSolution, error) {
	// List of compatible Spectrum objects.
	var spectrumList []store.Spectrum
	// Map of spectrumId to what the grant's policy (and thus priority)
	// would be in that Spectrum.
	spectrumIdPolicyMap := make(map[uuid.UUID]store.Policy)
	// A DST client, if any possible Policy objects require it.  We set this
	// in the loop below that processes possible spectrum objects.
	var dstClient *dst.DstClient

	// NB: we attempt to grab a dstClient immediately.  This can be wasted
	// work, but not a big deal.  We could wait until the first time we see
	// a Policy that requires it, or a conflict check that requires it, but
	// that complicates flows.  If we can't find one, we will exclude that
	// spectrum from the list of spectrum objects that can be searched to
	// fulfill this grant.
	ctx, cancel := context.WithTimeout(context.Background(), 4*time.Second)
	defer cancel()
	if c, err := rclient.GetDstClient(ctx); err != nil {
		m := fmt.Sprintf("dst service unavailable: will be unable to check occupancy and geospatial constraints if needed: %+v", err)
		log.Warn().Msg(m)
	} else {
		dstClient = &c
	}

	// NB: immediately generate the aggregate power map for this grant, even
	// if we might not need it.  Need to make this async.
	//
	// XXX: initially, in the case of range/bandwidth constraints that could
	// be edited down, any geospatial conflict "detected" in CheckConflict
	// is a possible one.
	//
	// XXX: also, in that case, the freq range (and thus the simulation
	// chosen, and agg power map chosen!) could be very wide and thus
	// somewhat imprecise on prediction.
	//  * At minimum we should regen the power map if we change the
    //    constraints after picking a bandwidth.
	//
	// Only do this when there are RadioPorts associated with the grant, of
	// course.
	if len(grant.RadioPorts) > 0 && dstClient != nil {
		reqId := uuid.New().String()
		hdr := dst.RequestHeader{
			ReqId: &reqId,
		}
		req := dst.CreatePropsimPowerGrantRequest{
			Header:   &hdr,
			GrantId: grant.Id.String(),
		}
		ctx, cancel := context.WithTimeout(context.Background(), 120*time.Second)
		defer cancel()
		if _, err := (*dstClient).CreatePropsimPowerGrant(ctx, &req); err != nil {
			m := fmt.Sprintf("creation of propsimPowerGrant aggregate map failed; geospatial conflict checks on new grant (%s) will fail (%+v)", grant.Id, err.Error())
			log.Info().Msg(m)
		}
	}

	//
	// Search for compatible Spectrum by comparing Spectrum Constraints and
	// Grant Constraints and start/expire timestamps.  Spectrum must be
	// Enabled, Approved, and not Deleted.
	//
	q := db.Where(&store.Spectrum{Enabled: true}).
		Where("deleted_at is NULL and approved_at is not NULL")
	// Ensure spectrum StartsAt/ExpiresAt accomodate grant StartsAt
	q = q.Where("starts_at <= ? and (expires_at is NULL or expires_at > ?)", grant.StartsAt, grant.StartsAt)
	// Ensure selected spectrum accomodates Grant.ExpiresAt
	if grant.ExpiresAt == nil {
		q = q.Where("expires_at is NULL")
	} else {
		q = q.Where("(expires_at is NULL or expires_at > ?)", *grant.ExpiresAt)
	}
	// If the grant requested a specific spectrum, force that.
	if grant.SpectrumId != nil {
		q = q.Where(&store.Spectrum{Id: *grant.SpectrumId})
	}
	q = q.Preload("Constraints.Constraint", func(db *gorm.DB) *gorm.DB {
		return db.Order("constraints.min_freq asc")
	})
	q = q.Preload("Policies")
	if res := q.Find(&spectrumList); res.Error != nil {
		return nil, fmt.Errorf("spectrum unavailable: query error: %s", res.Error.Error())
	} else if res.RowsAffected < 1 {
		return nil, fmt.Errorf("spectrum unavailable: time unavailable")
	}

	log.Debug().Msg(fmt.Sprintf("sched: spectrumList: %+v", spectrumList))

	var newList []store.Spectrum
	for _, s := range spectrumList {
		log.Debug().Msg(fmt.Sprintf("checking spectrum: %s", s.Id))
		// Check Spectrum.Policies
		pMatch := false
		var policy store.Policy
		for _, p := range s.Policies {
			log.Debug().Msg(fmt.Sprintf("checking policy: %+v", p))
			if p.ElementId == nil {
				if !pMatch {
					log.Debug().Msg(fmt.Sprintf("selecting wildcard policy: %+v", p))
					policy = p
					pMatch = true
				}
			} else if *p.ElementId == grant.ElementId {
				log.Debug().Msg(fmt.Sprintf("selecting element policy: %+v", p))
				policy = p
				pMatch = true
			}
		}
		if !pMatch || !policy.Allowed {
			continue
		}
		log.Debug().Msg(fmt.Sprintf("spectrum policy match (element %s): %+v", grant.ElementId, policy))

		if grant.Priority > policy.Priority {
			if grant.IsClaim {
				log.Debug().Msg(fmt.Sprintf("spectrum policy match (element %s): %+v (claim priority (%d) allowed to exceed policy priority (%d); allowed", grant.ElementId, policy, grant.Priority, policy.Priority))
			} else {
				log.Debug().Msg(fmt.Sprintf("spectrum policy match (element %s): %+v (but grant priority (%d) exceeds policy priority (%d); skipping", grant.ElementId, policy, grant.Priority, policy.Priority))
				continue
			}
		}

		// Check Spectrum.Constraints.  For each grant constraint, we must
		// find a matching spectrum constraint.
		var cMatch *store.Constraint
		for _, gc := range grant.Constraints {
			log.Debug().Msg(fmt.Sprintf("checking grant constraint: %+v", gc.Constraint))
			var scMatch *store.Constraint
			for _, sc := range s.Constraints {
				log.Debug().Msg(fmt.Sprintf("checking spectrum constraint: %+v", sc.Constraint))
				if gc.Constraint.MinFreq < sc.Constraint.MinFreq || gc.Constraint.MaxFreq > sc.Constraint.MaxFreq || gc.Constraint.MaxEirp > sc.Constraint.MaxEirp {
					log.Debug().Msg(fmt.Sprintf("failed to match freq/eirp"))
					continue
				}
				// XXX: exclusive? and area.
				scMatch = &sc.Constraint
				break
			}
			if scMatch == nil {
				cMatch = nil
				log.Debug().Msg("grant constraint not matched")
				break
			} else {
				cMatch = scMatch
				log.Debug().Msg("grant constraint matched")
			}
		}
		if cMatch == nil {
			continue
		}

		if (policy.WhenUnoccupied && !grant.IsClaim) || !policy.DisableEmitCheck {
			// We need a DST client; ensure we have one, else exclude this
			// Spectrum.
			if dstClient == nil {
				m := fmt.Sprintf("dst service unavailable to check occupancy and geospatial constraints (skipping spectrum %+v)", s.Id)
				log.Warn().Msg(m)
				continue
			}
		}

		newList = append(newList, s)
		spectrumIdPolicyMap[s.Id] = policy
	}
	spectrumList = newList

	log.Debug().Msg(fmt.Sprintf("sched: spectrumList post policy/constraint filter: %+v", spectrumList))

	if len(spectrumList) < 1 {
		return nil, fmt.Errorf("spectrum unavailable: policy or constraint mismatch")
	}
	var spectrumIdList []uuid.UUID
	spectrumIdMap := make(map[uuid.UUID]store.Spectrum)
	spectrumIdUsedMap := make(map[uuid.UUID][]*SpectrumRange)
	spectrumIdAvailMap := make(map[uuid.UUID][]*SpectrumRangeAvail)
	spectrumIdLowerPrioAvailMap := make(map[uuid.UUID][]*SpectrumRangeAvail)
	for _, s := range spectrumList {
		spectrumIdUsedMap[s.Id] = make([]*SpectrumRange, 0)
		spectrumIdAvailMap[s.Id] = make([]*SpectrumRangeAvail, 0)
		spectrumIdLowerPrioAvailMap[s.Id] = make([]*SpectrumRangeAvail, 0)
		spectrumIdList = append(spectrumIdList, s.Id)
		spectrumIdMap[s.Id] = s
	}

	// Now check approved, live grants in these spectrum, during the
	// proposed grant lifetime, that trivially "overlap" with us in terms of
	// frequency range.  We make multiple passes.
	//
	// 1) We create a list of available frequency ranges, and use the first
	// (NB: implement cost function max ("best") later) such range.
	//
	// 2) When there are no ranges available,
	//
	//   a) If this grant is higher-priority than an existing grant whose
	//      allocation could serve this grant, we displace its grant (either
	//      revoking it or setting inactive, according to what the grant
	//      requested).
	//
	//   b) If this grant is lower-priority, and the grant has set
	//      AllowInactive, we will make an overlapping but inactive grant.
	//
	// This allows us to check grant constraints to see if requested
	// spectrum overlaps with this grant.  If the request range is fixed (no
	// bandwidth < range), this is trivial.  If the scheduler is supposed to
	// select a free range of Y bandwidth within X range, then this
	// effectively creates a secondary constraint on the request.  For now,
	// we remove the overlapping live grant from the request constraint, and
	// reduce the request constraint to the largest remaining sub-range.
	//
	// XXX: currently ignoring area and exclusivity; currently everything
	// is exclusive.

	var grantList []store.Grant
	// Grant must be in an approved (waiting to activate), active, paused,
	// or pending (paused due to higher-prio conflicting active grant).
	q = db.Where("(status = 'approved' or status = 'active' or status = 'paused' or status='pending')")
	// Only check grants in spectrum we can use
	q = q.Where("spectrum_id in ?", spectrumIdList)
	// Only worry about grants that overlap with this grant's
	// StartsAt/ExpiresAt.
	if grant.ExpiresAt == nil {
		q = q.Where("(expires_at is NULL or expires_at > ?)", grant.StartsAt)
	} else {
		q = q.Where("(expires_at is NULL and starts_at < ?) or (expires_at is not NULL and ((? > starts_at and ? < expires_at) or (? > starts_at and ? < expires_at))) or (? < starts_at and ? > expires_at)", grant.ExpiresAt, grant.StartsAt, grant.StartsAt, grant.ExpiresAt, grant.ExpiresAt, grant.StartsAt, grant.ExpiresAt)
	}
	q = q.Preload("Constraints.Constraint", func(db *gorm.DB) *gorm.DB {
		return db.Order("constraints.min_freq asc")
	})
	q = q.Preload("RadioPorts.RadioPort")
	if res := q.Find(&grantList); res.Error != nil {
		return nil, fmt.Errorf("spectrum unavailable: grant query error: %s", res.Error.Error())
	}

	log.Debug().Msg(fmt.Sprintf("sched: grantList: %+v", grantList))

	// Index the live grants by spectrum id used
	for gi, _ := range grantList {
		g := &grantList[gi]

		// If this grant matches one we must skip conflict checks ien the
		// ignoreGrantIdList, skip it.
		var ignoreGrantId *uuid.UUID
		for _, igi := range ignoreGrantIdList {
			if g.Id == igi {
				ignoreGrantId = &igi
				break
			}
		}
		if ignoreGrantId != nil {
			log.Debug().Msg(fmt.Sprintf("sched: ignoring grantList item %+v", ignoreGrantId))
			continue
		}
		// NB: this assumes that we have exactly one policy per element per
		// spectrum.  If this were to change, this check function would be
		// invalid, since it assumes that if the new grant is scheduled in
		// grant g's spectrum, it would do so under the same policy, *if in
		// the same element*.
		gp := spectrumIdPolicyMap[*g.SpectrumId]
		isConflict := CheckConflict(grant, g, &gp, dstClient)
		log.Debug().Msg(fmt.Sprintf("sched: grant %d: %+v (isconflict=%t)", gi, *g, isConflict))
		for _, gco := range g.Constraints {
			sr := &SpectrumRange{
				Start: gco.Constraint.MinFreq,
				End: gco.Constraint.MaxFreq,
				Grant: g,
				IsConflict: isConflict,
			}
			spectrumIdUsedMap[*g.SpectrumId] = append(spectrumIdUsedMap[*g.SpectrumId], sr)
		}
	}

	// Sort the live grants per spectrum id
	for sid, _ := range spectrumIdUsedMap {
		sort.Slice(spectrumIdUsedMap[sid], func(i, j int) bool {
			return spectrumIdUsedMap[sid][i].Start < spectrumIdUsedMap[sid][j].Start
		})
	}

	log.Debug().Msg(fmt.Sprintf("sched: spectrumIdUsedMap inuse: %+v", spectrumIdUsedMap))

	// Create a list of spectrum availability, as well as potential,
	// fallback lower and higher-priority grant lists.
	//
	// XXX: does not handle Spectrum sub-area constraints yet.
	//
	// NB: allow lower-prio conflicts to appear to be available in the
	// spectrumIdLowerPrioAvailMap; we need this for the prepass that
	// immediately follows, that checks for possible solution space.
	for _, sid := range spectrumIdList {
		grantPriority := spectrumIdPolicyMap[sid].Priority
		if grant.IsClaim {
			grantPriority = grant.Priority
		}

		// For each spectrum constraint:
		for _, ssc := range spectrumIdMap[sid].Constraints {
			sc := ssc.Constraint
			// For each used chunk of this spectrum constraint:
			lra := &SpectrumRangeAvail{
				Start: sc.MinFreq,
				End: 0,
				Grants: make([]*SpectrumRange, 0),
				//Occupied: false,
			}
			for _, r := range spectrumIdUsedMap[sid] {
				// If the allocated range is not in this constraint, skip.
				if r.Start < sc.MinFreq || r.End > sc.MaxFreq {
					continue
				}

				closed, next := lra.Add(r, grantPriority)
				if len(closed) > 0 {
					spectrumIdAvailMap[sid] = append(spectrumIdAvailMap[sid], closed...)
				}
				lra = next
			}

			closed, final := lra.Finish(sc.MaxFreq)
			if len(closed) > 0 {
				spectrumIdAvailMap[sid] = append(spectrumIdAvailMap[sid], closed...)
			}
			if final != nil {
				spectrumIdAvailMap[sid] = append(spectrumIdAvailMap[sid], final)
			}
		}
		// NB: technically, we could join adjacent constraints, but do not
		// bother because they might be in different Areas.  Need to
		// revisit.

		// And compute again for the spectrumIdLowerPrioAvailMap, for each
		// spectrum constraint:
		for _, ssc := range spectrumIdMap[sid].Constraints {
			sc := ssc.Constraint
			// For each used chunk of this spectrum constraint:
			lra := &SpectrumRangeAvail{
				Start: sc.MinFreq,
				End: 0,
				Grants: make([]*SpectrumRange, 0),
				//Occupied: false,
			}
			for _, r := range spectrumIdUsedMap[sid] {
				// If the allocated range is not in this constraint, skip.
				if r.Start < sc.MinFreq || r.End > sc.MaxFreq {
					continue
				}
				// If the existing grant (r)'s priority is less than the new
				// possible prio, do not include it in the "used" space.
				if r.Grant != nil && r.Grant.Priority < grantPriority {
					continue
				}

				closed, next := lra.Add(r, grantPriority)
				if len(closed) > 0 {
					spectrumIdLowerPrioAvailMap[sid] = append(spectrumIdLowerPrioAvailMap[sid], closed...)
				}
				lra = next
			}

			closed, final := lra.Finish(sc.MaxFreq)
			if len(closed) > 0 {
				spectrumIdLowerPrioAvailMap[sid] = append(spectrumIdLowerPrioAvailMap[sid], closed...)
			}
			if final != nil {
				spectrumIdLowerPrioAvailMap[sid] = append(spectrumIdLowerPrioAvailMap[sid], final)
			}
		}
		// NB: technically, we could join adjacent constraints, but do not
		// bother because they might be in different Areas.  Need to
		// revisit.
	}
	// Dump the availMaps.
	for sid, al := range spectrumIdAvailMap {
		for _, ai := range al {
			log.Debug().Msg(fmt.Sprintf("sched: spectrumIdAvailMap[%s] item: %+v", sid, ai))
			for _, g := range ai.Grants {
				log.Debug().Msg(fmt.Sprintf("sched: spectrumIdAvailMap[%s] item grant: %s, %d, %d-%d, isconflict=%t", sid, g.Grant.Name, g.Grant.Priority, g.Start, g.End, g.IsConflict))
			}
		}
	}
	for sid, al := range spectrumIdLowerPrioAvailMap {
		for _, ai := range al {
			log.Debug().Msg(fmt.Sprintf("sched: spectrumIdLowerPrioAvailMap[%s] item: %+v", sid, ai))
			for _, g := range ai.Grants {
				log.Debug().Msg(fmt.Sprintf("sched: spectrumIdLowerPrioAvailMap[%s] item grant: %s, %d, %d-%d, isconflict=%t", sid, g.Grant.Name, g.Grant.Priority, g.Start, g.End, g.IsConflict))
			}
		}
	}

	// Before we attempt a best-fit solution, prepass to ensure there is actually a
	// schedulable solution given availability.
	//
	// NB: this blithely ignores that any one grant constraint might be
	// satisfied by any one available chunk, but that not all grant
	// constraints might be satisfiable.
	//
	// NB: Note that we use the avail map with lower prio computation, since
	// this is a gating check, not a final schedule.  This is just a guess
	// as to what might fit, to gate our calls to the expensive DST queries.
	var constraintsToAvail map[uuid.UUID][]uuid.UUID = make(map[uuid.UUID][]uuid.UUID)
	for _, gcc := range grant.Constraints {
		gc := gcc.Constraint
		constraintsToAvail[gc.Id] = make([]uuid.UUID, 0, 4)
		for sid, _ := range spectrumIdLowerPrioAvailMap {
			for i, _ := range spectrumIdLowerPrioAvailMap[sid] {
				r := spectrumIdLowerPrioAvailMap[sid][i]
				if gc.Bandwidth == 0 && gc.MinFreq >= r.Start && gc.MaxFreq <= r.End {
					log.Debug().Msg(fmt.Sprintf("sched: possible available range for constraint (%+v): %+v", gc, r))
					//possible = true
					constraintsToAvail[gc.Id] = append(constraintsToAvail[gc.Id], sid)
				} else if gc.Bandwidth > 0 {
					// If intersect width > gc.Bandwidth, a solution is
					// possible.
					if (MinInt64(gc.MaxFreq, r.End) - MaxInt64(gc.MinFreq, r.Start)) >= gc.Bandwidth {
						log.Debug().Msg(fmt.Sprintf("sched: possible available range for bandwidth constraint (%+v): %+v", gc, r))
						//possible = true
						constraintsToAvail[gc.Id] = append(constraintsToAvail[gc.Id], sid)
					}
				}
			}
		}
		if len(constraintsToAvail[gc.Id]) == 0 {
			return nil, fmt.Errorf("spectrum unavailable: no matching available ranges for grant constraint (%+v)", gc)
		}
	}

	// Set to scheduling, which makes this provisional... but we can't!  We
	// might change its constraints so we cannot just place it in the DB.
	// We will have to filter out provisionals in the above queries to
	// ignore them as we search.  Scheduling grants can either be denied or
	// deleted.  Then even if we find a solution, we have to verify it has
	// no new conflicts, ugh.
	grant.Status = "scheduling"

	// First, check the basic spectrumIdAvailMap to find a trivial solution,
	// if one exists, that does not require a lower-prio user to vacate.
	log.Debug().Msg("sched: search (displaceLower=false,allowInactive=false)")
	if solution, err := SearchAvailabilityMap(grant, spectrumIdMap, spectrumIdAvailMap, dstClient, false, false, spectrumIdPolicyMap); err != nil {
		m := fmt.Sprintf("sched: search availMap(lower=false,higher=false): %s", err.Error())
		log.Info().Msg(m)
		return nil, err
	} else if solution != nil {
		return solution, nil
	}

	// Second, check the basic spectrumIdAvailMap to find a solution that
	// involves lower-prio grants we can displace or co-schedule with.
	log.Debug().Msg("sched: search (displaceLower=true,allowInactive=false)")
	if solution, err := SearchAvailabilityMap(grant, spectrumIdMap, spectrumIdAvailMap, dstClient, true, false, spectrumIdPolicyMap); err != nil {
		m := fmt.Sprintf("sched: search availMap(lower=true,higher=false): %s", err.Error())
		log.Info().Msg(m)
		return nil, err
	} else if solution != nil {
		return solution, nil
	}

	if grant.AllowInactive != nil && *grant.AllowInactive {
		log.Debug().Msg("sched: search (displaceLower=true,allowInactive=true)")
		if solution, err := SearchAvailabilityMap(grant, spectrumIdMap, spectrumIdAvailMap, dstClient, true, true, spectrumIdPolicyMap); err != nil {
			m := fmt.Sprintf("sched: search availMap(lower=true,higher=true: %s", err.Error())
			log.Info().Msg(m)
			return nil, err
		} else if solution != nil {
			m := fmt.Sprintf("sched: granted to spectrum %s (%+v)", grant.Id, solution)
			log.Info().Msg(m)
			return solution, nil
		}
	}

	return nil, nil
}

func FindSpectrumRangeListOverlap(srl []SpectrumRange, minFreq int64, maxFreq int64) (out []SpectrumRange) {
	out = make([]SpectrumRange, 0)
	for _, sr := range srl {
		if (minFreq >= sr.Start && minFreq < sr.End) || (maxFreq >= sr.Start && maxFreq < sr.End) {
			out = append(out, sr)
		}
	}
	return out
}

func SearchAvailabilityMap(grant *store.Grant, spectrumMap map[uuid.UUID]store.Spectrum, availMap map[uuid.UUID][]*SpectrumRangeAvail, dstClient *dst.DstClient, displaceLower bool, allowInactive bool, spectrumIdPolicyMap map[uuid.UUID]store.Policy) (solution *SchedulerSolution, err error) {

	//
	// Next, we attempt to find the best fit across occupancy and emissions
	// bound/area, over each candidate spectrum.
	//
	// NB: eventually this needs to migrate to a parallel background task,
	// since it may spawn heavyweight simulation and measurement tasks.
	//
	// First we collect occupancy data as necessary; then we collect
	// propagation estimates.
	//

	// For each constraint, we search the availMap; if there is a
	// hole, assign it, remove the hole, and move on.
	//
	// First, if the bandwidth was set, and is smaller than MaxFreq - MinFreq,
	//   - if policy.WhenUnoccupied, we find an unoccupied range of Bandwidth width.
	//   - else, just pick first unoccupied such range, and set Min/MaxFreq to that range.
	//
	// Second, if radio_port_id is set, we check to see if simulated
	// transmissions will leave the 1) spectrum policy area, if set; else 2)
	// overall zone.
	//
	for sid, _ := range availMap {
		// Find the policy that applies to this Spectrum and Grant.
		p := spectrumIdPolicyMap[sid]
		grantPriority := p.Priority
		log.Debug().Msg(fmt.Sprintf("search: grantPriority=%d in %s", grantPriority, sid))
		if grant.IsClaim {
			grantPriority = grant.Priority
			log.Debug().Msg(fmt.Sprintf("search: allowing claim grantPriority=%d in %s", grantPriority, sid))
		}

		var lowerPrioConflictMap map[uuid.UUID]*store.Grant
		var higherOrEqualPrioConflictMap map[uuid.UUID]*store.Grant

		// Return early warnings if the spectrum constraints require a check
		// in the dst and if we have no dstClient.
		needDst := false
		for _, ci := range spectrumMap[sid].Constraints {
			if ci.Constraint.Area == nil {
				continue
			}
			needDst = true
			if dstClient == nil {
				m := fmt.Sprintf("search: no dst client available (spectrum area/power constraint); skipping spectrum %s", sid)
				log.Warn().Msg(m)
			}
		}
		if !p.DisableEmitCheck {
			needDst = true
			if dstClient == nil {
				m := fmt.Sprintf("search: no dst client available (zone emit check); skipping spectrum %s", sid)
				log.Warn().Msg(m)
			}
		}
		if p.WhenUnoccupied && !grant.IsClaim {
			needDst = true
			if dstClient == nil {
				m := fmt.Sprintf("search: no dst client available (occupancy); skipping spectrum %s", sid)
				log.Warn().Msg(m)
			}
		}
		// Skip this spectrum if we required a DST client.
		if needDst && dstClient == nil {
			continue
		}

		dupAvailList := make([]*SpectrumRangeAvail, 0, len(availMap[sid]))
		copy(dupAvailList, availMap[sid])

		var metConstraints []store.Constraint

		// Find a match for all grant constraints.
		for _, gcc := range grant.Constraints {
			gc := gcc.Constraint

			// Ensure that no grant radio port can leave the zone
			if !p.DisableEmitCheck {
				for _, grantRadioPort := range grant.RadioPorts {
					reqId := uuid.New().String()
					hdr := dst.RequestHeader{
						ReqId: &reqId,
					}
					req := dst.GetTxConstraintsRequest{
						Header:      &hdr,
						Freq:        gc.MinFreq,
						RadioPortId: grantRadioPort.RadioPortId.String(),
					}
					ctx, cancel := context.WithTimeout(context.Background(), 4*time.Second)
					defer cancel()
					if resp, err := (*dstClient).GetTxConstraints(ctx, &req); err != nil {
						portName := ""
						if grantRadioPort.RadioPort != nil {
							portName = grantRadioPort.RadioPort.Name
						}
						if status.Code(err) == codes.NotFound {
							m := fmt.Sprintf("no propsim for radio port %s (%s) at %d: %+v", portName, grantRadioPort.RadioPortId, gc.MinFreq, err)
							log.Warn().Msg(m)
							return nil, errors.New(m)
						} else {
							m := fmt.Sprintf("error finding propsim for radio port %s (%s) at %d: %+v", portName, grantRadioPort.RadioPortId, gc.MinFreq, err)
							log.Error().Msg(m)
							return nil, errors.New(m)
						}
					} else {
						log.Debug().Msg(fmt.Sprintf("tx constraints search: %+v", resp))
						if resp.MaxPower <= -70.0 {
							log.Debug().Msg(fmt.Sprintf("tx constraints search: below -70.0 threshold"))
						} else {
							m := fmt.Sprintf("tx constraints search: above -90.0 threshold")
							log.Warn().Msg(m)
							return nil, errors.New(m)
						}
					}
				}
			}

			var gmin, gmax, gbw, gw int64
			gmin = gc.MinFreq
			gmax = gc.MaxFreq
			gbw = gc.Bandwidth
			gw = gbw
			if gbw == 0 {
				gw = gmax - gmin
			}

			// If this is WhenOccupied spectrum, we have to get occupancy
			// data.  If not, we assume our grant request range is
			// unoccupied.
			unoccupiedRanges := make([]SpectrumRange, 0)
			if p.WhenUnoccupied && !grant.IsClaim {
				var endsAt *timestamppb.Timestamp
				if grant.ExpiresAt != nil {
					endsAt = timestamppb.New(*grant.ExpiresAt)
				}
				reqId := uuid.New().String()
				hdr := dst.RequestHeader{
					ReqId: &reqId,
				}
				reqStartsAt := time.Now().UTC().Add(-1 * time.Hour)
				req := dst.GetUnoccupiedSpectrumRequest{
					Header:             &hdr,
					MinFreq:            gmin,
					MaxFreq:            gmax,
					OccupancyThreshold: -120.0,
					MinBandwidth:       &gw,
					StartsAt:           timestamppb.New(reqStartsAt),
					EndsAt:             endsAt,
				}
				ctx, cancel := context.WithTimeout(context.Background(), 60*time.Second)
				defer cancel()
				if resp, err := (*dstClient).GetUnoccupiedSpectrum(ctx, &req); err != nil {
					m := fmt.Errorf("error finding unoccupied spectrum (%v): %+v", sid, err)
					log.Error().Msg(m.Error())
					break
				} else if len(resp.Ranges) < 1 {
					m := fmt.Errorf("no unoccupied spectrum available (%v)", sid)
					log.Error().Msg(m.Error())
					break
				} else {
					log.Debug().Msg(fmt.Sprintf("unoccupied spectrum available for bandwidth slice (%v): updating grant (%v: %v-%v, %v) (unoccupied ranges %+v)", sid, grant.Id, gmin, gmax, gbw, resp.Ranges))
					for _, rr := range resp.Ranges {
						sr := SpectrumRange{
							Start: rr.MinFreq,
							End: rr.MaxFreq,
						}
						unoccupiedRanges = append(unoccupiedRanges, sr)
					}
				}
			} else {
				unoccupiedRanges = []SpectrumRange{{Start: gmin, End: gmax,}}
			}

			// We need to find an intersection of width gw between
			// spectrumAvailMap and unoccupiedRanges.
			foundFit := false
			for _, ur := range unoccupiedRanges {
				log.Debug().Msg(fmt.Sprintf("intersecting unoccupied range: %+v", ur))

				// Reset our availMap and currentStart, currentEnd trackers
				// each time we intersect a new unoccupiedRange, or
				// encounter a used range in availMap that we cannot
				// override.
				currentStart := int64(0)
				currentEnd := int64(0)
				lowerPrioConflictMap = make(map[uuid.UUID]*store.Grant)
				higherOrEqualPrioConflictMap = make(map[uuid.UUID]*store.Grant)

				for _, r := range availMap[sid] {
					// If there is no overlap, skip
					if (r.Start < ur.Start && r.End < ur.Start) || (r.Start > ur.End && r.End > ur.End) {
						currentStart = 0
						currentEnd = 0
						clear(lowerPrioConflictMap)
						clear(higherOrEqualPrioConflictMap)
						continue
					}

					// If there is overlap, do priority checks.
					hasLower, hasHigherOrEqual := false, false
					if len(r.Grants) > 0 {
						hasLower = r.HasLowerPrioGrants(grantPriority)
						hasHigherOrEqual = r.HasHigherOrEqualPrioGrants(grantPriority)
					}
					log.Debug().Msg(fmt.Sprintf("intersecting availMap[%s] range: %+v (hasLower=%t, hasHigherOrEqual=%t)", sid, r, hasLower, hasHigherOrEqual))

					if hasHigherOrEqual {
						if !allowInactive {
							log.Debug().Msg(fmt.Sprintf("skipping availMap[%s] range: %+v (higher-prio grants; not allowInactive)", sid, r))
							currentStart = 0
							currentEnd = 0
							clear(lowerPrioConflictMap)
							clear(higherOrEqualPrioConflictMap)
							continue
						} else {
							gl := r.GetHigherOrEqualPrioGrants(grantPriority)
							for _, g := range gl {
								higherOrEqualPrioConflictMap[g.Id] = g
							}
						}
					}
					if hasLower {
						if !displaceLower {
							log.Debug().Msg(fmt.Sprintf("skipping availMap[%s] range: %+v (lower-prio grants; not displaceLower)", sid, r))
							currentStart = 0
							currentEnd = 0
							clear(lowerPrioConflictMap)
							clear(higherOrEqualPrioConflictMap)
							continue
						} else {
							gl := r.GetLowerPrioGrants(grantPriority)
							for _, g := range gl {
								lowerPrioConflictMap[g.Id] = g
							}
						}
					}

					// Since priority checks passed, add to the current range.
					if currentStart == 0 {
						currentStart = MaxInt64(r.Start, ur.Start)
					}
					currentEnd = MinInt64(ur.End, r.End)

					if (currentEnd - currentStart) < gw {
						log.Debug().Msg(fmt.Sprintf("sched: search: currentEnd=%d - currentStart=%d < %d; continuing to next availMap range", currentEnd, currentStart, gw))
						continue
					}
					// A fit was found.  Append updated constraints; update
					// availability range duplicate.
					log.Debug().Msg(fmt.Sprintf("spectrum available (%v): accepting grant (%v: %v-%v, %v) (%v-%v)", sid, grant.Id, gmin, gmax, gbw, currentStart, currentEnd))
					foundFit = true
					metConstraints = append(metConstraints, store.Constraint{MinFreq: currentStart, MaxFreq: currentStart + gw})
					// XXX: Update spectrumIdAvailMap -- in place -- if we run
					// out of space in this spectrum id, we fail and try
					// another.
					log.Debug().Msg("would have updated availMap")
					break
				}
				if foundFit {
					break
				}
			}
		}

		//
		// Next, if this spectrum has Area and power constraints, we need to
		// verify that the grant can comply.  If unconstrained, we use the
		// overall Zone boundary to restrict emissions.
		//
		msc := make([]*store.Constraint, 0, len(spectrumMap[sid].Constraints))
		gc := &grant.Constraints[0].Constraint
		for _, ci := range spectrumMap[sid].Constraints {
			sc := &ci.Constraint

			if sc.Area == nil {
				msc = append(msc, sc)
				continue
			}
			var reqArea geo.Area
			sc.Area.ToProto(&reqArea, true)

			mpc := 0
			for _, rp := range grant.RadioPorts {
				reqId := uuid.New().String()
				hdr := dst.RequestHeader{
					ReqId: &reqId,
				}
				req := &dst.GetTxConstraintsRequest{
					Header:      &hdr,
					RadioPortId: rp.Id.String(),
					Freq:        (gc.MaxFreq - gc.MinFreq) / 2,
					Area:        &reqArea,
					MaxExtRssi:  -90,
				}
				ctx, cancel := context.WithTimeout(context.Background(), 4*time.Second)
				defer cancel()
				if resp, err := (*dstClient).GetTxConstraints(ctx, req); err != nil {
					m := fmt.Errorf("error finding unoccupied spectrum (%v): %+v", sid, err)
					log.Error().Msg(m.Error())
					continue
				} else if false {
					m := fmt.Sprintf("no unoccupied spectrum available (%v)", sid)
					log.Debug().Msg(m)
					continue
				} else {
					log.Debug().Msg(fmt.Sprintf("GetTxConstraints resp = %+v", resp))
				}

				mpc += 1
			}
			if mpc == len(grant.RadioPorts) {
				msc = append(msc, sc)
			}
		}

		if len(metConstraints) == len(grant.Constraints) {
			log.Debug().Msg(fmt.Sprintf("spectrum %v met all constraints for grant %v; updating", sid, grant.Id))
			// Update the actual constraints:
			for i, mc := range metConstraints {
				grant.Constraints[i].Constraint.MinFreq = mc.MinFreq
				grant.Constraints[i].Constraint.MaxFreq = mc.MaxFreq
			}
		} else {
			continue
		}

		// Allow grant to set its own priority that is lower than the max
		// allowed to this Element; we checked above to ensure this is not
		// violated, so this is correct.
		if grant.Priority == 0 {
			grant.Priority = grantPriority
		}

		var lowerPrioConflicts []*store.Grant
		var higherPrioConflicts []*store.Grant
		if len(lowerPrioConflictMap) > 0 {
			lowerPrioConflicts = make([]*store.Grant, 0, len(lowerPrioConflictMap))
			for _, g := range lowerPrioConflictMap {
				m := fmt.Sprintf("lowerPrio conflicting grant: %+v", g)
				log.Debug().Msg(m)
				lowerPrioConflicts = append(lowerPrioConflicts, g)
			}
		} else {
			lowerPrioConflicts = nil
		}

		if len(higherOrEqualPrioConflictMap) > 0 {
			higherPrioConflicts = make([]*store.Grant, 0, len(higherOrEqualPrioConflictMap))
			for _, g := range higherOrEqualPrioConflictMap {
				m := fmt.Sprintf("higherOrEqualPrio conflicting grant: %+v", g)
				log.Debug().Msg(m)
				higherPrioConflicts = append(higherPrioConflicts, g)
			}
		} else {
			higherPrioConflicts = nil
		}

		solution = &SchedulerSolution{
			SpectrumId:                 sid,
			AllowInactive:              allowInactive,
			AllowConflicts:             false,
			LowerPrioConflicts:         lowerPrioConflicts,
			HigherOrEqualPrioConflicts: higherPrioConflicts,
		}

		return solution, nil
	}

	return nil, nil
}
