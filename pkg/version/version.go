package version

import (
	"fmt"
)

const (
	Major = 0
	Minor = 99
	Patch = 1
)

type Version struct {
	Major          int    `json:"major"`
	Minor          int    `json:"minor"`
	Patch          int    `json:"patch"`
	Version        string `json:"version"`
	Branch         string `json:"branch"`
	BuildTimestamp string `json:"build_timestamp"`
	Commit         string `json:"commit"`
}

var VersionString = fmt.Sprintf("%d.%d.%d", Major, Minor, Patch)
var version = Version{
	Major:          Major,
	Minor:          Minor,
	Patch:          Patch,
	Version:        VersionString,
	Branch:         _branch,
	BuildTimestamp: _buildTimestamp,
	Commit:         _commit,
}

func GetVersion() *Version {
	return &version
}
