// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package client

import (
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc/codes"
	"gorm.io/gorm"

	zmsclient "gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/client"
	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/subscription"

	alarm "gitlab.flux.utah.edu/openzms/zms-api/go/zms/alarm/v1"
	event "gitlab.flux.utah.edu/openzms/zms-api/go/zms/event/v1"

	"gitlab.flux.utah.edu/openzms/zms-zmc/pkg/config"
	grantcontroller "gitlab.flux.utah.edu/openzms/zms-zmc/pkg/controller/grant"
	"gitlab.flux.utah.edu/openzms/zms-zmc/pkg/store"
)

type AlarmWatcher struct {
	config  *config.Config
	db      *gorm.DB
	sm      *subscription.SubscriptionManager[*subscription.Event]
	gc      *grantcontroller.GrantController
	rclient *zmsclient.ZmsClient
	sub     *zmsclient.Subscription[alarm.SubscribeResponse]
}

func (vw *AlarmWatcher) StatusHandler(connected bool, err error) (abort bool) {
	if !connected {
		log.Warn().Msg(fmt.Sprintf("AlarmWatcher: disconnected (%+v)", err))
	} else {
		log.Info().Msg(fmt.Sprintf("AlarmWatcher: connected: %+v", connected))
	}

	return false
}

func (vw *AlarmWatcher) GrantChangeHandler(grantChange *alarm.GrantChange) (code codes.Code, err error) {
	// Verify grant uuid
	var id uuid.UUID
	if id, err = uuid.Parse(grantChange.GrantId); err != nil {
		return codes.InvalidArgument, fmt.Errorf("invalid grant id: %s", err.Error())
	}

	// Look up grant.
	var grant store.Grant
	res := vw.db.First(&grant, id)
	if res.RowsAffected != 1 {
		return codes.NotFound, fmt.Errorf("invalid grant id")
	}

	if grantChange.Action != nil && *grantChange.Action == "revoke" {
		m := "revoked by alarm service"
		if grantChange.Description != nil {
			m += ": " + *grantChange.Description
		}
		if err := vw.gc.ChangeGrantStatus(&grant, store.StatusRevoked, m, nil, nil); err != nil {
			m := fmt.Sprintf("error changing grant status to revoked: %s", err.Error())
			log.Error().Err(err).Msg(m)
		}
	} else if grantChange.Action != nil && *grantChange.Action == "pause" {
		m := "paused by alarm service"
		if grantChange.Description != nil {
			m += ": " + *grantChange.Description
		}
		if err := vw.gc.ChangeGrantStatus(&grant, store.StatusPaused, m, nil, nil); err != nil {
			m := fmt.Sprintf("error changing grant status to paused: %s", err.Error())
			log.Error().Err(err).Msg(m)
		}
	} else if grantChange.Action != nil && *grantChange.Action == "resume" {
		m := "resumed by alarm service"
		if grantChange.Description != nil {
			m += ": " + *grantChange.Description
		}
		if err := vw.gc.ChangeGrantStatus(&grant, store.StatusActive, m, nil, nil); err != nil {
			m := fmt.Sprintf("error changing grant status to active: %s", err.Error())
			log.Error().Err(err).Msg(m)
		}
	}

	return codes.OK, nil
}

func (vw *AlarmWatcher) ConstraintChangeHandler(constraintChange *alarm.ConstraintChange) (code codes.Code, err error) {
	// Verify uuids.
	var constraintChangeId uuid.UUID
	if constraintChangeId, err = uuid.Parse(constraintChange.Id); err != nil {
		return codes.InvalidArgument, fmt.Errorf("invalid constraintChange id: %s", err.Error())
	}
	var grantId uuid.UUID
	if grantId, err = uuid.Parse(constraintChange.GrantId); err != nil {
		return codes.InvalidArgument, fmt.Errorf("invalid grant id: %s", err.Error())
	}
	var constraintId uuid.UUID
	if constraintId, err = uuid.Parse(constraintChange.ConstraintId); err != nil {
		return codes.InvalidArgument, fmt.Errorf("invalid constraint id: %s", err.Error())
	}
	var observationId *uuid.UUID
	if constraintChange.ObservationId != nil {
		if obsId, err := uuid.Parse(*constraintChange.ObservationId); err != nil {
			return codes.InvalidArgument, fmt.Errorf("invalid constraintChange observation id: %s", err.Error())
		} else {
			observationId = &obsId
		}
	}

	// Look up grant.
	var grant store.Grant
	q := vw.db.Preload("Constraints.Constraint").Preload("RadioPorts.RadioPort")
	res := q.First(&grant, grantId)
	if res.RowsAffected != 1 {
		return codes.NotFound, fmt.Errorf("invalid grant id (%s)", grantId)
	}
	// Look up constraint.
	var oldConstraint *store.Constraint
	gcIdx := -1
	for i, gc := range grant.Constraints {
		if gc.Constraint.Id == constraintId {
			oldConstraint = &gc.Constraint
			gcIdx = i
			break
		}
	}
	if oldConstraint == nil {
		return codes.NotFound, fmt.Errorf("invalid constraint id (%s, grant %s)", constraintId, grantId)
	}

	// Ensure change is a reduction in power; support nothing else without a
	// trip through the scheduler.  Even reduction in power should go
	// through the scheduler so that our conflict relationships are still
	// according to our models.
	if constraintChange.MaxEirp == nil {
		return codes.InvalidArgument, fmt.Errorf("no max_eirp provided to constraintChange (constraint %s, grant %s)", constraintId, grantId)
	} else if *constraintChange.MaxEirp >= oldConstraint.MaxEirp {
		return codes.InvalidArgument, fmt.Errorf("new constraintChange.max_eirp %f exceeds current %f (constraint %s, grant %s)", *constraintChange.MaxEirp, oldConstraint.MaxEirp, constraintId, grantId)
	}

	// Clone the grant, its radio ports, constraints, except for the
	// modification.
	newGrant := grant.Clone(true)
	newGrant.Constraints[gcIdx].Constraint.MaxEirp = *constraintChange.MaxEirp

	replaceMsg := "replaced by alarm service"
	if constraintChange.Description != nil {
		replaceMsg += ": " + *constraintChange.Description
	}

	if err := vw.gc.ReplaceGrant(&grant, newGrant, replaceMsg, &constraintChangeId, observationId); err != nil {
		return codes.Internal, err
	}

	return codes.OK, nil
}

func (vw *AlarmWatcher) MessageHandler(resp *alarm.SubscribeResponse) (abort bool) {
	if resp.Events == nil {
		return
	}

	for _, e := range resp.Events {
		if e.Header == nil || e.Header.Type != int32(event.EventType_ET_ACTION) {
			continue
		}

		switch e.Header.Code {
		case int32(alarm.EventCode_EC_GRANTCHANGE):
			grantChange := e.GetGrantChange()
			log.Debug().Msg(fmt.Sprintf("AlarmWatcher: %+v", grantChange))
			if grantChange != nil {
				if code, err := vw.GrantChangeHandler(grantChange); err != nil {
					log.Error().Err(err).Msg(fmt.Sprintf("error handling grant change event: %s (%+v) (%+v)", err.Error(), code, grantChange))
				}
			}
		case int32(alarm.EventCode_EC_CONSTRAINTCHANGE):
			constraintChange := e.GetConstraintChange()
			log.Debug().Msg(fmt.Sprintf("AlarmWatcher: %+v", constraintChange))
			if constraintChange != nil {
				if code, err := vw.ConstraintChangeHandler(constraintChange); err != nil {
					log.Error().Err(err).Msg(fmt.Sprintf("error handling constraint change event: %s (%+v) (%+v)", err.Error(), code, constraintChange))
				}
			}
		default:
			log.Debug().Msg(fmt.Sprintf("AlarmWatcher: unhandled msg %+v", e))
		}
	}

	return false
}

func NewAlarmWatcher(ctx context.Context, config *config.Config, db *gorm.DB, rclient *zmsclient.ZmsClient, sm *subscription.SubscriptionManager[*subscription.Event], gc *grantcontroller.GrantController) (vw *AlarmWatcher) {
	filters := make([]*event.EventFilter, 0, 1)
	f := &event.EventFilter{
		Types: []int32{int32(event.EventType_ET_ACTION)},
		Codes: []int32{int32(alarm.EventCode_EC_GRANTCHANGE),int32(alarm.EventCode_EC_CONSTRAINTCHANGE)},
	}
	filters = append(filters, f)

	vw = &AlarmWatcher{
		config:  config,
		db:      db,
		sm:      sm,
		gc:      gc,
		rclient: rclient,
	}

	t := true
	sub := rclient.NewAlarmSubscription(
		vw.MessageHandler, vw.StatusHandler, filters, &t, &t)
	vw.sub = sub

	sub.Run(ctx)

	return vw
}
