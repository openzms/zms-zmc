// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package northbound

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"net/http"
	"slices"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"

	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/client"
	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/policy"
	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/subscription"
	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/token"

	identity "gitlab.flux.utah.edu/openzms/zms-api/go/zms/identity/v1"
	zmc "gitlab.flux.utah.edu/openzms/zms-api/go/zms/zmc/v1"
	event "gitlab.flux.utah.edu/openzms/zms-api/go/zms/event/v1"

	"gitlab.flux.utah.edu/openzms/zms-zmc/pkg/store"
	"gitlab.flux.utah.edu/openzms/zms-zmc/pkg/version"
)

func (s *Server) SetupHealthRoutes() error {
	s.ginHealth.GET("/health/alive", s.GetHealthAlive)
	s.ginHealth.GET("/health/ready", s.GetHealthReady)
	return nil
}

func (s *Server) GetHealthAlive(c *gin.Context) {
	c.Status(http.StatusOK)
}

func (s *Server) GetHealthReady(c *gin.Context) {
	if db, err := s.db.DB(); err == nil {
		if err := db.Ping(); err == nil {
			c.Status(http.StatusOK)
			return
		}
	}
	c.Status(http.StatusServiceUnavailable)
}

func CheckElaborateMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		if tokenString := c.GetHeader("X-Api-Elaborate"); tokenString == "1" || tokenString == "true" || tokenString == "True" {
			c.Set("zms.elaborate", true)
		} else {
			c.Set("zms.elaborate", false)
		}
	}
}

func CheckForceUpdateMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		if tokenString := c.GetHeader("X-Api-Force-Update"); tokenString == "1" || tokenString == "true" || tokenString == "True" {
			c.Set("zms.force-update", true)
		} else {
			c.Set("zms.force-update", false)
		}
	}
}

func CheckTokenValue(s *Server, tokenString string) (*client.CachedToken, int, error) {
	if err := token.Validate(tokenString); err != nil {
		return nil, http.StatusBadRequest, fmt.Errorf("invalid token")
	}

	if tok, err := s.rclient.LookupToken(tokenString); tok != nil && err == nil {
		return tok, http.StatusOK, nil
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	var identityClient identity.IdentityClient
	var terr error
	if identityClient, terr = s.rclient.GetIdentityClient(ctx); terr != nil || identityClient == nil {
		return nil, http.StatusBadRequest, fmt.Errorf("identity service unavailable")
	}

	ctx, cancel = context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	tt := true
	req := identity.GetTokenRequest{
		Header: &identity.RequestHeader{
			Elaborate: &tt,
		},
		Token: tokenString,
	}
	resp, err := identityClient.GetToken(ctx, &req)
	if err != nil {
		rpcErr, _ := status.FromError(err)
		if rpcErr.Code() == codes.NotFound {
			return nil, http.StatusNotFound, fmt.Errorf("token not found")
		} else if rpcErr.Code() == codes.Unauthenticated {
			return nil, http.StatusUnauthorized, fmt.Errorf(rpcErr.Message())
		} else {
			return nil, http.StatusInternalServerError, fmt.Errorf("identity service error: %v", rpcErr.Message())
		}
	}
	if tok, err := s.rclient.CheckAndCacheToken(resp.Token); err != nil {
		return nil, http.StatusForbidden, fmt.Errorf("invalid token: %s", err.Error())
	} else {
		return tok, http.StatusOK, nil
	}
}

func CheckTokenMiddleware(s *Server) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error
		tokenString := c.GetHeader("X-Api-Token")
		if tokenString == "" {
			c.Set("zms.token", nil)
			return
		}
		var tok *client.CachedToken
		var code int
		if tok, code, err = CheckTokenValue(s, tokenString); err != nil {
			c.AbortWithStatusJSON(code, gin.H{"error": err.Error()})
			return
		} else {
			c.Set("zms.token", tok)
		}
	}
}

func (s *Server) CheckPolicyMiddlewareExt(t *client.CachedToken, targetUserId *uuid.UUID, targetElementId *uuid.UUID, policies []policy.Policy) (bool, string, *client.CachedRoleBinding) {
	log.Debug().Msg(fmt.Sprintf("token: %+v", t))
	for _, p := range policies {
		log.Debug().Msg(fmt.Sprintf("policy: %s", p.Name))
		for _, trb := range t.RoleBindings {
			if match := p.Check(trb.Role.Value, &t.UserId, trb.ElementId, targetUserId, targetElementId); match == true {
				log.Debug().Msg(fmt.Sprintf("policy match: %s", p.Name))
				return true, p.Name, &trb
			} else {
				log.Debug().Msg(fmt.Sprintf("policy mismatch: %s", p.Name))
			}
		}
	}
	return false, "", nil
}

func (s *Server) CheckPolicyMiddleware(t *client.CachedToken, targetUserId *uuid.UUID, targetElementId *uuid.UUID, policies []policy.Policy) (bool, string) {
	log.Debug().Msg(fmt.Sprintf("token: %+v", t))
	m, n, _ := s.CheckPolicyMiddlewareExt(t, targetUserId, targetElementId, policies)
	return m, n
}

func (s *Server) RequireContextToken(c *gin.Context) (*client.CachedToken, error) {
	value, exists := c.Get("zms.token")
	if !exists || value == nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "missing X-Api-Token"})
		return nil, errors.New("missing X-Api-Token")
	}
	return value.(*client.CachedToken), nil
}

func CorsMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Next()
	}
}

func (s *Server) SetupRoutes() error {
	v1 := s.gin.Group("/v1")

	v1.Use(CorsMiddleware())
	v1.Use(CheckElaborateMiddleware())
	v1.Use(CheckForceUpdateMiddleware())
	v1.Use(CheckTokenMiddleware(s))

	v1.GET("/version", s.GetVersion)

	v1.GET("/zones", s.GetZoneList)

	v1.GET("/spectrum", s.ListSpectrum)
	v1.POST("/spectrum", s.CreateSpectrum)
	v1.GET("/spectrum/:spectrum_id", s.GetSpectrum)
	v1.PUT("/spectrum/:spectrum_id", s.UpdateSpectrum)
	v1.DELETE("/spectrum/:spectrum_id", s.DeleteSpectrum)
	v1.POST("/spectrum/:spectrum_id/constraints", s.CreateSpectrumConstraint)
	v1.GET("/spectrum/:spectrum_id/constraints/:constraint_id", s.GetSpectrumConstraint)
	v1.PUT("/spectrum/:spectrum_id/constraints/:constraint_id", s.UpdateSpectrumConstraint)
	v1.DELETE("/spectrum/:spectrum_id/constraints/:constraint_id", s.DeleteSpectrumConstraint)
	v1.POST("/spectrum/:spectrum_id/policies", s.CreateSpectrumPolicy)
	v1.GET("/spectrum/:spectrum_id/policies/:policy_id", s.GetSpectrumPolicy)
	v1.PUT("/spectrum/:spectrum_id/policies/:policy_id", s.UpdateSpectrumPolicy)
	v1.DELETE("/spectrum/:spectrum_id/policies/:policy_id", s.DeleteSpectrumPolicy)

	v1.GET("/radios", s.ListRadios)
	v1.POST("/radios", s.CreateRadio)
	v1.GET("/radios/:radio_id", s.GetRadio)
	v1.PUT("/radios/:radio_id", s.UpdateRadio)
	v1.DELETE("/radios/:radio_id", s.DeleteRadio)
	v1.GET("/radios/:radio_id/ports", s.ListRadioPorts)
	v1.POST("/radios/:radio_id/ports", s.CreateRadioPort)
	v1.GET("/radios/:radio_id/ports/:radio_port_id", s.GetRadioPort)
	v1.PUT("/radios/:radio_id/ports/:radio_port_id", s.UpdateRadioPort)
	v1.DELETE("/radios/:radio_id/ports/:radio_port_id", s.DeleteRadioPort)

	v1.GET("/antennas", s.ListAntennas)
	v1.POST("/antennas", s.CreateAntenna)
	v1.GET("/antennas/:antenna_id", s.GetAntenna)
	v1.PUT("/antennas/:antenna_id", s.UpdateAntenna)
	v1.DELETE("/antennas/:antenna_id", s.DeleteAntenna)

	v1.GET("/locations", s.ListLocations)
	v1.POST("/locations", s.CreateLocation)
	v1.GET("/locations/:location_id", s.GetLocation)
	v1.PUT("/locations/:location_id", s.UpdateLocation)
	v1.DELETE("/locations/:location_id", s.DeleteLocation)

	v1.GET("/grants", s.ListGrants)
	v1.POST("/grants", s.CreateGrant)
	v1.GET("/grants/:grant_id", s.GetGrant)
	v1.PUT("/grants/:grant_id", s.UpdateGrant)
	v1.DELETE("/grants/:grant_id", s.DeleteGrant)
	v1.POST("/grants/:grant_id", s.ReplaceGrant)
	v1.POST("/grants/:grant_id/replacement", s.CreateGrantReplacement)
	v1.POST("/grants/:grant_id/constraints", s.CreateGrantConstraint)
	v1.POST("/grants/:grant_id/intconstraints", s.CreateGrantIntConstraint)
	v1.POST("/grants/:grant_id/rtintconstraints", s.CreateGrantRtIntConstraint)
	v1.PUT("/grants/:grant_id/opstatus", s.UpdateGrantOpStatus)

	v1.GET("/claims", s.listClaims)
	v1.POST("/claims", s.CreateClaim)
	v1.GET("/claims/:claim_id", s.GetClaim)
	v1.PUT("/claims/:claim_id", s.UpdateClaim)
	v1.DELETE("/claims/:claim_id", s.DeleteClaim)

	v1.GET("/monitors", s.ListMonitors)
	v1.POST("/monitors", s.CreateMonitor)
	v1.GET("/monitors/:monitor_id", s.GetMonitor)
	v1.PUT("/monitors/:monitor_id", s.UpdateMonitor)
	v1.DELETE("/monitors/:monitor_id", s.DeleteMonitor)

	v1.GET("/subscriptions", s.ListSubscriptions)
	v1.POST("/subscriptions", s.CreateSubscription)
	v1.DELETE("/subscriptions/:subscription_id", s.DeleteSubscription)
	v1.GET("/subscriptions/:subscription_id/events", s.GetSubscriptionEvents)

	v1.POST("/tardys", s.CreateTardysReservation)

	return nil
}

func GetPaginateParams(c *gin.Context) (int, int) {
	page, _ := strconv.Atoi(c.Query("page"))
	if page < 1 {
		page = 1
	}
	itemsPerPage, _ := strconv.Atoi(c.Query("items_per_page"))
	if itemsPerPage < 10 {
		itemsPerPage = 10
	} else if itemsPerPage > 100 {
		itemsPerPage = 100
	}
	return page, itemsPerPage
}

func Paginate(page int, itemsPerPage int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		offset := (page - 1) * itemsPerPage
		return db.Offset(offset).Limit(itemsPerPage)
	}
}

func (s *Server) GetVersion(c *gin.Context) {
	if _, err := s.RequireContextToken(c); err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}
	c.JSON(http.StatusOK, version.GetVersion())
}

func (s *Server) GetZoneList(c *gin.Context) {
	var err error

	page, itemsPerPage := GetPaginateParams(c)

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	if tok, err = s.RequireContextToken(c); err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	if match, _ := s.CheckPolicyMiddleware(tok, nil, nil, policy.ViewerPolicy); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	var zoneList []store.Zone
	q := s.db
	if c.GetBool("zms.elaborate") {
		q = q.Preload("Area.Points")
	}
	q = q.Order("Zones.updated_at desc")

	var total int64
	cres := q.Model(&store.Zone{}).Count(&total)
	if cres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": cres.Error.Error()})
		return
	}
	qres := q.Scopes(store.Paginate(page, itemsPerPage)).Find(&zoneList)
	if qres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": cres.Error.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"zones": zoneList, "page": page, "total": total, "pages": int(math.Ceil(float64(total) / float64(itemsPerPage)))})
}

type ListSpectrumQueryParams struct {
	// NB: validator validates this, but gin-gonic does not have support for
	// using uuid.UUID as a type via TextUnmarshaler
	// (https://github.com/gin-gonic/gin/pull/3045), so we have to
	// workaround and do the extra unmarshal.
	//ElementId *string `form:"element_id" binding:"omitempty,uuid"`
	Spectrum *string `form:"spectrum" binding:"omitempty"`
	Enabled  *bool   `form:"enabled" binding:"omitempty"`
	Approved *bool   `form:"approved" binding:"omitempty"`
	Denied   *bool   `form:"denied" binding:"omitempty"`
	Deleted  *bool   `form:"deleted" binding:"omitempty"`
	Current  *bool   `form:"current" binding:"omitempty"`
	Sort     *string `form:"sort" binding:"omitempty,oneof=element_id name enabled created_at updated_at denied_at starts_at expires_at deleted_at"`
	SortAsc  *bool   `form:"sort_asc" binding:"omitempty"`
}

func (s *Server) ListSpectrum(c *gin.Context) {
	var err error

	// Check optional filter query parameters.
	params := ListSpectrumQueryParams{}
	if err = c.ShouldBindQuery(&params); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// Unmarshal query uuids.
	var elementId *uuid.UUID
	if p, exists := c.GetQuery("element_id"); exists {
		if idParsed, err := uuid.Parse(p); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid element_id uuid"})
			return
		} else {
			elementId = &idParsed
		}
	}

	page, itemsPerPage := GetPaginateParams(c)

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	if tok, err = s.RequireContextToken(c); err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	// Check policy.
	//
	// NB: we want approved spectrum chunks show to any Viewer.
	// Unapproved/denied is only shown to users in elements that provided
	// the spectrum, or to admins.
	policies := policy.MakeMatchAdminOrElementRoleOrViewerPolicy(policy.RoleViewer, policy.GreaterOrEqual)
	var match bool
	var policyName string
	if match, policyName = s.CheckPolicyMiddleware(tok, nil, elementId, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Admins can filter for anything; Viewers (e.g. if they have no element
	// binding) cannot filter; other users who have roles in Elements can
	// filter within those Elements.
	var elementIdList []*uuid.UUID
	if elementId != nil {
		elementIdList = []*uuid.UUID{elementId}
	} else if policyName != policy.MatchAdmin {
		elementIdList = tok.GetElementIds()
	}

	defTrue := true
	defFalse := false
	if policyName == policy.MatchViewer && len(elementIdList) == 0 {
		if (params.Approved != nil && !*params.Approved) ||
			(params.Denied != nil && *params.Denied) ||
			(params.Deleted != nil && *params.Deleted) ||
			(params.Enabled != nil && !*params.Enabled) {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized (viewer cannot filter)"})
			return
		}
		if params.Approved == nil {
			params.Approved = &defTrue
		}
		if params.Deleted == nil {
			params.Deleted = &defFalse
		}
		if params.Denied == nil {
			params.Denied = &defFalse
		}
		if params.Enabled == nil {
			params.Enabled = &defTrue
		}
	}

	var spectrumList []store.Spectrum
	q := s.db
	if len(elementIdList) > 0 {
		q = q.Where("Spectrums.element_id in ?", elementIdList)
	}
	if params.Current != nil {
		t := time.Now().UTC()
		if *params.Current {
			q = q.Where("Spectrums.starts_at <= ? and (Spectrums.expires_at is NULL or Spectrums.expires_at > ?)", t, t)
		} else {
			q = q.Where("not (Spectrums.starts_at <= ? and (Spectrums.expires_at is NULL or Spectrums.expires_at > ?))", t, t)
		}
	}
	if params.Enabled != nil {
		if !*params.Enabled {
			q = q.Where("not Spectrums.enabled")
		} else {
			q = q.Where("Spectrums.enabled")
		}
	}
	if params.Approved != nil {
		if !*params.Approved {
			q = q.Where("Spectrums.approved_at is NULL")
		} else {
			q = q.Where("Spectrums.approved_at is not NULL")
		}
	}
	if params.Denied != nil {
		if !*params.Denied {
			q = q.Where("Spectrums.denied_at is NULL")
		} else {
			q = q.Where("Spectrums.denied_at is not NULL")
		}
	}
	if params.Deleted == nil || !*params.Deleted {
		q = q.Where("Spectrums.deleted_at is NULL")
	} else {
		q = q.Where("Spectrums.deleted_at is not NULL")
	}
	if params.Spectrum != nil {
		q = q.Where(
			s.db.Where(store.MakeILikeClause(s.config, "Spectrums.name"), fmt.Sprintf("%%%s%%", *params.Spectrum)).
				Or(store.MakeILikeClause(s.config, "Spectrums.description"), fmt.Sprintf("%%%s%%", *params.Spectrum)))
	}
	// Viewers only can see wildcard policy or policy for their Element roles.
	if policyName == policy.MatchViewer {
		q = q.Preload("Policies", "element_id in ?", elementIdList)
	}
	if c.GetBool("zms.elaborate") {
		q = q.Preload("Constraints.Constraint")
	}
	if c.GetBool("zms.elaborate") && (policyName == policy.MatchAdmin || policyName == policy.MatchElementRole) {
		q = q.Preload("Policies")
	}
	sortDir := "desc"
	if params.SortAsc != nil && *params.SortAsc {
		sortDir = "asc"
	}
	if params.Sort != nil {
		q = q.Order("Spectrums." + *params.Sort + " " + sortDir)
	} else {
		q = q.Order("Spectrums.updated_at " + sortDir)
	}

	var total int64
	cres := q.Model(&store.Spectrum{}).Count(&total)
	if cres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": cres.Error.Error()})
		return
	}
	qres := q.Scopes(store.Paginate(page, itemsPerPage)).Find(&spectrumList)
	if qres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": cres.Error.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"spectrum": spectrumList, "page": page, "total": total, "pages": int(math.Ceil(float64(total) / float64(itemsPerPage)))})
}

func (s *Server) CreateSpectrum(c *gin.Context) {
	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var dm store.Spectrum
	if err := c.ShouldBindJSON(&dm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if dm.ExpiresAt != nil && dm.ExpiresAt.Compare(dm.StartsAt) < 1 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "expiration before start time"})
		return
	}
	for _, dc := range dm.Constraints {
		if dc.Constraint.AreaId != nil {
			if res := s.db.Where(&store.Area{Id: *dc.Constraint.AreaId}).First(&store.Area{}); res.RowsAffected < 1 {
				c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": fmt.Sprintf("area %s not found", *dc.Constraint.AreaId)})
				return
			}
		}
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleOperator, policy.GreaterOrEqual)
	match, policyName := s.CheckPolicyMiddleware(tok, nil, &dm.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}
	if policyName != policy.MatchAdmin {
		if dm.ApprovedAt != nil {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized to create approved Spectrum"})
			return
		}
	}

	// Additional post-auth validation.

	// NB: for now, auto-approved contributed spectrum.
	t := time.Now().UTC()
	spectrum := store.Spectrum{
		Id: uuid.New(), ElementId: dm.ElementId, Name: dm.Name, Description: dm.Description,
		Url: dm.Url, ExtId: dm.ExtId, Enabled: dm.Enabled, CreatorId: tok.UserId,
		StartsAt: dm.StartsAt, ExpiresAt: dm.ExpiresAt, ApprovedAt: &t, //dm.ApprovedAt,
	}
	for _, dmc := range dm.Constraints {
		dmc.Id = uuid.New()
		dmc.SpectrumId = spectrum.Id
		dmc.ConstraintId = uuid.New()
		dmc.Constraint.Id = dmc.ConstraintId
		spectrum.Constraints = append(spectrum.Constraints, dmc)
	}
	for _, dmp := range dm.Policies {
		dmp.Id = uuid.New()
		dmp.SpectrumId = spectrum.Id
		spectrum.Policies = append(spectrum.Policies, dmp)
	}

	if res := s.db.Create(&spectrum); res.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	} else {
		c.JSON(http.StatusCreated, spectrum)
	}

	// Send a spectrum creation event.
	oid := spectrum.Id.String()
	userIdStr := spectrum.CreatorId.String()
	elementIdStr := spectrum.ElementId.String()
	eh := subscription.EventHeader{
		Type:       int32(event.EventType_ET_CREATED),
		Code:       int32(zmc.EventCode_EC_SPECTRUM),
		SourceType: int32(event.EventSourceType_EST_ZMC),
		SourceId:   s.config.ServiceId,
		Id:         uuid.New().String(),
		ObjectId:   &oid,
		Time:       &spectrum.CreatedAt,
		UserId:     &userIdStr,
		ElementId:  &elementIdStr,
	}
	e := subscription.Event{
		Header: eh,
		Object: &spectrum,
	}
	go s.sm.Notify(&e)
}

func (s *Server) GetSpectrum(c *gin.Context) {
	var err error

	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("spectrum_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	var spectrum store.Spectrum
	if res := s.db.First(&spectrum, id); res.RowsAffected < 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Check policy.
	//
	// NB: we want approved spectrum chunks show to any Viewer.
	// Unapproved/denied is only shown to users in elements that provided
	// the spectrum, or to admins.
	policies := policy.MakeMatchAdminOrElementRoleOrViewerPolicy(policy.RoleViewer, policy.GreaterOrEqual)
	var match bool
	var policyName string
	if match, policyName = s.CheckPolicyMiddleware(tok, nil, &spectrum.ElementId, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	q := s.db
	// Viewers only can see wildcard policy or policy for their Element roles.
	if policyName == policy.MatchViewer {
		tokElements := tok.GetElementIds()
		cond := s.db.Where("policy.element_id is NULL")
		for _, teid := range tokElements {
			cond = cond.Or(&store.Policy{ElementId: teid})
		}
		q = q.Joins("Policy", cond)
	}

	if c.GetBool("zms.elaborate") {
		q = q.Preload("Constraints.Constraint")
	}
	if c.GetBool("zms.elaborate") && (policyName == policy.MatchAdmin || policyName == policy.MatchElementRole) {
		q = q.Preload("Policies")
	}

	res := q.First(&spectrum, id)
	if res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Viewers can only see the spectrum if approved and not denied nor deleted
	if policyName == policy.MatchViewer && (spectrum.ApprovedAt == nil || spectrum.DeniedAt != nil || spectrum.DeletedAt != nil) {
		c.AbortWithStatus(http.StatusForbidden)
		return
	}

	c.JSON(http.StatusOK, spectrum)
}

func (s *Server) HandleSpectrumChanged(spectrum *store.Spectrum, grants []store.Grant, claims []store.Claim, msg string, updaterId uuid.UUID, updateType event.EventType) {
	log.Debug().Msg(fmt.Sprintf("HandleSpectrumChanged(%s, %d/%d, %+v): %s", spectrum.Id, len(grants), len(claims), updateType, msg))

	//
	// Revoke the grants with the provided message.
	//
	for _, g := range grants {
		if err := s.gc.ChangeGrantStatus(&g, store.StatusRevoked, msg, &updaterId, nil); err != nil {
			m := fmt.Sprintf("HandleSpectrumChanged: error revoking grant %s after updating spectrum %s: %s", g.Id, spectrum.Id, err.Error())
			log.Error().Err(err).Msg(m)
		} else {
			m := fmt.Sprintf("HandleSpectrumChanged: revoked grant %s after updating spectrum %s", g.Id, spectrum.Id)
			log.Info().Msg(m)
		}
	}

	//
	// Delete the claims (and their grants) with the provided message.
	//
	for _, claim := range claims {
		t := time.Now().UTC()
		claim.DeletedAt = &t
		claim.UpdaterId = &updaterId
		s.db.Save(&claim)
		m := fmt.Sprintf("HandleSpectrumChanged: deleted claim %s", claim.Id)
		log.Info().Msg(m)

		if err := s.gc.ChangeGrantStatus(claim.Grant, store.StatusDeleted, "", &updaterId, nil); err != nil {
			m := fmt.Sprintf("HandleSpectrumChanged: error deleting claim %s grant %s: %s", claim.Id, claim.GrantId, err.Error())
			log.Error().Err(err).Msg(m)
		} else {
			m := fmt.Sprintf("HandleSpectrumChanged: deleted claim %s grant %s", claim.Id, claim.GrantId)
			log.Info().Msg(m)
		}
	}

	// Send a spectrum event.
	oid := spectrum.Id.String()
	userIdStr := spectrum.CreatorId.String()
	elementIdStr := spectrum.ElementId.String()
	eh := subscription.EventHeader{
		Type:       int32(updateType),
		Code:       int32(zmc.EventCode_EC_SPECTRUM),
		SourceType: int32(event.EventSourceType_EST_ZMC),
		SourceId:   s.config.ServiceId,
		Id:         uuid.New().String(),
		ObjectId:   &oid,
		Time:       spectrum.UpdatedAt,
		UserId:     &userIdStr,
		ElementId:  &elementIdStr,
	}
	e := subscription.Event{
		Header: eh,
		Object: spectrum,
	}
	go s.sm.Notify(&e)
}

func (s Server) LoadGrantsForSpectrum(spectrum *store.Spectrum, statusList []store.GrantStatus, startsAtChanged bool, expiresAtChanged bool) (grants []store.Grant, claims []store.Claim, err error) {
	qG := s.db.Where(&store.Grant{SpectrumId: &spectrum.Id}).
		Where("status in ?", statusList).
		Where("not is_claim")
	qC := s.db.
		Joins("join grants on claims.grant_id=grants.id and grants.spectrum_id = ? and grants.status in ? and grants.is_claim", spectrum.Id, statusList).
		Preload("Grant")
	if spectrum.Enabled {
		var qGs *gorm.DB
		var qCs *gorm.DB
		if startsAtChanged {
			qGs = s.db.Where("grants.starts_at < ?", spectrum.StartsAt)
			qCs = s.db.Where("grants.starts_at < ?", spectrum.StartsAt)
		}
		if expiresAtChanged && spectrum.ExpiresAt != nil {
			if qGs == nil {
				qGs = s.db.Where("grants.expires_at > ?", *spectrum.ExpiresAt)
				qCs = s.db.Where("grants.expires_at > ?", *spectrum.ExpiresAt)
			} else {
				qGs = qGs.Or("grants.expires_at > ?", *spectrum.ExpiresAt)
				qCs = qGs.Or("grants.expires_at > ?", *spectrum.ExpiresAt)
			}
		}
		if qGs != nil {
			qG.Where(qGs)
		}
		if qCs != nil {
			qC.Where(qCs)
		}
	}
	if resG := qG.Find(&grants); resG.Error != nil {
		return nil, nil, resG.Error
	}
	if resC := qC.Find(&claims); resC.Error != nil {
		return nil, nil, resC.Error
	}

	return grants, claims, nil
}

func (s *Server) UpdateSpectrum(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("spectrum_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Initial input validation.
	var um store.Spectrum
	if err := c.ShouldBindJSON(&um); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	forceUpdate := c.GetBool("zms.force-update")

	// We need to compare the Spectrum.ElementId and Spectrum.CreatorId with
	// the caller, to validate the match-element-role and match-user
	// policies.
	//
	// NB: since we intend to update the spectrum, lock its row at load
	// time in a nested transaction.
	var spectrum store.Spectrum
	var creatorId *uuid.UUID
	res := s.db.First(&spectrum, id)
	if res.Error != nil || res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	creatorId = &spectrum.CreatorId

	// Check policy.  Element managers and up can modify any managed
	// spectrum.  Operators can only modify the spectrum they created.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, creatorId, &spectrum.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	t := time.Now().UTC()

	// Validations that require authentication.
	if spectrum.DeniedAt != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "cannot modify denied spectrum"})
		return
	}
	if spectrum.DeletedAt != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "cannot modify deleted spectrum"})
		return
	}
	if spectrum.ExpiresAt != nil && spectrum.ExpiresAt.Compare(t) < 0 {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "cannot modify expired spectrum"})
		return
	}

	// Only send spectrum update events if `enabled` changes.
	enabledChanged := false
	startsChanged := false
	expiresChanged := false
	if spectrum.Enabled != um.Enabled {
		enabledChanged = true
	}
	if spectrum.StartsAt.Compare(um.StartsAt) != 0 {
		startsChanged = true
	}
	if (spectrum.ExpiresAt == nil && um.ExpiresAt != nil) ||
		(spectrum.ExpiresAt != nil && um.ExpiresAt == nil) ||
		(spectrum.ExpiresAt != nil && um.ExpiresAt != nil && spectrum.ExpiresAt.Compare(*um.ExpiresAt) != 0) {
		expiresChanged = true
	}

	// Check if spectrum is in use, if spectrum is being disabled, or if it
	// was enabled and the starts/expires times changed.  If not, we simply
	// notify.  If so, error if not forceUpdate.
	//
	// If not forceUpdate, must lock Spectrum and test/update while locked.
	// We created a nested transaction to handle that; we don't want to
	// pollute further grant state mutations from the default transaction.
	tx := s.db.Begin()
	if res := tx.Clauses(clause.Locking{Strength: "UPDATE"}).First(&spectrum, id); res.Error != nil || res.RowsAffected != 1 {
		tx.Rollback()
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	if spectrum.Enabled && (enabledChanged || startsChanged || expiresChanged) {
		var grants []store.Grant
		qG := tx.Where(&store.Grant{SpectrumId: &id}).
			Where("status in ?", store.AnyOperationalGrantStatusList)
		if !enabledChanged {
			var qGs *gorm.DB
			if startsChanged {
				qGs = tx.Where("starts_at < ?", um.StartsAt)
			}
			if expiresChanged {
				if qGs == nil {
					qGs = tx.Where("expires_at > ?", *um.ExpiresAt)
				} else {
					qGs = qGs.Or("expires_at > ?", *um.ExpiresAt)
				}
			}
			qG.Where(qGs)
		}
		if resG := qG.Find(&grants); res.Error != nil {
			tx.Rollback()
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
			return
		} else if resG.RowsAffected > 0 && !forceUpdate {
			tx.Rollback()
			c.AbortWithStatusJSON(http.StatusConflict, gin.H{"error": "Spectrum in use"})
			return
		}
	}

	// Assume obedience/compliance to the x-immutable-on-put annotations,
	// and only update mutable fields.

	// Change some fields regardless, without sending notifications.
	spectrum.Name = um.Name
	spectrum.Description = um.Description
	spectrum.Url = um.Url
	spectrum.ExtId = um.ExtId
	spectrum.Enabled = um.Enabled
	spectrum.StartsAt = um.StartsAt
	spectrum.ExpiresAt = um.ExpiresAt

	spectrum.UpdaterId = &tok.UserId
	if sres := tx.Save(&spectrum); sres.Error != nil {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": sres.Error.Error()})
		return
	} else if tres := tx.Commit(); tres.Error != nil {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": tres.Error.Error()})
		return
	}

	// Refresh after updates.
	q := s.db
	if c.GetBool("zms.elaborate") {
		q = q.Preload("Constraints.Constraint")
	}
	if c.GetBool("zms.elaborate") {
		q = q.Preload("Policies")
	}
	q.First(&spectrum, id)

	// Now that we have atomically changed the Spectrum object, revoke
	// grants and delete claims (and claim grants), and notify.
	var grants []store.Grant
	var claims []store.Claim
	if (enabledChanged && !um.Enabled) || startsChanged || expiresChanged {
		var lerr error
		grants, claims, lerr = s.LoadGrantsForSpectrum(&spectrum, store.AnyOperationalGrantStatusList, startsChanged, expiresChanged)
		if lerr != nil {
			log.Error().Msg("UpdateSpectrum: " + lerr.Error())
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": lerr.Error()})
			return
		}
	}

	cMsgBits := ""
	if enabledChanged {
		if !um.Enabled {
			cMsgBits += "disabled"
		} else {
			cMsgBits += "enabled"
		}
	}
	if startsChanged {
		if cMsgBits != "" {
			cMsgBits += ", "
		}
		cMsgBits += "startsAt changed"
	}
	if expiresChanged {
		if cMsgBits != "" {
			cMsgBits += ", "
		}
		cMsgBits += "expiresAt changed"
	}
	cMsg := fmt.Sprintf("spectrum %s updated (%s)", id, cMsgBits)
	s.HandleSpectrumChanged(&spectrum, grants, claims, cMsg, tok.UserId, event.EventType_ET_UPDATED)

	lMsg := fmt.Sprintf("spectrum %s updated (%d grants/%d claims revoked/deleted)", id, len(grants), len(claims))
	log.Info().Msg(lMsg)

	c.JSON(http.StatusOK, spectrum)
}

func (s *Server) DeleteSpectrum(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("spectrum_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	forceUpdate := c.GetBool("zms.force-update")

	// We need to compare the Spectrum.ElementId and Spectrum.CreatorId with
	// the caller, to validate the match-element-role and match-user
	// policies.
	//
	// NB: since we intend to delete the spectrum, lock its row at load
	// time in a nested transaction.
	var spectrum store.Spectrum
	var creatorId *uuid.UUID
	res := s.db.First(&spectrum, id)
	if res.Error != nil || res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	creatorId = &spectrum.CreatorId

	// Check policy.  Element managers and up can delete any managed
	// spectrum.  Operators can only delete the spectrum they created.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, creatorId, &spectrum.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	t := time.Now().UTC()

	// Validations that require authentication.
	if spectrum.DeletedAt != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "spectrum already deleted"})
		return
	}

	// Check if spectrum is in use.  If not, we simply notify.  If so, error
	// if not forceUpdate.
	//
	// If not forceUpdate, must lock Spectrum and test/update while locked.
	// We created a nested transaction to handle that; we don't want to
	// pollute further grant state mutations from the default transaction.
	tx := s.db.Begin()
	if res := tx.Clauses(clause.Locking{Strength: "UPDATE"}).First(&spectrum, id); res.Error != nil || res.RowsAffected != 1 {
		tx.Rollback()
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	var grants []store.Grant
	qG := tx.Where(&store.Grant{SpectrumId: &id}).
		Where("status in ?", store.AnyOperationalGrantStatusList)
	if resG := qG.Find(&grants); res.Error != nil {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	} else if resG.RowsAffected > 0 && !forceUpdate {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusConflict, gin.H{"error": "Spectrum in use"})
		return
	}

	spectrum.DeletedAt = &t
	spectrum.Enabled = false
	spectrum.UpdaterId = &tok.UserId

	if sres := tx.Save(&spectrum); sres.Error != nil {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": sres.Error.Error()})
		return
	} else if tres := tx.Commit(); tres.Error != nil {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": tres.Error.Error()})
		return
	}

	// Now that we have atomically deleted the Spectrum object, revoke
	// grants and delete claims (and claim grants), and notify.
	grants, claims, lerr := s.LoadGrantsForSpectrum(&spectrum, store.AnyOperationalGrantStatusList, false, false)
	if lerr != nil {
		log.Error().Msg("DeleteSpectrum: " + lerr.Error())
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": lerr.Error()})
		return
	}
	cMsg := fmt.Sprintf("spectrum %s deleted", id)
	s.HandleSpectrumChanged(&spectrum, grants, claims, cMsg, tok.UserId, event.EventType_ET_DELETED)

	lMsg := fmt.Sprintf("spectrum %s deleted (%d grants/%d claims revoked/deleted)", id, len(grants), len(claims))
	log.Info().Msg(lMsg)

	c.Status(http.StatusOK)
}

func (s *Server) CreateSpectrumConstraint(c *gin.Context) {
	// Check required path parameters.
	var spectrumId uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("spectrum_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		spectrumId = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var dm store.Constraint
	if err := c.ShouldBindJSON(&dm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Look up Spectrum.
	var spectrum store.Spectrum
	if res := s.db.First(&spectrum, spectrumId); res.Error != nil || res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	if dm.AreaId != nil {
		if res := s.db.Where(&store.Area{Id: *dm.AreaId}).First(&store.Area{}); res.RowsAffected < 1 {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": fmt.Sprintf("area %s not found", *dm.AreaId)})
			return
		}
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, nil, &spectrum.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Additional post-auth validation.

	// NB: for now, spectrum is auto-approved, so allow new constraints as
	// well.
	t := time.Now().UTC()
	constraint := store.Constraint{
		Id: uuid.New(), MinFreq: dm.MinFreq, MaxFreq: dm.MaxFreq,
		Bandwidth: dm.Bandwidth, MaxEirp: dm.MaxEirp, MinEirp: dm.MinEirp,
		Exclusive: dm.Exclusive, AreaId: dm.AreaId,
	}
	spectrumConstraint := store.SpectrumConstraint{
		Id: uuid.New(), SpectrumId: spectrumId, Constraint: constraint,
	}

	if res := s.db.Create(&spectrumConstraint); res.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	} else {
		c.JSON(http.StatusCreated, constraint)
	}

	// Reload Spectrum for notification.
	if res := s.db.First(&spectrum, spectrumId).Preload("Constraints.Constraint"); res.Error != nil || res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	
	// Send a spectrum updated event.
	oid := spectrum.Id.String()
	userIdStr := spectrum.CreatorId.String()
	elementIdStr := spectrum.ElementId.String()
	eh := subscription.EventHeader{
		Type:       int32(event.EventType_ET_UPDATED),
		Code:       int32(zmc.EventCode_EC_SPECTRUM),
		SourceType: int32(event.EventSourceType_EST_ZMC),
		SourceId:   s.config.ServiceId,
		Id:         uuid.New().String(),
		ObjectId:   &oid,
		Time:       &t,
		UserId:     &userIdStr,
		ElementId:  &elementIdStr,
	}
	e := subscription.Event{
		Header: eh,
		Object: &spectrum,
	}
	go s.sm.Notify(&e)
}

func (s *Server) GetSpectrumConstraint(c *gin.Context) {
	// Check required path parameters.
	var spectrumId uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("spectrum_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		spectrumId = idParsed
	}
	var constraintId uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("constraint_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		constraintId = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// We need to compare the Spectrum.ElementId and Spectrum.CreatorId with
	// the caller, to validate the match-element-role and match-user
	// policies.
	var spectrum store.Spectrum
	var creatorId *uuid.UUID
	if res := s.db.First(&spectrum, spectrumId); res.Error != nil || res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	creatorId = &spectrum.CreatorId

	
	// Check policy.  Element managers and up can delete any managed
	// spectrum.  Operators can only delete the spectrum they created.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, creatorId, &spectrum.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	var spectrumConstraint store.SpectrumConstraint
	qC := s.db.Where(store.SpectrumConstraint{SpectrumId: spectrumId, ConstraintId: constraintId}).
		Preload("Constraint")
	if res := qC.First(&spectrumConstraint); res.Error != nil || res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	c.JSON(http.StatusOK, spectrumConstraint.Constraint)
}

func (s *Server) UpdateSpectrumConstraint(c *gin.Context) {
	// Check required path parameters.
	var spectrumId uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("spectrum_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		spectrumId = idParsed
	}
	var constraintId uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("constraint_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		constraintId = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var dm store.Constraint
	if err := c.ShouldBindJSON(&dm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	forceUpdate := c.GetBool("zms.force-update")

	// At the moment, updating a spectrum constraint is practically the same
	// as deleting a spectrum object.  So, do the locking on the spectrum
	// object, and then handle as a spectrum update, until
	// SpectrumConstraints are first-class objects.
	//
	// We need to compare the Spectrum.ElementId and Spectrum.CreatorId with
	// the caller, to validate the match-element-role and match-user
	// policies.
	var spectrum store.Spectrum
	var creatorId *uuid.UUID
	if res := s.db.First(&spectrum, spectrumId); res.Error != nil || res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	creatorId = &spectrum.CreatorId

	
	// Check policy.  Element managers and up can delete any managed
	// spectrum.  Operators can only delete the spectrum they created.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, creatorId, &spectrum.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	if dm.AreaId != nil {
		if res := s.db.Where(&store.Area{Id: *dm.AreaId}).First(&store.Area{}); res.RowsAffected < 1 {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": fmt.Sprintf("area %s not found", *dm.AreaId)})
			return
		}
	}

	// Check if spectrum is in use.  If not, we simply notify.  If so, error
	// if not forceUpdate.  Reload spectrum with locking in advance.
	//
	// If not forceUpdate, must lock Spectrum and test/update while locked.
	// We created a nested transaction to handle that; we don't want to
	// pollute further grant state mutations from the default transaction.
	tx := s.db.Begin()
	if res := tx.Clauses(clause.Locking{Strength: "UPDATE"}).First(&spectrum, spectrumId); res.Error != nil || res.RowsAffected != 1 {
		tx.Rollback()
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	var spectrumConstraint store.SpectrumConstraint
	qC := tx.Where(store.SpectrumConstraint{SpectrumId: spectrumId, ConstraintId: constraintId}).
		Preload("Constraint")
	if res := qC.First(&spectrumConstraint); res.Error != nil || res.RowsAffected != 1 {
		tx.Rollback()
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	var grants []store.Grant
	qG := tx.Where(&store.Grant{SpectrumId: &spectrum.Id}).
		Where("status in ?", store.AnyOperationalGrantStatusList)
	if res := qG.Find(&grants); res.Error != nil {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	} else if res.RowsAffected > 0 && !forceUpdate {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusConflict, gin.H{"error": "Spectrum in use"})
		return
	}

	constraint := &spectrumConstraint.Constraint
	constraint.MinFreq = dm.MinFreq
	constraint.MaxFreq = dm.MaxFreq
	constraint.Bandwidth = dm.Bandwidth
	constraint.MaxEirp = dm.MaxEirp
	constraint.MinEirp = dm.MinEirp
	constraint.AreaId = dm.AreaId

	//constraint.DeletedAt = &t
	//constraint.UpdaterId = &tok.UserId

	if sres := tx.Save(constraint); sres.Error != nil {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": sres.Error.Error()})
		return
	} else if tres := tx.Commit(); tres.Error != nil {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": tres.Error.Error()})
		return
	}

	// Now that we have atomically updated the constraint object, revoke
	// grants and delete claims (and claim grants), and notify.
	grants, claims, lerr := s.LoadGrantsForSpectrum(&spectrum, store.AnyOperationalGrantStatusList, false, false)
	if lerr != nil {
		log.Error().Msg("UpdateSpectrum: " + lerr.Error())
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": lerr.Error()})
		return
	}
	cMsg := fmt.Sprintf("spectrum constraint %s updated", constraintId)
	s.HandleSpectrumChanged(&spectrum, grants, claims, cMsg, tok.UserId, event.EventType_ET_UPDATED)

	lMsg := fmt.Sprintf("spectrum constraint %s updated (%d grants/%d claims revoked/deleted)", constraintId, len(grants), len(claims))
	log.Info().Msg(lMsg)

	c.JSON(http.StatusOK, constraint)
}

func (s *Server) DeleteSpectrumConstraint(c *gin.Context) {
	// Check required path parameters.
	var spectrumId uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("spectrum_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		spectrumId = idParsed
	}
	var constraintId uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("constraint_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		constraintId = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	forceUpdate := c.GetBool("zms.force-update")

	// At the moment, deleting a spectrum constraint is practically the same
	// as deleting a spectrum object.  So, do the locking on the spectrum
	// object, and then handle as a spectrum update, until
	// SpectrumConstraints are first-class objects.
	//
	// We need to compare the Spectrum.ElementId and Spectrum.CreatorId with
	// the caller, to validate the match-element-role and match-user
	// policies.
	var spectrum store.Spectrum
	var creatorId *uuid.UUID
	if res := s.db.First(&spectrum, spectrumId); res.Error != nil || res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	creatorId = &spectrum.CreatorId

	
	// Check policy.  Element managers and up can delete any managed
	// spectrum.  Operators can only delete the spectrum they created.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, creatorId, &spectrum.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Check if spectrum is in use.  If not, we simply notify.  If so, error
	// if not forceUpdate.  Reload spectrum with locking in advance.
	//
	// If not forceUpdate, must lock Spectrum and test/update while locked.
	// We created a nested transaction to handle that; we don't want to
	// pollute further grant state mutations from the default transaction.
	tx := s.db.Begin()
	if res := tx.Clauses(clause.Locking{Strength: "UPDATE"}).First(&spectrum, spectrumId); res.Error != nil || res.RowsAffected != 1 {
		tx.Rollback()
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	var spectrumConstraint store.SpectrumConstraint
	qC := tx.Where(store.SpectrumConstraint{SpectrumId: spectrumId, ConstraintId: constraintId}).
		Preload("Constraint")
	if res := qC.First(&spectrumConstraint); res.Error != nil || res.RowsAffected != 1 {
		tx.Rollback()
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	var grants []store.Grant
	qG := tx.Where(&store.Grant{SpectrumId: &spectrum.Id}).
		Where("status in ?", store.AnyOperationalGrantStatusList)
	if res := qG.Find(&grants); res.Error != nil {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	} else if res.RowsAffected > 0 && !forceUpdate {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusConflict, gin.H{"error": "Spectrum in use"})
		return
	}

	//constraint.DeletedAt = &t
	//constraint.UpdaterId = &tok.UserId

	if sres := tx.Delete(&spectrumConstraint); sres.Error != nil {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": sres.Error.Error()})
		return
	} else if sres := tx.Delete(&spectrumConstraint.Constraint); sres.Error != nil {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": sres.Error.Error()})
		return
	} else if tres := tx.Commit(); tres.Error != nil {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": tres.Error.Error()})
		return
	}

	// Now that we have atomically deleted the constraint object, revoke
	// grants and delete claims (and claim grants), and notify.
	grants, claims, lerr := s.LoadGrantsForSpectrum(&spectrum, store.AnyOperationalGrantStatusList, false, false)
	if lerr != nil {
		log.Error().Msg("DeleteSpectrum: " + lerr.Error())
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": lerr.Error()})
		return
	}
	cMsg := fmt.Sprintf("spectrum constraint %s deleted", constraintId)
	s.HandleSpectrumChanged(&spectrum, grants, claims, cMsg, tok.UserId, event.EventType_ET_UPDATED)

	lMsg := fmt.Sprintf("spectrum constraint %s deleted (%d grants/%d claims revoked/deleted)", constraintId, len(grants), len(claims))
	log.Info().Msg(lMsg)

	c.Status(http.StatusOK)
}

func (s *Server) CreateSpectrumPolicy(c *gin.Context) {
	// Check required path parameters.
	var spectrumId uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("spectrum_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		spectrumId = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var dm store.Policy
	if err := c.ShouldBindJSON(&dm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Look up Spectrum.
	var spectrum store.Spectrum
	if res := s.db.First(&spectrum, spectrumId); res.Error != nil || res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	// Check if policy for this SpectrumId,ElementId already exists.
	qExisting := s.db.Where(store.Policy{SpectrumId: spectrum.Id})
	if dm.ElementId == nil {
		qExisting = qExisting.Where("policies.element_id is NULL")
	} else {
		qExisting = qExisting.Where("policies.element_id = ?", *dm.ElementId)
	}
	if res := qExisting.Find(&store.Policy{}); res.Error != nil || res.RowsAffected != 0 {
		c.AbortWithStatusJSON(http.StatusConflict, gin.H{"error": "spectrum policy for this element_id already exists"})
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, nil, &spectrum.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Additional post-auth validation.

	t := time.Now().UTC()
	policy := store.Policy{
		Id: uuid.New(), SpectrumId: spectrum.Id, ElementId: dm.ElementId,
		Allowed: dm.Allowed, AutoApprove: dm.AutoApprove, Priority: dm.Priority,
		MaxDuration: dm.MaxDuration, WhenUnoccupied: dm.WhenUnoccupied,
		DisableEmitCheck: dm.DisableEmitCheck, AllowSkipAcks: dm.AllowSkipAcks,
		AllowInactive: dm.AllowInactive, AllowConflicts: dm.AllowConflicts,
	}

	if res := s.db.Create(&policy); res.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	} else {
		c.JSON(http.StatusCreated, policy)
	}

	// Reload Spectrum for notification.
	if res := s.db.First(&spectrum, spectrumId).Preload("Constraints.Constraint"); res.Error != nil || res.RowsAffected != 1 {
		return
	}

	// Send a spectrum updated event.
	oid := spectrum.Id.String()
	userIdStr := spectrum.CreatorId.String()
	elementIdStr := spectrum.ElementId.String()
	eh := subscription.EventHeader{
		Type:       int32(event.EventType_ET_UPDATED),
		Code:       int32(zmc.EventCode_EC_SPECTRUM),
		SourceType: int32(event.EventSourceType_EST_ZMC),
		SourceId:   s.config.ServiceId,
		Id:         uuid.New().String(),
		ObjectId:   &oid,
		Time:       &t,
		UserId:     &userIdStr,
		ElementId:  &elementIdStr,
	}
	e := subscription.Event{
		Header: eh,
		Object: &spectrum,
	}
	go s.sm.Notify(&e)
}

func (s *Server) GetSpectrumPolicy(c *gin.Context) {
	// Check required path parameters.
	var spectrumId uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("spectrum_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		spectrumId = idParsed
	}
	var policyId uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("policy_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		policyId = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// We need to compare the Spectrum.ElementId and Spectrum.CreatorId with
	// the caller, to validate the match-element-role and match-user
	// policies.
	var spectrum store.Spectrum
	var creatorId *uuid.UUID
	if res := s.db.First(&spectrum, spectrumId); res.Error != nil || res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	creatorId = &spectrum.CreatorId

	var spectrumPolicy store.Policy
	if res := s.db.First(&spectrumPolicy, policyId); res.Error != nil || res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Check policy.  Element managers and up can delete any managed
	// spectrum policy.  Operators can only delete the spectrum policy they created.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, creatorId, &spectrum.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	c.JSON(http.StatusOK, spectrumPolicy)
}

func (s *Server) UpdateSpectrumPolicy(c *gin.Context) {
	// Check required path parameters.
	var spectrumId uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("spectrum_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		spectrumId = idParsed
	}
	var policyId uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("policy_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		policyId = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var dm store.Policy
	if err := c.ShouldBindJSON(&dm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	forceUpdate := c.GetBool("zms.force-update")

	// At the moment, updating a spectrum policy is practically the same as
	// updating a spectrum object.  So, do the locking on the spectrum
	// object, and then handle as a spectrum update, until SpectrumPolicy
	// objects are first-class objects.  We lock so that no new or
	// transitioning grants can do so until they can read this row of
	// spectrum.
	//
	// We need to compare the Spectrum.ElementId and Spectrum.CreatorId with
	// the caller, to validate the match-element-role and match-user
	// policies.
	var spectrum store.Spectrum
	var creatorId *uuid.UUID
	if res := s.db.First(&spectrum, spectrumId); res.Error != nil || res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	creatorId = &spectrum.CreatorId

	var spectrumPolicy store.Policy
	if res := s.db.First(&spectrumPolicy, policyId); res.Error != nil || res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Check policy.  Element managers and up can update any managed
	// spectrum policy.  Operators can only update the spectrum policy they created.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, creatorId, &spectrum.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Validations that require authentication.
	if spectrumPolicy.SpectrumId != dm.SpectrumId || (spectrumPolicy.ElementId != nil && dm.ElementId == nil) || (spectrumPolicy.ElementId == nil && dm.ElementId != nil) || (spectrumPolicy.ElementId != nil && *spectrumPolicy.ElementId != *dm.ElementId) {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "cannot update spectrum policy spectrum or element ids"})
		return
	}

	// Check if spectrum is in use.  If not, we simply notify.  If so, error
	// if not forceUpdate.
	//
	// If not forceUpdate, must lock Spectrum and test/update while locked.
	// We created a nested transaction to handle that; we don't want to
	// pollute further grant state mutations from the default transaction.
	tx := s.db.Begin()
	if res := tx.Clauses(clause.Locking{Strength: "UPDATE"}).Preload("Policies").First(&spectrum, spectrumId); res.Error != nil || res.RowsAffected != 1 {
		tx.Rollback()
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	var grants []store.Grant
	var claims []store.Claim
	qG := tx
	qC := tx
	if spectrumPolicy.ElementId != nil {
		// If the policy we are deleting has an ElementId, just look for any
		// grants associated with both the target spectrum and policy.
		qG = qG.
			Where(&store.Grant{SpectrumId: &spectrum.Id, ElementId: *spectrumPolicy.ElementId, IsClaim: false}).
			Where("status in ?", store.AnyOperationalGrantStatusList)
		qC = qC.
			Joins("join grants on grants.spectrum_id = ? and grants.element_id = ? and grants.is_claim and grants.status in ?", spectrum.Id, *spectrumPolicy.ElementId,	store.AnyOperationalGrantStatusList).
			Preload("Grant")
	} else {
		//type SpectrumPolicyElementId struct {
		//	ElementId *uuid.UUID
		//
		// This is the wildcard policy, so any grants whose Spectrum
		// matches and whose element id is not specifically named as a
		// SpectrumId,ElementId policy would match.
		nonWildcardElementIdQuery := tx.
			Model(&store.Policy{}).
			Where("spectrum_id = ? and element_id is not NULL", spectrum.Id).
			Select("element_id")
		qG = qG.
			Where(&store.Grant{SpectrumId: &spectrum.Id}).
			Where("status in ?", store.AnyOperationalGrantStatusList).
			Where("element_id not in (?)", nonWildcardElementIdQuery)
		qC = qC.
			Joins("join grants on grants.spectrum_id = ? and grants.is_claim and grants.status in ? and grants.element_id not in (?)", spectrum.Id, store.AnyOperationalGrantStatusList, nonWildcardElementIdQuery).
			Preload("Grant")
	}

	// Run the query composed above to select all grants operating under the
	// spectrum policy to be deleted.
	if res := qG.Find(&grants); res.Error != nil {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	} else if res.RowsAffected > 0 && !forceUpdate {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusConflict, gin.H{"error": "Spectrum in use"})
		return
	} else if resC := qC.Find(&claims); resC.Error != nil {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	} else if resC.RowsAffected > 0 && !forceUpdate {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusConflict, gin.H{"error": "Spectrum in use"})
		return
	}

	spectrumPolicy.Allowed = dm.Allowed
	spectrumPolicy.AutoApprove = dm.AutoApprove
	spectrumPolicy.Priority = dm.Priority
	spectrumPolicy.MaxDuration = dm.MaxDuration
	spectrumPolicy.WhenUnoccupied = dm.WhenUnoccupied
	spectrumPolicy.DisableEmitCheck = dm.DisableEmitCheck
	spectrumPolicy.AllowSkipAcks = dm.AllowSkipAcks
	spectrumPolicy.AllowInactive = dm.AllowInactive
	spectrumPolicy.AllowConflicts = dm.AllowConflicts

	if sres := tx.Save(&spectrumPolicy); sres.Error != nil {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": sres.Error.Error()})
		return
	} else if tres := tx.Commit(); tres.Error != nil {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": tres.Error.Error()})
		return
	}

	// Now that we have atomically update the policy object, revoke
	// affected grants and delete claims (and claim grants), and notify.
	cMsg := fmt.Sprintf("spectrum policy %s updated", policyId)
	s.HandleSpectrumChanged(&spectrum, grants, claims, cMsg, tok.UserId, event.EventType_ET_UPDATED)

	lMsg := fmt.Sprintf("spectrum policy %s updated (%d grants/%d claims revoked/deleted)", policyId, len(grants), len(claims))
	log.Info().Msg(lMsg)

	c.JSON(http.StatusOK, spectrumPolicy)
}

func (s *Server) DeleteSpectrumPolicy(c *gin.Context) {
	// Check required path parameters.
	var spectrumId uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("spectrum_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		spectrumId = idParsed
	}
	var policyId uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("policy_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		policyId = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	forceUpdate := c.GetBool("zms.force-update")

	// At the moment, deleting a spectrum policy is practically the same as
	// deleting a spectrum object.  So, do the locking on the spectrum
	// object, and then handle as a spectrum update, until SpectrumPolicy
	// objects are first-class objects.  We lock so that no new or
	// transitioning grants can do so until they can read this row of
	// spectrum.
	//
	// We need to compare the Spectrum.ElementId and Spectrum.CreatorId with
	// the caller, to validate the match-element-role and match-user
	// policies.
	var spectrum store.Spectrum
	var creatorId *uuid.UUID
	if res := s.db.First(&spectrum, spectrumId); res.Error != nil || res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	creatorId = &spectrum.CreatorId

	var spectrumPolicy store.Policy
	if res := s.db.First(&spectrumPolicy, policyId); res.Error != nil || res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Check policy.  Element managers and up can delete any managed
	// spectrum policy.  Operators can only delete the spectrum policy they created.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, creatorId, &spectrum.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Check if spectrum is in use.  If not, we simply notify.  If so, error
	// if not forceUpdate.
	//
	// If not forceUpdate, must lock Spectrum and test/update while locked.
	// We created a nested transaction to handle that; we don't want to
	// pollute further grant state mutations from the default transaction.
	tx := s.db.Begin()
	if res := tx.Clauses(clause.Locking{Strength: "UPDATE"}).Preload("Policies").First(&spectrum, spectrumId); res.Error != nil || res.RowsAffected != 1 {
		tx.Rollback()
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	var grants []store.Grant
	var claims []store.Claim
	qG := tx
	qC := tx
	if spectrumPolicy.ElementId != nil {
		// If the policy we are deleting has an ElementId, just look for any
		// grants associated with both the target spectrum and policy.
		qG = qG.
			Where(&store.Grant{SpectrumId: &spectrum.Id, ElementId: *spectrumPolicy.ElementId, IsClaim: false}).
			Where("status in ?", store.AnyOperationalGrantStatusList)
		qC = qC.
			Joins("join grants on grants.spectrum_id = ? and grants.element_id = ? and grants.is_claim and grants.status in ?", spectrum.Id, *spectrumPolicy.ElementId,	store.AnyOperationalGrantStatusList).
			Preload("Grant")
	} else {
		//type SpectrumPolicyElementId struct {
		//	ElementId *uuid.UUID
		//
		// This is the wildcard policy, so any grants whose Spectrum
		// matches and whose element id is not specifically named as a
		// SpectrumId,ElementId policy would match.
		nonWildcardElementIdQuery := tx.
			Model(&store.Policy{}).
			Where("spectrum_id = ? and element_id is not NULL", spectrum.Id).
			Select("element_id")
		qG = qG.
			Where(&store.Grant{SpectrumId: &spectrum.Id}).
			Where("status in ?", store.AnyOperationalGrantStatusList).
			Where("element_id not in (?)", nonWildcardElementIdQuery)
		qC = qC.
			Joins("join grants on grants.spectrum_id = ? and grants.is_claim and grants.status in ? and grants.element_id not in (?)", spectrum.Id, store.AnyOperationalGrantStatusList, nonWildcardElementIdQuery).
			Preload("Grant")
	}

	// Run the query composed above to select all grants operating under the
	// spectrum policy to be deleted.
	if res := qG.Find(&grants); res.Error != nil {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	} else if res.RowsAffected > 0 && !forceUpdate {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusConflict, gin.H{"error": "Spectrum in use"})
		return
	} else if resC := qC.Find(&claims); resC.Error != nil {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	} else if resC.RowsAffected > 0 && !forceUpdate {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusConflict, gin.H{"error": "Spectrum in use"})
		return
	}

	//constraint.DeletedAt = &t
	//constraint.UpdaterId = &tok.UserId

	if sres := tx.Delete(&spectrumPolicy); sres.Error != nil {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": sres.Error.Error()})
		return
	} else if tres := tx.Commit(); tres.Error != nil {
		tx.Rollback()
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": tres.Error.Error()})
		return
	}

	// Now that we have atomically deleted the policy object, revoke
	// affected grants and delete claims (and claim grants), and notify.
	cMsg := fmt.Sprintf("spectrum policy %s deleted", policyId)
	s.HandleSpectrumChanged(&spectrum, grants, claims, cMsg, tok.UserId, event.EventType_ET_UPDATED)

	lMsg := fmt.Sprintf("spectrum policy %s deleted (%d grants/%d claims revoked/deleted)", policyId, len(grants), len(claims))
	log.Info().Msg(lMsg)

	c.Status(http.StatusOK)
}

type ListRadiosQueryParams struct {
	//Pending gin support...
	//ElementId  *string `form:"element_id" binding:"omitempty,uuid"`
	Radio   *string `form:"radio" binding:"omitempty"`
	Enabled *bool   `form:"enabled" binding:"omitempty"`
	Deleted *bool   `form:"deleted" binding:"omitempty"`
	Sort    *string `form:"sort" binding:"omitempty,oneof=element_id name fcc_id device_id serial_number enabled created_at updated_at deleted_at"`
	SortAsc *bool   `form:"sort_asc" binding:"omitempty"`
}

func (s *Server) ListRadios(c *gin.Context) {
	var err error

	// Check optional filter query parameters.
	params := ListRadiosQueryParams{}
	if err = c.ShouldBindQuery(&params); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// Unmarshal query uuids.
	var elementId *uuid.UUID
	if p, exists := c.GetQuery("element_id"); exists {
		if idParsed, err := uuid.Parse(p); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid element_id uuid"})
			return
		} else {
			elementId = &idParsed
		}
	}

	page, itemsPerPage := GetPaginateParams(c)

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	if tok, err = s.RequireContextToken(c); err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRoleOrViewerPolicy(policy.RoleViewer, policy.GreaterOrEqual)
	var match bool
	var policyName string
	if match, policyName = s.CheckPolicyMiddleware(tok, nil, elementId, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Admins can filter for anything; Viewers (e.g. if they have no element
	// binding) cannot filter; other users who have roles in Elements can
	// filter within those Elements.
	var elementIdList []*uuid.UUID
	if elementId != nil {
		elementIdList = []*uuid.UUID{elementId}
	} else if policyName != policy.MatchAdmin {
		elementIdList = tok.GetElementIds()
	}

	defTrue := true
	defFalse := false
	if policyName == policy.MatchViewer && len(elementIdList) == 0 {
		if (params.Deleted != nil && *params.Deleted) ||
			(params.Enabled != nil && !*params.Enabled) {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized (viewer cannot filter)"})
			return
		}
		if params.Deleted == nil {
			params.Deleted = &defFalse
		}
		if params.Enabled == nil {
			params.Enabled = &defTrue
		}
	}

	var radioList []store.Radio
	q := s.db
	if len(elementIdList) > 0 {
		q = q.Where("Radios.element_id in ?", elementIdList)
	}
	if params.Enabled != nil {
		if !*params.Enabled {
			q = q.Where("not Radios.enabled")
		} else {
			q = q.Where("Radios.enabled")
		}
	}
	if params.Deleted == nil || !*params.Deleted {
		q = q.Where("Radios.deleted_at is NULL")
	} else {
		q = q.Where("Radios.deleted_at is not NULL")
	}
	if params.Radio != nil {
		q = q.Where(
			s.db.Where(store.MakeILikeClause(s.config, "Radios.name"), fmt.Sprintf("%%%s%%", *params.Radio)).
				Or(store.MakeILikeClause(s.config, "Radios.description"), fmt.Sprintf("%%%s%%", *params.Radio)).
				Or(store.MakeILikeClause(s.config, "Radios.fcc_id"), fmt.Sprintf("%%%s%%", *params.Radio)).
				Or(store.MakeILikeClause(s.config, "Radios.device_id"), fmt.Sprintf("%%%s%%", *params.Radio)).
				Or(store.MakeILikeClause(s.config, "Radios.serial_number"), fmt.Sprintf("%%%s%%", *params.Radio)))
	}
	sortDir := "desc"
	if params.SortAsc != nil && *params.SortAsc {
		sortDir = "asc"
	}
	if params.Sort != nil {
		q = q.Order("Radios." + *params.Sort + " " + sortDir)
	} else {
		q = q.Order("Radios.updated_at " + sortDir)
	}

	if c.GetBool("zms.elaborate") {
		var f func (*gorm.DB) (*gorm.DB)
		if params.Deleted == nil || !*params.Deleted {
			f = func(db *gorm.DB) *gorm.DB {
				return db.Where("radio_ports.deleted_at is NULL")
			}
		} else {
			f = func(db *gorm.DB) *gorm.DB {
				return db.Where("radio_ports.deleted_at is not NULL")
			}
		}
		q = q.Preload("Ports", f)
		q = q.Preload("Ports.AntennaLocation")
		q = q.Preload("Location")
	}
	q = q.Order("Radios.updated_at desc")

	var total int64
	cres := q.Model(&store.Radio{}).Count(&total)
	if cres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": cres.Error.Error()})
		return
	}
	qres := q.Scopes(store.Paginate(page, itemsPerPage)).Find(&radioList)
	if qres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": cres.Error.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"radios": radioList, "page": page, "total": total, "pages": int(math.Ceil(float64(total) / float64(itemsPerPage)))})
}

func (s *Server) CreateRadio(c *gin.Context) {
	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var dm store.Radio
	if err := c.ShouldBindJSON(&dm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, nil, &dm.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Additional post-auth validation.

	// If LocationId is non-null, it must exist; otherwise Location must not
	// be nil and we will create one as a side-effect.
	if dm.LocationId != nil {
		if res := s.db.First(&store.Location{Id: *dm.LocationId}); res.RowsAffected != 1 {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "location not found"})
			return
		}
	} else if dm.Location == nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "must provide location_id or location"})
		return
	}
	// Validate that per-port AntennaId and LocationId exist, if provided.
	for _, dmp := range dm.Ports {
		if dmp.AntennaId != nil {
			if res := s.db.First(&store.Antenna{Id: *dmp.AntennaId}); res.RowsAffected != 1 {
				c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "antenna not found"})
				return
			}
		}
		if dmp.AntennaLocationId != nil {
			if res := s.db.First(&store.Location{Id: *dmp.AntennaLocationId}); res.RowsAffected != 1 {
				c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "antenna location not found"})
				return
			}
		}
	}

	// Create a radio, overriding or forcing bits that should not be set by
	// caller.
	radio := store.Radio{
		Id: uuid.New(), ElementId: dm.ElementId, Name: dm.Name, Description: dm.Description,
		FccId: dm.FccId, DeviceId: dm.DeviceId, SerialNumber: dm.SerialNumber,
		CreatorId: tok.UserId, Enabled: dm.Enabled, HtmlUrl: dm.HtmlUrl,
	}
	// Handle location association
	if dm.LocationId != nil {
		radio.LocationId = dm.LocationId
		radio.Location = nil
	} else {
		radio.Location = dm.Location
		radio.Location.Id = uuid.New()
		radio.LocationId = &radio.Location.Id
	}
	// Handle ports association
	for _, dmp := range dm.Ports {
		log.Debug().Msg(fmt.Sprintf("port: %+v", dmp))
		dmp.Id = uuid.New()
		dmp.RadioId = radio.Id
		dmp.CreatorId = tok.UserId
		// Handle Antenna association
		if dmp.AntennaId != nil {
			dmp.Antenna = nil
		} else if dmp.Antenna != nil {
			dmp.Antenna.Id = uuid.New()
			dmp.Antenna.ElementId = dm.ElementId
			dmp.AntennaId = &dmp.Antenna.Id
		}
		// Handle AntennaLocation association
		if dmp.AntennaLocationId != nil {
			dmp.AntennaLocation = nil
		} else if dmp.AntennaLocation != nil {
			dmp.AntennaLocation.Id = uuid.New()
			dmp.AntennaLocationId = &dmp.AntennaLocation.Id
		}
		radio.Ports = append(radio.Ports, dmp)
	}

	if res := s.db.Create(&radio); res.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	} else {
		c.JSON(http.StatusCreated, radio)
	}

	// Send a radio creation event.
	// NB: we do not generate extra (redundant) events for ports that
	// were also create above, if any, in this case!
	oid := radio.Id.String()
	userIdStr := radio.CreatorId.String()
	elementIdStr := radio.ElementId.String()
	eh := subscription.EventHeader{
		Type:       int32(event.EventType_ET_CREATED),
		Code:       int32(zmc.EventCode_EC_RADIO),
		SourceType: int32(event.EventSourceType_EST_ZMC),
		SourceId:   s.config.ServiceId,
		Id:         uuid.New().String(),
		ObjectId:   &oid,
		Time:       &radio.CreatedAt,
		UserId:     &userIdStr,
		ElementId:  &elementIdStr,
	}
	e := subscription.Event{
		Header: eh,
		Object: &radio,
	}
	go s.sm.Notify(&e)
}

func (s *Server) GetRadio(c *gin.Context) {
	var err error

	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("radio_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRoleOrViewerPolicy(policy.RoleViewer, policy.GreaterOrEqual)
	var match bool
	var policyName string
	if match, policyName = s.CheckPolicyMiddleware(tok, nil, nil, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	var radio store.Radio
	q := s.db
	if c.GetBool("zms.elaborate") {
		q = q.Preload("Ports", func(db *gorm.DB) *gorm.DB {
			return db.Where("radio_ports.deleted_at is NULL")
		})
		q = q.Preload("Ports.AntennaLocation")
		q = q.Preload("Location")
	}
	res := q.First(&radio, id)
	if res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Viewers can only see the radio if approved and not denied nor deleted
	if policyName == policy.MatchViewer && (radio.DeletedAt != nil) {
		c.AbortWithStatus(http.StatusForbidden)
		return
	}

	c.JSON(http.StatusOK, radio)
}

func (s *Server) HandleRadioChanged(radio *store.Radio) {

}

func (s *Server) UpdateRadio(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("radio_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Initial input validation.
	var um store.Radio
	if err := c.ShouldBindJSON(&um); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	//forceUpdate := c.GetBool("zms.force-update")

	// We need to compare the Radio.ElementId and Radio.CreatorId with
	// the caller, to validate the match-element-role and match-user
	// policies.  However, note that we do not return StatusNotFound until
	// after policy verification.  This is a bit pendantic.
	var radio store.Radio
	var creatorId *uuid.UUID
	res := s.db.First(&radio, id)
	if res.RowsAffected == 1 {
		creatorId = &radio.CreatorId
	}

	// Check policy.  Element managers and up can modify any managed
	// radio.  Operators can only modify the radio they created.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, creatorId, &radio.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	if res.RowsAffected < 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	//t := time.Now().UTC()

	// Validations that require authentication.
	if radio.DeletedAt != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "cannot modify deleted radio"})
		return
	}

	// Handle LocationId updates.  We only update fields on PUT, not
	// associations.
	locationChanged := false
	if um.LocationId == nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "must set radio location"})
		return
	} else if um.LocationId != radio.LocationId {
		if res := s.db.First(&store.Location{}, *um.LocationId); res.RowsAffected != 1 {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "location id not found"})
			return
		}
		radio.LocationId = um.LocationId
		radio.Location = nil
		locationChanged = true
	}

	// Assume obedience/compliance to the x-immutable-on-put annotations,
	// and only update mutable fields.

	// Change some fields regardless, without sending notifications.
	radio.Name = um.Name
	radio.Description = um.Description
	radio.FccId = um.FccId
	radio.DeviceId = um.DeviceId
	radio.SerialNumber = um.SerialNumber
	radio.Enabled = um.Enabled
	radio.HtmlUrl = um.HtmlUrl

	radio.UpdaterId = &tok.UserId

	sres := s.db.Save(&radio)
	if sres == nil || sres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": sres.Error.Error()})
		return
	}
	// Refresh after updates.
	q := s.db
	if c.GetBool("zms.elaborate") {
		q = q.Preload("Ports")
	}
	q.First(&radio, id)

	if locationChanged {
		s.HandleRadioChanged(&radio)
	}

	c.JSON(http.StatusOK, &radio)
}

func (s *Server) HandleRadioDeleted(radio *store.Radio) {

}

func (s *Server) DeleteRadio(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("radio_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	//forceUpdate := c.GetBool("zms.force-update")

	// We need to compare the Radio.ElementId and Radio.CreatorId with
	// the caller, to validate the match-element-role and match-user
	// policies.  However, note that we do not return StatusNotFound until
	// after policy verification.  This is a bit pendantic.
	var radio store.Radio
	var creatorId *uuid.UUID
	res := s.db.First(&radio, id)
	if res.RowsAffected == 1 {
		creatorId = &radio.CreatorId
	}

	// Check policy.  Element managers and up can delete any managed
	// radio.  Operators can only delete the radio they created.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, creatorId, &radio.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	if res.RowsAffected < 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Cannot delete if in use by radio ports.
	var total int64
	resInuse := s.db.Model(&store.RadioPort{}).
		Where(&store.RadioPort{RadioId: id}).
		Where("radio_ports.deleted_at is NULL").
		Count(&total)
	if resInuse.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": resInuse.Error.Error()})
		return
	}
	if total > 0 {
		c.AbortWithStatusJSON(http.StatusConflict, gin.H{"error": fmt.Sprintf("cannot delete radio; %d radio ports still reference it", total)})
		return
	}

	t := time.Now().UTC()

	radio.DeletedAt = &t
	radio.Enabled = false
	radio.UpdaterId = &tok.UserId

	s.db.Save(&radio)

	s.HandleRadioDeleted(&radio)

	c.Status(http.StatusOK)
}

type ListRadioPortsQueryParams struct {
	Tx      *bool   `form:"tx" binding:"omitempty"`
	Rx      *bool   `form:"rx" binding:"omitempty"`
	Enabled *bool   `form:"enabled" binding:"omitempty"`
	Deleted *bool   `form:"deleted" binding:"omitempty"`
	Sort    *string `form:"sort" binding:"omitempty,oneof=name tx rx min_freq max_freq max_power enabled created_at updated_at deleted_at"`
	SortAsc *bool   `form:"sort_asc" binding:"omitempty"`
}

func (s *Server) ListRadioPorts(c *gin.Context) {
	var err error

	// Check optional filter query parameters.
	params := ListRadioPortsQueryParams{}
	if err = c.ShouldBindQuery(&params); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// Unmarshal query uuids.
	var radioId *uuid.UUID
	if p, exists := c.GetQuery("radio_id"); exists {
		if idParsed, err := uuid.Parse(p); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid radio_id uuid"})
			return
		} else {
			radioId = &idParsed
		}
	}

	page, itemsPerPage := GetPaginateParams(c)

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	if tok, err = s.RequireContextToken(c); err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	var radio store.Radio
	if res := s.db.First(&radio, *radioId); res.Error != nil || res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleViewer, policy.GreaterOrEqual)
	var match bool
	var policyName string
	if match, policyName = s.CheckPolicyMiddleware(tok, nil, &radio.ElementId, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	defTrue := true
	defFalse := false
	if policyName == policy.MatchViewer {
		if (params.Deleted != nil && *params.Deleted) ||
			(params.Enabled != nil && !*params.Enabled) {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized (viewer cannot filter)"})
			return
		}
		if params.Deleted == nil {
			params.Deleted = &defFalse
		}
		if params.Enabled == nil {
			params.Enabled = &defTrue
		}
	}

	var radioPortList []store.RadioPort
	q := s.db
	q = q.Where("radio_ports.radio_id = ?", radioId)
	if params.Enabled != nil {
		if !*params.Enabled {
			q = q.Where("not radio_ports.enabled")
		} else {
			q = q.Where("radio_ports.enabled")
		}
	}
	if params.Deleted == nil || !*params.Deleted {
		q = q.Where("radio_ports.deleted_at is NULL")
	} else {
		q = q.Where("radio_ports.deleted_at is not NULL")
	}
	if params.Tx != nil {
		q = q.Where("radio_ports.tx")
	}
	if params.Rx != nil {
		q = q.Where("radio_ports.rx")
	}
	sortDir := "desc"
	if params.SortAsc != nil && *params.SortAsc {
		sortDir = "asc"
	}
	if params.Sort != nil {
		q = q.Order("radio_ports." + *params.Sort + " " + sortDir)
	} else {
		q = q.Order("radio_ports.updated_at " + sortDir)
	}

	if c.GetBool("zms.elaborate") {
		q = q.Preload("AntennaLocation")
		q = q.Preload("Location")
	}

	var total int64
	cres := q.Model(&store.RadioPort{}).Count(&total)
	if cres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": cres.Error.Error()})
		return
	}
	qres := q.Scopes(store.Paginate(page, itemsPerPage)).Find(&radioPortList)
	if qres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": cres.Error.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"radio_ports": radioPortList, "page": page, "total": total, "pages": int(math.Ceil(float64(total) / float64(itemsPerPage)))})
}

func (s *Server) CreateRadioPort(c *gin.Context) {
	// Check required path parameters.
	var radioId uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("radio_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		radioId = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var dm store.RadioPort
	if err := c.ShouldBindJSON(&dm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// Must have Radio.ElementId for authorization policy check.
	var radio store.Radio
	if res := s.db.First(&radio, radioId); res.RowsAffected != 1 {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "radio not found"})
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, nil, &radio.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Additional post-auth validation.

	// If AntennaId is non-null, it must exist; otherwise Antenna must not
	// be nil and we will create one as a side-effect.
	// NB: we have relaxed the latter constraint for now to support inline
	// monitors with no antennas; this may change.
	if dm.AntennaId != nil {
		if res := s.db.First(&store.Antenna{Id: *dm.AntennaId}); res.RowsAffected != 1 {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "antenna not found"})
			return
		}
	} else if false && dm.Antenna == nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "must provide antenna_id or antenna"})
		return
	}
	// If AntennaLocationId is non-null, it must exist; otherwise AntennaLocation must not
	// be nil and we will create one as a side-effect.
	// NB: we have relaxed the latter constraint for now to support inline
	// monitors with no antennas; this may change.
	if dm.AntennaLocationId != nil {
		if res := s.db.First(&store.Location{Id: *dm.AntennaLocationId}); res.RowsAffected != 1 {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "location not found"})
			return
		}
	} else if false && dm.AntennaLocation == nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "must provide antenna_location_id or antenna_location"})
		return
	}

	// Handle Antenna association
	if dm.AntennaId != nil {
		dm.Antenna = nil
	} else if dm.Antenna != nil {
		dm.Antenna.Id = uuid.New()
		dm.AntennaId = &dm.Antenna.Id
	}
	// Handle AntennaLocation association
	if dm.AntennaLocationId != nil {
		dm.AntennaLocation = nil
	} else if dm.AntennaLocation != nil {
		dm.AntennaLocation.Id = uuid.New()
		dm.AntennaLocationId = &dm.AntennaLocation.Id
	}

	// Create a radio port, overriding or forcing bits that should not be
	// set by caller.
	radioPort := store.RadioPort{
		Id: uuid.New(), RadioId: dm.RadioId, Name: dm.Name, Tx: dm.Tx, Rx: dm.Rx,
		MinFreq: dm.MinFreq, MaxFreq: dm.MaxFreq, MaxPower: dm.MaxPower,
		AntennaId: dm.AntennaId, AntennaLocationId: dm.AntennaLocationId,
		AntennaAzimuthAngle: dm.AntennaAzimuthAngle, AntennaElevationAngle: dm.AntennaElevationAngle,
		AntennaElevationDelta: dm.AntennaElevationDelta,
		Enabled: dm.Enabled, Antenna: dm.Antenna, AntennaLocation: dm.AntennaLocation,
		CreatorId: tok.UserId, DeviceId: dm.DeviceId,
	}

	if res := s.db.Create(&radioPort); res.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	} else {
		c.JSON(http.StatusCreated, radioPort)
	}

	// Send a radioport creation event.
	oid := radioPort.Id.String()
	userIdStr := radioPort.CreatorId.String()
	elementIdStr := radio.ElementId.String()
	eh := subscription.EventHeader{
		Type:       int32(event.EventType_ET_CREATED),
		Code:       int32(zmc.EventCode_EC_RADIOPORT),
		SourceType: int32(event.EventSourceType_EST_ZMC),
		SourceId:   s.config.ServiceId,
		Id:         uuid.New().String(),
		ObjectId:   &oid,
		Time:       &radioPort.CreatedAt,
		UserId:     &userIdStr,
		ElementId:  &elementIdStr,
	}
	e := subscription.Event{
		Header: eh,
		Object: &radioPort,
	}
	go s.sm.Notify(&e)
}

func (s *Server) GetRadioPort(c *gin.Context) {
	// Check required path parameters.
	var radioId uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("radio_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		radioId = idParsed
	}
	var radioPortId uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("radio_port_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		radioPortId = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	var radio store.Radio
	if res := s.db.First(&radio, radioId); res.RowsAffected != 1 {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "radio not found"})
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRoleOrViewerPolicy(policy.RoleViewer, policy.GreaterOrEqual)
	var match bool
	var policyName string
	if match, policyName = s.CheckPolicyMiddleware(tok, nil, &radio.ElementId, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	var radioPort store.RadioPort
	q := s.db
	if c.GetBool("zms.elaborate") {
		q = q.Preload("Antenna").Preload("AntennaLocation")
	}
	res := q.First(&radioPort, radioPortId)
	if res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Viewers can only see the radio port if radio and port are not Deleted
	if policyName == policy.MatchViewer && radio.DeletedAt != nil && radioPort.DeletedAt != nil {
		c.AbortWithStatus(http.StatusForbidden)
		return
	}

	c.JSON(http.StatusOK, radioPort)
}

func (s *Server) HandleRadioPortChanged(radioPort *store.RadioPort) {

}

func (s *Server) UpdateRadioPort(c *gin.Context) {
	// Check required path parameters.
	var radioId uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("radio_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		radioId = idParsed
	}
	var radioPortId uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("radio_port_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		radioPortId = idParsed
	}

	// Initial input validation.
	var um store.RadioPort
	if err := c.ShouldBindJSON(&um); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	var radio store.Radio
	if res := s.db.First(&radio, radioId); res.RowsAffected != 1 {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "radio not found"})
		return
	}
	var radioPort store.RadioPort
	if res := s.db.First(&radioPort, radioPortId); res.RowsAffected != 1 {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "radio port not found"})
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, &radioPort.CreatorId, &radio.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	//forceUpdate := c.GetBool("zms.force-update")

	// Validations that require authentication.
	if radio.DeletedAt != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "cannot modify deleted radio"})
		return
	}
	if radioPort.DeletedAt != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "cannot modify deleted radio"})
		return
	}

	// Handle AntennaId update.
	antennaChanged := false
	if um.AntennaId == nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "must set antenna "})
		return
	} else if um.AntennaId != radioPort.AntennaId {
		if res := s.db.First(&store.Antenna{}, *um.AntennaId); res.RowsAffected != 1 {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "antenna  id not found"})
			return
		}
		radioPort.AntennaId = um.AntennaId
		radioPort.Antenna = nil
		antennaChanged = true
	}
	// Handle AntennaLocationId update.
	locationChanged := false
	if um.AntennaLocationId == nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "must set antenna location"})
		return
	} else if um.AntennaLocationId != radioPort.AntennaLocationId {
		if res := s.db.First(&store.Location{}, *um.AntennaLocationId); res.RowsAffected != 1 {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "antenna location id not found"})
			return
		}
		radioPort.AntennaLocationId = um.AntennaLocationId
		radioPort.AntennaLocation = nil
		locationChanged = true
	}

	antennaOrientationChanged := false
	if radioPort.AntennaAzimuthAngle != um.AntennaAzimuthAngle ||
		(radioPort.AntennaAzimuthAngle != nil && um.AntennaAzimuthAngle != nil && *radioPort.AntennaAzimuthAngle != *um.AntennaAzimuthAngle) ||
		radioPort.AntennaElevationAngle != um.AntennaElevationAngle ||
		(radioPort.AntennaElevationAngle != nil && um.AntennaElevationAngle != nil && *radioPort.AntennaElevationAngle != *um.AntennaElevationAngle) ||
		radioPort.AntennaElevationDelta != um.AntennaElevationDelta ||
		(radioPort.AntennaElevationDelta != nil && um.AntennaElevationDelta != nil && *radioPort.AntennaElevationDelta != *um.AntennaElevationDelta) {
		antennaOrientationChanged = true
	}
	enabledChanged := false
	if radioPort.Enabled != um.Enabled {
		enabledChanged = true
	}

	// Assume obedience/compliance to the x-immutable-on-put annotations,
	// and only update mutable fields.

	// Change some fields regardless, without sending notifications.
	radioPort.Name = um.Name
	radioPort.DeviceId = um.DeviceId
	radioPort.Tx = um.Tx
	radioPort.Rx = um.Rx
	radioPort.AntennaAzimuthAngle = um.AntennaAzimuthAngle
	radioPort.AntennaElevationAngle = um.AntennaElevationAngle
	radioPort.AntennaElevationDelta = um.AntennaElevationDelta
	radioPort.Enabled = um.Enabled
	radioPort.MinFreq = um.MinFreq
	radioPort.MaxFreq = um.MaxFreq
	radioPort.MaxPower = um.MaxPower

	radio.UpdaterId = &tok.UserId

	sres := s.db.Save(&radioPort)
	if sres == nil || sres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": sres.Error.Error()})
		return
	}
	// Refresh after updates.
	q := s.db
	if c.GetBool("zms.elaborate") {
		q = q.Preload("Antenna").Preload("AntennaLocation")
	}
	q.First(&radioPort, radioPortId)

	if locationChanged || antennaChanged || antennaOrientationChanged || enabledChanged {
		s.HandleRadioPortChanged(&radioPort)
	}

	c.JSON(http.StatusOK, &radioPort)
}

func (s *Server) HandleRadioPortDeleted(radioPort *store.RadioPort) {

}

func (s *Server) DeleteRadioPort(c *gin.Context) {
	// Check required path parameters.
	var radioId uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("radio_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		radioId = idParsed
	}
	var radioPortId uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("radio_port_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		radioPortId = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	//forceUpdate := c.GetBool("zms.force-update")

	// We need to compare the Radio.ElementId and Radio.CreatorId with
	// the caller, to validate the match-element-role and match-user
	// policies.  However, note that we do not return StatusNotFound until
	// after policy verification.  This is a bit pendantic.
	var radio store.Radio
	var elementId *uuid.UUID
	if res := s.db.First(&radio, radioId); res.RowsAffected == 1 {
		elementId = &radio.ElementId
	}
	var radioPort store.RadioPort
	var creatorId *uuid.UUID
	res := s.db.First(&radioPort, radioPortId)
	if res.RowsAffected == 1 {
		creatorId = &radioPort.CreatorId
	}

	// Check policy.  Element managers and up can delete any managed
	// radio.  Operators can only delete the radio they created.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, creatorId, elementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	if res.RowsAffected < 1 || radioPort.RadioId != radioId {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Cannot delete if in use by monitors.
	var total int64
	sq := s.db.Where(&store.Monitor{RadioPortId: radioPortId}).
		Or(&store.Monitor{MonitoredRadioPortId: &radioPortId})
	resInuse := s.db.Model(&store.Monitor{}).
		Where(sq).Where("monitors.deleted_at is NULL").
		Count(&total)
	if resInuse.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": resInuse.Error.Error()})
		return
	}
	if total > 0 {
		c.AbortWithStatusJSON(http.StatusConflict, gin.H{"error": fmt.Sprintf("cannot delete radio port; %d monitors still reference it", total)})
		return
	}

	// Cannot delete if in use by undeleted grants.
	// XXX: this cannot stand... we must not allow one element's grants
	// to block another's resources.
	resInuse = s.db.Model(&store.Grant{}).
		Select("grants.id").
		Joins("left join grant_radio_ports on grant_radio_ports.grant_id=grants.id").
		Joins("left join radio_ports on radio_ports.id=grant_radio_ports.radio_port_id").
		Where("grants.deleted_at is NULL").
		Where("radio_ports.id = ?", radioPort.Id).
		Count(&total)
	if resInuse.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": resInuse.Error.Error()})
		return
	}
	if total > 0 {
		c.AbortWithStatusJSON(http.StatusConflict, gin.H{"error": fmt.Sprintf("cannot delete radio port; %d grant constraints still reference it", total)})
		return
	}

	t := time.Now().UTC()

	radioPort.DeletedAt = &t
	radioPort.Enabled = false
	radioPort.UpdaterId = &tok.UserId

	s.db.Save(&radioPort)

	s.HandleRadioPortDeleted(&radioPort)

	c.Status(http.StatusOK)
}

type ListAntennasQueryParams struct {
	//Pending gin support...
	//ElementId *string `form:"element_id" binding:"omitempty,uuid"`
	Antenna *string `form:"antenna" binding:"omitempty"`
	Type    *string `form:"type" binding:"omitempty"`
	Deleted *bool   `form:"deleted" binding:"omitempty"`
	Sort    *string `form:"sort" binding:"omitempty,oneof=element_id name type vendor model gain beam_width gain_profile_format gain_profile_url enabled created_at updated_at deleted_at"`
	SortAsc *bool   `form:"sort_asc" binding:"omitempty"`
}

func (s *Server) ListAntennas(c *gin.Context) {
	var err error

	// Check optional filter query parameters.
	params := ListAntennasQueryParams{}
	if err = c.ShouldBindQuery(&params); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// Unmarshal query uuids.
	var elementId *uuid.UUID
	if p, exists := c.GetQuery("element_id"); exists {
		if idParsed, err := uuid.Parse(p); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid element_id uuid"})
			return
		} else {
			elementId = &idParsed
		}
	}

	page, itemsPerPage := GetPaginateParams(c)

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	if tok, err = s.RequireContextToken(c); err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRoleOrViewerPolicy(policy.RoleViewer, policy.GreaterOrEqual)
	var match bool
	var policyName string
	if match, policyName = s.CheckPolicyMiddleware(tok, nil, elementId, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Admins can filter for anything; Viewers (e.g. if they have no element
	// binding) cannot filter; other users who have roles in Elements can
	// filter within those Elements.
	var elementIdList []*uuid.UUID
	if elementId != nil {
		elementIdList = []*uuid.UUID{elementId}
	} else if policyName != policy.MatchAdmin {
		elementIdList = tok.GetElementIds()
	}

	defFalse := false
	if policyName == policy.MatchViewer && len(elementIdList) == 0 {
		if params.Deleted != nil && *params.Deleted {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized (viewer cannot filter)"})
			return
		}
		if params.Deleted == nil {
			params.Deleted = &defFalse
		}
	}

	var antennaList []store.Antenna
	q := s.db
	if len(elementIdList) > 0 {
		q = q.Where("Antennas.element_id in ?", elementIdList)
	}
	if params.Deleted == nil ||  !*params.Deleted {
		q = q.Where("Antennas.deleted_at is NULL")
	} else {
		q = q.Where("Antennas.deleted_at is not NULL")
	}
	if params.Antenna != nil {
		q = q.Where(
			s.db.Where(store.MakeILikeClause(s.config, "Antennas.name"), fmt.Sprintf("%%%s%%", *params.Antenna)).
				Or(store.MakeILikeClause(s.config, "Antennas.description"), fmt.Sprintf("%%%s%%", *params.Antenna)).
				Or(store.MakeILikeClause(s.config, "Antennas.model"), fmt.Sprintf("%%%s%%", *params.Antenna)).
				Or(store.MakeILikeClause(s.config, "Antennas.vendor"), fmt.Sprintf("%%%s%%", *params.Antenna)))
	}
	if params.Type != nil {
		q = q.Where(store.MakeILikeClause(s.config, "Antennas.type"), fmt.Sprintf("%%%s%%", *params.Type))
	}
	sortDir := "desc"
	if params.SortAsc != nil && *params.SortAsc {
		sortDir = "asc"
	}
	if params.Sort != nil {
		q = q.Order("Antennas." + *params.Sort + " " + sortDir)
	} else {
		q = q.Order("Antennas.updated_at " + sortDir)
	}

	var total int64
	cres := q.Model(&store.Antenna{}).Count(&total)
	if cres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": cres.Error.Error()})
		return
	}
	qres := q.Scopes(store.Paginate(page, itemsPerPage)).Find(&antennaList)
	if qres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": cres.Error.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"antennas": antennaList, "page": page, "total": total, "pages": int(math.Ceil(float64(total) / float64(itemsPerPage)))})
}

func (s *Server) CreateAntenna(c *gin.Context) {
	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var dm store.Antenna
	if err := c.ShouldBindJSON(&dm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if dm.ElementId == uuid.Nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "invalid element_id"})
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, nil, &dm.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Create an antenna, overriding or forcing bits that should not be set
	// by caller.
	antenna := store.Antenna{
		Id: uuid.New(), ElementId: dm.ElementId, Name: dm.Name, Description: dm.Description,
		Type: dm.Type, Vendor: dm.Vendor, Model: dm.Model, Url: dm.Url,
		Gain: dm.Gain, BeamWidth: dm.BeamWidth, GainProfileData: dm.GainProfileData,
		GainProfileFormat: dm.GainProfileFormat, GainProfileUrl: dm.GainProfileUrl,
		CreatorId: tok.UserId,
	}

	if res := s.db.Create(&antenna); res.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	} else {
		c.JSON(http.StatusCreated, antenna)
	}

	// Send an antenna event.
	oid := antenna.Id.String()
	userIdStr := antenna.CreatorId.String()
	elementIdStr := antenna.ElementId.String()
	eh := subscription.EventHeader{
		Type:       int32(event.EventType_ET_CREATED),
		Code:       int32(zmc.EventCode_EC_ANTENNA),
		SourceType: int32(event.EventSourceType_EST_ZMC),
		SourceId:   s.config.ServiceId,
		Id:         uuid.New().String(),
		ObjectId:   &oid,
		Time:       &antenna.CreatedAt,
		UserId:     &userIdStr,
		ElementId:  &elementIdStr,
	}
	e := subscription.Event{
		Header: eh,
		Object: &antenna,
	}
	go s.sm.Notify(&e)
}

func (s *Server) GetAntenna(c *gin.Context) {
	var err error

	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("antenna_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRoleOrViewerPolicy(policy.RoleViewer, policy.GreaterOrEqual)
	var match bool
	var policyName string
	if match, policyName = s.CheckPolicyMiddleware(tok, nil, nil, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	var antenna store.Antenna
	res := s.db.First(&antenna, id)
	if res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Viewers can only see the antenna if approved and not denied nor deleted
	if policyName == policy.MatchViewer && (antenna.DeletedAt != nil) {
		c.AbortWithStatus(http.StatusForbidden)
		return
	}

	c.JSON(http.StatusOK, antenna)
}

func (s *Server) HandleAntennaChanged(antenna *store.Antenna) {

}

func (s *Server) UpdateAntenna(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("antenna_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Initial input validation.
	var um store.Antenna
	if err := c.ShouldBindJSON(&um); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	//forceUpdate := c.GetBool("zms.force-update")

	// We need to compare the Antenna.ElementId and Antenna.CreatorId with
	// the caller, to validate the match-element-role and match-user
	// policies.  However, note that we do not return StatusNotFound until
	// after policy verification.  This is a bit pendantic.
	var antenna store.Antenna
	var creatorId *uuid.UUID
	var elementId *uuid.UUID
	res := s.db.First(&antenna, id)
	if res.RowsAffected == 1 {
		creatorId = &antenna.CreatorId
		elementId = &antenna.ElementId
	}

	// Check policy.  Element managers and up can modify any managed
	// antenna.  Operators can only modify the antenna they created.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, creatorId, elementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	if res.RowsAffected < 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	//t := time.Now().UTC()

	// Validations that require authentication.
	if antenna.DeletedAt != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "cannot modify deleted antenna"})
		return
	}

	// Assume obedience/compliance to the x-immutable-on-put annotations,
	// and only update mutable fields.

	// Change some fields regardless, without sending notifications.
	antenna.Name = um.Name
	antenna.Description = um.Description
	antenna.Type = um.Type
	antenna.Vendor = um.Vendor
	antenna.Model = um.Model
	antenna.Url = um.Url
	antenna.Gain = um.Gain
	antenna.BeamWidth = um.BeamWidth
	antenna.GainProfileData = um.GainProfileData
	antenna.GainProfileFormat = um.GainProfileFormat
	antenna.GainProfileUrl = um.GainProfileUrl

	antenna.UpdaterId = &tok.UserId

	sres := s.db.Save(&antenna)
	if sres == nil || sres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": sres.Error.Error()})
		return
	}
	// Refresh after updates.
	s.db.First(&antenna, id)

	s.HandleAntennaChanged(&antenna)

	c.JSON(http.StatusOK, &antenna)
}

func (s *Server) HandleAntennaDeleted(antenna *store.Antenna) {

}

func (s *Server) DeleteAntenna(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("antenna_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	//forceUpdate := c.GetBool("zms.force-update")

	// We need to compare the Antenna.ElementId and Antenna.CreatorId with
	// the caller, to validate the match-element-role and match-user
	// policies.  However, note that we do not return StatusNotFound until
	// after policy verification.  This is a bit pendantic.
	var antenna store.Antenna
	var creatorId *uuid.UUID
	var elementId *uuid.UUID
	res := s.db.First(&antenna, id)
	if res.RowsAffected == 1 {
		creatorId = &antenna.CreatorId
		elementId = &antenna.ElementId
	}

	// Check policy.  Element managers and up can delete any managed
	// antenna.  Operators can only delete the antenna they created.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, creatorId, elementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	if res.RowsAffected < 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Cannot delete if in use by radio ports.
	var total int64
	resInuse := s.db.Model(&store.RadioPort{}).
		Where(&store.RadioPort{AntennaId: &id}).
		Where("radio_ports.deleted_at is NULL").
		Count(&total)
	if resInuse.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": resInuse.Error.Error()})
		return
	}
	if total > 0 {
		c.AbortWithStatusJSON(http.StatusConflict, gin.H{"error": fmt.Sprintf("cannot delete antenna; %d radio ports still reference it", total)})
		return
	}

	t := time.Now().UTC()

	antenna.DeletedAt = &t
	antenna.UpdaterId = &tok.UserId

	s.db.Save(&antenna)

	s.HandleAntennaDeleted(&antenna)

	c.Status(http.StatusOK)
}

type ListLocationsQueryParams struct {
	//Pending gin support...
	//ElementId *string `form:"element_id" binding:"omitempty,uuid"`
	Name    *string `form:"name" binding:"omitempty"`
	Deleted *bool   `form:"deleted" binding:"omitempty"`
}

func (s *Server) ListLocations(c *gin.Context) {
	var err error

	// Check optional filter query parameters.
	params := ListLocationsQueryParams{}
	if err = c.ShouldBindQuery(&params); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// Unmarshal query uuids.
	var elementId *uuid.UUID
	if p, exists := c.GetQuery("element_id"); exists {
		if idParsed, err := uuid.Parse(p); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid element_id uuid"})
			return
		} else {
			elementId = &idParsed
		}
	}

	page, itemsPerPage := GetPaginateParams(c)

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	if tok, err = s.RequireContextToken(c); err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRoleOrViewerPolicy(policy.RoleViewer, policy.GreaterOrEqual)
	var match bool
	var policyName string
	if match, policyName = s.CheckPolicyMiddleware(tok, nil, elementId, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}
	if policyName == policy.MatchViewer {
		if params.Deleted != nil && *params.Deleted {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized (must provide authorized filter when non-admin)"})
			return
		}
	}

	var locationList []store.Location
	var elementIdList []*uuid.UUID
	if elementId != nil {
		elementIdList = []*uuid.UUID{elementId}
	} else if policyName != policy.MatchAdmin {
		elementIdList = tok.GetElementIds()
	}
	q := s.db
	if elementIdList != nil {
		q = q.Where("Locations.element_id in ?", elementIdList)
	}
	if params.Deleted == nil || !*params.Deleted {
		q = q.Where("Locations.deleted_at is NULL")
	} else {
		q = q.Where("Locations.deleted_at is not NULL")
	}
	if params.Name != nil {
		q = q.Where(store.MakeILikeClause(s.config, "Locations.name"), fmt.Sprintf("%%%s%%", *params.Name))
	}
	q = q.Order("Locations.updated_at desc")

	var total int64
	cres := q.Model(&store.Location{}).Count(&total)
	if cres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": cres.Error.Error()})
		return
	}
	qres := q.Scopes(store.Paginate(page, itemsPerPage)).Find(&locationList)
	if qres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": cres.Error.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"locations": locationList, "page": page, "total": total, "pages": int(math.Ceil(float64(total) / float64(itemsPerPage)))})
}

func (s *Server) CreateLocation(c *gin.Context) {
	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var dm store.Location
	if err := c.ShouldBindJSON(&dm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if dm.ElementId == uuid.Nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "invalid element_id"})
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, nil, &dm.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Create an location, overriding or forcing bits that should not be set
	// by caller.
	location := store.Location{
		Id: uuid.New(), ElementId: dm.ElementId, Name: dm.Name,
		Srid: dm.Srid, X: dm.X, Y: dm.Y, Z: dm.Z,
		CreatorId: tok.UserId,
	}

	if res := s.db.Create(&location); res.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	} else {
		c.JSON(http.StatusCreated, location)
	}
}

func (s *Server) GetLocation(c *gin.Context) {
	var err error

	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("location_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRoleOrViewerPolicy(policy.RoleViewer, policy.GreaterOrEqual)
	var match bool
	var policyName string
	if match, policyName = s.CheckPolicyMiddleware(tok, nil, nil, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	var location store.Location
	res := s.db.First(&location, id)
	if res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Viewers can only see the location if not deleted
	if policyName == policy.MatchViewer && (location.DeletedAt != nil) {
		c.AbortWithStatus(http.StatusForbidden)
		return
	}

	c.JSON(http.StatusOK, location)
}

func (s *Server) HandleLocationChanged(location *store.Location) {

}

func (s *Server) UpdateLocation(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("location_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Initial input validation.
	var um store.Location
	if err := c.ShouldBindJSON(&um); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	//forceUpdate := c.GetBool("zms.force-update")

	// We need to compare the Location.ElementId and Location.CreatorId with
	// the caller, to validate the match-element-role and match-user
	// policies.  However, note that we do not return StatusNotFound until
	// after policy verification.  This is a bit pendantic.
	var location store.Location
	var creatorId *uuid.UUID
	var elementId *uuid.UUID
	res := s.db.First(&location, id)
	if res.RowsAffected == 1 {
		creatorId = &location.CreatorId
		elementId = &location.ElementId
	}

	// Check policy.  Element managers and up can modify any managed
	// location.  Operators can only modify the location they created.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, creatorId, elementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	if res.RowsAffected < 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	//t := time.Now().UTC()

	// Validations that require authentication.
	if location.DeletedAt != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "cannot modify deleted location"})
		return
	}

	// Assume obedience/compliance to the x-immutable-on-put annotations,
	// and only update mutable fields.

	// Change some fields regardless, without sending notifications.
	location.Name = um.Name
	location.X = um.X
	location.Y = um.Y
	location.Y = um.Y

	location.UpdaterId = &tok.UserId

	sres := s.db.Save(&location)
	if sres == nil || sres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": sres.Error.Error()})
		return
	}
	// Refresh after updates.
	s.db.First(&location, id)

	s.HandleLocationChanged(&location)

	c.JSON(http.StatusOK, &location)
}

func (s *Server) HandleLocationDeleted(location *store.Location) {

}

func (s *Server) DeleteLocation(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("location_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	//forceUpdate := c.GetBool("zms.force-update")

	// We need to compare the Location.ElementId and Location.CreatorId with
	// the caller, to validate the match-element-role and match-user
	// policies.  However, note that we do not return StatusNotFound until
	// after policy verification.  This is a bit pendantic.
	var location store.Location
	var creatorId *uuid.UUID
	var elementId *uuid.UUID
	res := s.db.First(&location, id)
	if res.RowsAffected == 1 {
		creatorId = &location.CreatorId
		elementId = &location.ElementId
	}

	// Check policy.  Element managers and up can delete any managed
	// location.  Operators can only delete the location they created.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, creatorId, elementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	if res.RowsAffected < 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Cannot delete if in use by radios or ports.
	var total int64
	if resInuse := s.db.Model(&store.Radio{}).Where(&store.Radio{LocationId: &id}).Where("radios.deleted_at is NULL").Count(&total); resInuse.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": resInuse.Error.Error()})
		return
	} else if total > 0 {
		c.AbortWithStatusJSON(http.StatusConflict, gin.H{"error": fmt.Sprintf("cannot delete location; %d radios still reference it", total)})
		return
	}
	if resInuse := s.db.Model(&store.RadioPort{}).Where(&store.RadioPort{AntennaLocationId: &id}).Where("radio_ports.deleted_at is NULL").Count(&total); resInuse.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": resInuse.Error.Error()})
		return
	} else if total > 0 {
		c.AbortWithStatusJSON(http.StatusConflict, gin.H{"error": fmt.Sprintf("cannot delete location; %d radio ports still reference it", total)})
		return
	}

	t := time.Now().UTC()

	location.DeletedAt = &t
	location.UpdaterId = &tok.UserId

	s.db.Save(&location)

	s.HandleLocationDeleted(&location)

	c.Status(http.StatusOK)
}

type ListGrantsQueryParams struct {
	//Pending gin support...
	//ElementId *string `form:"element_id" binding:"omitempty,uuid"`
	Grant         *string `form:"grant" binding:"omitempty"`
	Approved      *bool   `form:"approved" binding:"omitempty"`
	OwnerApproved *bool   `form:"owner_approved" binding:"omitempty"`
	Current       *bool   `form:"current" binding:"omitempty"`
	Denied        *bool   `form:"denied" binding:"omitempty"`
	Revoked       *bool   `form:"revoked" binding:"omitempty"`
	Deleted       *bool   `form:"deleted" binding:"omitempty"`
	Claim       *bool     `form:"claim" binding:"omitempty"`
	//SpectrumId *string `form:"spectrum_id" binding:"omitempty,uuid"`
	Sort    *string `form:"sort" binding:"omitempty,oneof=element_id name status op_status created_at updated_at approved_at owner_approved_at denied_at starts_at expires_at revoked_at deleted_at spectrum_id"`
	SortAsc *bool   `form:"sort_asc" binding:"omitempty"`
}

func (s *Server) ListGrants(c *gin.Context) {
	var err error

	// Check optional filter query parameters.
	params := ListGrantsQueryParams{}
	if err = c.ShouldBindQuery(&params); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// Unmarshal query uuids.
	var elementId *uuid.UUID
	if p, exists := c.GetQuery("element_id"); exists {
		if idParsed, err := uuid.Parse(p); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid element_id uuid"})
			return
		} else {
			elementId = &idParsed
		}
	}
	var spectrumId *uuid.UUID
	if p, exists := c.GetQuery("spectrum_id"); exists {
		if idParsed, err := uuid.Parse(p); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid spectrum_id uuid"})
			return
		} else {
			spectrumId = &idParsed
		}
	}

	page, itemsPerPage := GetPaginateParams(c)

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	if tok, err = s.RequireContextToken(c); err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	// Check policy.
	// XXX: really want to return unapproved grants as well
	// for callers with element roles, even if they do not specify
	// element_id.  Just need to apply the policy check to each element_id
	// in the token, and then allow grants matching those element_ids to be
	// unapproved or approved.
	policies := policy.MakeMatchAdminOrElementRoleOrViewerPolicy(policy.RoleViewer, policy.GreaterOrEqual)
	var match bool
	var policyName string
	if match, policyName = s.CheckPolicyMiddleware(tok, nil, elementId, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Admins can filter for anything; Viewers (e.g. if they have no element
	// binding) cannot filter; other users who have roles in Elements can
	// filter within those Elements.
	var elementIdList []*uuid.UUID
	if elementId != nil {
		elementIdList = []*uuid.UUID{elementId}
	} else if policyName != policy.MatchAdmin {
		elementIdList = tok.GetElementIds()
	}

	defTrue := true
	defFalse := false

	// NB: never show claims by default.
	if params.Claim == nil {
		params.Claim = &defFalse
	}

	if policyName == policy.MatchViewer && len(elementIdList) == 0 {
		if (params.Approved != nil && !*params.Approved) ||
			(params.OwnerApproved != nil && !*params.OwnerApproved) ||
			(params.Current != nil && !*params.Current) ||
			(params.Denied != nil && *params.Denied) ||
			(params.Revoked != nil && *params.Revoked) ||
			(params.Deleted != nil && *params.Deleted) {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized (viewer cannot filter)"})
			return
		}
		if params.Approved == nil {
			params.Approved = &defTrue
		}
		if params.OwnerApproved == nil {
			params.OwnerApproved = &defTrue
		}
		if params.Current == nil {
			params.Current = &defTrue
		}
		if params.Deleted == nil {
			params.Deleted = &defFalse
		}
		if params.Revoked == nil {
			params.Revoked = &defFalse
		}
		if params.Denied == nil {
			params.Denied = &defFalse
		}
	}

	var grantList []store.Grant
	q := s.db
	if params.Claim == nil || !*params.Claim {
		q = q.Where("not grants.is_claim")
	} else {
		q = q.Where("grants.is_claim")
	}
	if spectrumId != nil {
		q = q.Where("Grants.spectrum_id = ?", *spectrumId)
	}
	if len(elementIdList) > 0 {
		q = q.Where("Grants.element_id in ?", elementIdList)
	}
	if params.Current != nil {
		t := time.Now().UTC()
		if *params.Current {
			q = q.Where("Grants.starts_at <= ? and (Grants.expires_at is NULL or Grants.expires_at > ?)", t, t)
		} else {
			q = q.Where("not (Grants.starts_at <= ? and (Grants.expires_at is NULL or Grants.expires_at > ?))", t, t)
		}
	}
	if params.Approved != nil {
		if !*params.Approved {
			q = q.Where("Grants.approved_at is NULL")
		} else {
			q = q.Where("Grants.approved_at is not NULL")
		}
	}
	if params.OwnerApproved != nil {
		if !*params.OwnerApproved {
			q = q.Where("Grants.owner_approved_at is NULL")
		} else {
			q = q.Where("Grants.owner_approved_at is not NULL")
		}
	}
	if params.Denied != nil {
		if !*params.Denied {
			q = q.Where("Grants.denied_at is NULL")
		} else {
			q = q.Where("Grants.denied_at is not NULL")
		}
	}
	if params.Revoked != nil {
		if !*params.Revoked {
			q = q.Where("Grants.revoked_at is NULL")
		} else {
			q = q.Where("Grants.revoked_at is not NULL")
		}
	}
	if params.Deleted == nil || !*params.Deleted {
		q = q.Where("Grants.deleted_at is NULL")
	} else {
		q = q.Where("Grants.deleted_at is not NULL")
	}
	if params.Grant != nil {
		q = q.Where(
			s.db.Where(store.MakeILikeClause(s.config, "Grants.name"), fmt.Sprintf("%%%s%%", *params.Grant)).
				Or(store.MakeILikeClause(s.config, "Grants.description"), fmt.Sprintf("%%%s%%", *params.Grant)))
	}
	sortDir := "desc"
	if params.SortAsc != nil && *params.SortAsc {
		sortDir = "asc"
	}
	if params.Sort != nil {
		q = q.Order("Grants." + *params.Sort + " " + sortDir)
	} else {
		q = q.Order("Grants.updated_at " + sortDir)
	}

	var total int64
	cres := q.Model(&store.Grant{}).Count(&total)
	if cres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": cres.Error.Error()})
		return
	}

	if c.GetBool("zms.elaborate") {
		q = q.Preload("Constraints.Constraint")
		q = q.Preload("IntConstraints.IntConstraint")
		q = q.Preload("RtIntConstraints.RtIntConstraint")
		q = q.Preload("RadioPorts.RadioPort")
		q = q.Preload("Logs")
		q = q.Preload("Replacement")
		q = q.Preload("Replaces")
	}

	qres := q.Scopes(store.Paginate(page, itemsPerPage)).Find(&grantList)
	if qres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": cres.Error.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"grants": grantList, "page": page, "total": total, "pages": int(math.Ceil(float64(total) / float64(itemsPerPage)))})
}

func (s *Server) createGrant(dm *store.Grant, userId uuid.UUID) (grant *store.Grant, status int, err error) {
	tt := true

	// Check OpStatus.  If unset, if there are constraints, set to
	// submitted; if not, set to unknown.  Allow Unknown status any time;
	// allow Submitted status if there is at least one constraint.
	if dm.OpStatus == "" {
		if len(dm.Constraints) > 0 {
			dm.OpStatus = store.OpStatusSubmitted
		} else {
			dm.OpStatus = store.OpStatusUnknown
		}
	} else if dm.OpStatus == store.OpStatusSubmitted {
		if len(dm.Constraints) < 1 {
			return nil, http.StatusBadRequest, fmt.Errorf("no constraints in grant and op_status set to 'submitted'")
		}
	} else if dm.OpStatus != store.OpStatusUnknown {
		return nil, http.StatusBadRequest, fmt.Errorf("invalid op_status %s", dm.OpStatus)
	}

	// Create an grant, overriding or forcing bits that should not be set
	// by caller.
	grant = &store.Grant{
		Id: uuid.New(), ElementId: dm.ElementId, Name: dm.Name, Description: dm.Description,
		ExtId: dm.ExtId, HtmlUrl: dm.HtmlUrl,
		StartsAt: dm.StartsAt, ExpiresAt: dm.ExpiresAt,
		SpectrumId: dm.SpectrumId, CreatorId: userId,
		Status: store.StatusCreated, OpStatus: dm.OpStatus,
		Priority: dm.Priority, IsClaim: dm.IsClaim,
	}

	// NB: if the user sets AllowSkipAcks, but the policy via which they are assigned
	// spectrum does not allow for it to be set, an error will be returned
    // via the scheduler.
	//
	// Claims are always allowed to skip acks.
	if dm.IsClaim {
		log.Debug().Msg(fmt.Sprintf("disabling acks for claim grant (%v)", grant.Id))
		grant.AllowSkipAcks = &tt
	} else if dm.AllowSkipAcks != nil {
		grant.AllowSkipAcks = dm.AllowSkipAcks
	} else if s.config.GrantAcksAllowSkipDefault {
		grant.AllowSkipAcks = &tt
	}

	// NB: if the user provides no RadioPorts, and any constraints are not
	// marked Exclusive, throw an error.
	//
	// NB: this may need relaxing if we later establish a use case for
	// non-exclusive constraints with no RadioPorts.  But at the moment, for
	// all practical purposes, we would have to treat such a Constraint
	// (e.g. non-exclusive with an Area, for instance).
	if len(dm.RadioPorts) == 0 {
		for _, dmc := range dm.Constraints {
			if !dmc.Constraint.Exclusive {
				return nil, http.StatusBadRequest, fmt.Errorf("no radio ports in grant, but constraint not marked exclusive")
			}
		}
	}

	// Handle RadioPorts association.  Note that only radio_port_id is
	// accepted; we do not allow hierarchical creation of device state in
	// this context.
	for _, dmp := range dm.RadioPorts {
		dmp.Id = uuid.New()
		dmp.GrantId = grant.Id
		// Check
		rp := &store.RadioPort{}
		if res := s.db.
			Joins("left join radios on radios.id=radio_ports.radio_id").
			Where("radios.element_id = ?", grant.ElementId).
			First(rp, dmp.RadioPortId); res.RowsAffected != 1 {
			return nil, http.StatusNotFound, fmt.Errorf("radio port %s not found", dmp.RadioPortId)
		}
		grant.RadioPorts = append(grant.RadioPorts, dmp)
	}
	for _, dmc := range dm.Constraints {
		dmc.Id = uuid.New()
		dmc.GrantId = grant.Id
		dmc.ConstraintId = uuid.New()
		dmc.Constraint.Id = dmc.ConstraintId
		// XXX NB: until we implement parallel and deconflicted geospatial
		// scheduling, decline all attempts to set Area or AreaId.
		if dmc.Constraint.AreaId != nil {
			log.Warn().Msg(fmt.Sprintf("forcing grant(%v).constraint(%v).AreaId to nil", grant.Id, dmc.Id))
			dmc.Constraint.AreaId = nil
		}
		
		grant.Constraints = append(grant.Constraints, dmc)
	}
	for _, dmic := range dm.IntConstraints {
		dmic.Id = uuid.New()
		dmic.GrantId = grant.Id
		dmic.IntConstraintId = uuid.New()
		dmic.IntConstraint.Id = dmic.IntConstraintId
		// XXX NB: until we implement parallel and deconflicted geospatial
		// scheduling, decline all attempts to set Area or AreaId.
		if dmic.IntConstraint.AreaId != nil {
			log.Warn().Msg(fmt.Sprintf("forcing grant(%v) IntConstraint(%v).AreaId to nil", grant.Id, dmic.Id))
			dmic.IntConstraint.AreaId = nil
		}
		
		grant.IntConstraints = append(grant.IntConstraints, dmic)
	}
	if len(grant.IntConstraints) == 0 && !dm.IsClaim {
		mp := float64(-120)
		defIntConstraint := store.IntConstraint{
			Id:       uuid.New(),
			Name:     "default(-120)",
			MaxPower: &mp,
		}
		defGrantIntConstraint := store.GrantIntConstraint{
			Id:              uuid.New(),
			GrantId:         grant.Id,
			IntConstraintId: defIntConstraint.Id,
			IntConstraint:   defIntConstraint,
		}
		grant.IntConstraints = append(grant.IntConstraints, defGrantIntConstraint)
	}
	for _, dmric := range dm.RtIntConstraints {
		dmric.Id = uuid.New()
		dmric.GrantId = grant.Id
		dmric.RtIntConstraintId = uuid.New()
		dmric.RtIntConstraint.Id = dmric.RtIntConstraintId
		grant.RtIntConstraints = append(grant.RtIntConstraints, dmric)
	}

	if res := s.db.Create(&grant); res.Error != nil {
		return nil, http.StatusInternalServerError, res.Error
	}

	// Reload to ensure we have proper state across relationships.
	rq := s.db.Preload("Constraints.Constraint").
		Preload("RadioPorts.RadioPort").
		Preload("IntConstraints.IntConstraint").
		Preload("RtIntConstraints.RtIntConstraint")

	if res := rq.First(&grant, grant.Id); res.Error != nil {
		return nil, http.StatusInternalServerError, res.Error
	}

	if err := s.gc.ChangeGrantStatus(grant, store.StatusCreated, "", &userId, nil); err != nil {
		m := fmt.Sprintf("error changing grant status to created: %s", err.Error())
		log.Error().Err(err).Msg(m)
		return nil, http.StatusInternalServerError, errors.New(m)
	}

	return grant, http.StatusCreated, nil
}

func (s *Server) CreateGrant(c *gin.Context) {
	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var dm store.Grant
	if err := c.ShouldBindJSON(&dm); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if dm.ElementId == uuid.Nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid element_id"})
		return
	}
	if dm.IsClaim {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "grant may not set is_claim"})
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleMember, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, nil, &dm.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	var grant *store.Grant
	if g, code, err := s.createGrant(&dm, tok.UserId); err != nil {
		c.AbortWithStatusJSON(code, gin.H{"error": err.Error()})
		return
	} else {
		grant = g
	}

	if grant.OpStatus == store.OpStatusSubmitted {
		if err := s.gc.Schedule(grant, tok.UserId, nil, ""); err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
	}
	
	c.JSON(http.StatusCreated, grant)
}

func (s *Server) GetGrant(c *gin.Context) {
	var err error

	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("grant_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	var grant store.Grant
	q := s.db
	if c.GetBool("zms.elaborate") {
		q = q.Preload("Constraints.Constraint")
		q = q.Preload("IntConstraints.IntConstraint")
		q = q.Preload("RtIntConstraints.RtIntConstraint")
		q = q.Preload("RadioPorts.RadioPort")
		q = q.Preload("Logs")
		q = q.Preload("Replacement")
		q = q.Preload("Replaces")
	}
	res := q.First(&grant, id)
	if res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRoleOrViewerPolicy(policy.RoleViewer, policy.GreaterOrEqual)
	var match bool
	var policyName string
	if match, policyName = s.CheckPolicyMiddleware(tok, nil, &grant.ElementId, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Viewers can only see the grant if approved
	if policyName == policy.MatchViewer && grant.ApprovedAt == nil {
		c.AbortWithStatus(http.StatusForbidden)
		return
	}

	c.JSON(http.StatusOK, grant)
}

func (s *Server) UpdateGrant(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("grant_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var um store.Grant
	if err := c.ShouldBindJSON(&um); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// We need to compare the Grant.ElementId and Grant.CreatorId with
	// the caller, to validate the match-element-role and match-user
	// policies.
	var grant store.Grant
	if res := s.db.First(&grant, id); res.Error != nil {
		if errors.Is(res.Error, gorm.ErrRecordNotFound) {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "grant not found"})
			return
		} else  {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
			return
		}
	}

	// Check policy.  Element managers and up can modify any managed
	// grant.  Member/Operators can only modify grants they created.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleMember, policy.GreaterOrEqual)
	match, policyName := s.CheckPolicyMiddleware(tok, &grant.CreatorId, &grant.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Validations that require authentication.
	if slices.Contains(store.TerminalGrantStatusList, grant.Status) {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "cannot modify terminal grant"})
		return
	}

	// Figure what/if the status changed.  We look for changes to Status and
	// OpStatus.
	oldStatus := grant.Status
	oldOpStatus := grant.OpStatus

	if oldStatus != um.Status {
		if policyName != policy.MatchAdmin {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": fmt.Sprintf("non-admins may not directly set status (%s)", policyName)})
			return
		} else if um.Status != "active" && um.Status != "paused" && um.Status != "revoked" && um.Status != "denied" {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": fmt.Sprintf("admins may not directly set status to %s (%s)", um.Status, policyName)})
			return
		}
		if err := s.gc.ChangeGrantStatus(&grant, um.Status, "", &tok.UserId, nil); err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": fmt.Sprintf("error changing grant %s from status=%s to %s: %s", grant.Id, oldStatus, um.Status, err.Error())})
			return
		}
		// Refresh after updates.
		q := s.db
		if c.GetBool("zms.elaborate") {
			q = q.Preload("Constraints.Constraint")
			q = q.Preload("IntConstraints.IntConstraint")
			q = q.Preload("RtIntConstraints.RtIntConstraint")
			q = q.Preload("RadioPorts.RadioPort")
			q = q.Preload("Logs")
			q = q.Preload("Replacement")
			q = q.Preload("Replaces")
		}
		q.First(&grant, id)

		c.JSON(http.StatusOK, &grant)
		return
	} else if oldOpStatus != um.OpStatus || (grant.OpStatusUpdatedAt != nil && um.OpStatusUpdatedAt != nil && *grant.OpStatusUpdatedAt != *um.OpStatusUpdatedAt) || (grant.OpStatusUpdatedAt == nil && um.OpStatusUpdatedAt != nil) {
		if err := s.gc.ChangeGrantOpStatus(&grant, um.OpStatus, "", &tok.UserId, nil); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": fmt.Sprintf("error changing grant %s from op_status=%s to %s: %s", grant.Id, oldOpStatus, um.OpStatus, err.Error())})
			return
		}
		// Refresh after updates.
		q := s.db
		if c.GetBool("zms.elaborate") {
			q = q.Preload("Constraints.Constraint")
			q = q.Preload("IntConstraints.IntConstraint")
			q = q.Preload("RtIntConstraints.RtIntConstraint")
			q = q.Preload("RadioPorts.RadioPort")
			q = q.Preload("Logs")
			q = q.Preload("Replacement")
			q = q.Preload("Replaces")
		}
		q.First(&grant, id)

		c.JSON(http.StatusOK, &grant)
		return
	} else {
		if grant.ApprovedAt != nil && policyName != policy.MatchAdmin {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": fmt.Sprintf("non-admin cannot modify approved grant (%s)", policyName)})
			return
		}

		// Assume obedience/compliance to the x-immutable-on-put annotations,
		// and only update mutable fields.

		// Change some fields regardless, without sending notifications.
		grant.Name = um.Name
		grant.Description = um.Description
		grant.ExtId = um.ExtId
		grant.HtmlUrl = grant.HtmlUrl
		//grant.StartsAt = um.StartsAt
		//grant.ExpiresAt = um.ExpiresAt
		//grant.SpectrumId = um.SpectrumId

		grant.UpdaterId = &tok.UserId

		sres := s.db.Save(&grant)
		if sres == nil || sres.Error != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": sres.Error.Error()})
			return
		}

		// Refresh after updates.
		q := s.db
		if c.GetBool("zms.elaborate") {
			q = q.Preload("Constraints.Constraint")
			q = q.Preload("IntConstraints.IntConstraint")
			q = q.Preload("RtIntConstraints.RtIntConstraint")
			q = q.Preload("RadioPorts.RadioPort")
			q = q.Preload("Logs")
			q = q.Preload("Replacement")
			q = q.Preload("Replaces")
		}
		q.First(&grant, id)

		// Generate an event with the update.
		oid := grant.Id.String()
		ot := *grant.UpdatedAt
		userIdStr := grant.CreatorId.String()
		elementIdStr := grant.ElementId.String()
		eh := subscription.EventHeader{
			Type:       int32(event.EventType_ET_UPDATED),
			Code:       int32(zmc.EventCode_EC_GRANT),
			SourceType: int32(event.EventSourceType_EST_ZMC),
			SourceId:   s.config.ServiceId,
			Id:         uuid.New().String(),
			ObjectId:   &oid,
			Time:       &ot,
			UserId:     &userIdStr,
			ElementId:  &elementIdStr,
		}
		e := subscription.Event{
			Header: eh,
			Object: &grant,
		}
		go s.sm.Notify(&e)

		c.JSON(http.StatusOK, &grant)
		return
	}
}

func (s *Server) DeleteGrant(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("grant_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	//forceUpdate := c.GetBool("zms.force-update")

	// We need to compare the Grant.ElementId and Grant.CreatorId with
	// the caller, to validate the match-element-role and match-user
	// policies.  However, note that we do not return StatusNotFound until
	// after policy verification.  This is a bit pendantic.
	var grant store.Grant
	var creatorId *uuid.UUID
	var elementId *uuid.UUID
	res := s.db.First(&grant, id)
	if res.RowsAffected == 1 {
		creatorId = &grant.CreatorId
		elementId = &grant.ElementId
	}

	// Check policy.  Element managers and up can delete any managed
	// grant.  Member/Operators can only delete the grant they created.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleMember, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, creatorId, elementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	if res.RowsAffected < 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	if grant.DeletedAt != nil {
		c.AbortWithStatusJSON(http.StatusConflict, gin.H{"error": "grant already deleted"})
		return
	}

	if err := s.gc.ChangeGrantStatus(&grant, store.StatusDeleted, "", &tok.UserId, nil); err != nil {
		m := fmt.Sprintf("error changing grant status to deleted: %s", err.Error())
		log.Error().Err(err).Msg(m)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": m})
		return
	}

	c.Status(http.StatusOK)
	return
}

func (s *Server) ReplaceGrant(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("grant_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var dm store.Grant
	if err := c.ShouldBindJSON(&dm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// We need to compare the Grant.ElementId and Grant.CreatorId with
	// the caller, to validate the match-element-role and match-user
	// policies.
	var grant store.Grant
	if res := s.db.First(&grant, id); res.Error != nil {
		if errors.Is(res.Error, gorm.ErrRecordNotFound) {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "grant not found"})
			return
		} else  {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
			return
		}
	}

	// Check policy.  Element managers and up can replace any managed
	// grant.  Member/Operators can only replace grants they created.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleMember, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, &grant.CreatorId, &grant.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Validations that require authentication.
	if slices.Contains(store.TerminalGrantStatusList, grant.Status) {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "cannot modify terminal grant"})
		return
	}
	if dm.ElementId != grant.ElementId {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "element_id of replacing grant must match existing grant element_id"})
		return
	}

	// Reload the grant with relations, in case scheduler path needs
	// context.
	q := s.db.
		Preload("Constraints.Constraint").
		Preload("IntConstraints.IntConstraint").
		Preload("RtIntConstraints.RtIntConstraint").
		Preload("RadioPorts.RadioPort").
		Preload("Logs").
		Preload("Replacement").
		Preload("Replaces")
	if res := q.First(&grant, id); res.Error != nil {
		if errors.Is(res.Error, gorm.ErrRecordNotFound) {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "grant not found"})
			return
		} else  {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
			return
		}
	} else if grant.Replacement != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "grant already replaced"})
		return
	}

	// Artificial, partial: require only narrowed EIRP constraints.
	m := "replace can only reduce Constraints.MaxEirp"
	if len(grant.Constraints) != len(dm.Constraints) {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": m})
		return
	}
	for i, dc := range dm.Constraints {
		if dc.Constraint.MaxEirp > grant.Constraints[i].Constraint.MaxEirp {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": m})
			return
		}
	}
	var newGrant *store.Grant
	if g, code, err := s.createGrant(&dm, grant.CreatorId); err != nil {
		c.AbortWithStatusJSON(code, gin.H{"error": err.Error()})
		return
	} else {
		newGrant = g
	}

	replaceMsg := "replaced by nbapi"
	if err := s.gc.ReplaceGrant(&grant, newGrant, replaceMsg, nil, nil); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// Reload to ensure we have proper state across relationships.
	rq := s.db.
		Preload("Constraints.Constraint").
		Preload("RadioPorts.RadioPort").
		Preload("IntConstraints.IntConstraint").
		Preload("RtIntConstraints.RtIntConstraint").
		Preload("Logs").
		Preload("Replacement").
		Preload("Replaces")
	if res := rq.First(&newGrant, newGrant.Id); res.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	}

	c.JSON(http.StatusOK, newGrant)
	return
}

func (s *Server) CreateGrantReplacement(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("grant_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var dm store.GrantReplacement
	if err := c.ShouldBindJSON(&dm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if dm.GrantId != id {
		c.JSON(http.StatusBadRequest, gin.H{"error": "grant_id must match path grant_id parameter"})
		return
	}

	// We need to compare the Grant.ElementId and Grant.CreatorId with
	// the caller, to validate the match-element-role and match-user
	// policies.
	var grant store.Grant
	if res := s.db.First(&grant, id); res.Error != nil {
		if errors.Is(res.Error, gorm.ErrRecordNotFound) {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "grant not found"})
			return
		} else  {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
			return
		}
	}

	// Check policy.  Element managers and up can replace any managed
	// grant.  Member/Operators can only replace grants they created.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleMember, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, &grant.CreatorId, &grant.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Load referenced grant.  We might schedule, so just load the whole
	// thing right away, including relationships.
	var newGrant store.Grant
	q := s.db.
		Preload("Constraints.Constraint").
		Preload("IntConstraints.IntConstraint").
		Preload("RtIntConstraints.RtIntConstraint").
		Preload("RadioPorts.RadioPort").
		Preload("Logs").
		Preload("Replacement").
		Preload("Replaces")
	if res := q.First(&newGrant, dm.NewGrantId); res.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	} else if res.RowsAffected != 1 {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "new_grant_id not found"})
		return
	} else if grant.ElementId != newGrant.ElementId {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "grant and replacement grant (new_grant_id) element_ids do not match"})
		return
	} else if newGrant.Status != store.StatusCreated || newGrant.OpStatus != store.OpStatusUnknown {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "replacement grant must be status=created and not yet submitted (op_status=unknown)"})
		return
	}

	// Reload full original grant for additional checks.
	q = s.db.
		Preload("Constraints.Constraint").
		Preload("IntConstraints.IntConstraint").
		Preload("RtIntConstraints.RtIntConstraint").
		Preload("RadioPorts.RadioPort").
		Preload("Logs").
		Preload("Replacement").
		Preload("Replaces")
	if res := q.First(&grant, id); res.Error != nil {
		if errors.Is(res.Error, gorm.ErrRecordNotFound) {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "grant not found"})
			return
		} else  {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
			return
		}
	}

	// Artificial, partial: require only narrowed EIRP constraints.
	m := "replace can only reduce Constraints.MaxEirp"
	if len(grant.Constraints) != len(newGrant.Constraints) {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": m})
		return
	}
	for i, dc := range newGrant.Constraints {
		if dc.Constraint.MaxEirp > grant.Constraints[i].Constraint.MaxEirp {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": m})
			return
		}
	}

	replaceMsg := "replaced by nbapi"
	if dm.Description != "" {
		replaceMsg = dm.Description + " (" + replaceMsg + ")"
	}
	if err := s.gc.ReplaceGrant(&grant, &newGrant, replaceMsg, nil, nil); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// Reload to ensure we have proper state across relationships.
	rq := s.db.
		Preload("Constraints.Constraint").
		Preload("RadioPorts.RadioPort").
		Preload("IntConstraints.IntConstraint").
		Preload("RtIntConstraints.RtIntConstraint").
		Preload("Logs").
		Preload("Replacement").
		Preload("Replaces")
	if res := rq.First(&newGrant, newGrant.Id); res.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	}

	c.JSON(http.StatusOK, grant.Replacement)
	return
}

func (s *Server) CreateGrantConstraint(c *gin.Context) {
	// Check required path parameters.
	var grantId uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("grant_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		grantId = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var dm store.Constraint
	if err := c.ShouldBindJSON(&dm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Look up Grant.
	var grant store.Grant
	if res := s.db.First(&grant, grantId); res.Error != nil || res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	if dm.AreaId != nil {
		if res := s.db.Where(&store.Area{Id: *dm.AreaId}).First(&store.Area{}); res.RowsAffected < 1 {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": fmt.Sprintf("area %s not found", *dm.AreaId)})
			return
		}
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRoleOrUserPolicy(policy.RoleManager, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, nil, &grant.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Additional post-auth validation.
	if grant.Status != store.StatusCreated || grant.OpStatus != store.OpStatusUnknown {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "cannot modify submitted grant"})
		return
	}

	// NB: for now, spectrum is auto-approved, so allow new constraints as
	// well.
	t := time.Now().UTC()
	constraint := store.Constraint{
		Id: uuid.New(), MinFreq: dm.MinFreq, MaxFreq: dm.MaxFreq,
		Bandwidth: dm.Bandwidth, MaxEirp: dm.MaxEirp, MinEirp: dm.MinEirp,
		Exclusive: dm.Exclusive, AreaId: dm.AreaId,
	}
	grantConstraint := store.GrantConstraint{
		Id: uuid.New(), GrantId: grantId, Constraint: constraint,
	}

	if res := s.db.Create(&grantConstraint); res.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	} else {
		c.JSON(http.StatusCreated, constraint)
	}

	// Reload Grant for notification.
	qR := s.db.Preload("Constraints.Constraint").
		Preload("IntConstraints.IntConstraint").
		Preload("RtIntConstraints.RtIntConstraint").
		Preload("RadioPorts.RadioPort")
	if res := qR.First(&grant, grantId); res.Error != nil || res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Send a grant updated event.
	oid := grant.Id.String()
	userIdStr := grant.CreatorId.String()
	elementIdStr := grant.ElementId.String()
	eh := subscription.EventHeader{
		Type:       int32(event.EventType_ET_UPDATED),
		Code:       int32(zmc.EventCode_EC_GRANT),
		SourceType: int32(event.EventSourceType_EST_ZMC),
		SourceId:   s.config.ServiceId,
		Id:         uuid.New().String(),
		ObjectId:   &oid,
		Time:       &t,
		UserId:     &userIdStr,
		ElementId:  &elementIdStr,
	}
	e := subscription.Event{
		Header: eh,
		Object: &grant,
	}
	go s.sm.Notify(&e)
}

func (s *Server) CreateGrantIntConstraint(c *gin.Context) {
	// Check required path parameters.
	var grantId uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("grant_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		grantId = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var dm store.IntConstraint
	if err := c.ShouldBindJSON(&dm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Look up Grant.
	var grant store.Grant
	if res := s.db.First(&grant, grantId); res.Error != nil || res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	if dm.AreaId != nil {
		if res := s.db.Where(&store.Area{Id: *dm.AreaId}).First(&store.Area{}); res.RowsAffected < 1 {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": fmt.Sprintf("area %s not found", *dm.AreaId)})
			return
		}
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRoleOrUserPolicy(policy.RoleManager, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, nil, &grant.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Additional post-auth validation.
	if grant.Status != store.StatusCreated || grant.OpStatus != store.OpStatusUnknown {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "cannot modify submitted grant"})
		return
	}

	// NB: for now, spectrum is auto-approved, so allow new constraints as
	// well.
	t := time.Now().UTC()
	intConstraint := store.IntConstraint{
		Id: uuid.New(), Name: dm.Name, Description: dm.Description,
		MaxPower: dm.MaxPower, AreaId: dm.AreaId, Area: dm.Area,
	}
	grantIntConstraint := store.GrantIntConstraint{
		Id: uuid.New(), GrantId: grantId, IntConstraint: intConstraint,
	}

	if res := s.db.Create(&grantIntConstraint); res.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	} else {
		c.JSON(http.StatusCreated, intConstraint)
	}

	// Reload Grant for notification.
	qR := s.db.Preload("Constraints.Constraint").
		Preload("IntConstraints.IntConstraint").
		Preload("RtIntConstraints.RtIntConstraint").
		Preload("RadioPorts.RadioPort")
	if res := qR.First(&grant, grantId); res.Error != nil || res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Send a grant updated event.
	oid := grant.Id.String()
	userIdStr := grant.CreatorId.String()
	elementIdStr := grant.ElementId.String()
	eh := subscription.EventHeader{
		Type:       int32(event.EventType_ET_UPDATED),
		Code:       int32(zmc.EventCode_EC_GRANT),
		SourceType: int32(event.EventSourceType_EST_ZMC),
		SourceId:   s.config.ServiceId,
		Id:         uuid.New().String(),
		ObjectId:   &oid,
		Time:       &t,
		UserId:     &userIdStr,
		ElementId:  &elementIdStr,
	}
	e := subscription.Event{
		Header: eh,
		Object: &grant,
	}
	go s.sm.Notify(&e)
}

func (s *Server) CreateGrantRtIntConstraint(c *gin.Context) {
	// Check required path parameters.
	var grantId uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("grant_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		grantId = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var dm store.RtIntConstraint
	if err := c.ShouldBindJSON(&dm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Look up Grant.
	var grant store.Grant
	if res := s.db.First(&grant, grantId); res.Error != nil || res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRoleOrUserPolicy(policy.RoleManager, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, nil, &grant.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Additional post-auth validation.
	if grant.Status != store.StatusCreated || grant.OpStatus != store.OpStatusUnknown {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "cannot modify submitted grant"})
		return
	}

	// NB: for now, spectrum is auto-approved, so allow new constraints as
	// well.
	t := time.Now().UTC()
	rtIntConstraint := store.RtIntConstraint{
		Id: uuid.New(), Name: dm.Name, Description: dm.Description,
		Metric: dm.Metric, Comparator: dm.Comparator, Value: dm.Value,
		Aggregator: dm.Aggregator, Period: dm.Period, Criticality: dm.Criticality,
	}
	grantRtIntConstraint := store.GrantRtIntConstraint{
		Id: uuid.New(), GrantId: grantId, RtIntConstraint: rtIntConstraint,
	}

	if res := s.db.Create(&grantRtIntConstraint); res.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	} else {
		c.JSON(http.StatusCreated, rtIntConstraint)
	}

	// Reload Grant for notification.
	qR := s.db.Preload("Constraints.Constraint").
		Preload("IntConstraints.IntConstraint").
		Preload("RtIntConstraints.RtIntConstraint").
		Preload("RadioPorts.RadioPort")
	if res := qR.First(&grant, grantId); res.Error != nil || res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Send a grant updated event.
	oid := grant.Id.String()
	userIdStr := grant.CreatorId.String()
	elementIdStr := grant.ElementId.String()
	eh := subscription.EventHeader{
		Type:       int32(event.EventType_ET_UPDATED),
		Code:       int32(zmc.EventCode_EC_GRANT),
		SourceType: int32(event.EventSourceType_EST_ZMC),
		SourceId:   s.config.ServiceId,
		Id:         uuid.New().String(),
		ObjectId:   &oid,
		Time:       &t,
		UserId:     &userIdStr,
		ElementId:  &elementIdStr,
	}
	e := subscription.Event{
		Header: eh,
		Object: &grant,
	}
	go s.sm.Notify(&e)
}

type UpdateGrantOpStatusModel struct {
	OpStatus store.GrantOpStatus `json:"op_status" binding:"required"`
	Message string `json:"message"`
}
func (s *Server) UpdateGrantOpStatus(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("grant_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Initial input validation.
	var um UpdateGrantOpStatusModel
	if err := c.ShouldBindJSON(&um); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	//forceUpdate := c.GetBool("zms.force-update")

	// We need to compare the Grant.ElementId and Grant.CreatorId with
	// the caller, to validate the match-element-role and match-user
	// policies.  However, note that we do not return StatusNotFound until
	// after policy verification.  This is a bit pendantic.
	var grant store.Grant
	var creatorId *uuid.UUID
	var elementId *uuid.UUID
	res := s.db.First(&grant, id)
	if res.RowsAffected == 1 {
		creatorId = &grant.CreatorId
		elementId = &grant.ElementId
	}

	// Check policy.  Element managers and up can update any managed
	// grant.  Member/Operators can only update the grant they created.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleMember, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, creatorId, elementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	if res.RowsAffected < 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Reload the grant fully so that the scheduler has all constraints if
	// it is invoked.
	if res := s.db.
		Preload("Constraints.Constraint").
		Preload("IntConstraints.IntConstraint").
		Preload("RtIntConstraints.RtIntConstraint").
		Preload("RadioPorts.RadioPort").
		First(&grant, id); res.Error != nil {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	oldOpStatus := grant.OpStatus
	if err := s.gc.ChangeGrantOpStatus(&grant, um.OpStatus, um.Message, &tok.UserId, nil); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": fmt.Sprintf("error changing grant %s from op_status=%s to %s: %s", grant.Id, oldOpStatus, um.OpStatus, err.Error())})
		return
	}
	// Refresh after updates.
	q := s.db
	if c.GetBool("zms.elaborate") {
		q = q.Preload("Constraints.Constraint")
		q = q.Preload("IntConstraints.IntConstraint")
		q = q.Preload("RtIntConstraints.RtIntConstraint")
		q = q.Preload("RadioPorts.RadioPort")
		q = q.Preload("Logs")
		q = q.Preload("Replacement")
		q = q.Preload("Replaces")
	}
	q.First(&grant, id)

	c.JSON(http.StatusOK, &grant)
	return
}

type Tardys3 struct {
	Id              uuid.UUID               `json:"transactionId" binding:"required"`
	PublishedAt     time.Time               `json:"dateTimePublished" binding:"required"`
	CreatedAt       time.Time               `json:"dateTimeCreated" binding:"required"`
	Checksum        string                  `json:"checksum" binding:"required"`
	ScheduledEvents []Tardys3ScheduledEvent `json:"scheduledEvents" binding:"dive,required"`
}

type Tardys4 struct {
	Id              uuid.UUID               `json:"transactionId" binding:"required"`
	PublishedAt     time.Time               `json:"dateTimePublished" binding:"required"`
	CreatedAt       time.Time               `json:"dateTimeCreated" binding:"required"`
	Checksum        string                  `json:"checksum" binding:"required"`
	ScheduledEvents []Tardys4ScheduledEvent `json:"scheduledEvents" binding:"dive,required"`
}

type Tardys3ScheduledEvent struct {
	EventId    uuid.UUID   `json:"eventId" binding:"required"`
	DpaId      uuid.UUID   `json:"dpaId" binding:"required"`
	DpaName    string      `json:"dpaName" binding:"required"`
	StartsAt   time.Time   `json:"dateTimeStart" binding:"required"`
	ExpiresAt  time.Time   `json:"dateTimeEnd" binding:"required"`
	Recurrence *string     `json:"recurrence,omitempty"`
	Channels   []uuid.UUID `json:"channels" binding:"required"`
}

type Tardys4ScheduledEvent struct {
	EventId     uuid.UUID  `json:"eventId" binding:"required"`
	DpaId       *uuid.UUID `json:"dpaId,omitempty"`
	StartsAt    time.Time  `json:"dateTimeStart" binding:"required"`
	ExpiresAt   time.Time  `json:"dateTimeEnd" binding:"required""`
	X           *float64   `json:"locLong,omitempty"`
	Y           *float64   `json:"locLat,omitempty"`
	Z           *float64   `json:"locElevation,omitempty"`
	LocRadius   *float64   `json:"locRadius,omitempty"`
	CoordType   string     `json:"coordType" binding:"required"`
	EventStatus string     `json:"eventStatus" binding:"required"`
	MinFreq     int64      `json:"freqStart" binding:"required"`
	MaxFreq     int64      `json:"freqStop" binding:"required"`
	RegionSize  float64    `json:"regionSize" binding:"required"`
	RegionX     float64    `json:"regionX" binding:"required"`
	RegionY     float64    `json:"regionY" binding:"required"`
}

type TardysScheduledEvent interface {
	GetStartsExpiresAt() (time.Time, time.Time)
	GetMinMaxFreq() (int64, int64)
}

func (t Tardys3ScheduledEvent) GetStartsExpiresAt() (time.Time, time.Time) {
	return t.StartsAt, t.ExpiresAt
}

func (t Tardys3ScheduledEvent) GetMinMaxFreq() (int64, int64) {
	// These are defined UUIDs mapping to frequency values in the TARDYS3 IDD
	var channelFreqRanges = map[string]struct {
		MinFreq int64
		MaxFreq int64
	}{
		"4385ae93-5466-48d4-8024-14442193d783": {MinFreq: 3550000000, MaxFreq: 3560000000},
		"7a7c3ccf-0e71-476f-8a5b-d7fd420e9442": {MinFreq: 3560000000, MaxFreq: 3570000000},
		"49d38292-02d6-4afe-b491-c5d6ab89c79c": {MinFreq: 3570000000, MaxFreq: 3580000000},
		"4e819f77-65d5-4bbd-a6ae-8522b2f6b330": {MinFreq: 3580000000, MaxFreq: 3590000000},
		"9b6e6a06-d939-4053-a055-a6e4cc5a3ab9": {MinFreq: 3590000000, MaxFreq: 3600000000},
		"70e2550c-4b00-4e23-ae1a-cce3f1dc35b7": {MinFreq: 3600000000, MaxFreq: 3610000000},
		"f3d585d8-9f74-4dea-9c01-16a06940f291": {MinFreq: 3610000000, MaxFreq: 3620000000},
		"99af70ea-34a0-4e08-9bd3-d229746f426d": {MinFreq: 3620000000, MaxFreq: 3630000000},
		"22907118-8112-4ffa-9b33-dbde66312ea0": {MinFreq: 3630000000, MaxFreq: 3640000000},
		"da2693bd-4cba-495a-981d-3d6ce1b0cb8d": {MinFreq: 3640000000, MaxFreq: 3650000000},
	}

	var minFreq, maxFreq int64

	for _, channelID := range t.Channels {
		channelFreq, ok := channelFreqRanges[channelID.String()]
		if !ok {
			continue
		}

		// From list of channels get the minimum frequency
		if minFreq == 0 || channelFreq.MinFreq < minFreq {
			minFreq = channelFreq.MinFreq
		}

		// From list of channels get the maximum frequency
		if channelFreq.MaxFreq > maxFreq {
			maxFreq = channelFreq.MaxFreq
		}
	}

	return minFreq, maxFreq
}

func (t Tardys4ScheduledEvent) GetStartsExpiresAt() (time.Time, time.Time) {
	return t.StartsAt, t.ExpiresAt
}

func (t Tardys4ScheduledEvent) GetMinMaxFreq() (int64, int64) {
	return t.MinFreq, t.MaxFreq
}

func (s *Server) CreateTardysReservation(c *gin.Context) {
	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	elementIdList := tok.GetElementIds()
	if len(elementIdList) != 1 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "token must be associated with a single element"})
		return
	}

	elementId := elementIdList[0]

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleMember, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, nil, elementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	var scheduledEvents []TardysScheduledEvent

	// Check if it is Tardys3 or 4 and extract the relevant ScheduledEvents
	var dm3 Tardys3
	if err := c.ShouldBindBodyWith(&dm3, binding.JSON); err == nil {
		for _, event := range dm3.ScheduledEvents {
			scheduledEvents = append(scheduledEvents, TardysScheduledEvent(event))
		}
	}

	var dm4 Tardys4
	if err := c.ShouldBindBodyWith(&dm4, binding.JSON); err == nil {
		for _, event := range dm4.ScheduledEvents {
			scheduledEvents = append(scheduledEvents, TardysScheduledEvent(event))
		}
	}

	// Did not match a Tardys3 or Tardys4 format
	if len(scheduledEvents) == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "invalid Tardys3/4 reservation"})
		return
	}

	// NB: some checks below are inline.

	grants := []store.Grant{}

	// Map each ScheduledEvent to a grant
	for _, scheduledEvent := range scheduledEvents {
		// Get relevant info from the Tardys3/4 reservation
		startsAt, expiresAt := scheduledEvent.GetStartsExpiresAt()
		minFreq, maxFreq := scheduledEvent.GetMinMaxFreq()

		// Create an grant, overriding or forcing bits that should not be set
		// by caller.
		grant := store.Grant{
			Id: uuid.New(),
			ElementId: *elementId,
			CreatorId: tok.UserId,
			StartsAt: startsAt,
			ExpiresAt: &expiresAt,
		}

		constraint := store.Constraint {
			Id: uuid.New(),
			MinFreq: minFreq,
			MaxFreq: maxFreq,
			Exclusive: true,
		}

		grant_constraint := store.GrantConstraint {
			Id: uuid.New(),
			GrantId: grant.Id,
			ConstraintId: constraint.Id,
			Constraint: constraint,
		}

		grant.Constraints = append(grant.Constraints, grant_constraint)

		if res := s.db.Create(&grant); res.Error != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
			return
		}

		// append to grants list
		grants = append(grants, grant)

		// Attempt an inline schedule.
		if err := s.gc.Schedule(&grant, tok.UserId, nil, ""); err != nil {
			log.Error().Msg(fmt.Sprintf("tardys4 schedule error: %s", err.Error()))
		}
	}

	c.JSON(http.StatusCreated, gin.H{"grants": grants})
}

type listClaimsQueryParams struct {
	//Pending gin support...
	//ElementId *string `form:"element_id" binding:"omitempty,uuid"`
	Claim    *string `form:"claim" binding:"omitempty"`
	Ext      *string `form:"ext" binding:"omitempty"`
	ExtId    *string `form:"ext_id" binding:"omitempty"`
	Verified *bool   `form:"verified" binding:"omitempty"`
	Denied   *bool   `form:"denied" binding:"omitempty"`
	Deleted  *bool   `form:"deleted" binding:"omitempty"`
	Sort    *string `form:"sort" binding:"omitempty,oneof=element_id ext_id type source html_url name created_at updated_at verified_at denied_at deleted_at"`
	SortAsc *bool   `form:"sort_asc" binding:"omitempty"`
}

func (s *Server) listClaims(c *gin.Context) {
	var err error

	// Check optional filter query parameters.
	params := listClaimsQueryParams{}
	if err = c.ShouldBindQuery(&params); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// Unmarshal query uuids.
	var elementId *uuid.UUID
	if p, exists := c.GetQuery("element_id"); exists {
		if idParsed, err := uuid.Parse(p); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid element_id uuid"})
			return
		} else {
			elementId = &idParsed
		}
	}
	var spectrumId *uuid.UUID
	if p, exists := c.GetQuery("spectrum_id"); exists {
		if idParsed, err := uuid.Parse(p); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid spectrum_id uuid"})
			return
		} else {
			spectrumId = &idParsed
		}
	}

	page, itemsPerPage := GetPaginateParams(c)

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	if tok, err = s.RequireContextToken(c); err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	// Check policy.
	// XXX: really want to return unapproved claims as well
	// for callers with element roles, even if they do not specify
	// element_id.  Just need to apply the policy check to each element_id
	// in the token, and then allow claims matching those element_ids to be
	// unapproved or approved.
	policies := policy.MakeMatchAdminOrElementRoleOrViewerPolicy(policy.RoleViewer, policy.GreaterOrEqual)
	var match bool
	var policyName string
	if match, policyName = s.CheckPolicyMiddleware(tok, nil, elementId, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Admins can filter for anything; Viewers (e.g. if they have no element
	// binding) cannot filter; other users who have roles in Elements can
	// filter within those Elements.
	var elementIdList []*uuid.UUID
	if elementId != nil {
		elementIdList = []*uuid.UUID{elementId}
	} else if policyName != policy.MatchAdmin {
		elementIdList = tok.GetElementIds()
	}

	defTrue := true
	defFalse := false
	if policyName == policy.MatchViewer && len(elementIdList) == 0 {
		if ((params.Verified != nil && !*params.Verified) ||
			(params.Denied != nil && *params.Denied) ||
			(params.Deleted != nil && *params.Deleted)) {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized (viewer cannot filter)"})
			return
		}
		if params.Verified == nil {
			params.Verified = &defTrue
		}
		if params.Deleted == nil {
			params.Deleted = &defFalse
		}
		if params.Denied == nil {
			params.Denied = &defFalse
		}
	}

	var claimList []store.Claim
	q := s.db
	if spectrumId != nil {
		q = q.Where("Claims.spectrum_id = ?", *spectrumId)
	}
	if len(elementIdList) > 0 {
		q = q.Where("Claims.element_id in ?", elementIdList)
	}
	if params.Verified != nil {
		if !*params.Verified {
			q = q.Where("Claims.verified_at is NULL")
		} else {
			q = q.Where("Claims.verified_at is not NULL")
		}
	}
	if params.Denied != nil {
		if !*params.Denied {
			q = q.Where("Claims.denied_at is NULL")
		} else {
			q = q.Where("Claims.denied_at is not NULL")
		}
	}
	if params.Deleted == nil || !*params.Deleted {
		q = q.Where("Claims.deleted_at is NULL")
	} else {
		q = q.Where("Claims.deleted_at is not NULL")
	}
	if params.Claim != nil {
		q = q.Where(
			s.db.Where(store.MakeILikeClause(s.config, "Claims.name"), fmt.Sprintf("%%%s%%", *params.Claim)).
				Or(store.MakeILikeClause(s.config, "Claims.description"), fmt.Sprintf("%%%s%%", *params.Claim)).
				Or(store.MakeILikeClause(s.config, "Claims.html_url"), fmt.Sprintf("%%%s%%", *params.Claim)))
	}
	if params.Ext != nil {
		q = q.Where(
			s.db.Where(store.MakeILikeClause(s.config, "Claims.type"), fmt.Sprintf("%%%s%%", *params.Ext)).
				Or(store.MakeILikeClause(s.config, "Claims.source"), fmt.Sprintf("%%%s%%", *params.Ext)))
	}
	if params.ExtId != nil {
		q = q.Where(store.MakeILikeClause(s.config, "Claims.ext_id"), fmt.Sprintf("%%%s%%", *params.ExtId))
	}
	sortDir := "desc"
	if params.SortAsc != nil && *params.SortAsc {
		sortDir = "asc"
	}
	if params.Sort != nil {
		q = q.Order("Claims." + *params.Sort + " " + sortDir)
	} else {
		q = q.Order("Claims.updated_at " + sortDir)
	}

	var total int64
	cres := q.Model(&store.Claim{}).Count(&total)
	if cres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": cres.Error.Error()})
		return
	}

	if c.GetBool("zms.elaborate") {
		q = q.Preload("Grant.Constraints.Constraint")
		q = q.Preload("Grant.IntConstraints.IntConstraint")
		q = q.Preload("Grant.RtIntConstraints.RtIntConstraint")
		q = q.Preload("Grant.RadioPorts.RadioPort")
		q = q.Preload("Grant.Logs")
		q = q.Preload("Grant.Replacement")
		q = q.Preload("Grant.Replaces")
	}

	qres := q.Scopes(store.Paginate(page, itemsPerPage)).Find(&claimList)
	if qres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": cres.Error.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"claims": claimList, "page": page, "total": total, "pages": int(math.Ceil(float64(total) / float64(itemsPerPage)))})
}

func (s *Server) CreateClaim(c *gin.Context) {
	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var dm store.Claim
	if err := c.ShouldBindJSON(&dm); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if dm.ElementId == uuid.Nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid element_id"})
		return
	}
	if dm.ExtId == "" {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "must provide valid ext_id"})
		return
	}
	if dm.Type == "" {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "must provide valid type"})
		return
	}
	if dm.Source == "" {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "must provide valid source"})
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleOperator, policy.GreaterOrEqual)
	var match bool
	var tokenRoleBinding *client.CachedRoleBinding
	match, _, tokenRoleBinding = s.CheckPolicyMiddlewareExt(tok, nil, &dm.ElementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// For a Claim, caller must be an operator of the Element that delegated
	// the Spectrum.
	if tokenRoleBinding.Role.Value < policy.RoleOperator {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "only operator or higher roles are allowed to create grant claims."})
		return
	}

	// Check if claim with this ext_id/type/source exists.
	if res := s.db.Where("ext_id = ? and type = ? and source = ? and deleted_at is NULL", dm.ExtId, dm.Type, dm.Source).First(&store.Claim{}); res.RowsAffected == 1 {
		c.AbortWithStatusJSON(http.StatusConflict, gin.H{"error":fmt.Sprintf("claim (ext_id=% source=s% type=%s) unavailable", dm.ExtId, dm.Type, dm.Source)})
		return
	}

	// Create an claim, overriding or forcing bits that should not be set
	// by caller.  Don't save it yet, though.
	claim := store.Claim{
		Id: uuid.New(), ElementId: dm.ElementId, ExtId: dm.ExtId,
		Type: dm.Type, Name: dm.Name, Source: dm.Source, HtmlUrl: dm.HtmlUrl,
		Description: dm.Description, CreatorId: tok.UserId,
	}

	var grant *store.Grant
	if dm.Grant != nil {
		// Force IsClaim==true for Claims.
		dm.Grant.IsClaim = true
		dm.Grant.ClaimIdRef = &claim.Id

		if dm.Grant.ElementId != dm.ElementId {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid grant.element_id; must match claim element_id"})
			return
		}

		// For a Claim, SpectrumId must be set and valid.  The scheduler checks
		// to be sure caller has appropriate permission to use it.
		if dm.Grant.SpectrumId == nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "grant.spectrum_id must be set for claims"})
			return
		}
		cs := store.Spectrum{}
		if res := s.db.First(&cs, *dm.Grant.SpectrumId); res.Error != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": fmt.Sprintf("error looking up spectrum_id for claim: %s", res.Error.Error())})
			return
		} else if res.RowsAffected < 1 {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "spectrum_id for claim does not exist"})
			return
		} else if cs.ElementId != dm.ElementId {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "spectrum_id for grant claim is not owned by grant claim element_id"})
			return
		}

		// Create the grant.
		if g, code, err := s.createGrant(dm.Grant, tok.UserId); err != nil {
			c.AbortWithStatusJSON(code, gin.H{"error": err.Error()})
			return
		} else {
			grant = g
		}
	}

	if res := s.db.Create(&claim); res.Error != nil {
		if grant != nil {
			s.db.Delete(grant)
		}
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	}

	// Update the claim if there is an inline grant.
	if grant != nil {
		claim.GrantId = &grant.Id
		// NB XXX: trust all claims for now and immediately verify.
		claim.VerifiedAt = &claim.CreatedAt
		if res := s.db.Save(claim); res.Error != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": fmt.Sprintf("error saving claim: %s", err.Error())})
		}
		claim.Grant = grant

		// "Schedule" the grant.  This is typically a noop from the Claim's
		// perspective, unless the creator specified a legitimately lower
		// Priority than the max.  However, it may displace other existing
		// grants due to priority.
		approvalMsg := fmt.Sprintf("grant for claim %s", claim.Id)
		if err := s.gc.Schedule(grant, tok.UserId, nil, approvalMsg); err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		} else {
			// Handle possible (although unlikely) Claim denial.
			if grant.DeniedAt != nil {
				claim.DeniedAt = grant.DeniedAt
				if res := s.db.Save(claim); res.Error != nil {
					c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": fmt.Sprintf("error saving claim: %s", err.Error())})
				}
			}
		}
	}

	c.JSON(http.StatusCreated, claim)

	// Notify.
	oid := claim.Id.String()
	userIdStr := claim.CreatorId.String()
	elementIdStr := claim.ElementId.String()
	eh := subscription.EventHeader{
		Type:       int32(event.EventType_ET_CREATED),
		Code:       int32(zmc.EventCode_EC_CLAIM),
		SourceType: int32(event.EventSourceType_EST_ZMC),
		SourceId:   s.config.ServiceId,
		Id:         uuid.New().String(),
		ObjectId:   &oid,
		Time:       &claim.CreatedAt,
		UserId:     &userIdStr,
		ElementId:  &elementIdStr,
	}
	e := subscription.Event{
		Header: eh,
		Object: &claim,
	}
	go s.sm.Notify(&e)
}

func (s *Server) GetClaim(c *gin.Context) {
	var err error

	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("claim_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	var claim store.Claim
	q := s.db
	if c.GetBool("zms.elaborate") {
		q = q.Preload("Grant.Constraints.Constraint")
		q = q.Preload("Grant.IntConstraints.IntConstraint")
		q = q.Preload("Grant.RtIntConstraints.RtIntConstraint")
		q = q.Preload("Grant.RadioPorts.RadioPort")
		q = q.Preload("Grant.Logs")
		q = q.Preload("Grant.Replacement")
		q = q.Preload("Grant.Replaces")
	}
	res := q.First(&claim, id)
	if res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRoleOrViewerPolicy(policy.RoleViewer, policy.GreaterOrEqual)
	var match bool
	var policyName string
	if match, policyName = s.CheckPolicyMiddleware(tok, nil, &claim.ElementId, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Viewers can only see the claim if approved
	if policyName == policy.MatchViewer && claim.VerifiedAt == nil {
		c.AbortWithStatus(http.StatusForbidden)
		return
	}

	c.JSON(http.StatusOK, claim)
}

func (s *Server) UpdateClaim(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("claim_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Initial input validation.
	var um store.Claim
	if err := c.ShouldBindJSON(&um); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// We need to compare the Claim.ElementId and Claim.CreatorId with
	// the caller, to validate the match-element-role and match-user
	// policies.  However, note that we do not return StatusNotFound until
	// after policy verification.  This is a bit pendantic.
	var claim store.Claim
	var creatorId *uuid.UUID
	var elementId *uuid.UUID
	res := s.db.First(&claim, id)
	if res.RowsAffected == 1 {
		creatorId = &claim.CreatorId
		elementId = &claim.ElementId
	}

	// Check policy.  Element managers and up can modify any managed
	// claim.  Operators can only modify the claim they created.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleOperator, policy.GreaterOrEqual)
	match, policyName := s.CheckPolicyMiddleware(tok, creatorId, elementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	if res.RowsAffected < 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Validations that require authentication.
	if claim.DeletedAt != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "cannot modify deleted claim"})
		return
	}
	if claim.DeniedAt != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "cannot modify denied claim"})
		return
	}
	if claim.VerifiedAt != nil && policyName != policy.MatchAdmin {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "cannot modify verified claim unless admin"})
		return
	}
	var grant *store.Grant
	if claim.GrantId != nil && (um.GrantId == nil || *claim.GrantId != *um.GrantId) {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "cannot modify claim grant_id"})
		return
	} else if claim.GrantId == nil && um.GrantId != nil {
		var g store.Grant
		var cs store.Spectrum
		// Fully load the grant; we will send it to the scheduler.
		gq := s.db.Preload("Constraints.Constraint").
			Preload("IntConstraints.IntConstraint").
			Preload("RtIntConstraints.RtIntConstraint").
			Preload("RadioPorts.RadioPort").
			Preload("Logs").
			Preload("Replacement").
			Preload("Replaces")
		if gres := gq.First(&g, um.GrantId); gres.Error != nil || gres.RowsAffected != 1 {
			emsg := fmt.Sprintf("claim grant_id %s not found", um.GrantId)
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": emsg})
			return
		} else if policyName != policy.MatchAdmin && grant.CreatorId != claim.CreatorId {
			emsg := fmt.Sprintf("claim %s and grant %s creator_id do not match", claim.Id, g.Id)
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": emsg})
			return
		} else if g.Status != store.StatusCreated {
			emsg := fmt.Sprintf("cannot attach grant %s to claim %s: grant already operational (status=%v)", g.Id, claim.Id, g.Status)
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": emsg})
			return
		} else if g.SpectrumId == nil {
			emsg := fmt.Sprintf("cannot attach grant %s to claim %s: grant.spectrum_id not set", g.Id, claim.Id)
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": emsg})
			return
		} else if sres := s.db.First(&cs, *g.SpectrumId); sres.Error != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": fmt.Sprintf("error looking up grant.spectrum_id for claim: %s", sres.Error.Error())})
			return
		} else if res.RowsAffected < 1 {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "grant.spectrum_id for grant claim does not exist"})
			return
		} else if cs.ElementId != claim.ElementId {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "grant.spectrum_id for grant claim is not owned by grant claim element_id"})
			return
		} else {
			grant = &g
		}
	}

	claim.UpdaterId = &tok.UserId

	// Assume obedience/compliance to the x-immutable-on-put annotations,
	// and only update mutable fields.

	// Change some fields regardless, without sending notifications.
	claim.Description = um.Description
	claim.Name = um.Name
	claim.HtmlUrl = um.HtmlUrl

	if policyName == policy.MatchAdmin {
		claim.VerifiedAt = um.VerifiedAt
		claim.DeniedAt = um.DeniedAt
	}

	// If we are associating an existing grant with this claim, handle and
	// schedule it.
	if grant != nil {
		grant.IsClaim = true
		grant.ClaimIdRef = &claim.Id
		claim.GrantId = &grant.Id
		// NB XXX: trust all claims for now and immediately verify.
		t := time.Now().UTC()
		claim.VerifiedAt = &t
		if sres := s.db.Save(&claim); sres.Error != nil {
			emsg := fmt.Sprintf("error saving claim: %s", err.Error())
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": emsg})
			return
		}
		claim.Grant = grant

		// "Schedule" the grant.  This is typically a noop from the Claim's
		// perspective, unless the creator specified a legitimately lower
		// Priority than the max.  However, it may displace other existing
		// grants due to priority.
		approvalMsg := fmt.Sprintf("grant for claim %s", claim.Id)
		if err := s.gc.Schedule(grant, tok.UserId, nil, approvalMsg); err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		} else {
			// Handle possible (although unlikely) Claim denial.
			if grant.DeniedAt != nil {
				claim.DeniedAt = grant.DeniedAt
				if res := s.db.Save(claim); res.Error != nil {
					c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": fmt.Sprintf("error saving claim: %s", err.Error())})
				}
			}
		}
	} else 	if sres := s.db.Save(&claim); sres.Error != nil {
		emsg := fmt.Sprintf("error saving claim: %s", err.Error())
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": emsg})
		return
	}

	// Refresh after updates.
	q := s.db
	if c.GetBool("zms.elaborate") {
		q = q.Preload("Grant.Constraints.Constraint")
		q = q.Preload("Grant.IntConstraints.IntConstraint")
		q = q.Preload("Grant.RtIntConstraints.RtIntConstraint")
		q = q.Preload("Grant.RadioPorts.RadioPort")
		q = q.Preload("Grant.Logs")
		q = q.Preload("Grant.Replacement")
		q = q.Preload("Grant.Replaces")
	}
	q.First(&claim, id)

	c.JSON(http.StatusOK, &claim)

	// Notify.
	oid := claim.Id.String()
	userIdStr := claim.CreatorId.String()
	elementIdStr := claim.ElementId.String()
	eh := subscription.EventHeader{
		Type:       int32(event.EventType_ET_UPDATED),
		Code:       int32(zmc.EventCode_EC_CLAIM),
		SourceType: int32(event.EventSourceType_EST_ZMC),
		SourceId:   s.config.ServiceId,
		Id:         uuid.New().String(),
		ObjectId:   &oid,
		Time:       &claim.CreatedAt,
		UserId:     &userIdStr,
		ElementId:  &elementIdStr,
	}
	e := subscription.Event{
		Header: eh,
		Object: &claim,
	}
	go s.sm.Notify(&e)
}

func (s *Server) DeleteClaim(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("claim_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	//forceUpdate := c.GetBool("zms.force-update")

	// We need to compare the Claim.ElementId and Claim.CreatorId with
	// the caller, to validate the match-element-role and match-user
	// policies.  However, note that we do not return StatusNotFound until
	// after policy verification.  This is a bit pendantic.
	var claim store.Claim
	var creatorId *uuid.UUID
	var elementId *uuid.UUID
	res := s.db.Preload("Grant").First(&claim, id)
	if res.RowsAffected == 1 {
		creatorId = &claim.CreatorId
		elementId = &claim.ElementId
	}

	// Check policy.  Element managers and up can delete any managed
	// claim.  Operators can only delete the claim they created.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, creatorId, elementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	if res.RowsAffected < 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	if claim.DeletedAt != nil {
		c.AbortWithStatusJSON(http.StatusConflict, gin.H{"error": "claim already deleted"})
		return
	}
	// NB: do not worry if the underlying grant was already deleted.
	if false && claim.Grant.DeletedAt != nil {
		c.AbortWithStatusJSON(http.StatusConflict, gin.H{"error": "claim grant already deleted"})
		return
	}

	t := time.Now().UTC()
	claim.DeletedAt = &t
	claim.UpdaterId = &tok.UserId
	s.db.Save(&claim)

	if err := s.gc.ChangeGrantStatus(claim.Grant, store.StatusDeleted, "", &tok.UserId, nil); err != nil {
		m := fmt.Sprintf("error changing claim grant status to deleted: %s", err.Error())
		log.Error().Err(err).Msg(m)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": m})
		return
	}

	c.Status(http.StatusOK)

	// Notify.
	oid := claim.Id.String()
	userIdStr := claim.CreatorId.String()
	elementIdStr := claim.ElementId.String()
	eh := subscription.EventHeader{
		Type:       int32(event.EventType_ET_DELETED),
		Code:       int32(zmc.EventCode_EC_CLAIM),
		SourceType: int32(event.EventSourceType_EST_ZMC),
		SourceId:   s.config.ServiceId,
		Id:         uuid.New().String(),
		ObjectId:   &oid,
		Time:       &claim.CreatedAt,
		UserId:     &userIdStr,
		ElementId:  &elementIdStr,
	}
	e := subscription.Event{
		Header: eh,
		Object: &claim,
	}
	go s.sm.Notify(&e)
}

type ListMonitorsQueryParams struct {
	//Pending gin support...
	//ElementId *string `form:"element_id" binding:"omitempty,uuid"`
	//RadioPortId *string `form:"radio_port_id" binding:"omitempty,uuid"`
	//MonitoredRadioPortId *string `form:"monitored_radio_port_id" binding:"omitempty,uuid"`
	Monitor   *string `form:"monitor" binding:"omitempty"`
	Types     *string `form:"types" binding:"omitempty"`
	Formats   *string `form:"formats" binding:"omitempty"`
	Exclusive *bool   `form:"exclusive" binding:"omitempty"`
	Enabled   *bool   `form:"enabled" binding:"omitempty"`
	Deleted   *bool   `form:"deleted" binding:"omitempty"`
	Sort      *string `form:"sort" binding:"omitempty,oneof=name types formats exclusive enabled created_at updated_at deleted_at"`
	SortAsc   *bool   `form:"sort_asc" binding:"omitempty"`
}

func (s *Server) ListMonitors(c *gin.Context) {
	var err error

	// Check optional filter query parameters.
	params := ListMonitorsQueryParams{}
	if err = c.ShouldBindQuery(&params); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// Unmarshal query uuids.
	var elementId *uuid.UUID
	if p, exists := c.GetQuery("element_id"); exists {
		if idParsed, err := uuid.Parse(p); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid element_id uuid"})
			return
		} else {
			elementId = &idParsed
		}
	}
	// Unmarshal query uuids.
	var radioPortId *uuid.UUID
	if p, exists := c.GetQuery("radio_port_id"); exists {
		if idParsed, err := uuid.Parse(p); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid radio_port_id uuid"})
			return
		} else {
			radioPortId = &idParsed
		}
	}
	var monitoredRadioPortId *uuid.UUID
	if p, exists := c.GetQuery("monitored_radio_port_id"); exists {
		if idParsed, err := uuid.Parse(p); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid monitored_radio_port_id uuid"})
			return
		} else {
			monitoredRadioPortId = &idParsed
		}
	}

	page, itemsPerPage := GetPaginateParams(c)

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	if tok, err = s.RequireContextToken(c); err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRoleOrViewerPolicy(policy.RoleViewer, policy.GreaterOrEqual)
	var match bool
	var policyName string
	if match, policyName = s.CheckPolicyMiddleware(tok, nil, elementId, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Admins can filter for anything; Viewers (e.g. if they have no element
	// binding) cannot filter; other users who have roles in Elements can
	// filter within those Elements.
	var elementIdList []*uuid.UUID
	if elementId != nil {
		elementIdList = []*uuid.UUID{elementId}
	} else if policyName != policy.MatchAdmin {
		elementIdList = tok.GetElementIds()
	}

	defTrue := true
	defFalse := false
	if policyName == policy.MatchViewer && len(elementIdList) == 0 {
		if (params.Deleted != nil && *params.Deleted) ||
			(params.Enabled != nil && *params.Enabled) {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "viewer unauthorized (forbidden authorized filter)"})
			return
		}
		if params.Deleted == nil {
			params.Deleted = &defFalse
		}
		if params.Enabled == nil {
			params.Enabled = &defTrue
		}
	}

	var monitorList []store.Monitor
	q := s.db.Joins("left join radio_ports on radio_ports.id=monitors.radio_port_id").
		Joins("left join radios on radios.id=radio_ports.radio_id")
	if len(elementIdList) > 0 {
		q = q.Where("radios.element_id in ?", elementIdList)
	}
	if radioPortId != nil {
		q = q.Where("Monitors.radio_port_id = ?", *radioPortId)
	}
	if monitoredRadioPortId != nil {
		q = q.Where("Monitors.monitored_radio_port_id = ?", *monitoredRadioPortId)
	}
	if params.Enabled != nil {
		if !*params.Enabled {
			q = q.Where("not Monitors.enabled")
		} else {
			q = q.Where("Monitors.enabled")
		}
	}
	if params.Deleted == nil || !*params.Deleted {
		q = q.Where("Monitors.deleted_at is NULL")
	} else {
		q = q.Where("Monitors.deleted_at is not NULL")
	}
	if params.Exclusive != nil {
		if !*params.Exclusive {
			q = q.Where("not Monitors.exclusive")
		} else {
			q = q.Where("Monitors.exclusive")
		}
	}
	if params.Monitor != nil {
		q = q.Where(
			s.db.Where(store.MakeILikeClause(s.config, "Monitors.name"), fmt.Sprintf("%%%s%%", *params.Monitor)).
				Or(store.MakeILikeClause(s.config, "Monitors.description"), fmt.Sprintf("%%%s%%", *params.Monitor)).
				Or(store.MakeILikeClause(s.config, "Monitors.device_id"), fmt.Sprintf("%%%s%%", *params.Monitor)))
	}
	if params.Types != nil {
		q = q.Where(store.MakeILikeClause(s.config, "Monitors.types"), fmt.Sprintf("%%%s%%", *params.Types))
	}
	if params.Formats != nil {
		q = q.Where(store.MakeILikeClause(s.config, "Monitors.formats"), fmt.Sprintf("%%%s%%", *params.Formats))
	}
	sortDir := "desc"
	if params.SortAsc != nil && *params.SortAsc {
		sortDir = "asc"
	}
	if params.Sort != nil {
		q = q.Order("Monitors." + *params.Sort + " " + sortDir)
	} else {
		q = q.Order("Monitors.updated_at " + sortDir)
	}

	var total int64
	cres := q.Model(&store.Monitor{}).Count(&total)
	if cres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": cres.Error.Error()})
		return
	}

	if c.GetBool("zms.elaborate") {
		q = q.Preload("RadioPort").Preload("MonitoredRadioPort")
	}

	qres := q.Scopes(store.Paginate(page, itemsPerPage)).Find(&monitorList)
	if qres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": cres.Error.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"monitors": monitorList, "page": page, "total": total, "pages": int(math.Ceil(float64(total) / float64(itemsPerPage)))})
}

func (s *Server) CreateMonitor(c *gin.Context) {
	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var dm store.Monitor
	if err := c.ShouldBindJSON(&dm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if dm.RadioPortId == uuid.Nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "invalid radio_port_id"})
		return
	}

	var elementId *uuid.UUID
	var radio store.Radio
	if res := s.db.Joins("left join radio_ports on radios.id=radio_ports.radio_id").Where("radio_ports.id = ?", dm.RadioPortId).First(&radio); res.RowsAffected != 1 {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": fmt.Sprintf("radio port %s not found", dm.RadioPortId)})
		return
	} else {
		elementId = &radio.ElementId
	}
	if dm.MonitoredRadioPortId != nil {
		var mRadio store.Radio
		if res := s.db.Joins("left join radio_ports on radios.id=radio_ports.radio_id").Where("radio_ports.id = ?", *dm.MonitoredRadioPortId).First(&mRadio); res.RowsAffected != 1 {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": fmt.Sprintf("monitored radio port %s not found", *dm.MonitoredRadioPortId)})
			return
		} else if mRadio.ElementId != *elementId {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": fmt.Sprintf("monitored radio port %s not owned by monitor radio port %s", *dm.MonitoredRadioPortId, dm.RadioPortId)})
			return
		}
	}
	if dm.ElementId != nil && *dm.ElementId != *elementId {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": fmt.Sprintf("monitor.element_id (%s) does not match the radio_port's radio.element_id (%s)", *dm.ElementId, *elementId)})
		return
	} else if dm.ElementId == nil {
		dm.ElementId = elementId
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, nil, elementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Create an monitor, overriding or forcing bits that should not be set
	// by caller.
	monitor := store.Monitor{
		Id: uuid.New(), RadioPortId: dm.RadioPortId, Name: dm.Name, Description: dm.Description,
		DeviceId: dm.DeviceId, HtmlUrl: dm.HtmlUrl,
		Types: dm.Types, Formats: dm.Formats, Exclusive: dm.Exclusive, Enabled: dm.Enabled,
		CreatorId: tok.UserId, MonitoredRadioPortId: dm.MonitoredRadioPortId, Config: dm.Config,
		ElementId: dm.ElementId,
	}

	if res := s.db.Create(&monitor); res.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	} else {
		c.JSON(http.StatusCreated, monitor)
	}

	// Send a monitor create event.
	oid := monitor.Id.String()
	userIdStr := monitor.CreatorId.String()
	elementIdStr := monitor.ElementId.String()
	eh := subscription.EventHeader{
		Type:       int32(event.EventType_ET_CREATED),
		Code:       int32(zmc.EventCode_EC_MONITOR),
		SourceType: int32(event.EventSourceType_EST_ZMC),
		SourceId:   s.config.ServiceId,
		Id:         uuid.New().String(),
		ObjectId:   &oid,
		Time:       &monitor.CreatedAt,
		UserId:     &userIdStr,
		ElementId:  &elementIdStr,
	}
	e := subscription.Event{
		Header: eh,
		Object: &monitor,
	}
	go s.sm.Notify(&e)
}

func (s *Server) GetMonitor(c *gin.Context) {
	var err error

	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("monitor_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRoleOrViewerPolicy(policy.RoleViewer, policy.GreaterOrEqual)
	var match bool
	var policyName string
	if match, policyName = s.CheckPolicyMiddleware(tok, nil, nil, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	var monitor store.Monitor
	q := s.db
	if c.GetBool("zms.elaborate") {
		q = q.Preload("RadioPort").Preload("MonitoredRadioPort")
	}
	res := q.First(&monitor, id)
	if res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Viewers can only see the monitor if approved and not denied nor deleted
	if policyName == policy.MatchViewer && (monitor.DeletedAt != nil) {
		c.AbortWithStatus(http.StatusForbidden)
		return
	}

	c.JSON(http.StatusOK, monitor)
}

func (s *Server) HandleMonitorChanged(monitor *store.Monitor) {

}

func (s *Server) UpdateMonitor(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("monitor_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Initial input validation.
	var um store.Monitor
	if err := c.ShouldBindJSON(&um); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	//forceUpdate := c.GetBool("zms.force-update")

	// We need to compare the Monitor.RadioPort.Radio.ElementId and
	// Monitor.CreatorId with the caller, to validate the match-element-role
	// and match-user policies.  However, note that we do not return
	// StatusNotFound until after policy verification.  This is a bit
	// pendantic.
	var monitor store.Monitor
	res := s.db.Preload("RadioPort").Preload("MonitoredRadioPort").First(&monitor, id)
	if res.RowsAffected < 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	creatorId := &monitor.CreatorId
	var radio store.Radio
	res = s.db.First(&radio, monitor.RadioPort.RadioId)
	if res.RowsAffected < 1 {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": "internal inconsistency"})
		return
	}
	elementId := &radio.ElementId

	// Check policy.  Element managers and up can modify any managed
	// monitor.  Operators can only modify the monitor they created.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, creatorId, elementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	//t := time.Now().UTC()

	// Validations that require authentication.
	if monitor.DeletedAt != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "cannot modify deleted monitor"})
		return
	}

	// Assume obedience/compliance to the x-immutable-on-put annotations,
	// and only update mutable fields.

	// Change some fields regardless, without sending notifications.
	monitor.Name = um.Name
	monitor.Description = um.Description
	monitor.DeviceId = um.DeviceId
	monitor.HtmlUrl = um.HtmlUrl
	monitor.Types = um.Types
	monitor.Formats = um.Formats
	monitor.Exclusive = um.Exclusive
	monitor.Enabled = um.Enabled

	monitor.UpdaterId = &tok.UserId

	sres := s.db.Save(&monitor)
	if sres == nil || sres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": sres.Error.Error()})
		return
	}
	// Refresh after updates.
	s.db.First(&monitor, id)

	s.HandleMonitorChanged(&monitor)

	c.JSON(http.StatusOK, &monitor)
}

func (s *Server) HandleMonitorDeleted(monitor *store.Monitor) {

}

func (s *Server) DeleteMonitor(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("monitor_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	//forceUpdate := c.GetBool("zms.force-update")

	// We need to compare the Monitor.RadioPort.Radio.ElementId and
	// Monitor.CreatorId with the caller, to validate the match-element-role
	// and match-user policies.  However, note that we do not return
	// StatusNotFound until after policy verification.  This is a bit
	// pendantic.
	var monitor store.Monitor
	res := s.db.Preload("RadioPort").First(&monitor, id)
	if res.RowsAffected < 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	creatorId := &monitor.CreatorId
	var radio store.Radio
	res = s.db.First(&radio, monitor.RadioPort.RadioId)
	if res.RowsAffected < 1 {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": "internal inconsistency"})
		return
	}
	elementId := &radio.ElementId

	// Check policy.  Element managers and up can delete any managed
	// monitor.  Operators can only delete the monitor they created.
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual, policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, creatorId, elementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// XXX: cannot delete if referenced by Grants or Claims.

	t := time.Now().UTC()

	monitor.DeletedAt = &t
	monitor.UpdaterId = &tok.UserId

	s.db.Save(&monitor)

	s.HandleMonitorDeleted(&monitor)

	c.Status(http.StatusOK)
}

type CreateSubscriptionModel struct {
	Id      string                     `json:"id" binding:"required,uuid"`
	Filters []subscription.EventFilter `json:"filters" binding:"omitempty"`
}

func (s *Server) CreateSubscription(c *gin.Context) {
	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var dm CreateSubscriptionModel
	if err := c.ShouldBindJSON(&dm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// NB: for now, non-admins will be restricted to self-user-associated
	// events.
	if match, _ := s.CheckPolicyMiddleware(tok, nil, nil, policy.AdminOnlyPolicy); !match {

		// Build a map of elementId string to (max) role value in that
		// element.
		// Also build a list of all ElementIds in the token.
		// Also build a list of ElementIds with role >= Operator.
		elementRoleMap := make(map[string]int)
		elementIds := make([]string, 0, len(tok.RoleBindings))
		opElementIds := make([]string, 0)
		for _, rb := range tok.RoleBindings {
			if rb.ElementId == nil || rb.Role == nil {
				continue
			}
			eis := rb.ElementId.String()
			nv := rb.Role.Value
			if v, ok := elementRoleMap[eis]; ok {
				if nv > v {
					elementRoleMap[eis] = nv
				}
			} else {
				elementRoleMap[eis] = nv
			}
			elementIds = append(elementIds, eis)
			if nv >= policy.RoleOperator {
				opElementIds = append(opElementIds, eis)
			}
		}
		userIds := []string{tok.UserId.String()}

		//
		// Each filter must match these constraint checks:
		//   * have UserIds set to exactly the calling user, and must
		//     have ElementIds set to a subset of the ElementIds in the token; OR
		//   * have UserIds set to exactly the calling user, and ElementIds nil; OR
		//   * have UserIds set to nil, and ElementIds set to elements in which the user has >= Operator role.
		//
		if dm.Filters == nil || len(dm.Filters) < 1 {
			if false {
				c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "non-admin user must provide at least one filter, with UserIds set to exactly their user ID, and ElementIds to a subset of the calling token's element IDs."})
				return
			} else {
				dm.Filters = []subscription.EventFilter{
					subscription.EventFilter{UserIds: userIds},
					subscription.EventFilter{UserIds: userIds, ElementIds: elementIds},
				}
				if len(opElementIds) > 0 {
					dm.Filters = append(dm.Filters, subscription.EventFilter{ElementIds: opElementIds})
				}
				log.Debug().Msg(fmt.Sprintf("autocreated user subscription %+v for token %+v", dm, tok))
			}
		}

		// Filters must be bound to calling user unless user has >= operator
		// role in the associated elements.
		for _, filter := range dm.Filters {
			if filter.UserIds == nil || len(filter.UserIds) == 0 {
				// In this case, the user must provide at least one element
				// in which they have >= Operator role.
				if len(opElementIds) == 0 {
					msg := "non-admin, non-operator token subscriptions must set UserIds to exactly their token UserId"
					c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": msg})
					return
				} else if filter.ElementIds == nil || len(filter.ElementIds) == 0 {
					msg := "token subscriptions without UserIds must set ElementIds to at least one Element in which they have >= Operator role."
					c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": msg})
					return
				} else {
					for _, eis := range filter.ElementIds {
						if rv, ok := elementRoleMap[eis]; !ok || rv < policy.RoleOperator {
							msg := fmt.Sprintf("non-admin, wildcard user filter cannot include a sub-Operator role (element %s)", eis)
							c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": msg})
							return
						}
					}
				}
			} else {
				if len(filter.UserIds) != 1 || filter.UserIds[0] != tok.UserId.String() {
					msg := "non-admin subscription must set each filter.UserIds field to a one-item list containing exactly the calling token UserId."
					c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": msg})
					return
				}
				if filter.ElementIds == nil || len(filter.ElementIds) < 1 {
					//msg := "non-admin subscription must set each filter.ElementIds field to a subset of the ElementIds in the calling token."
					//c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": msg})
					//return
				} else {
					for _, elementId := range filter.ElementIds {
						if _, ok := elementRoleMap[elementId]; !ok {
							msg := "non-admin user must set each filter's ElementIds field to a subset of the ElementIds in the calling token."
							c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": msg})
							return
						}
					}
				}
			}
		}
	}

	if sub, err := s.sm.Subscribe(dm.Id, dm.Filters, nil, c.ClientIP(), subscription.Rest, &tok.Token); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	} else {
		c.JSON(http.StatusCreated, sub)
		return
	}
}

func (s *Server) ListSubscriptions(c *gin.Context) {
	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Check policy.
	if match, _ := s.CheckPolicyMiddleware(tok, nil, nil, policy.AdminOnlyPolicy); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"subscriptions": s.sm.GetSubscriptions()})
	return
}

func (s *Server) GetSubscriptionEvents(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("subscription_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
	} else {
		id = idParsed
	}
	idStr := id.String()

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	var subprotos []string
	if xApiTok := c.GetHeader("X-Api-Token"); xApiTok != "" {
		if tok, err = s.RequireContextToken(c); err != nil {
			return
		}
	} else if wsProto := c.GetHeader("Sec-WebSocket-Protocol"); wsProto != "" {
		if wsTok, code, altErr := CheckTokenValue(s, wsProto); altErr != nil {
			log.Warn().Msg(fmt.Sprintf("bad token (%s) in Sec-WebSocket-Protocol: %+v", wsProto, altErr.Error()))
			c.AbortWithStatusJSON(code, gin.H{"error": altErr.Error()})
			return
		} else {
			tok = wsTok
			subprotos = append(subprotos, wsProto)
		}
	}
	if tok == nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "no token provided"})
		return
	} else {
		log.Debug().Msg(fmt.Sprintf("valid token from websocket"))
	}

	ech := make(chan *subscription.Event)
	if err := s.sm.UpdateChannel(idStr, ech, &tok.Token); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// See if client wants to reuse subscription upon websocket close.
	deleteOnClose := true
	if tokenString := c.GetHeader("X-Api-Delete-On-Close"); tokenString == "false" {
		c.Set("zms.deleteOnClose", false)
		deleteOnClose = false
	} else {
		c.Set("zms.deleteOnClose", true)
	}

	defer func() {
		if deleteOnClose {
			s.sm.Unsubscribe(idStr)
		} else {
			log.Debug().Msg(fmt.Sprintf("retaining subscription %s", idStr))
			s.sm.UpdateChannel(idStr, nil, &tok.Token)
		}
	}()

	wsUpgrader := websocket.Upgrader{
		ReadBufferSize: 0,
		WriteBufferSize: 16384,
		Subprotocols: subprotos,
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}

	var conn *websocket.Conn
	if conn, err = wsUpgrader.Upgrade(c.Writer, c.Request, nil); err != nil {
		log.Debug().Msg(fmt.Sprintf("error upgrading websocket conn: %+v", err.Error()))
		return
	}

	// Handle client disconnect.
	cch := make(chan interface{})
	go func() {
		for {
			_, _, err := conn.ReadMessage()
			if err != nil {
				cch <- nil
				conn.Close()
				return
			}
		}
	}()

	// Read events from SubscriptionManager, and client read errors or
	// client disconnection.
	for {
		select {
		case e := <-ech:
			if body, jErr := json.Marshal(e); jErr != nil {
				log.Error().Err(jErr).Msg(fmt.Sprintf("error marshaling event (%+v): %s", e, err.Error()))
				continue
			} else {
				//log.Debug().Msg(fmt.Sprintf("marshaled event: %+v", body))
				if err = conn.WriteMessage(websocket.TextMessage, body); err != nil {
					return
				}
			}
		case <-cch:
			// NB: the deferred handler covers this case.
			return
		}
	}

	return
}

func (s *Server) DeleteSubscription(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("subscription_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
	} else {
		id = idParsed
	}
	idStr := id.String()

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	var sub *subscription.Subscription[*subscription.Event]
	if sub = s.sm.GetSubscription(idStr); sub == nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "not found"})
		return
	}

	// Check policy.
	if match, _ := s.CheckPolicyMiddleware(tok, nil, nil, policy.AdminOnlyPolicy); !match {
		if sub.Token == nil || *sub.Token != tok.Token {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
			return
		}
	}

	if err := s.sm.Unsubscribe(idStr); err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "not found"})
		return
	}

	c.Status(http.StatusAccepted)

	return
}
