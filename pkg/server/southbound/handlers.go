// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package southbound

import (
	"context"
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/peer"
	"google.golang.org/grpc/status"

	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/subscription"

	zmc "gitlab.flux.utah.edu/openzms/zms-api/go/zms/zmc/v1"

	"gitlab.flux.utah.edu/openzms/zms-zmc/pkg/store"
)

func (s *Server) GetZone(ctx context.Context, req *zmc.GetZoneRequest) (resp *zmc.GetZoneResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid ZoneRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	var id uuid.UUID
	if id, err = uuid.Parse(req.Id); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid zone id")
	}
	elaborate := req.Header != nil && req.Header.Elaborate != nil && *req.Header.Elaborate

	log.Info().Msg(fmt.Sprintf("GetZone(%+v)", req))

	var zone *store.Zone
	if id == uuid.Nil {
		if s.zone != nil {
			zone = s.zone
		} else {
			return nil, status.Errorf(codes.NotFound, "Default zone not found")
		}
	} else {
		var item store.Zone
		q := s.db.Where(&store.Zone{Id: id})
		if elaborate {
			q = q.Preload("Area")
		}
		res := q.First(&item)
		if res.Error != nil || res.RowsAffected != 1 {
			return nil, status.Errorf(codes.NotFound, "Zone not found")
		}
		zone = &item
	}

	var outval zmc.Zone
	if err := zone.ToProto(&outval, elaborate); err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to marshal Zone")
	}

	resp = &zmc.GetZoneResponse{
		Header: &zmc.ResponseHeader{},
		Zone:   &outval,
	}
	if req.Header != nil {
		resp.Header.ReqId = req.Header.ReqId
	}
	return resp, nil
}

func (s *Server) GetLocation(ctx context.Context, req *zmc.GetLocationRequest) (resp *zmc.GetLocationResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid LocationRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	var id uuid.UUID
	if id, err = uuid.Parse(req.Id); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid location id")
	}

	log.Info().Msg(fmt.Sprintf("GetLocation(%+v)", req))

	var item store.Location
	q := s.db.Where(&store.Location{Id: id})
	res := q.First(&item)
	if res.Error != nil || res.RowsAffected != 1 {
		return nil, status.Errorf(codes.NotFound, "Location not found")
	}

	// NB: we exclude any unapproved or deleted RoleBindings.
	var outval zmc.Location
	elaborate := req.Header != nil && req.Header.Elaborate != nil && *req.Header.Elaborate
	if err := item.ToProto(&outval, elaborate); err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to marshal Location")
	}

	resp = &zmc.GetLocationResponse{
		Header:   &zmc.ResponseHeader{},
		Location: &outval,
	}
	if req.Header != nil {
		resp.Header.ReqId = req.Header.ReqId
	}
	return resp, nil
}

func (s *Server) GetAntenna(ctx context.Context, req *zmc.GetAntennaRequest) (resp *zmc.GetAntennaResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid AntennaRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	var id uuid.UUID
	if id, err = uuid.Parse(req.Id); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid antenna id")
	}

	log.Info().Msg(fmt.Sprintf("GetAntenna(%+v)", req))

	var item store.Antenna
	q := s.db.Where(&store.Antenna{Id: id})
	res := q.First(&item)
	if res.Error != nil || res.RowsAffected != 1 {
		return nil, status.Errorf(codes.NotFound, "Antenna not found")
	}

	// NB: we exclude any unapproved or deleted RoleBindings.
	var outval zmc.Antenna
	elaborate := req.Header != nil && req.Header.Elaborate != nil && *req.Header.Elaborate
	if err := item.ToProto(&outval, elaborate); err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to marshal Antenna")
	}

	resp = &zmc.GetAntennaResponse{
		Header:  &zmc.ResponseHeader{},
		Antenna: &outval,
	}
	if req.Header != nil {
		resp.Header.ReqId = req.Header.ReqId
	}
	return resp, nil
}

func (s *Server) GetRadioPort(ctx context.Context, req *zmc.GetRadioPortRequest) (resp *zmc.GetRadioPortResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid RadioPortRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	var id uuid.UUID
	if id, err = uuid.Parse(req.Id); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid radio port id")
	}
	elaborate := req.Header != nil && req.Header.Elaborate != nil && *req.Header.Elaborate

	log.Info().Msg(fmt.Sprintf("GetRadioPort(%+v)", req))

	var item store.RadioPort
	q := s.db.Where(&store.RadioPort{Id: id})
	if elaborate {
		q = q.Preload("Antenna").Preload("AntennaLocation")
	}
	res := q.First(&item)
	if res.Error != nil || res.RowsAffected != 1 {
		return nil, status.Errorf(codes.NotFound, "RadioPort not found")
	}

	// NB: we exclude any unapproved or deleted RoleBindings.
	var outval zmc.RadioPort
	if err := item.ToProto(&outval, elaborate); err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to marshal RadioPort")
	}

	resp = &zmc.GetRadioPortResponse{
		Header:    &zmc.ResponseHeader{},
		RadioPort: &outval,
	}
	if req.Header != nil {
		resp.Header.ReqId = req.Header.ReqId
	}
	return resp, nil
}

func (s *Server) GetRadio(ctx context.Context, req *zmc.GetRadioRequest) (resp *zmc.GetRadioResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid RadioRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	var id uuid.UUID
	if id, err = uuid.Parse(req.Id); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid radio  id")
	}
	elaborate := req.Header != nil && req.Header.Elaborate != nil && *req.Header.Elaborate

	log.Info().Msg(fmt.Sprintf("GetRadio(%+v)", req))

	var item store.Radio
	q := s.db.Where(&store.Radio{Id: id})
	if elaborate {
		log.Debug().Msg("preloading")
		q = q.Preload("Ports").Preload("Location")
	}
	res := q.First(&item)
	if res.Error != nil || res.RowsAffected != 1 {
		return nil, status.Errorf(codes.NotFound, "Radio not found")
	}

	// NB: we exclude any unapproved or deleted RoleBindings.
	var outval zmc.Radio
	if err := item.ToProto(&outval, elaborate); err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to marshal Radio")
	}

	resp = &zmc.GetRadioResponse{
		Header: &zmc.ResponseHeader{},
		Radio:  &outval,
	}
	if req.Header != nil {
		resp.Header.ReqId = req.Header.ReqId
	}
	return resp, nil
}

func (s *Server) GetMonitor(ctx context.Context, req *zmc.GetMonitorRequest) (resp *zmc.GetMonitorResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid MonitorRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	var id uuid.UUID
	if id, err = uuid.Parse(req.Id); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid monitor  id")
	}
	elaborate := req.Header != nil && req.Header.Elaborate != nil && *req.Header.Elaborate

	log.Info().Msg(fmt.Sprintf("GetMonitor(%+v)", req))

	var item store.Monitor
	q := s.db.Where(&store.Monitor{Id: id})
	if elaborate {
		q = q.Preload("RadioPort").Preload("MonitoredRadioPort")
	}
	res := q.First(&item)
	if res.Error != nil || res.RowsAffected != 1 {
		return nil, status.Errorf(codes.NotFound, "Monitor not found")
	}

	var outval zmc.Monitor
	if err := item.ToProto(&outval, elaborate); err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to marshal Monitor")
	}

	resp = &zmc.GetMonitorResponse{
		Header:  &zmc.ResponseHeader{},
		Monitor: &outval,
	}
	if req.Header != nil {
		resp.Header.ReqId = req.Header.ReqId
	}
	return resp, nil

}

func (s *Server) GetMonitors(ctx context.Context, req *zmc.GetMonitorsRequest) (resp *zmc.GetMonitorsResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid MonitorsRequest")
	}
	if err = req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	elaborate := req.Header != nil && req.Header.Elaborate != nil && *req.Header.Elaborate

	log.Info().Msg(fmt.Sprintf("GetMonitor(%+v)", req))

	// Unmarshal query uuids.
	var elementId *uuid.UUID
	if req.ElementId != nil {
		if idParsed, err := uuid.Parse(*req.ElementId); err != nil {
			return nil, status.Errorf(codes.InvalidArgument, "Invalid ElementId")
		} else {
			elementId = &idParsed
		}
	}
	var radioPortId *uuid.UUID
	if req.RadioPortId != nil {
		if idParsed, err := uuid.Parse(*req.RadioPortId); err != nil {
			return nil, status.Errorf(codes.InvalidArgument, "Invalid RadioPortId")
		} else {
			radioPortId = &idParsed
		}
	}

	var elementIdList []*uuid.UUID
	if elementId != nil {
		elementIdList = []*uuid.UUID{elementId}
	}

	defTrue := true
	if req.Enabled == nil {
		req.Enabled = &defTrue
	}

	var monitorList []store.Monitor
	q := s.db.Joins("left join radio_ports on radio_ports.id=monitors.radio_port_id").
		Joins("left join radios on radios.id=radio_ports.radio_id")
	if len(elementIdList) > 0 {
		q = q.Where("radios.element_id in ?", elementIdList)
	}
	if radioPortId != nil {
		q = q.Where("Monitors.radio_port_id = ?", *radioPortId)
	}
	if req.Enabled != nil {
		if !*req.Enabled {
			q = q.Where("not Monitors.enabled")
		} else {
			q = q.Where("Monitors.enabled")
		}
	}
	if req.Exclusive != nil {
		if !*req.Exclusive {
			q = q.Where("Monitors.exclusive_at is NULL")
		} else {
			q = q.Where("Monitors.exclusive_at is not NULL")
		}
	}
	if req.Monitor != nil {
		q = q.Where(
			s.db.Where(store.MakeILikeClause(s.config, "Monitors.name"), fmt.Sprintf("%%%s%%", *req.Monitor)).
				Or(store.MakeILikeClause(s.config, "Monitors.description"), fmt.Sprintf("%%%s%%", *req.Monitor)))
	}
	if req.Types != nil {
		q = q.Where(store.MakeILikeClause(s.config, "Monitors.types"), fmt.Sprintf("%%%s%%", *req.Types))
	}
	if req.Formats != nil {
		q = q.Where(store.MakeILikeClause(s.config, "Monitors.formats"), fmt.Sprintf("%%%s%%", *req.Formats))
	}
	q = q.Order("updated_at desc")

	if elaborate {
		q = q.Preload("RadioPort").Preload("MonitoredRadioPort")
	}

	qres := q.Find(&monitorList)
	if qres.Error != nil {
		return nil, status.Errorf(codes.Internal, "DB error: %+v", qres.Error)
	}

	resp = &zmc.GetMonitorsResponse{
		Header: &zmc.ResponseHeader{},
	}
	if req.Header != nil {
		resp.Header.ReqId = req.Header.ReqId
	}
	for _, item := range monitorList {
		var outval zmc.Monitor
		if err := item.ToProto(&outval, elaborate); err != nil {
			return nil, status.Errorf(codes.Internal, "Failed to marshal Monitor %+v", item.Id)
		}
		resp.Monitors = append(resp.Monitors, &outval)
	}

	return resp, nil
}

func (s *Server) GetGrant(ctx context.Context, req *zmc.GetGrantRequest) (resp *zmc.GetGrantResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid GrantRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	var id uuid.UUID
	if id, err = uuid.Parse(req.Id); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid grant id")
	}
	elaborate := req.Header != nil && req.Header.Elaborate != nil && *req.Header.Elaborate

	log.Info().Msg(fmt.Sprintf("GetGrant(%+v)", req))

	var item store.Grant
	q := s.db.Where(&store.Grant{Id: id})
	if elaborate {
		q = q.Preload("Constraints.Constraint")
		q = q.Preload("IntConstraints.IntConstraint")
		q = q.Preload("RtIntConstraints.RtIntConstraint")
		q = q.Preload("RadioPorts.RadioPort")
		q = q.Preload("Logs")
		q = q.Preload("Replacement")
	}
	res := q.First(&item)
	if res.Error != nil || res.RowsAffected != 1 {
		return nil, status.Errorf(codes.NotFound, "Grant not found")
	}

	var outval zmc.Grant
	if err := item.ToProto(&outval, elaborate); err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to marshal Grant")
	}

	resp = &zmc.GetGrantResponse{
		Header: &zmc.ResponseHeader{},
		Grant:  &outval,
	}
	if req.Header != nil {
		resp.Header.ReqId = req.Header.ReqId
	}
	return resp, nil
}

func (s *Server) GetGrants(ctx context.Context, req *zmc.GetGrantsRequest) (resp *zmc.GetGrantsResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid GrantsRequest")
	}
	if err = req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	elaborate := req.Header != nil && req.Header.Elaborate != nil && *req.Header.Elaborate

	log.Info().Msg(fmt.Sprintf("GetGrants(%+v)", req))

	// Unmarshal query uuids.
	var elementId *uuid.UUID
	if req.ElementId != nil {
		if idParsed, err := uuid.Parse(*req.ElementId); err != nil {
			return nil, status.Errorf(codes.InvalidArgument, "Invalid ElementId")
		} else {
			elementId = &idParsed
		}
	}
	var radioPortId *uuid.UUID
	if req.RadioPortId != nil {
		if idParsed, err := uuid.Parse(*req.RadioPortId); err != nil {
			return nil, status.Errorf(codes.InvalidArgument, "Invalid RadioPortId")
		} else {
			radioPortId = &idParsed
		}
	}
	var creatorId *uuid.UUID
	if req.CreatorId != nil {
		if idParsed, err := uuid.Parse(*req.CreatorId); err != nil {
			return nil, status.Errorf(codes.InvalidArgument, "Invalid CreatorId")
		} else {
			creatorId = &idParsed
		}
	}
	var spectrumId *uuid.UUID
	if req.SpectrumId != nil {
		if idParsed, err := uuid.Parse(*req.SpectrumId); err != nil {
			return nil, status.Errorf(codes.InvalidArgument, "Invalid SpectrumId")
		} else {
			spectrumId = &idParsed
		}
	}

	var grantList []store.Grant
	q := s.db
	// NB: filter out claims by default unless caller asks.
	q = q.Where("not grants.is_claim")
	if elementId != nil {
		q = q.Where("grants.element_id = ?", *elementId)
	}
	if creatorId != nil {
		q = q.Where("grants.creator_id = ?", *creatorId)
	}
	if spectrumId != nil {
		q = q.Where("grants.spectrum_id = ?", *spectrumId)
	}
	if radioPortId != nil {
		q = q.Joins("left join grant_radio_ports on grant_radio_ports.grant_id=grants.id")
		q = q.Where("grant_radio_ports.radio_port_id = ?", *radioPortId)
	}
	if req.Freq != nil || req.Exclusive != nil {
		q = q.
			Joins("left join grant_constraints on grant_constraints.grant_id=grants.id").
			Joins("left join constraints on grant_constraints.constraint_id=constraints.id")
		if req.Freq != nil {
			q = q.Where("(constraints.min_freq <= ? and constraints.max_freq >= ?)", *req.Freq, *req.Freq)
		}
		if req.Exclusive != nil {
			q = q.Where("constraints.Exclusive = ?", *req.Exclusive)
		}
	}
	if req.Time == nil || req.TimeMax == nil {
		t := time.Now().UTC()
		if req.Time != nil {
			t = req.Time.AsTime()
		}
		q = q.Where("(grants.starts_at <= ? and (grants.expires_at is NULL or grants.expires_at >= ?))", t, t)
	} else {
		t := req.Time.AsTime()
		tmax := req.TimeMax.AsTime()
		q = q.Where("((grants.starts_at <= ? and (grants.expires_at is NULL or grants.expires_at >= ?)) or (grants.starts_at <= ? and (grants.expires_at is NULL or grants.expires_at >= ?)))", t, t, tmax, tmax)
	}
	if req.Status != nil {
		q = q.Where("grants.status = ?", *req.Status)
	}
	if req.OpStatus != nil {
		q = q.Where("grants.op_status = ?", *req.OpStatus)
	}
	if req.Approved != nil {
		if !*req.Approved {
			q = q.Where("grants.approved_at is NULL")
		} else {
			q = q.Where("grants.approved_at is not NULL")
		}
	}
	if req.OwnerApproved != nil {
		if !*req.OwnerApproved {
			q = q.Where("grants.owner_approved_at is NULL")
		} else {
			q = q.Where("grants.owner_approved_at is not NULL")
		}
	}
	if req.Denied != nil {
		if !*req.Denied {
			q = q.Where("grants.denied_at is NULL")
		} else {
			q = q.Where("grants.denied_at is not NULL")
		}
	}
	if req.Revoked != nil {
		if !*req.Revoked {
			q = q.Where("grants.revoked_at is NULL")
		} else {
			q = q.Where("grants.revoked_at is not NULL")
		}
	}
	if req.Deleted != nil {
		if !*req.Deleted {
			q = q.Where("grants.deleted_at is NULL")
		} else {
			q = q.Where("grants.deleted_at is not NULL")
		}
	}
	q = q.Order("starts_at asc")

	if elaborate {
		q = q.Preload("Constraints.Constraint")
		q = q.Preload("IntConstraints.IntConstraint")
		q = q.Preload("RtIntConstraints.RtIntConstraint")
		q = q.Preload("RadioPorts.RadioPort")
		q = q.Preload("Logs")
		q = q.Preload("Replacement")
	}

	qres := q.Find(&grantList)
	if qres.Error != nil {
		return nil, status.Errorf(codes.Internal, "DB error: %+v", qres.Error)
	}

	resp = &zmc.GetGrantsResponse{
		Header: &zmc.ResponseHeader{},
	}
	if req.Header != nil {
		resp.Header.ReqId = req.Header.ReqId
	}
	for _, item := range grantList {
		var outval zmc.Grant
		if err := item.ToProto(&outval, elaborate); err != nil {
			return nil, status.Errorf(codes.Internal, "Failed to marshal Grant %+v", item.Id)
		}
		resp.Grants = append(resp.Grants, &outval)
	}

	return resp, nil
}

func (s *Server) GetSpectrum(ctx context.Context, req *zmc.GetSpectrumRequest) (resp *zmc.GetSpectrumResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid SpectrumRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	var id uuid.UUID
	if id, err = uuid.Parse(req.Id); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid spectrum id")
	}
	elaborate := req.Header != nil && req.Header.Elaborate != nil && *req.Header.Elaborate

	log.Info().Msg(fmt.Sprintf("GetSpectrum(%+v)", req))

	var item store.Spectrum
	q := s.db.Where(&store.Spectrum{Id: id})
	if elaborate {
		q = q.Preload("Constraints.Constraint").Preload("Policies")
	}
	res := q.First(&item)
	if res.Error != nil || res.RowsAffected != 1 {
		return nil, status.Errorf(codes.NotFound, "Spectrum not found")
	}

	var outval zmc.Spectrum
	if err := item.ToProto(&outval, elaborate); err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to marshal Spectrum")
	}

	resp = &zmc.GetSpectrumResponse{
		Header:   &zmc.ResponseHeader{},
		Spectrum: &outval,
	}
	if req.Header != nil {
		resp.Header.ReqId = req.Header.ReqId
	}
	return resp, nil
}

func EventToProto(e *subscription.Event, include bool, elaborate bool) (*zmc.Event, error) {
	ep := &zmc.Event{
		Header: e.Header.ToProto(),
	}

	if include && e.Object != nil {
		switch v := e.Object.(type) {
		case *store.Zone:
			x := new(zmc.Zone)
			if err := v.ToProto(x, elaborate); err != nil {
				return nil, fmt.Errorf("failed to encode object type %T: %s", v, err.Error())
			}
			ep.Object = &zmc.Event_Zone{Zone: x}
		case *store.Antenna:
			x := new(zmc.Antenna)
			if err := v.ToProto(x, elaborate); err != nil {
				return nil, fmt.Errorf("failed to encode object type %T: %s", v, err.Error())
			}
			ep.Object = &zmc.Event_Antenna{Antenna: x}
		case *store.Radio:
			x := new(zmc.Radio)
			if err := v.ToProto(x, elaborate); err != nil {
				return nil, fmt.Errorf("failed to encode object type %T: %s", v, err.Error())
			}
			ep.Object = &zmc.Event_Radio{Radio: x}
		case *store.RadioPort:
			x := new(zmc.RadioPort)
			if err := v.ToProto(x, elaborate); err != nil {
				return nil, fmt.Errorf("failed to encode object type %T: %s", v, err.Error())
			}
			ep.Object = &zmc.Event_RadioPort{RadioPort: x}
		case *store.Monitor:
			x := new(zmc.Monitor)
			if err := v.ToProto(x, elaborate); err != nil {
				return nil, fmt.Errorf("failed to encode object type %T: %s", v, err.Error())
			}
			ep.Object = &zmc.Event_Monitor{Monitor: x}
		case *store.Grant:
			x := new(zmc.Grant)
			if err := v.ToProto(x, elaborate); err != nil {
				return nil, fmt.Errorf("failed to encode object type %T: %s", v, err.Error())
			}
			ep.Object = &zmc.Event_Grant{Grant: x}
		case *store.Spectrum:
			x := new(zmc.Spectrum)
			if err := v.ToProto(x, elaborate); err != nil {
				return nil, fmt.Errorf("failed to encode object type %T: %s", v, err.Error())
			}
			ep.Object = &zmc.Event_Spectrum{Spectrum: x}
		case *store.Claim:
			x := new(zmc.Claim)
			if err := v.ToProto(x, elaborate); err != nil {
				return nil, fmt.Errorf("failed to encode object type %T: %s", v, err.Error())
			}
			ep.Object = &zmc.Event_Claim{Claim: x}
		default:
			return nil, fmt.Errorf("unknown event object type %T", v)
		}
	}

	log.Debug().Msg(fmt.Sprintf("EventToProto: %+v", ep))

	return ep, nil
}

func (s *Server) Subscribe(req *zmc.SubscribeRequest, stream zmc.Zmc_SubscribeServer) error {
	if req == nil || req.Header == nil || req.Header.ReqId == nil {
		return status.Errorf(codes.InvalidArgument, "Invalid SubscribeRequest")
	}
	if err := req.Validate(); err != nil {
		return status.Errorf(codes.InvalidArgument, err.Error())
	}

	var filters []subscription.EventFilter
	if req.Filters != nil && len(req.Filters) > 0 {
		filters = make([]subscription.EventFilter, 0, len(req.Filters))
		for _, f := range req.Filters {
			filters = append(filters, subscription.EventFilterFromProto(f))
		}
	}
	ch := make(chan *subscription.Event)
	sid := *req.Header.ReqId
	endpoint := ""
	if p, ok := peer.FromContext(stream.Context()); ok {
		endpoint = p.Addr.String()
	}
	if _, err := s.sm.Subscribe(sid, filters, ch, endpoint, subscription.Rpc, nil); err != nil {
		return status.Errorf(codes.Internal, err.Error())
	}

	// Handle write panics as well as the regular return cases.
	defer func() {
		if true || recover() != nil {
			s.sm.Unsubscribe(sid)
		}
	}()

	include := req.Include == nil || *req.Include
	elaborate := req.Header.Elaborate == nil || *req.Header.Elaborate

	// Watch for events and client connection error conditions
	// (e.g. disconnect).
	for {
		select {
		case e := <-ch:
			if ep, err := EventToProto(e, include, elaborate); err != nil {
				log.Error().Err(err).Msg("failed to marshal event to RPC")
				continue
			} else {
				events := make([]*zmc.Event, 0, 1)
				events = append(events, ep)
				resp := &zmc.SubscribeResponse{
					Header: &zmc.ResponseHeader{
						ReqId: req.Header.ReqId,
					},
					Events: events,
				}
				if err := stream.Send(resp); err != nil {
					break
				}
			}
		case <-stream.Context().Done():
			log.Debug().Msg(fmt.Sprintf("stream closed unexpectedly (%s)", sid))
			return nil
		}
	}

	return nil
}
