// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"gorm.io/gorm"
	"gorm.io/gorm/logger"

	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/subscription"

	identity "gitlab.flux.utah.edu/openzms/zms-api/go/zms/identity/v1"
	zmsclient "gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/client"

	"gitlab.flux.utah.edu/openzms/zms-zmc/pkg/client"
	"gitlab.flux.utah.edu/openzms/zms-zmc/pkg/config"
	grantcontroller "gitlab.flux.utah.edu/openzms/zms-zmc/pkg/controller/grant"
	"gitlab.flux.utah.edu/openzms/zms-zmc/pkg/server/northbound"
	"gitlab.flux.utah.edu/openzms/zms-zmc/pkg/server/southbound"
	"gitlab.flux.utah.edu/openzms/zms-zmc/pkg/store"
	"gitlab.flux.utah.edu/openzms/zms-zmc/pkg/version"
)

func main() {
	var serverConfig *config.Config
	if c, err := config.LoadConfig(); err != nil {
		log.Fatal().Err(err).Msg("failed to load config")
	} else {
		serverConfig = c
	}
	fmt.Printf("serverConfig: %+v\n", serverConfig)

	if serverConfig.Debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	} else if serverConfig.Verbose {
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	} else {
		zerolog.SetGlobalLevel(zerolog.WarnLevel)
	}

	dbConfig := gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	}
	db, err := store.GetDatabase(serverConfig, &dbConfig)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to connect to database")
	}
	err = store.InitDatabase(serverConfig, db)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to initialize database")
	}

	sm := subscription.NewSubscriptionManager[*subscription.Event]()

	clientConfig := zmsclient.ZmsClientConfig{
		IdentityRpcEndpoint:   serverConfig.IdentityRpcEndpoint,
		WatchIdentityServices: true,
		WatchIdentityTokens:   true,
	}
	rclient := zmsclient.New(clientConfig)

	var zone *store.Zone
	if serverConfig.IdentityRpcEndpoint != "" {
		service := &identity.Service{
			Id:             serverConfig.ServiceId,
			Name:           serverConfig.ServiceName,
			Kind:           "zmc",
			Endpoint:       serverConfig.RpcEndpoint,
			EndpointApiUri: serverConfig.HttpEndpoint,
			Description:    "OpenZMS zmc service",
			Version:        version.VersionString,
			ApiVersion:     "v1",
			Enabled:        true,
		}
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		if err = rclient.RegisterService(ctx, service); err != nil {
			log.Fatal().Err(err).Msg(fmt.Sprintf("failed to register service (%+v) at identity service (%s)", service, serverConfig.IdentityRpcEndpoint))
		}

		bootstrapElementId, bootstrapUserId := rclient.GetBootstrapInfo()
		if zone, err = store.PostRegisterBootstrap(serverConfig, db, bootstrapElementId, bootstrapUserId); err != nil {
			log.Fatal().Err(err).Msg(fmt.Sprintf("failed to bootstrap database (%+v, %+v): %s", bootstrapElementId, bootstrapUserId, err.Error()))
		} else {
			log.Debug().Msg(fmt.Sprintf("Bootstrap (%+v, %+v): created zone: %+v", bootstrapElementId, bootstrapUserId, zone))
		}
	}

	gc := grantcontroller.NewGrantController(serverConfig, db, sm, &rclient)
	gc.Run()

	httpServer, err := northbound.NewServer(serverConfig, db, sm, gc, &rclient, zone)
	if err != nil {
		log.Error().Err(err).Msg("failed to start http server; terminating")
		os.Exit(1)
	}
	httpServer.Run()

	grpcServer, err := southbound.NewServer(serverConfig, db, sm, gc, &rclient, zone)
	if err != nil {
		log.Error().Err(err).Msg("failed to start rpc server; terminating")
		os.Exit(1)
	}
	grpcServer.Run()

	// Subscribe to Alarm actions.
	vw := client.NewAlarmWatcher(context.TODO(), serverConfig, db, &rclient, sm, gc)
	log.Info().Msg(fmt.Sprintf("started alarm action watcher: %+v", vw))

	// Run until signaled
	sigch := make(chan os.Signal)
	signal.Notify(sigch, syscall.SIGINT, syscall.SIGTERM)
	sig := <-sigch
	log.Info().Msg(fmt.Sprintf("shutting down server (signal %d)", sig))

	ctx, cancel := context.WithTimeout(context.Background(), time.Second * 5)
	defer cancel()
	sm.Shutdown()
	gc.Shutdown(ctx, 10)

	if err := httpServer.Shutdown(context.Background()); err != nil {
		log.Error().Err(err).Msg("failed to gracefully stop http server; terminating")
	}

	if err := grpcServer.Shutdown(context.Background()); err != nil {
		log.Error().Err(err).Msg("failed to gracefully stop gRPC server; terminating")
	}

	log.Info().Msg("server shutdown complete")
}
